﻿namespace HeroEdit {
	partial class EntitySelectDialog {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.entityListBox = new HeroEdit.EntityListBox();
			this.groupBoxInfo = new System.Windows.Forms.GroupBox();
			this.labelInfo = new System.Windows.Forms.Label();
			this.buttonSelect = new System.Windows.Forms.Button();
			this.buttonSelectNone = new System.Windows.Forms.Button();
			this.groupBoxInfo.SuspendLayout();
			this.SuspendLayout();
			// 
			// entityListBox
			// 
			this.entityListBox.AllowAddAndDelete = false;
			this.entityListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.entityListBox.Location = new System.Drawing.Point(13, 13);
			this.entityListBox.Name = "entityListBox";
			this.entityListBox.SelectedEntity = null;
			this.entityListBox.Size = new System.Drawing.Size(240, 467);
			this.entityListBox.TabIndex = 0;
			this.entityListBox.SelectedEntityChanged += new System.EventHandler<HeroEdit.EntityArgs>(this.entityListBox_SelectedEntityChanged);
			// 
			// groupBoxInfo
			// 
			this.groupBoxInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBoxInfo.Controls.Add(this.labelInfo);
			this.groupBoxInfo.Controls.Add(this.buttonSelect);
			this.groupBoxInfo.Location = new System.Drawing.Point(260, 13);
			this.groupBoxInfo.Name = "groupBoxInfo";
			this.groupBoxInfo.Size = new System.Drawing.Size(338, 438);
			this.groupBoxInfo.TabIndex = 1;
			this.groupBoxInfo.TabStop = false;
			this.groupBoxInfo.Text = "Entity Info";
			// 
			// labelInfo
			// 
			this.labelInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.labelInfo.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelInfo.Location = new System.Drawing.Point(7, 20);
			this.labelInfo.Name = "labelInfo";
			this.labelInfo.Size = new System.Drawing.Size(325, 335);
			this.labelInfo.TabIndex = 4;
			this.labelInfo.Text = "ID: ceac305b-80c7-4c0a-b8a3-faf20e34de2b\r\nName: N/A";
			// 
			// buttonSelect
			// 
			this.buttonSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSelect.Location = new System.Drawing.Point(10, 358);
			this.buttonSelect.Name = "buttonSelect";
			this.buttonSelect.Size = new System.Drawing.Size(322, 74);
			this.buttonSelect.TabIndex = 3;
			this.buttonSelect.Text = "Select";
			this.buttonSelect.UseVisualStyleBackColor = true;
			this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
			// 
			// buttonSelectNone
			// 
			this.buttonSelectNone.Location = new System.Drawing.Point(270, 457);
			this.buttonSelectNone.Name = "buttonSelectNone";
			this.buttonSelectNone.Size = new System.Drawing.Size(322, 23);
			this.buttonSelectNone.TabIndex = 5;
			this.buttonSelectNone.Text = "Select None";
			this.buttonSelectNone.UseVisualStyleBackColor = true;
			this.buttonSelectNone.Click += new System.EventHandler(this.buttonSelectNone_Click);
			// 
			// EntitySelectDialog
			// 
			this.AcceptButton = this.buttonSelect;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(610, 492);
			this.Controls.Add(this.buttonSelectNone);
			this.Controls.Add(this.groupBoxInfo);
			this.Controls.Add(this.entityListBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "EntitySelectDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Select Entity...";
			this.Load += new System.EventHandler(this.EntitySelectDialog_Load);
			this.groupBoxInfo.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private EntityListBox entityListBox;
		private System.Windows.Forms.GroupBox groupBoxInfo;
		private System.Windows.Forms.Button buttonSelect;
		private System.Windows.Forms.Label labelInfo;
		private System.Windows.Forms.Button buttonSelectNone;
	}
}