﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeroEdit {
	[DefaultEvent("TeamDataChanged")]
	public partial class TeamConfig : UserControl {

		[Browsable(true), Description("Triggered when the underlying team data is modified."), Category("Data")]
		public event EventHandler<ScenarioTeamEventArgs> TeamDataChanged;

		public ScenarioEntityTeam TeamData {
			get {
				return teamData;
			}
			set {
				if(value == null) {
					baseData = null;
					teamData = null;
				}
				else {
					baseData = new ScenarioEntityTeam(value);
					teamData = new ScenarioEntityTeam(value);
				}

				UpdateInfo();
			}
		}
		private ScenarioEntityTeam baseData;
		private ScenarioEntityTeam teamData;
		private bool isRefreshing = false;

		private List<EntitySelectField> entitySelectFields = new List<EntitySelectField>();

		public TeamConfig() {
			InitializeComponent();
			for(int i = 0; i < panel.Controls.Count; ++i) {
				entitySelectFields.Add(panel.Controls[panel.Controls.Count - i - 1] as EntitySelectField);
			}
		}

		private void UpdateInfo() {
			if(teamData == null) {
				groupBox.Text = "Slot " + (string)Tag + " Empty";
				groupBox.Enabled = false;
				return;
			}
			isRefreshing = true;

			groupBox.Enabled = true;

			groupBox.Text = "Team Slot " + (string)Tag;

			comboBoxControl.SelectedItem = teamData.control;
			textBoxTeamID.Text = teamData.teamId.ToString();

			if(teamData.control == "human") {
				panel.Visible = false;
			}
			else {
				for(int i = 0; i < teamData.heroIds.Length; ++i) {
					entitySelectFields[i].EntityID = teamData.heroIds[i];
				}
				panel.Visible = true;
			}

			isRefreshing = false;
		}

		private void UpdateInDB() {
			teamData.control = (string)comboBoxControl.SelectedItem;
			teamData.teamId = int.Parse(textBoxTeamID.Text);

			baseData = new ScenarioEntityTeam(teamData);

			UpdateInfo();
			TeamDataChanged?.Invoke(this, new ScenarioTeamEventArgs() { TeamData = teamData });
		}

		private bool DidChange() {
			return
				textBoxTeamID.Text != teamData.teamId.ToString();
		}

		private void textBox_KeyDown(object sender, KeyEventArgs e) {
			if(e.KeyCode == Keys.Enter) {
				UpdateInDB();
				e.SuppressKeyPress = true;
			}
			else if(e.KeyCode == Keys.Escape) {
				teamData = new ScenarioEntityTeam(baseData);
				UpdateInfo();
				((TextBox)sender).SelectAll();
			}
		}

		private void textBox_Leave(object sender, EventArgs e) {
			if(DidChange()) {
				UpdateInDB();
			}
		}

		private void comboBoxControl_SelectedIndexChanged(object sender, EventArgs e) {
			if(isRefreshing) {
				return;
			}
			teamData.control = (string)comboBoxControl.SelectedItem;
			UpdateInDB();
		}

		private void entitySelectField_EntitySelected(object sender, EntityArgs e) {
			EntitySelectField esf = sender as EntitySelectField;
			int index = entitySelectFields.IndexOf(esf);
			if(index < 0) {
				return;
			}

			teamData.heroIds[index] = e.EntityData.ID;
			UpdateInDB();
		}
	}

	public class ScenarioTeamEventArgs : EventArgs {
		public ScenarioEntityTeam TeamData { get; set; }
	}
}
