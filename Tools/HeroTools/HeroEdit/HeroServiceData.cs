﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LitJson;

namespace HeroEdit {
	public enum EntityType {
		Hero,
		Scenario,
	}

	public static class HeroServiceData {

		public delegate void DataLoadedHandler();
		public static event DataLoadedHandler OnDataLoaded;

		public delegate void DataSavedHandler();
		public static event DataSavedHandler OnDataSaved;

		public delegate void EntityUpdateHandler(IEntity entityData);
		public static event EntityUpdateHandler OnEntityUpdated;

		public delegate void EntityDeleteHandler(string entityId);
		public static event EntityDeleteHandler OnEntityDeleted;

		public static HeroDB Data { get { return heroDb; } }

		public static bool FilterHero(IEntity entity) {
			return true;
		}

		public static bool FilterScenario(IEntity entity) {
			return true;
		}

		public static void LoadData(string path) {
			System.IO.StreamReader file = new System.IO.StreamReader(path);
			string json = file.ReadLine();
			file.Close();

			HeroServiceData.path = path;
			heroDb = JsonMapper.ToObject<HeroDB>(json);
			
			OnDataLoaded?.Invoke();
		}

		public static void SaveData() {
			SaveData(path);
		}

		public static void SaveData(string path) {
			HeroServiceData.path = path;
			System.IO.StreamWriter file = new System.IO.StreamWriter(path);
			file.WriteLine(JsonMapper.ToJson(heroDb));
			file.Close();

			OnDataSaved?.Invoke();
		}

		public static void StripAndSave(IdVersion[] entityUpdates) {
			if(string.IsNullOrEmpty(path)) {
				Console.WriteLine("[HeroServiceData]: Path isn't set. We don't know where to save.");
				return;
			}

			heroDb.UpdateAndStrip(entityUpdates);
			SaveData(path);
		}

		public static void UpdateEntity(IEntity newEntity) {
			heroDb.UpdateEntity(newEntity);
			OnEntityUpdated?.Invoke(newEntity);
		}

		public static IEntity CreateEntity(Type type) {
			IEntity entity = null;
			if(type == typeof(HeroEntity)) {
				entity = new HeroEntity() {
					heroId = GenerateNewID(),
				};
			}
			else if(type == typeof(ScenarioEntity)) {
				entity = new ScenarioEntity() {
					scenarioId = GenerateNewID()
				};
			}
			if(entity == null) {
				Console.WriteLine("[HeroServiceData]: Attempting to create entity of unsupported type " + type.ToString());
				return null;
			}
			heroDb.AddEntity(entity);
			return entity;
		}

		public static void DeleteEntity(string entityId) {
			heroDb.DeleteEntity(entityId);
			OnEntityDeleted?.Invoke(entityId);
		}

		public static IEntity GetEntityByID(string entityId) {
			if(heroDb == null || string.IsNullOrWhiteSpace(entityId)) {
				return null;
			}
			IEntity entity;
			if((entity = Array.Find(heroDb.heroes, e => e.ID == entityId)) != null) {
				return entity;
			}
			else if((entity = Array.Find(heroDb.scenarios, e => e.ID == entityId)) != null) {
				return entity;
			}

			return null;
		}

		private static string GenerateNewID() {
			return "new-" + Guid.NewGuid().ToString();
		}

		private static string path;
		private static HeroDB heroDb;

	}
	
	public interface IEntity {
		string ID { get; }
		string Name { get; }
		int Version { get; }
	}

	public class HeroEntityStats {
		public int health	= 10;
		public int power	= 2;
		public int armor	= 0;
		public int dodge	= 5;
		public int resist	= 5;
		public int step		= 2;

		public HeroEntityStats() { }
		public HeroEntityStats(HeroEntityStats o) {
			health = o.health;
			power = o.power;
			armor = o.armor;
			dodge = o.dodge;
			resist = o.resist;
			step = o.step;
		}
	}

	public class HeroEntityData {
		public string assetId = "assetId";
		public HeroEntityStats stats = new HeroEntityStats();

		public HeroEntityData() { }
		public HeroEntityData(HeroEntityData o) {
			assetId = o.assetId;
			stats = new HeroEntityStats(o.stats);
		}
		
	}

	public class HeroEntity : IEntity {
		public string heroId = "newHero";
		public HeroEntityData data = new HeroEntityData();
		public int version = 0;

		public string ID { get { return heroId; } }
		public string Name { get { return data.assetId; } }
		public int Version { get { return version; } }

		public HeroEntity() { }
		public HeroEntity(HeroEntity o) {
			heroId = o.heroId;
			data = new HeroEntityData(o.data);
			version = o.version;
		}
	}

	public class ScenarioEntityTeam {
		public string control = "human";
		public int teamId = 0;
		public string[] heroIds = new string[7];

		public ScenarioEntityTeam() { }
		public ScenarioEntityTeam(ScenarioEntityTeam o) {
			control = o.control;
			teamId = o.teamId;
			heroIds = new string[o.heroIds.Length];
			Array.Copy(o.heroIds, heroIds, o.heroIds.Length);
		}
	}

	public class ScenarioEntityData {
		public string arena = "arenaName";
		public string locId = "newScenario";
		public string scenarioType = "solo";
		public ScenarioEntityTeam[] teams = new ScenarioEntityTeam[] {
			new ScenarioEntityTeam() { teamId = 1 },
			new ScenarioEntityTeam() { teamId = 2 }
		};

		public ScenarioEntityData() {}
		public ScenarioEntityData(ScenarioEntityData o) {
			arena = o.arena;
			locId = o.locId;
			scenarioType = o.scenarioType;
			teams = new ScenarioEntityTeam[o.teams.Length];
			for(int i = 0; i < o.teams.Length; ++i) {
				teams[i] = new ScenarioEntityTeam(o.teams[i]);
			}
		}
	}

	public class ScenarioEntity : IEntity {
		public string scenarioId = "newScenario";
		public ScenarioEntityData data = new ScenarioEntityData();
		public int version;

		public string ID { get { return scenarioId; } }
		public string Name { get { return data.locId; } }
		public int Version { get { return version; } }

		public ScenarioEntity() { }
		public ScenarioEntity(ScenarioEntity o) {
			scenarioId = o.scenarioId;
			data = new ScenarioEntityData(o.data);
			version = o.version;
		}
	}

	public class IdVersion {
		public string oldId = null;
		public string id;
		public int version;

		public IdVersion() { }
		public IdVersion(string id, int version) {
			this.id = id;
			this.version = version;
		}
	}

	public class HeroDB {
		public HeroEntity[] heroes = new HeroEntity[0];
		public ScenarioEntity[] scenarios = new ScenarioEntity[0];

		public string[] changedEntityIds = new string[0];
		public IdVersion[] deletedEntities = new IdVersion[0];

		public HeroDB() { }

		public HeroDB GetChanges() {
			HeroDB ret = new HeroDB();

			List<HeroEntity> changedHeroes;
			changedHeroes = new List<HeroEntity>();
			if(heroes != null && heroes.Length > 0 &&
				changedEntityIds != null && changedEntityIds.Length > 0) {
				for(int i = 0; i < heroes.Length; ++i) {
					// Did the entity change?
					if(changedEntityIds.Contains(heroes[i].ID)) {
						// It is a changed entity!
						changedHeroes.Add(heroes[i]);
					}
				}
			}
			ret.heroes = changedHeroes.ToArray();

			List<ScenarioEntity> changedScenarios;
			changedScenarios = new List<ScenarioEntity>();
			if(scenarios != null && scenarios.Length > 0 &&
				changedEntityIds != null && changedEntityIds.Length > 0) {
				for(int i = 0; i < scenarios.Length; ++i) {
					// Did the entity change?
					if(changedEntityIds.Contains(scenarios[i].ID)) {
						// It is a changed entity!
						changedScenarios.Add(scenarios[i]);
					}
				}
			}
			ret.scenarios = changedScenarios.ToArray();

			ret.changedEntityIds = changedEntityIds;
			ret.deletedEntities = deletedEntities;

			return ret;
		}

		public void UpdateAndStrip(IdVersion[] entityUpdates) {
			// Clear out our saved changes.
			changedEntityIds = new string[0];
			deletedEntities = new IdVersion[0];

			// Update the things that have to be updated.
			for(int updateIndex = 0; updateIndex < entityUpdates.Length; ++updateIndex) {
				for(int i = 0; i < heroes.Length; ++i) {
					if(heroes[i].ID == entityUpdates[updateIndex].oldId) {
						// It was a new entity.
						UpdateHeroID(heroes[i], entityUpdates[updateIndex].id);
						heroes[i].version = entityUpdates[updateIndex].version;
						break;
					}
					else if(heroes[i].ID == entityUpdates[updateIndex].id) {
						// It was an updated entity.
						heroes[i].version = entityUpdates[updateIndex].version;
						break;
					}
				}

				for(int i = 0; i < scenarios.Length; ++i) {
					if(scenarios[i].ID == entityUpdates[updateIndex].oldId) {
						// It was a new entity.
						UpdateScenarioID(scenarios[i], entityUpdates[updateIndex].id);
						scenarios[i].version = entityUpdates[updateIndex].version;
						break;
					}
					else if(scenarios[i].ID == entityUpdates[updateIndex].id) {
						// It was an updated entity.
						scenarios[i].version = entityUpdates[updateIndex].version;
						break;
					}
				}
			}
		}

		private void UpdateScenarioID(ScenarioEntity entity, string newId) {
			// What references scenario ID?

			// At the moment, nothing!

			entity.scenarioId = newId;
		}

		private void UpdateHeroID(HeroEntity entity, string newId) {
			// What references hero ID?

			// Scenarios
			for(int i = 0; i < heroes.Length; ++i) {
				ScenarioEntity check = scenarios[i];
				// Check each of the teams.
				for(int teamIndex = 0; teamIndex < scenarios[i].data.teams.Length; ++teamIndex) {
					// Each of the heroes.
					for(int heroIndex = 0; heroIndex < scenarios[i].data.teams[teamIndex].heroIds.Length; ++heroIndex) {
						if(scenarios[i].data.teams[teamIndex].heroIds[heroIndex] != entity.ID) {
							continue;
						}
						scenarios[i].data.teams[teamIndex].heroIds[heroIndex] = newId;
						FlagChangedEntity(check);
					}
				}
			}

			entity.heroId = newId;
		}

		private void FlagChangedEntity(IEntity entity) {
			if(!changedEntityIds.Contains(entity.ID)) {
				Array.Resize(ref changedEntityIds, changedEntityIds.Length + 1);
				changedEntityIds[changedEntityIds.Length - 1] = entity.ID;
			}
		}

		public void UpdateEntity(IEntity newEntity) {
			FlagChangedEntity(newEntity);
			
			if(newEntity.GetType() == typeof(HeroEntity)) {
				int index = Array.FindIndex(heroes, e => e.ID == newEntity.ID);
				if(index != -1) {
					heroes[index] = newEntity as HeroEntity;
				}
			}
			else if(newEntity.GetType() == typeof(ScenarioEntity)) {
				int index = Array.FindIndex(scenarios, e => e.ID == newEntity.ID);
				if(index != -1) {
					scenarios[index] = newEntity as ScenarioEntity;
				}
			}
		}

		public void AddEntity(IEntity newEntity) {
			if(!changedEntityIds.Contains(newEntity.ID)) {
				Array.Resize(ref changedEntityIds, changedEntityIds.Length + 1);
				changedEntityIds[changedEntityIds.Length - 1] = newEntity.ID;
			}

			if(newEntity.GetType() == typeof(HeroEntity)) {
				Array.Resize(ref heroes, heroes.Length + 1);
				heroes[heroes.Length - 1] = newEntity as HeroEntity;
			}
			else if(newEntity.GetType() == typeof(ScenarioEntity)) {
				Array.Resize(ref scenarios, scenarios.Length + 1);
				scenarios[scenarios.Length - 1] = newEntity as ScenarioEntity;
			}
		}

		public void DeleteEntity(string entityId) {
			int index;
			int version = 0;
			if((index = Array.FindIndex(heroes, e => e.ID == entityId)) != -1) {
				version = heroes[index].Version;
				if(index < heroes.Length) {
					Array.Copy(heroes, index + 1, heroes, index, (heroes.Length - index - 1));
				}
				Array.Resize(ref heroes, heroes.Length - 1);
			}
			else if((index = Array.FindIndex(scenarios, e => e.ID == entityId)) != -1) {
				version = scenarios[index].Version;
				if(index < scenarios.Length) {
					Array.Copy(scenarios, index + 1, scenarios, index, (scenarios.Length - index - 1));
				}
				Array.Resize(ref scenarios, scenarios.Length - 1);
			}
			else {
				Console.WriteLine("[HeroServiceData]: Trying to delete entity ID " + entityId + " but it doesn't exist.");
				return;
			}

			// Entities prefixed with "new" haven't actually be created on the server yet.
			if(!entityId.StartsWith("new")) {
				if(Array.Find(deletedEntities, idv => idv.id == entityId) == null) {
					Array.Resize(ref deletedEntities, deletedEntities.Length + 1);
					deletedEntities[deletedEntities.Length - 1] = new IdVersion(entityId, version);
				}
			}
			if((index = Array.FindIndex(changedEntityIds, m => m == entityId)) != -1) {
				if(index < changedEntityIds.Length) {
					Array.Copy(changedEntityIds, index + 1, changedEntityIds, index, (changedEntityIds.Length - index - 1));
				}
				Array.Resize(ref changedEntityIds, changedEntityIds.Length - 1);
			}
		}

		public string GetDesignUpdateParams() {
			return JsonMapper.ToJson(this);
		}
	}
}
