﻿namespace HeroEdit {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.downloadDBFromCloudToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.uploadDBToCloudToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.openDBFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.tabControl = new System.Windows.Forms.TabControl();
			this.tabHeroes = new System.Windows.Forms.TabPage();
			this.heroesTab1 = new HeroEdit.HeroesTab();
			this.tabScenarios = new System.Windows.Forms.TabPage();
			this.scenariosTab1 = new HeroEdit.ScenariosTab();
			this.labelNoData = new System.Windows.Forms.Label();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.menuStrip1.SuspendLayout();
			this.tabControl.SuspendLayout();
			this.tabHeroes.SuspendLayout();
			this.tabScenarios.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(940, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.downloadDBFromCloudToolStripMenuItem,
            this.uploadDBToCloudToolStripMenuItem,
            this.toolStripMenuItem2,
            this.openDBFileToolStripMenuItem,
            this.saveToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// downloadDBFromCloudToolStripMenuItem
			// 
			this.downloadDBFromCloudToolStripMenuItem.Name = "downloadDBFromCloudToolStripMenuItem";
			this.downloadDBFromCloudToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
			this.downloadDBFromCloudToolStripMenuItem.Text = "Download DB from cloud...";
			this.downloadDBFromCloudToolStripMenuItem.Click += new System.EventHandler(this.downloadDBFromCloudToolStripMenuItem_Click);
			// 
			// uploadDBToCloudToolStripMenuItem
			// 
			this.uploadDBToCloudToolStripMenuItem.Name = "uploadDBToCloudToolStripMenuItem";
			this.uploadDBToCloudToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
			this.uploadDBToCloudToolStripMenuItem.Text = "&Upload DB to cloud...";
			this.uploadDBToCloudToolStripMenuItem.Click += new System.EventHandler(this.uploadDBToCloudToolStripMenuItem_Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(214, 6);
			// 
			// openDBFileToolStripMenuItem
			// 
			this.openDBFileToolStripMenuItem.Name = "openDBFileToolStripMenuItem";
			this.openDBFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openDBFileToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
			this.openDBFileToolStripMenuItem.Text = "&Open DB file...";
			this.openDBFileToolStripMenuItem.Click += new System.EventHandler(this.openDBFileToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
			this.saveToolStripMenuItem.Text = "&Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// openFileDialog
			// 
			this.openFileDialog.DefaultExt = "json";
			this.openFileDialog.FileName = "htdb.json";
			this.openFileDialog.Filter = "HT DB files|*.json|All files|*.*";
			this.openFileDialog.Title = "Open DB File";
			// 
			// tabControl
			// 
			this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl.Controls.Add(this.tabHeroes);
			this.tabControl.Controls.Add(this.tabScenarios);
			this.tabControl.Location = new System.Drawing.Point(13, 27);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(915, 640);
			this.tabControl.TabIndex = 1;
			// 
			// tabHeroes
			// 
			this.tabHeroes.Controls.Add(this.heroesTab1);
			this.tabHeroes.Location = new System.Drawing.Point(4, 22);
			this.tabHeroes.Name = "tabHeroes";
			this.tabHeroes.Padding = new System.Windows.Forms.Padding(3);
			this.tabHeroes.Size = new System.Drawing.Size(907, 614);
			this.tabHeroes.TabIndex = 1;
			this.tabHeroes.Text = "Heroes";
			this.tabHeroes.UseVisualStyleBackColor = true;
			// 
			// heroesTab1
			// 
			this.heroesTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.heroesTab1.Location = new System.Drawing.Point(6, 6);
			this.heroesTab1.Name = "heroesTab1";
			this.heroesTab1.Size = new System.Drawing.Size(895, 602);
			this.heroesTab1.TabIndex = 0;
			// 
			// tabScenarios
			// 
			this.tabScenarios.Controls.Add(this.scenariosTab1);
			this.tabScenarios.Location = new System.Drawing.Point(4, 22);
			this.tabScenarios.Name = "tabScenarios";
			this.tabScenarios.Size = new System.Drawing.Size(907, 614);
			this.tabScenarios.TabIndex = 2;
			this.tabScenarios.Text = "Scenarios";
			this.tabScenarios.UseVisualStyleBackColor = true;
			// 
			// scenariosTab1
			// 
			this.scenariosTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.scenariosTab1.Enabled = false;
			this.scenariosTab1.Location = new System.Drawing.Point(6, 6);
			this.scenariosTab1.Name = "scenariosTab1";
			this.scenariosTab1.Size = new System.Drawing.Size(895, 602);
			this.scenariosTab1.TabIndex = 0;
			// 
			// labelNoData
			// 
			this.labelNoData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.labelNoData.Location = new System.Drawing.Point(10, 24);
			this.labelNoData.Name = "labelNoData";
			this.labelNoData.Size = new System.Drawing.Size(918, 647);
			this.labelNoData.TabIndex = 2;
			this.labelNoData.Text = "Load data to begin editing.";
			this.labelNoData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.DefaultExt = "json";
			this.saveFileDialog.Filter = "HT DB files|*.json|All files|*.*";
			this.saveFileDialog.Title = "Save DB File";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(940, 680);
			this.Controls.Add(this.tabControl);
			this.Controls.Add(this.menuStrip1);
			this.Controls.Add(this.labelNoData);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "HeroEdit";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.tabControl.ResumeLayout(false);
			this.tabHeroes.ResumeLayout(false);
			this.tabScenarios.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem downloadDBFromCloudToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem openDBFileToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage tabHeroes;
		private System.Windows.Forms.TabPage tabScenarios;
		private HeroesTab heroesTab1;
		private System.Windows.Forms.Label labelNoData;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem uploadDBToCloudToolStripMenuItem;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
		private ScenariosTab scenariosTab1;
	}
}