﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LitJson;

namespace HeroEdit {
	public partial class UploadDBDialog : Form {
		private bool isClosing = false;

		private HeroDB changes;

		public UploadDBDialog() {
			InitializeComponent();
		}

		private void EnableParameterUI(bool enable) {
			groupBoxSignInInfo.Enabled = enable;
			buttonUpload.Enabled = enable;
			buttonClose.Enabled = enable;
		}

		private void UpdateUploadButtonState() {
			bool enable = true;
			if(string.IsNullOrWhiteSpace(textBoxEmail.Text)) {
				enable = false;
			}
			if(string.IsNullOrWhiteSpace(textBoxPassword.Text)) {
				enable = false;
			}
			if(string.IsNullOrWhiteSpace(textBoxAppID.Text)) {
				enable = false;
			}
			if(string.IsNullOrWhiteSpace(textBoxSecret.Text)) {
				enable = false;
			}
			buttonUpload.Enabled = enable;
		}

		private void buttonClose_Click(object sender, EventArgs e) {
			Close();
		}

		private void UploadDBDialog_Load(object sender, EventArgs e) {
			backgroundWorkerBC.RunWorkerAsync();

			changes = HeroServiceData.Data.GetChanges();

			EnableParameterUI(true);
			UpdateUploadButtonState();

			textBoxEmail.Text = (string)Properties.Settings.Default["bcEmail"];
			textBoxPassword.Text = (string)Properties.Settings.Default["bcPassword"];
			textBoxAppID.Text = (string)Properties.Settings.Default["bcAppID"];
			textBoxSecret.Text = (string)Properties.Settings.Default["bcSecret"];

			StringBuilder sb = new StringBuilder();
			sb.AppendFormat("Prepared changes to upload:\n\n");
			sb.AppendFormat("- {0} changed/new entit{1}.\n", changes.changedEntityIds.Length, changes.changedEntityIds.Length == 1 ? "y" : "ies");
			sb.AppendFormat("- {0} deleted entit{1}.\n\n", changes.deletedEntities.Length, changes.deletedEntities.Length == 1 ? "y" : "ies");
			sb.AppendFormat("Uploading will overwrite the data in the cloud and overwrite the local DB file to strip changes.");
			labelLog.Text = sb.ToString();
		}

		private void UpdateBrainCloud() {
			BrainCloudWrapper.GetInstance().Update();
		}

		private void backgroundWorkerBC_DoWork(object sender, DoWorkEventArgs e) {
			BackgroundWorker bw = sender as BackgroundWorker;
			while(!isClosing) {
				Invoke((MethodInvoker)UpdateBrainCloud);
				System.Threading.Thread.Sleep(60);
			}
		}

		private void backgroundWorkerBC_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
			if(isClosing) {
				Close();
			}
			isClosing = false;
		}

		private void UploadDBDialog_FormClosing(object sender, FormClosingEventArgs e) {
			if(backgroundWorkerBC.IsBusy) {
				isClosing = true;
				backgroundWorkerBC.CancelAsync();
				e.Cancel = true;
				Enabled = false;
				return;
			}
		}

		private void textBox_TextChanged(object sender, EventArgs e) {
			UpdateUploadButtonState();
		}

		private void SaveSettings() {
			Properties.Settings.Default["bcEmail"] = textBoxEmail.Text;
			Properties.Settings.Default["bcPassword"] = textBoxPassword.Text;
			Properties.Settings.Default["bcAppID"] = textBoxAppID.Text;
			Properties.Settings.Default["bcSecret"] = textBoxSecret.Text;
			Properties.Settings.Default.Save();
		}

		private void buttonUpload_Click(object sender, EventArgs e) {
			SaveSettings();

			DialogResult result = MessageBox.Show(
				"Are you sure you want to upload these changes and strip the local changeset?",
				"Confirm Upload",
				MessageBoxButtons.YesNo);
			if(result != DialogResult.Yes) {
				return;
			}

			EnableParameterUI(false);

			Upload_SignIn();
		}

		private void Upload_SignIn() {
			labelLog.Text = "Signing in...";
			// Kick off download process by signing in!
			BrainCloudWrapper.Initialize("https://sharedprod.braincloudservers.com/dispatcherv2", textBoxSecret.Text, textBoxAppID.Text, "0.0");

			BrainCloudWrapper.GetInstance().AuthenticateEmailPassword(
				textBoxEmail.Text,
				textBoxPassword.Text,
				false,
				(string jsonResponse, object cbOject) => {
					labelLog.Text += "done!\n";
					Upload_ValidateAttributes();
				},
				(int status, int reasonCode, string jsonError, object cbObject) => {
					labelLog.Text += "error!\n" + jsonError;
					Upload_Finish(false);
				});
		}

		private void Upload_ValidateAttributes() {
			labelLog.Text += "Checking permissions...";
			BrainCloudWrapper.GetBC().PlayerStateService.GetAttributes(
				(string jsonResponse, object cbOject) => {
					JsonData data = JsonMapper.ToObject(jsonResponse)["data"];
					if(data["attributes"].Keys.Contains("designer")) {
						labelLog.Text += "granted!\n";
						Upload_SendChanges();
					}
					else {
						labelLog.Text += "denied!\n";
						Upload_Finish(false);
					}
				},
				(int status, int reasonCode, string jsonError, object cbObject) => {
					labelLog.Text += "error!\n" + jsonError;
					Upload_Finish(false);
				});
		}

		private void Upload_SendChanges() {
			labelLog.Text += "\nSubmitting changes...\n";
			string changesJson = changes.GetDesignUpdateParams();
			labelLog.Text += changesJson + "\n";
			BrainCloudWrapper.GetBC().ScriptService.RunScript(
				"DesignUpdate",
				changesJson,
				(string jsonResponse, object cbOject) => {
				JsonData data = JsonMapper.ToObject(jsonResponse)["data"]["response"];
				labelLog.Text += "complete!\n";

				if((bool)data["success"] == false) {
					labelLog.Text += "\nUpload failed: status code " + (int)data["reasonCode"];
						Upload_Finish(false);
						return;
					}

					Upload_StripLocalChanges(data);
				},
				(int status, int reasonCode, string jsonError, object cbObject) => {
					labelLog.Text += "error!\n" + jsonError;
					Upload_Finish(false);
				});
		}

		private void Upload_StripLocalChanges(JsonData data) {
			IdVersion[] entityUpdates = JsonMapper.ToObject<IdVersion[]>(data["updatedEntities"].ToJson());
			HeroServiceData.StripAndSave(entityUpdates);
			Upload_Finish(true);
		}

		private void Upload_Finish(bool success) {
			if(success) {
				labelLog.Text += "\n\nDone!";
			}
			else {
				labelLog.Text += "\n\nNot completed due to error!";
			}
			EnableParameterUI(true);
		}
	}
}
