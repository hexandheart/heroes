﻿namespace HeroEdit {
	partial class DownloadDBDialog {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.groupBoxSignInInfo = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.textBoxSecret = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.textBoxAppID = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxPassword = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxEmail = new System.Windows.Forms.TextBox();
			this.groupBoxSettings = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this.buttonChoosePath = new System.Windows.Forms.Button();
			this.buttonDownload = new System.Windows.Forms.Button();
			this.buttonClose = new System.Windows.Forms.Button();
			this.labelLog = new System.Windows.Forms.Label();
			this.backgroundWorkerBC = new System.ComponentModel.BackgroundWorker();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.groupBoxSignInInfo.SuspendLayout();
			this.groupBoxSettings.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBoxSignInInfo
			// 
			this.groupBoxSignInInfo.Controls.Add(this.label5);
			this.groupBoxSignInInfo.Controls.Add(this.textBoxSecret);
			this.groupBoxSignInInfo.Controls.Add(this.label4);
			this.groupBoxSignInInfo.Controls.Add(this.textBoxAppID);
			this.groupBoxSignInInfo.Controls.Add(this.label2);
			this.groupBoxSignInInfo.Controls.Add(this.textBoxPassword);
			this.groupBoxSignInInfo.Controls.Add(this.label1);
			this.groupBoxSignInInfo.Controls.Add(this.textBoxEmail);
			this.groupBoxSignInInfo.Location = new System.Drawing.Point(12, 78);
			this.groupBoxSignInInfo.Name = "groupBoxSignInInfo";
			this.groupBoxSignInInfo.Size = new System.Drawing.Size(354, 132);
			this.groupBoxSignInInfo.TabIndex = 0;
			this.groupBoxSignInInfo.TabStop = false;
			this.groupBoxSignInInfo.Text = "Sign In Info";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(5, 101);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(106, 17);
			this.label5.TabIndex = 7;
			this.label5.Text = "App Secret";
			this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxSecret
			// 
			this.textBoxSecret.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxSecret.Location = new System.Drawing.Point(117, 98);
			this.textBoxSecret.Name = "textBoxSecret";
			this.textBoxSecret.Size = new System.Drawing.Size(230, 20);
			this.textBoxSecret.TabIndex = 6;
			this.textBoxSecret.TextChanged += new System.EventHandler(this.textBox_TextChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(6, 75);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(106, 17);
			this.label4.TabIndex = 5;
			this.label4.Text = "App ID";
			this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxAppID
			// 
			this.textBoxAppID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxAppID.Location = new System.Drawing.Point(118, 72);
			this.textBoxAppID.Name = "textBoxAppID";
			this.textBoxAppID.Size = new System.Drawing.Size(230, 20);
			this.textBoxAppID.TabIndex = 4;
			this.textBoxAppID.TextChanged += new System.EventHandler(this.textBox_TextChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(6, 49);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(106, 17);
			this.label2.TabIndex = 3;
			this.label2.Text = "Password";
			this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxPassword
			// 
			this.textBoxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxPassword.Location = new System.Drawing.Point(118, 46);
			this.textBoxPassword.Name = "textBoxPassword";
			this.textBoxPassword.PasswordChar = '*';
			this.textBoxPassword.Size = new System.Drawing.Size(230, 20);
			this.textBoxPassword.TabIndex = 2;
			this.textBoxPassword.TextChanged += new System.EventHandler(this.textBox_TextChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(6, 23);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(106, 17);
			this.label1.TabIndex = 1;
			this.label1.Text = "Email";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxEmail
			// 
			this.textBoxEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxEmail.Location = new System.Drawing.Point(118, 20);
			this.textBoxEmail.Name = "textBoxEmail";
			this.textBoxEmail.Size = new System.Drawing.Size(230, 20);
			this.textBoxEmail.TabIndex = 0;
			this.textBoxEmail.TextChanged += new System.EventHandler(this.textBox_TextChanged);
			// 
			// groupBoxSettings
			// 
			this.groupBoxSettings.Controls.Add(this.label3);
			this.groupBoxSettings.Controls.Add(this.buttonChoosePath);
			this.groupBoxSettings.Location = new System.Drawing.Point(12, 12);
			this.groupBoxSettings.Name = "groupBoxSettings";
			this.groupBoxSettings.Size = new System.Drawing.Size(354, 60);
			this.groupBoxSettings.TabIndex = 1;
			this.groupBoxSettings.TabStop = false;
			this.groupBoxSettings.Text = "Download Settings";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(5, 25);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(106, 17);
			this.label3.TabIndex = 4;
			this.label3.Text = "Save Location";
			this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// buttonChoosePath
			// 
			this.buttonChoosePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonChoosePath.Location = new System.Drawing.Point(117, 20);
			this.buttonChoosePath.Name = "buttonChoosePath";
			this.buttonChoosePath.Size = new System.Drawing.Size(231, 23);
			this.buttonChoosePath.TabIndex = 0;
			this.buttonChoosePath.Text = "Choose...";
			this.buttonChoosePath.UseVisualStyleBackColor = true;
			this.buttonChoosePath.Click += new System.EventHandler(this.buttonChoosePath_Click);
			// 
			// buttonDownload
			// 
			this.buttonDownload.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonDownload.Location = new System.Drawing.Point(12, 216);
			this.buttonDownload.Name = "buttonDownload";
			this.buttonDownload.Size = new System.Drawing.Size(354, 86);
			this.buttonDownload.TabIndex = 2;
			this.buttonDownload.Text = "Download!";
			this.buttonDownload.UseVisualStyleBackColor = true;
			this.buttonDownload.Click += new System.EventHandler(this.buttonDownload_Click);
			// 
			// buttonClose
			// 
			this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonClose.Location = new System.Drawing.Point(12, 308);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(354, 23);
			this.buttonClose.TabIndex = 4;
			this.buttonClose.Text = "Close";
			this.buttonClose.UseVisualStyleBackColor = true;
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// labelLog
			// 
			this.labelLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.labelLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.labelLog.Location = new System.Drawing.Point(373, 13);
			this.labelLog.Name = "labelLog";
			this.labelLog.Size = new System.Drawing.Size(434, 319);
			this.labelLog.TabIndex = 5;
			this.labelLog.Text = "Results...";
			// 
			// backgroundWorkerBC
			// 
			this.backgroundWorkerBC.WorkerSupportsCancellation = true;
			this.backgroundWorkerBC.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerBC_DoWork);
			this.backgroundWorkerBC.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerBC_RunWorkerCompleted);
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.DefaultExt = "json";
			this.saveFileDialog.FileName = "htdb.json";
			this.saveFileDialog.Filter = "HT DB files|*.json|All files|*.*";
			this.saveFileDialog.Title = "Choose DB save location";
			// 
			// DownloadDBDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(819, 341);
			this.ControlBox = false;
			this.Controls.Add(this.labelLog);
			this.Controls.Add(this.buttonClose);
			this.Controls.Add(this.buttonDownload);
			this.Controls.Add(this.groupBoxSettings);
			this.Controls.Add(this.groupBoxSignInInfo);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "DownloadDBDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Download DB from cloud...";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DownloadDBDialog_FormClosing);
			this.Load += new System.EventHandler(this.DownloadDBDialog_Load);
			this.groupBoxSignInInfo.ResumeLayout(false);
			this.groupBoxSignInInfo.PerformLayout();
			this.groupBoxSettings.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBoxSignInInfo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxPassword;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxEmail;
		private System.Windows.Forms.GroupBox groupBoxSettings;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button buttonChoosePath;
		private System.Windows.Forms.Button buttonDownload;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBoxSecret;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxAppID;
		private System.Windows.Forms.Label labelLog;
		private System.ComponentModel.BackgroundWorker backgroundWorkerBC;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
	}
}