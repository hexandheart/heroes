﻿namespace HeroEdit {
	partial class EntitySelectField {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.button = new System.Windows.Forms.Button();
			this.label = new System.Windows.Forms.Label();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.SuspendLayout();
			// 
			// button
			// 
			this.button.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.button.Location = new System.Drawing.Point(80, 0);
			this.button.Name = "button";
			this.button.Size = new System.Drawing.Size(166, 23);
			this.button.TabIndex = 0;
			this.button.Text = "None";
			this.button.UseVisualStyleBackColor = true;
			this.button.Click += new System.EventHandler(this.button_Click);
			// 
			// label
			// 
			this.label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.label.Location = new System.Drawing.Point(0, 0);
			this.label.Name = "label";
			this.label.Size = new System.Drawing.Size(74, 23);
			this.label.TabIndex = 1;
			this.label.Text = "Entity";
			this.label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// EntitySelectField
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.label);
			this.Controls.Add(this.button);
			this.Name = "EntitySelectField";
			this.Size = new System.Drawing.Size(246, 23);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button button;
		private System.Windows.Forms.Label label;
		private System.Windows.Forms.ToolTip toolTip;
	}
}
