﻿namespace HeroEdit {
	partial class ScenariosTab {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.entityListBox = new HeroEdit.EntityListBox();
			this.groupBoxDetails = new System.Windows.Forms.GroupBox();
			this.teamConfig0 = new HeroEdit.TeamConfig();
			this.textBoxArena = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.comboBoxType = new System.Windows.Forms.ComboBox();
			this.label13 = new System.Windows.Forms.Label();
			this.textBoxVersion = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxAssetID = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxID = new System.Windows.Forms.TextBox();
			this.teamConfig1 = new HeroEdit.TeamConfig();
			this.groupBoxDetails.SuspendLayout();
			this.SuspendLayout();
			// 
			// entityListBox
			// 
			this.entityListBox.AllowAddAndDelete = true;
			this.entityListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.entityListBox.Location = new System.Drawing.Point(3, 3);
			this.entityListBox.Name = "entityListBox";
			this.entityListBox.SelectedEntity = null;
			this.entityListBox.Size = new System.Drawing.Size(220, 594);
			this.entityListBox.TabIndex = 0;
			this.entityListBox.SelectedEntityChanged += new System.EventHandler<HeroEdit.EntityArgs>(this.entityListBox_SelectedEntityChanged);
			this.entityListBox.VisibleChanged += new System.EventHandler(this.ScenariosTab_VisibleChanged);
			// 
			// groupBoxDetails
			// 
			this.groupBoxDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBoxDetails.Controls.Add(this.teamConfig1);
			this.groupBoxDetails.Controls.Add(this.teamConfig0);
			this.groupBoxDetails.Controls.Add(this.textBoxArena);
			this.groupBoxDetails.Controls.Add(this.label4);
			this.groupBoxDetails.Controls.Add(this.label3);
			this.groupBoxDetails.Controls.Add(this.comboBoxType);
			this.groupBoxDetails.Controls.Add(this.label13);
			this.groupBoxDetails.Controls.Add(this.textBoxVersion);
			this.groupBoxDetails.Controls.Add(this.label2);
			this.groupBoxDetails.Controls.Add(this.textBoxAssetID);
			this.groupBoxDetails.Controls.Add(this.label1);
			this.groupBoxDetails.Controls.Add(this.textBoxID);
			this.groupBoxDetails.Location = new System.Drawing.Point(230, 4);
			this.groupBoxDetails.Name = "groupBoxDetails";
			this.groupBoxDetails.Size = new System.Drawing.Size(567, 593);
			this.groupBoxDetails.TabIndex = 1;
			this.groupBoxDetails.TabStop = false;
			this.groupBoxDetails.Text = "Scenario Details";
			// 
			// teamConfig0
			// 
			this.teamConfig0.Location = new System.Drawing.Point(6, 141);
			this.teamConfig0.Name = "teamConfig0";
			this.teamConfig0.Size = new System.Drawing.Size(271, 264);
			this.teamConfig0.TabIndex = 17;
			this.teamConfig0.Tag = "0";
			this.teamConfig0.TeamData = null;
			this.teamConfig0.TeamDataChanged += new System.EventHandler<HeroEdit.ScenarioTeamEventArgs>(this.teamConfig_TeamDataChanged);
			// 
			// textBoxArena
			// 
			this.textBoxArena.Location = new System.Drawing.Point(85, 88);
			this.textBoxArena.Name = "textBoxArena";
			this.textBoxArena.Size = new System.Drawing.Size(153, 20);
			this.textBoxArena.TabIndex = 16;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(6, 91);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(73, 15);
			this.label4.TabIndex = 15;
			this.label4.Text = "Arena";
			this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(6, 117);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(73, 15);
			this.label3.TabIndex = 14;
			this.label3.Text = "Type";
			this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// comboBoxType
			// 
			this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxType.FormattingEnabled = true;
			this.comboBoxType.Items.AddRange(new object[] {
            "solo",
            "versus"});
			this.comboBoxType.Location = new System.Drawing.Point(85, 114);
			this.comboBoxType.Name = "comboBoxType";
			this.comboBoxType.Size = new System.Drawing.Size(153, 21);
			this.comboBoxType.TabIndex = 13;
			this.comboBoxType.SelectedIndexChanged += new System.EventHandler(this.comboBoxType_SelectedIndexChanged);
			// 
			// label13
			// 
			this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label13.Location = new System.Drawing.Point(514, 49);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(14, 15);
			this.label13.TabIndex = 12;
			this.label13.Text = "v";
			this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxVersion
			// 
			this.textBoxVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxVersion.Location = new System.Drawing.Point(528, 46);
			this.textBoxVersion.Name = "textBoxVersion";
			this.textBoxVersion.ReadOnly = true;
			this.textBoxVersion.Size = new System.Drawing.Size(32, 20);
			this.textBoxVersion.TabIndex = 11;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(6, 49);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(73, 15);
			this.label2.TabIndex = 10;
			this.label2.Text = "Loc ID";
			this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxAssetID
			// 
			this.textBoxAssetID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxAssetID.Location = new System.Drawing.Point(85, 46);
			this.textBoxAssetID.Name = "textBoxAssetID";
			this.textBoxAssetID.Size = new System.Drawing.Size(423, 20);
			this.textBoxAssetID.TabIndex = 9;
			this.textBoxAssetID.Text = "scenario_whatever";
			this.textBoxAssetID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
			this.textBoxAssetID.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(6, 23);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(73, 15);
			this.label1.TabIndex = 8;
			this.label1.Text = "ID";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxID
			// 
			this.textBoxID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxID.Location = new System.Drawing.Point(85, 20);
			this.textBoxID.Name = "textBoxID";
			this.textBoxID.ReadOnly = true;
			this.textBoxID.Size = new System.Drawing.Size(476, 20);
			this.textBoxID.TabIndex = 7;
			this.textBoxID.Text = "2d0d33f6-8807-44e8-b8e8-acba00685340";
			// 
			// teamConfig1
			// 
			this.teamConfig1.Location = new System.Drawing.Point(283, 141);
			this.teamConfig1.Name = "teamConfig1";
			this.teamConfig1.Size = new System.Drawing.Size(271, 264);
			this.teamConfig1.TabIndex = 17;
			this.teamConfig1.Tag = "1";
			this.teamConfig1.TeamData = null;
			this.teamConfig1.TeamDataChanged += new System.EventHandler<HeroEdit.ScenarioTeamEventArgs>(this.teamConfig_TeamDataChanged);
			// 
			// ScenariosTab
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.groupBoxDetails);
			this.Controls.Add(this.entityListBox);
			this.Name = "ScenariosTab";
			this.Size = new System.Drawing.Size(800, 600);
			this.Load += new System.EventHandler(this.ScenariosTab_Load);
			this.groupBoxDetails.ResumeLayout(false);
			this.groupBoxDetails.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private EntityListBox entityListBox;
		private System.Windows.Forms.GroupBox groupBoxDetails;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox textBoxVersion;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxAssetID;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxID;
		private System.Windows.Forms.ComboBox comboBoxType;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxArena;
		private System.Windows.Forms.Label label4;
		private TeamConfig teamConfig0;
		private TeamConfig teamConfig1;
	}
}
