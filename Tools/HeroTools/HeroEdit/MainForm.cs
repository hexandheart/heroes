﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LitJson;

namespace HeroEdit {
	public partial class MainForm : Form {
		public MainForm() {
			InitializeComponent();
			tabControl.Visible = false;
			labelNoData.Visible = true;

			saveToolStripMenuItem.Enabled = false;
			uploadDBToCloudToolStripMenuItem.Enabled = false;
		}

		private void downloadDBFromCloudToolStripMenuItem_Click(object sender, EventArgs e) {
			DownloadDBDialog dialog = new DownloadDBDialog();
			dialog.ShowDialog();
			dialog.Dispose();
		}

		private void openDBFileToolStripMenuItem_Click(object sender, EventArgs e) {
			if(openFileDialog.ShowDialog() == DialogResult.OK) {
				HeroServiceData.LoadData(openFileDialog.FileName);
			}
		}

		private void MainForm_Load(object sender, EventArgs e) {
			HeroServiceData.OnDataLoaded += OnHeroServiceDataLoaded;
			HeroServiceData.OnDataSaved += OnHeroServiceDataSaved;
		}

		private void OnHeroServiceDataLoaded() {
			tabControl.Visible = true;
			labelNoData.Visible = false;

			saveToolStripMenuItem.Enabled = true;
			uploadDBToCloudToolStripMenuItem.Enabled = true;
		}

		private void OnHeroServiceDataSaved() {

		}

		private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
			HeroServiceData.SaveData();
		}

		private void uploadDBToCloudToolStripMenuItem_Click(object sender, EventArgs e) {
			UploadDBDialog dialog = new UploadDBDialog();
			dialog.ShowDialog();
			dialog.Dispose();
		}
	}
}
