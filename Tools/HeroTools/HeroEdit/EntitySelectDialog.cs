﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeroEdit {
	public partial class EntitySelectDialog : Form {
		public string SelectedEntityID { get; set; }

		private string initialSelection;
		private EntityType entityType;

		public EntitySelectDialog(string selectedEntityId, EntityType entityType) {
			InitializeComponent();
			initialSelection = selectedEntityId;
			this.entityType = entityType;
		}

		private void EntitySelectDialog_Load(object sender, EventArgs e) {
			groupBoxInfo.Enabled = false;
			
			switch(entityType) {
			case EntityType.Hero: entityListBox.SetData(typeof(HeroEntity), HeroServiceData.FilterHero); break;
			case EntityType.Scenario: entityListBox.SetData(typeof(ScenarioEntity), HeroServiceData.FilterScenario); break;
			}

			// Select initial selection.
			entityListBox.SelectedEntity = HeroServiceData.GetEntityByID(initialSelection);
		}

		private void entityListBox_SelectedEntityChanged(object sender, EventArgs e) {
			EntityArgs ea = e as EntityArgs;
			if(ea.EntityData == null) {
				SelectedEntityID = null;
				groupBoxInfo.Enabled = false;
				return;
			}

			SelectedEntityID = ea.EntityData.ID;
			ShowEntityInfo(ea.EntityData);
			groupBoxInfo.Enabled = true;
		}

		private void ShowEntityInfo(IEntity entity) {
			StringBuilder sb = new StringBuilder();

			switch(entityType) {
			case EntityType.Hero: GetHeroInfo(entity, sb); break;
			case EntityType.Scenario: GetScenarioInfo(entity, sb); break;
			}

			labelInfo.Text = sb.ToString();
		}

		private void GetHeroInfo(IEntity entity, StringBuilder sb) {
			HeroEntity item = entity as HeroEntity;
			sb.AppendFormat("Hero: {0}\nID: {1}\n\n", item.Name, item.ID);

			sb.AppendFormat("Health: {0}\n", item.data.stats.health);
			sb.AppendFormat("Power:  {0}\n", item.data.stats.power);
			sb.AppendFormat("Armor:  {0}\n", item.data.stats.armor);
			sb.AppendFormat("Dodge:  {0}\n", item.data.stats.dodge);
			sb.AppendFormat("Resist: {0}\n", item.data.stats.resist);
			sb.AppendFormat("Step:   {0}\n", item.data.stats.step);
		}

		private void GetScenarioInfo(IEntity entity, StringBuilder sb) {
			ScenarioEntity item = entity as ScenarioEntity;
			sb.AppendFormat("Scenario: {0}\nID: {1}\n\n", item.Name, item.ID);

			sb.AppendFormat("Arena: {0}\n", item.data.arena);
		}

		private void buttonSelect_Click(object sender, EventArgs e) {
			DialogResult = DialogResult.OK;
			Close();
		}

		private void buttonSelectNone_Click(object sender, EventArgs e) {
			SelectedEntityID = null;
			DialogResult = DialogResult.OK;
			Close();
		}
	}
}
