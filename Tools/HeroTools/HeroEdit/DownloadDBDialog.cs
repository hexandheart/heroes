﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LitJson;

namespace HeroEdit {
	public partial class DownloadDBDialog : Form {
		private bool isClosing = false;

		public string DBPath { get; private set; }

		public DownloadDBDialog() {
			InitializeComponent();
		}

		private void EnableParameterUI(bool enable) {
			groupBoxSettings.Enabled = enable;
			groupBoxSignInInfo.Enabled = enable;
			buttonDownload.Enabled = enable;
			buttonClose.Enabled = enable;
		}

		private void UpdateDownloadButtonState() {
			bool enable = true;
			if(string.IsNullOrWhiteSpace(DBPath)) {
				enable = false;
			}
			if(string.IsNullOrWhiteSpace(textBoxEmail.Text)) {
				enable = false;
			}
			if(string.IsNullOrWhiteSpace(textBoxPassword.Text)) {
				enable = false;
			}
			if(string.IsNullOrWhiteSpace(textBoxAppID.Text)) {
				enable = false;
			}
			if(string.IsNullOrWhiteSpace(textBoxSecret.Text)) {
				enable = false;
			}
			buttonDownload.Enabled = enable;
		}

		private void buttonClose_Click(object sender, EventArgs e) {
			Close();
		}

		private void DownloadDBDialog_Load(object sender, EventArgs e) {
			backgroundWorkerBC.RunWorkerAsync();
			EnableParameterUI(true);
			UpdateDownloadButtonState();

			DBPath = (string)Properties.Settings.Default["bcDBPath"];
			if(string.IsNullOrEmpty(DBPath)) {
				buttonChoosePath.Text = "Choose Path...";
			}
			else {
				buttonChoosePath.Text = DBPath;
			}
			textBoxEmail.Text = (string)Properties.Settings.Default["bcEmail"];
			textBoxPassword.Text = (string)Properties.Settings.Default["bcPassword"];
			textBoxAppID.Text = (string)Properties.Settings.Default["bcAppID"];
			textBoxSecret.Text = (string)Properties.Settings.Default["bcSecret"];
		}

		private void UpdateBrainCloud() {
			BrainCloudWrapper.GetInstance().Update();
		}

		private void backgroundWorkerBC_DoWork(object sender, DoWorkEventArgs e) {
			BackgroundWorker bw = sender as BackgroundWorker;
			while(!isClosing) {
				Invoke((MethodInvoker)UpdateBrainCloud);
				System.Threading.Thread.Sleep(60);
			}
		}

		private void backgroundWorkerBC_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
			if(isClosing) {
				Close();
			}
			isClosing = false;
		}

		private void DownloadDBDialog_FormClosing(object sender, FormClosingEventArgs e) {
			if(backgroundWorkerBC.IsBusy) {
				isClosing = true;
				backgroundWorkerBC.CancelAsync();
				e.Cancel = true;
				Enabled = false;
				return;
			}
		}

		private void textBox_TextChanged(object sender, EventArgs e) {
			UpdateDownloadButtonState();
		}

		private void buttonChoosePath_Click(object sender, EventArgs e) {
			if(saveFileDialog.ShowDialog() == DialogResult.OK) {
				DBPath = saveFileDialog.FileName;
				buttonChoosePath.Text = DBPath;
				UpdateDownloadButtonState();
			}
		}

		private void SaveSettings() {
			Properties.Settings.Default["bcDBPath"] = DBPath;
			Properties.Settings.Default["bcEmail"] = textBoxEmail.Text;
			Properties.Settings.Default["bcPassword"] = textBoxPassword.Text;
			Properties.Settings.Default["bcAppID"] = textBoxAppID.Text;
			Properties.Settings.Default["bcSecret"] = textBoxSecret.Text;
			Properties.Settings.Default.Save();

		}

		private void buttonDownload_Click(object sender, EventArgs e) {
			SaveSettings();
			EnableParameterUI(false);

			Download_SignIn();
		}

		private void Download_SignIn() {
			labelLog.Text = "Signing in...";
			// Kick off download process by signing in!
			BrainCloudWrapper.Initialize("https://sharedprod.braincloudservers.com/dispatcherv2", textBoxSecret.Text, textBoxAppID.Text, "0.0");

			BrainCloudWrapper.GetInstance().AuthenticateEmailPassword(
				textBoxEmail.Text,
				textBoxPassword.Text,
				false,
				(string jsonResponse, object cbOject) => {
					labelLog.Text += "done!\n";
					Download_ValidateAttributes();
				},
				(int status, int reasonCode, string jsonError, object cbObject) => {
					labelLog.Text += "error!\n" + jsonError;
					Download_Finish(false);
				});
		}

		private void Download_ValidateAttributes() {
			labelLog.Text += "Checking permissions...";
			BrainCloudWrapper.GetBC().PlayerStateService.GetAttributes(
				(string jsonResponse, object cbOject) => {
					JsonData data = JsonMapper.ToObject(jsonResponse)["data"];
					if(data["attributes"].Keys.Contains("designer")) {
						labelLog.Text += "granted!\n";
						Download_GetDatabase();
					}
					else {
						labelLog.Text += "denied!\n";
						Download_Finish(false);
					}
				},
				(int status, int reasonCode, string jsonError, object cbObject) => {
					labelLog.Text += "error!\n" + jsonError;
					Download_Finish(false);
				});
		}

		private void Download_GetDatabase() {
			labelLog.Text += "\nDownloading database...";
			BrainCloudWrapper.GetBC().ScriptService.RunScript(
				"DesignGetDatabase",
				"{}",
				(string jsonResponse, object cbOject) => {
					JsonData data = JsonMapper.ToObject(jsonResponse)["data"]["response"]["data"];
					labelLog.Text += "complete!\n";
					Download_SaveDB(data);
				},
				(int status, int reasonCode, string jsonError, object cbObject) => {
					labelLog.Text += "error!\n" + jsonError;
					Download_Finish(false);
				});
		}

		private void Download_SaveDB(JsonData data) {
			labelLog.Text += "\nSaving to " + DBPath + "...";
			System.IO.StreamWriter file = new System.IO.StreamWriter(DBPath);
			file.WriteLine(JsonMapper.ToJson(data));
			file.Close();

			Download_Finish(true);
		}

		private void Download_Finish(bool success) {
			if(success) {
				labelLog.Text += "\n\nDone!";
			}
			else {
				labelLog.Text += "\n\nNot completed due to error!";
			}
			EnableParameterUI(true);
		}
	}
}
