﻿namespace HeroEdit {
	partial class TeamConfig {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.groupBox = new System.Windows.Forms.GroupBox();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxTeamID = new System.Windows.Forms.TextBox();
			this.panel = new System.Windows.Forms.Panel();
			this.entitySelectField7 = new HeroEdit.EntitySelectField();
			this.entitySelectField6 = new HeroEdit.EntitySelectField();
			this.entitySelectField5 = new HeroEdit.EntitySelectField();
			this.entitySelectField4 = new HeroEdit.EntitySelectField();
			this.entitySelectField3 = new HeroEdit.EntitySelectField();
			this.entitySelectField2 = new HeroEdit.EntitySelectField();
			this.entitySelectField1 = new HeroEdit.EntitySelectField();
			this.label5 = new System.Windows.Forms.Label();
			this.comboBoxControl = new System.Windows.Forms.ComboBox();
			this.groupBox.SuspendLayout();
			this.panel.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox
			// 
			this.groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox.Controls.Add(this.label6);
			this.groupBox.Controls.Add(this.textBoxTeamID);
			this.groupBox.Controls.Add(this.panel);
			this.groupBox.Controls.Add(this.label5);
			this.groupBox.Controls.Add(this.comboBoxControl);
			this.groupBox.Location = new System.Drawing.Point(0, 0);
			this.groupBox.Name = "groupBox";
			this.groupBox.Size = new System.Drawing.Size(270, 264);
			this.groupBox.TabIndex = 18;
			this.groupBox.TabStop = false;
			this.groupBox.Text = "Team X";
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label6.Location = new System.Drawing.Point(202, 25);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(23, 15);
			this.label6.TabIndex = 23;
			this.label6.Text = "ID";
			this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxTeamID
			// 
			this.textBoxTeamID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxTeamID.Location = new System.Drawing.Point(231, 22);
			this.textBoxTeamID.Name = "textBoxTeamID";
			this.textBoxTeamID.Size = new System.Drawing.Size(30, 20);
			this.textBoxTeamID.TabIndex = 22;
			this.textBoxTeamID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
			this.textBoxTeamID.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// panel
			// 
			this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel.Controls.Add(this.entitySelectField7);
			this.panel.Controls.Add(this.entitySelectField6);
			this.panel.Controls.Add(this.entitySelectField5);
			this.panel.Controls.Add(this.entitySelectField4);
			this.panel.Controls.Add(this.entitySelectField3);
			this.panel.Controls.Add(this.entitySelectField2);
			this.panel.Controls.Add(this.entitySelectField1);
			this.panel.Location = new System.Drawing.Point(9, 49);
			this.panel.Name = "panel";
			this.panel.Size = new System.Drawing.Size(252, 204);
			this.panel.TabIndex = 21;
			this.panel.Visible = false;
			// 
			// entitySelectField7
			// 
			this.entitySelectField7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.entitySelectField7.EntityID = null;
			this.entitySelectField7.EntityType = HeroEdit.EntityType.Hero;
			this.entitySelectField7.Label = "Reserve 1";
			this.entitySelectField7.Location = new System.Drawing.Point(3, 177);
			this.entitySelectField7.Name = "entitySelectField7";
			this.entitySelectField7.Size = new System.Drawing.Size(246, 23);
			this.entitySelectField7.TabIndex = 26;
			this.entitySelectField7.EntitySelected += new System.EventHandler<HeroEdit.EntityArgs>(this.entitySelectField_EntitySelected);
			// 
			// entitySelectField6
			// 
			this.entitySelectField6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.entitySelectField6.EntityID = null;
			this.entitySelectField6.EntityType = HeroEdit.EntityType.Hero;
			this.entitySelectField6.Label = "Reserve 0";
			this.entitySelectField6.Location = new System.Drawing.Point(3, 148);
			this.entitySelectField6.Name = "entitySelectField6";
			this.entitySelectField6.Size = new System.Drawing.Size(246, 23);
			this.entitySelectField6.TabIndex = 25;
			this.entitySelectField6.EntitySelected += new System.EventHandler<HeroEdit.EntityArgs>(this.entitySelectField_EntitySelected);
			// 
			// entitySelectField5
			// 
			this.entitySelectField5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.entitySelectField5.EntityID = null;
			this.entitySelectField5.EntityType = HeroEdit.EntityType.Hero;
			this.entitySelectField5.Label = "Slot 4";
			this.entitySelectField5.Location = new System.Drawing.Point(3, 119);
			this.entitySelectField5.Name = "entitySelectField5";
			this.entitySelectField5.Size = new System.Drawing.Size(246, 23);
			this.entitySelectField5.TabIndex = 24;
			this.entitySelectField5.EntitySelected += new System.EventHandler<HeroEdit.EntityArgs>(this.entitySelectField_EntitySelected);
			// 
			// entitySelectField4
			// 
			this.entitySelectField4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.entitySelectField4.EntityID = null;
			this.entitySelectField4.EntityType = HeroEdit.EntityType.Hero;
			this.entitySelectField4.Label = "Slot 3";
			this.entitySelectField4.Location = new System.Drawing.Point(3, 90);
			this.entitySelectField4.Name = "entitySelectField4";
			this.entitySelectField4.Size = new System.Drawing.Size(246, 23);
			this.entitySelectField4.TabIndex = 23;
			this.entitySelectField4.EntitySelected += new System.EventHandler<HeroEdit.EntityArgs>(this.entitySelectField_EntitySelected);
			// 
			// entitySelectField3
			// 
			this.entitySelectField3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.entitySelectField3.EntityID = null;
			this.entitySelectField3.EntityType = HeroEdit.EntityType.Hero;
			this.entitySelectField3.Label = "Slot 2";
			this.entitySelectField3.Location = new System.Drawing.Point(3, 61);
			this.entitySelectField3.Name = "entitySelectField3";
			this.entitySelectField3.Size = new System.Drawing.Size(246, 23);
			this.entitySelectField3.TabIndex = 22;
			this.entitySelectField3.EntitySelected += new System.EventHandler<HeroEdit.EntityArgs>(this.entitySelectField_EntitySelected);
			// 
			// entitySelectField2
			// 
			this.entitySelectField2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.entitySelectField2.EntityID = null;
			this.entitySelectField2.EntityType = HeroEdit.EntityType.Hero;
			this.entitySelectField2.Label = "Slot 1";
			this.entitySelectField2.Location = new System.Drawing.Point(3, 32);
			this.entitySelectField2.Name = "entitySelectField2";
			this.entitySelectField2.Size = new System.Drawing.Size(246, 23);
			this.entitySelectField2.TabIndex = 21;
			this.entitySelectField2.EntitySelected += new System.EventHandler<HeroEdit.EntityArgs>(this.entitySelectField_EntitySelected);
			// 
			// entitySelectField1
			// 
			this.entitySelectField1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.entitySelectField1.EntityID = null;
			this.entitySelectField1.EntityType = HeroEdit.EntityType.Hero;
			this.entitySelectField1.Label = "Slot 0";
			this.entitySelectField1.Location = new System.Drawing.Point(3, 3);
			this.entitySelectField1.Name = "entitySelectField1";
			this.entitySelectField1.Size = new System.Drawing.Size(246, 23);
			this.entitySelectField1.TabIndex = 20;
			this.entitySelectField1.EntitySelected += new System.EventHandler<HeroEdit.EntityArgs>(this.entitySelectField_EntitySelected);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(11, 25);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(73, 15);
			this.label5.TabIndex = 19;
			this.label5.Text = "Control";
			this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// comboBoxControl
			// 
			this.comboBoxControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxControl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxControl.FormattingEnabled = true;
			this.comboBoxControl.Items.AddRange(new object[] {
            "human",
            "bot"});
			this.comboBoxControl.Location = new System.Drawing.Point(92, 22);
			this.comboBoxControl.Name = "comboBoxControl";
			this.comboBoxControl.Size = new System.Drawing.Size(104, 21);
			this.comboBoxControl.TabIndex = 18;
			this.comboBoxControl.SelectedIndexChanged += new System.EventHandler(this.comboBoxControl_SelectedIndexChanged);
			// 
			// TeamConfig
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.groupBox);
			this.Name = "TeamConfig";
			this.Size = new System.Drawing.Size(271, 264);
			this.groupBox.ResumeLayout(false);
			this.groupBox.PerformLayout();
			this.panel.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textBoxTeamID;
		private System.Windows.Forms.Panel panel;
		private EntitySelectField entitySelectField7;
		private EntitySelectField entitySelectField6;
		private EntitySelectField entitySelectField5;
		private EntitySelectField entitySelectField4;
		private EntitySelectField entitySelectField3;
		private EntitySelectField entitySelectField2;
		private EntitySelectField entitySelectField1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox comboBoxControl;
	}
}
