﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeroEdit {
	[DefaultEvent("EntitySelected")]
	public partial class EntitySelectField : UserControl {

		[Description("Label for the entity field."), Category("Data")]
		public string Label {
			get { return label.Text; }
			set { label.Text = value; }
		}

		public string EntityID {
			get {
				if(entity == null) {
					return null;
				}
				return entity.ID;
			}
			set {
				entity = HeroServiceData.GetEntityByID(value);
				if(entity == null) {
					button.Text = "None";
					toolTip.SetToolTip(button, null);
				}
				else {
					button.Text = "<" + entity.Name + ">";
					toolTip.SetToolTip(button, entity.ID);
				}
			}
		}
		private IEntity entity;

		[Description("The type of entity selectable here."), Category("Data")]
		public EntityType EntityType { get; set; }

		[Browsable(true), Description("Triggered when an entity is selected."), Category("Data")]
		public event EventHandler<EntityArgs> EntitySelected;

		public EntitySelectField() {
			InitializeComponent();
		}

		private void button_Click(object sender, EventArgs e) {
			EntitySelectDialog dlg = new EntitySelectDialog(EntityID, EntityType);
			if(dlg.ShowDialog() == DialogResult.OK) {
				EntityID =  dlg.SelectedEntityID;
				EntitySelected?.Invoke(this, new EntityArgs(HeroServiceData.GetEntityByID(EntityID)));
			}
		}
	}
}
