﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LitJson;

namespace HeroEdit {
	public partial class ScenariosTab : UserControl {
		private ScenarioEntity baseDetails;
		private ScenarioEntity currentDetails;

		[System.Runtime.InteropServices.DllImport("user32.dll")]
		public static extern bool LockWindowUpdate(IntPtr hWndLock);

		public ScenariosTab() {
			InitializeComponent();
			ClearDetails();
		}

		public void UpdateData() {
			if(DesignMode) {
				return;
			}
			entityListBox.SetData(typeof(ScenarioEntity), HeroServiceData.FilterScenario);
			if(HeroServiceData.Data == null) {
				Enabled = false;
				return;
			}
			else {
				Enabled = true;
			}
		}

		private void ScenariosTab_Load(object sender, EventArgs e) {
			UpdateData();
		}

		private void entityListBox_SelectedEntityChanged(object sender, EventArgs e) {
			EntityArgs ea = e as EntityArgs;
			ShowDetails(ea.EntityData);
		}

		private void ScenariosTab_VisibleChanged(object sender, EventArgs e) {
			if(!Visible) {
				return;
			}

			UpdateData();
		}

		private void ClearDetails() {
			LockWindowUpdate(Handle);

			textBoxID.Text = "";
			textBoxVersion.Text = "";

			textBoxAssetID.Text = "";

			comboBoxType.SelectedItem = "solo";

			teamConfig0.TeamData = null;

			/*textBoxHealth.Text = "";
			textBoxBodyPower.Text = "";
			textBoxArcanePower.Text = "";
			textBoxSpiritPower.Text = "";
			textBoxBodyDefense.Text = "";
			textBoxArcaneDefense.Text = "";
			textBoxSpiritDefense.Text = "";

			textBoxDodgePhysical.Text = "";
			textBoxDodgeMagic.Text = "";

			textBoxCritical.Text = "";*/

			Refresh();
			LockWindowUpdate(IntPtr.Zero);
		}

		private void ShowDetails(IEntity data) {
			if(data == null) {
				baseDetails = null;
				currentDetails = null;
				groupBoxDetails.Enabled = false;
				return;
			}
			LockWindowUpdate(Handle);
			groupBoxDetails.Enabled = true;

			baseDetails = data as ScenarioEntity;
			currentDetails = new ScenarioEntity(baseDetails);

			textBoxID.Text = currentDetails.scenarioId;
			textBoxVersion.Text = currentDetails.version.ToString();

			textBoxAssetID.Text = currentDetails.data.locId;

			textBoxArena.Text = currentDetails.data.arena;
			comboBoxType.SelectedItem = currentDetails.data.scenarioType;

			teamConfig0.TeamData = currentDetails.data.teams[0];
			teamConfig1.TeamData = currentDetails.data.teams[1];

			/*textBoxHealth.Text = currentDetails.data.stats.health.ToString();
			textBoxBodyPower.Text = currentDetails.data.stats.bodyPower.ToString();
			textBoxArcanePower.Text = currentDetails.data.stats.arcanePower.ToString();
			textBoxSpiritPower.Text = currentDetails.data.stats.spiritDefense.ToString();
			textBoxBodyDefense.Text = currentDetails.data.stats.bodyDefense.ToString();
			textBoxArcaneDefense.Text = currentDetails.data.stats.arcaneDefense.ToString();
			textBoxSpiritDefense.Text = currentDetails.data.stats.spiritDefense.ToString();

			textBoxDodgePhysical.Text = currentDetails.data.stats.evasion.ToString();
			textBoxDodgeMagic.Text = currentDetails.data.stats.will.ToString();

			textBoxCritical.Text = currentDetails.data.stats.critical.ToString();*/

			Refresh();
			LockWindowUpdate(IntPtr.Zero);
		}

		private void UpdateInDB() {
			currentDetails.data.locId = textBoxAssetID.Text;

			currentDetails.data.arena = textBoxArena.Text;
			currentDetails.data.scenarioType = (string)comboBoxType.SelectedItem;

			/*currentDetails.data.stats.health = int.Parse(textBoxHealth.Text);
			currentDetails.data.stats.bodyPower = int.Parse(textBoxBodyPower.Text);
			currentDetails.data.stats.arcanePower = int.Parse(textBoxArcanePower.Text);
			currentDetails.data.stats.spiritDefense = int.Parse(textBoxSpiritPower.Text);
			currentDetails.data.stats.bodyDefense = int.Parse(textBoxBodyDefense.Text);
			currentDetails.data.stats.arcaneDefense = int.Parse(textBoxArcaneDefense.Text);
			currentDetails.data.stats.spiritDefense = int.Parse(textBoxSpiritDefense.Text);

			currentDetails.data.stats.evasion = int.Parse(textBoxDodgePhysical.Text);
			currentDetails.data.stats.will = int.Parse(textBoxDodgeMagic.Text);

			currentDetails.data.stats.critical = int.Parse(textBoxCritical.Text);*/

			HeroServiceData.UpdateEntity(currentDetails);
			ShowDetails(currentDetails);
		}

		private bool DidChange() {
			return 
				textBoxAssetID.Text != currentDetails.data.locId ||
				textBoxArena.Text != currentDetails.data.arena ||
				(string)comboBoxType.SelectedItem != currentDetails.data.scenarioType /*||

				textBoxHealth.Text != currentDetails.data.stats.health.ToString() ||
				textBoxBodyPower.Text != currentDetails.data.stats.bodyPower.ToString() ||
				textBoxArcanePower.Text != currentDetails.data.stats.arcanePower.ToString() ||
				textBoxSpiritPower.Text != currentDetails.data.stats.spiritDefense.ToString() ||
				textBoxBodyDefense.Text != currentDetails.data.stats.bodyDefense.ToString() ||
				textBoxArcaneDefense.Text != currentDetails.data.stats.arcaneDefense.ToString() ||
				textBoxSpiritDefense.Text != currentDetails.data.stats.spiritDefense.ToString() ||

				textBoxDodgePhysical.Text != currentDetails.data.stats.evasion.ToString() ||
				textBoxDodgeMagic.Text != currentDetails.data.stats.will.ToString() ||

				textBoxCritical.Text != currentDetails.data.stats.critical.ToString()*/;
		}

		private void textBox_KeyDown(object sender, KeyEventArgs e) {
			if(e.KeyCode == Keys.Enter) {
				UpdateInDB();
				e.SuppressKeyPress = true;
			}
			else if(e.KeyCode == Keys.Escape) {
				ShowDetails(baseDetails);
				((TextBox)sender).SelectAll();
			}
		}

		private void textBox_Leave(object sender, EventArgs e) {
			if(DidChange()) {
				UpdateInDB();
			}
		}

		private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e) {
			if(currentDetails == null) {
				return;
			}

			currentDetails.data.scenarioType = (string)comboBoxType.SelectedItem;

			if(DidChange()) {
				UpdateInDB();
			}
		}

		private void teamConfig_TeamDataChanged(object sender, ScenarioTeamEventArgs e) {
			TeamConfig tc = sender as TeamConfig;
			int teamSlot = -1;
			
			if(tc == teamConfig0) {
				teamSlot = 0;
			}
			else if(tc == teamConfig1) {
				teamSlot = 1;
			}
			else {
				return;
			}

			currentDetails.data.teams[teamSlot] = new ScenarioEntityTeam(tc.TeamData);
			UpdateInDB();
		}
	}
}
