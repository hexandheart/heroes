﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LitJson;

namespace HeroEdit {
	public partial class HeroesTab : UserControl {
		private HeroEntity baseDetails;
		private HeroEntity currentDetails;

		[System.Runtime.InteropServices.DllImport("user32.dll")]
		public static extern bool LockWindowUpdate(IntPtr hWndLock);

		public HeroesTab() {
			InitializeComponent();
			ClearDetails();
		}

		public void UpdateData() {
			if(DesignMode) {
				return;
			}
			entityListBox.SetData(typeof(HeroEntity), HeroServiceData.FilterHero);
			if(HeroServiceData.Data == null) {
				Enabled = false;
				return;
			}
			else {
				Enabled = true;
			}
		}

		private void HeroesTab_Load(object sender, EventArgs e) {
			UpdateData();
		}

		private void entityListBox_SelectedEntityChanged(object sender, EventArgs e) {
			EntityArgs ea = e as EntityArgs;
			ShowDetails(ea.EntityData);
		}

		private void HeroesTab_VisibleChanged(object sender, EventArgs e) {
			if(!Visible) {
				return;
			}

			UpdateData();
		}

		private void ClearDetails() {
			textBoxID.Text = "";
			textBoxVersion.Text = "";
			textBoxPowerLevel.Text = "";

			textBoxAssetID.Text = "";

			textBoxHealth.Text = "";

			textBoxPower.Text = "";
			textBoxArmor.Text = "";

			textBoxDodge.Text = "";
			textBoxResist.Text = "";

			textBoxStep.Text = "";
		}

		private void ShowDetails(IEntity data) {
			if(data == null) {
				baseDetails = null;
				currentDetails = null;
				groupBoxDetails.Enabled = false;
				return;
			}
			LockWindowUpdate(Handle);
			groupBoxDetails.Enabled = true;

			baseDetails = data as HeroEntity;
			currentDetails = new HeroEntity(baseDetails);

			textBoxID.Text			= currentDetails.heroId;
			textBoxVersion.Text		= currentDetails.version.ToString();

			textBoxAssetID.Text		= currentDetails.data.assetId;

			textBoxHealth.Text		= currentDetails.data.stats.health.ToString();

			textBoxPower.Text		= currentDetails.data.stats.power.ToString();
			textBoxArmor.Text		= currentDetails.data.stats.armor.ToString();

			textBoxDodge.Text		= currentDetails.data.stats.dodge.ToString();
			textBoxResist.Text		= currentDetails.data.stats.resist.ToString();

			textBoxStep.Text		= currentDetails.data.stats.step.ToString();

			Refresh();
			LockWindowUpdate(IntPtr.Zero);
		}

		private void UpdateInDB() {
			currentDetails.data.assetId			= textBoxAssetID.Text;

			currentDetails.data.stats.health	= int.Parse(textBoxHealth.Text);

			currentDetails.data.stats.power		= int.Parse(textBoxPower.Text);
			currentDetails.data.stats.armor		= int.Parse(textBoxArmor.Text);

			currentDetails.data.stats.dodge		= int.Parse(textBoxDodge.Text);
			currentDetails.data.stats.resist	= int.Parse(textBoxResist.Text);

			currentDetails.data.stats.step		= int.Parse(textBoxStep.Text);

			HeroServiceData.UpdateEntity(currentDetails);
			ShowDetails(currentDetails);
		}

		private bool DidChange() {
			return
				textBoxAssetID.Text != currentDetails.data.assetId ||

				textBoxHealth.Text != currentDetails.data.stats.health.ToString() ||

				textBoxPower.Text != currentDetails.data.stats.power.ToString() ||
				textBoxArmor.Text != currentDetails.data.stats.armor.ToString() ||

				textBoxDodge.Text != currentDetails.data.stats.dodge.ToString() ||
				textBoxResist.Text != currentDetails.data.stats.resist.ToString() ||

				textBoxStep.Text != currentDetails.data.stats.step.ToString();
		}

		private void textBox_KeyDown(object sender, KeyEventArgs e) {
			if(e.KeyCode == Keys.Enter) {
				UpdateInDB();
				e.SuppressKeyPress = true;
			}
			else if(e.KeyCode == Keys.Escape) {
				ShowDetails(baseDetails);
				((TextBox)sender).SelectAll();
			}
		}

		private void textBox_Leave(object sender, EventArgs e) {
			if(DidChange()) {
				UpdateInDB();
			}
		}
	}
}
