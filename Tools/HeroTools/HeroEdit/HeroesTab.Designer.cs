﻿namespace HeroEdit {
	partial class HeroesTab {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.entityListBox = new HeroEdit.EntityListBox();
			this.groupBoxDetails = new System.Windows.Forms.GroupBox();
			this.label13 = new System.Windows.Forms.Label();
			this.textBoxVersion = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label14 = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.textBoxStep = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.textBoxResist = new System.Windows.Forms.TextBox();
			this.textBoxHealth = new System.Windows.Forms.TextBox();
			this.textBoxDodge = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.textBoxArmor = new System.Windows.Forms.TextBox();
			this.textBoxPower = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxPowerLevel = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxAssetID = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxID = new System.Windows.Forms.TextBox();
			this.groupBoxDetails.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// entityListBox
			// 
			this.entityListBox.AllowAddAndDelete = true;
			this.entityListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.entityListBox.Location = new System.Drawing.Point(3, 3);
			this.entityListBox.Name = "entityListBox";
			this.entityListBox.SelectedEntity = null;
			this.entityListBox.Size = new System.Drawing.Size(220, 594);
			this.entityListBox.TabIndex = 0;
			this.entityListBox.SelectedEntityChanged += new System.EventHandler<HeroEdit.EntityArgs>(this.entityListBox_SelectedEntityChanged);
			// 
			// groupBoxDetails
			// 
			this.groupBoxDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBoxDetails.Controls.Add(this.label13);
			this.groupBoxDetails.Controls.Add(this.textBoxVersion);
			this.groupBoxDetails.Controls.Add(this.groupBox1);
			this.groupBoxDetails.Controls.Add(this.label2);
			this.groupBoxDetails.Controls.Add(this.textBoxAssetID);
			this.groupBoxDetails.Controls.Add(this.label1);
			this.groupBoxDetails.Controls.Add(this.textBoxID);
			this.groupBoxDetails.Location = new System.Drawing.Point(230, 4);
			this.groupBoxDetails.Name = "groupBoxDetails";
			this.groupBoxDetails.Size = new System.Drawing.Size(567, 593);
			this.groupBoxDetails.TabIndex = 1;
			this.groupBoxDetails.TabStop = false;
			this.groupBoxDetails.Text = "Hero Details";
			// 
			// label13
			// 
			this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label13.Location = new System.Drawing.Point(514, 49);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(14, 15);
			this.label13.TabIndex = 6;
			this.label13.Text = "v";
			this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxVersion
			// 
			this.textBoxVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxVersion.Location = new System.Drawing.Point(528, 46);
			this.textBoxVersion.Name = "textBoxVersion";
			this.textBoxVersion.ReadOnly = true;
			this.textBoxVersion.Size = new System.Drawing.Size(32, 20);
			this.textBoxVersion.TabIndex = 5;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.label14);
			this.groupBox1.Controls.Add(this.tableLayoutPanel1);
			this.groupBox1.Controls.Add(this.textBoxPowerLevel);
			this.groupBox1.Location = new System.Drawing.Point(9, 72);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(552, 515);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Stats";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(219, 25);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(73, 15);
			this.label14.TabIndex = 8;
			this.label14.Text = "Power Level";
			this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.textBoxStep, 1, 8);
			this.tableLayoutPanel1.Controls.Add(this.label6, 0, 8);
			this.tableLayoutPanel1.Controls.Add(this.label5, 0, 6);
			this.tableLayoutPanel1.Controls.Add(this.label12, 0, 5);
			this.tableLayoutPanel1.Controls.Add(this.textBoxResist, 1, 6);
			this.tableLayoutPanel1.Controls.Add(this.textBoxHealth, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.textBoxDodge, 1, 5);
			this.tableLayoutPanel1.Controls.Add(this.label8, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.textBoxArmor, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.textBoxPower, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 9;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28577F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28612F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28612F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28469F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28577F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28577F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(149, 178);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// textBoxStep
			// 
			this.textBoxStep.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxStep.Location = new System.Drawing.Point(54, 153);
			this.textBoxStep.Name = "textBoxStep";
			this.textBoxStep.Size = new System.Drawing.Size(92, 20);
			this.textBoxStep.TabIndex = 22;
			this.textBoxStep.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
			this.textBoxStep.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// label6
			// 
			this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label6.Location = new System.Drawing.Point(3, 150);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(45, 28);
			this.label6.TabIndex = 21;
			this.label6.Text = "Step";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label5
			// 
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Location = new System.Drawing.Point(3, 116);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(45, 24);
			this.label5.TabIndex = 20;
			this.label5.Text = "Resist";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label12
			// 
			this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label12.Location = new System.Drawing.Point(3, 92);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(45, 24);
			this.label12.TabIndex = 19;
			this.label12.Text = "Dodge";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBoxResist
			// 
			this.textBoxResist.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxResist.Location = new System.Drawing.Point(54, 119);
			this.textBoxResist.Name = "textBoxResist";
			this.textBoxResist.Size = new System.Drawing.Size(92, 20);
			this.textBoxResist.TabIndex = 15;
			this.textBoxResist.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
			this.textBoxResist.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// textBoxHealth
			// 
			this.textBoxHealth.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxHealth.Location = new System.Drawing.Point(54, 3);
			this.textBoxHealth.Name = "textBoxHealth";
			this.textBoxHealth.Size = new System.Drawing.Size(92, 20);
			this.textBoxHealth.TabIndex = 13;
			this.textBoxHealth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
			this.textBoxHealth.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// textBoxDodge
			// 
			this.textBoxDodge.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxDodge.Location = new System.Drawing.Point(54, 95);
			this.textBoxDodge.Name = "textBoxDodge";
			this.textBoxDodge.Size = new System.Drawing.Size(92, 20);
			this.textBoxDodge.TabIndex = 12;
			this.textBoxDodge.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
			this.textBoxDodge.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// label8
			// 
			this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label8.Location = new System.Drawing.Point(3, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(45, 24);
			this.label8.TabIndex = 11;
			this.label8.Text = "Health";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label4
			// 
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Location = new System.Drawing.Point(3, 58);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(45, 24);
			this.label4.TabIndex = 7;
			this.label4.Text = "Armor";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBoxArmor
			// 
			this.textBoxArmor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxArmor.Location = new System.Drawing.Point(54, 61);
			this.textBoxArmor.Name = "textBoxArmor";
			this.textBoxArmor.Size = new System.Drawing.Size(92, 20);
			this.textBoxArmor.TabIndex = 3;
			this.textBoxArmor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
			this.textBoxArmor.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// textBoxPower
			// 
			this.textBoxPower.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxPower.Location = new System.Drawing.Point(54, 37);
			this.textBoxPower.Name = "textBoxPower";
			this.textBoxPower.Size = new System.Drawing.Size(92, 20);
			this.textBoxPower.TabIndex = 0;
			this.textBoxPower.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
			this.textBoxPower.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// label3
			// 
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Location = new System.Drawing.Point(3, 34);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(45, 24);
			this.label3.TabIndex = 6;
			this.label3.Text = "Power";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// textBoxPowerLevel
			// 
			this.textBoxPowerLevel.Enabled = false;
			this.textBoxPowerLevel.Location = new System.Drawing.Point(298, 22);
			this.textBoxPowerLevel.Name = "textBoxPowerLevel";
			this.textBoxPowerLevel.ReadOnly = true;
			this.textBoxPowerLevel.Size = new System.Drawing.Size(57, 20);
			this.textBoxPowerLevel.TabIndex = 7;
			this.textBoxPowerLevel.Text = "N/A";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(6, 49);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(73, 15);
			this.label2.TabIndex = 3;
			this.label2.Text = "Asset ID";
			this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxAssetID
			// 
			this.textBoxAssetID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxAssetID.Location = new System.Drawing.Point(85, 46);
			this.textBoxAssetID.Name = "textBoxAssetID";
			this.textBoxAssetID.Size = new System.Drawing.Size(423, 20);
			this.textBoxAssetID.TabIndex = 2;
			this.textBoxAssetID.Text = "warrior";
			this.textBoxAssetID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
			this.textBoxAssetID.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(6, 23);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(73, 15);
			this.label1.TabIndex = 1;
			this.label1.Text = "ID";
			this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxID
			// 
			this.textBoxID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxID.Location = new System.Drawing.Point(85, 20);
			this.textBoxID.Name = "textBoxID";
			this.textBoxID.ReadOnly = true;
			this.textBoxID.Size = new System.Drawing.Size(476, 20);
			this.textBoxID.TabIndex = 0;
			this.textBoxID.Text = "2d0d33f6-8807-44e8-b8e8-acba00685340";
			// 
			// HeroesTab
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.groupBoxDetails);
			this.Controls.Add(this.entityListBox);
			this.Name = "HeroesTab";
			this.Size = new System.Drawing.Size(800, 600);
			this.Load += new System.EventHandler(this.HeroesTab_Load);
			this.VisibleChanged += new System.EventHandler(this.HeroesTab_VisibleChanged);
			this.groupBoxDetails.ResumeLayout(false);
			this.groupBoxDetails.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private EntityListBox entityListBox;
		private System.Windows.Forms.GroupBox groupBoxDetails;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox textBoxResist;
		private System.Windows.Forms.TextBox textBoxHealth;
		private System.Windows.Forms.TextBox textBoxDodge;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxArmor;
		private System.Windows.Forms.TextBox textBoxPower;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox textBoxPowerLevel;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox textBoxVersion;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxAssetID;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxID;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBoxStep;
		private System.Windows.Forms.Label label6;
	}
}
