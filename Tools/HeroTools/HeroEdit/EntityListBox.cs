﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LitJson;

namespace HeroEdit {
	[DefaultEvent("SelectedEntityChanged")]
	public partial class EntityListBox : UserControl {
		
		[Browsable(true), Description("Triggered when an entity is selected or deselected."), Category("data")]
		public event EventHandler<EntityArgs> SelectedEntityChanged;

		[Description("Whether the Add and Delete buttons are usable."), Category("Data")]
		public bool AllowAddAndDelete {
			get {
				return allowAddAndDelete;
			}
			set {
				allowAddAndDelete = value;
				if(allowAddAndDelete) {
					buttonAdd.Enabled = true;
					buttonDelete.Enabled = enableDelete;
				}
				else {
					buttonAdd.Enabled = false;
					buttonDelete.Enabled = false;
				}
			}
		}
		private bool allowAddAndDelete = true;

		public IEntity SelectedEntity {
			get {
				if(listView.SelectedItems.Count == 0) {
					return null;
				}
				return GetEntity(listView.SelectedItems[0].SubItems[1].Text);
			}
			set {
				if(value == null) {
					listView.SelectedItems.Clear();
					return;
				}

				for(int i = 0; i < listView.Items.Count; ++i) {
					if(listView.Items[i].SubItems[1].Text == value.ID) {
						listView.Items[i].Selected = true;
					}
				}
			}
		}

		public delegate bool EntityFilterMethod(IEntity entityData);

		private Type dataType;
		private IEntity[] data;
		private EntityFilterMethod filterMethod;

		private bool isRefreshingList = false;
		private string lastSelectedId = null;
		
		private bool enableDelete = false;

		public EntityListBox() {
			InitializeComponent();
		}

		public void SetData(Type dataType, EntityFilterMethod filterMethod) {
			this.dataType = dataType;
			if(HeroServiceData.Data == null) {
				return;
			}

			data = null;
			if(dataType == typeof(HeroEntity)) {
				HeroEntity[] source = HeroServiceData.Data.heroes;
				data = new IEntity[source.Length];
				Array.Copy(source, data, source.Length);
			}
			else if(dataType == typeof(ScenarioEntity)) {
				ScenarioEntity[] source = HeroServiceData.Data.scenarios;
				data = new IEntity[source.Length];
				Array.Copy(source, data, source.Length);
			}
			this.filterMethod = filterMethod;
			if(data == null) {
				listView.Items.Clear();
				return;
			}

			ApplyFilter(textBoxSearch.Text);
			SelectedEntityChanged?.Invoke(this, new EntityArgs(GetEntity(lastSelectedId)));
		}

		private void OnEntityUpdated(IEntity entity) {
			if(data == null || lastSelectedId == null) {
				return;
			}

			IEntity selection = GetEntity(lastSelectedId);
			if(entity.GetType() == selection.GetType()) {
				if(entity.ID == lastSelectedId) {
					SetData(dataType, filterMethod);
					ApplyFilter(textBoxSearch.Text);
				}
			}
		}

		private void OnEntityDeleted(string entityId) {
			if(lastSelectedId == entityId) {
				SetData(dataType, filterMethod);
				listView.SelectedIndices.Clear();
				listView_SelectedIndexChanged(listView, EventArgs.Empty);
				ApplyFilter(textBoxSearch.Text);
			}
		}

		private void OnDataLoaded() {
			SetData(dataType, filterMethod);
		}

		protected override void OnResize(EventArgs e) {
			base.OnResize(e);
			listView.Columns[0].Width = -2;
		}

		private void ApplyFilter(string filter) {
			if(data == null) {
				return;
			}
			listView.BeginUpdate();
			string selectedId = null;
			isRefreshingList = true;
			if(listView.SelectedItems.Count != 0) {
				selectedId = listView.SelectedItems[0].SubItems[1].Text;
			}
			listView.Items.Clear();

			for(int i = 0; i < data.Length; ++i) {
				IEntity itemData = data[i];
				string id = itemData.ID;
				string name = itemData.Name;

				if(!string.IsNullOrWhiteSpace(filter)) {
					bool isVisible = false;
					if(name.Contains(filter, StringComparison.OrdinalIgnoreCase) ||
						id == filter) {
						isVisible = true;
					}
					if(!isVisible && filterMethod != null) {
						if(!filterMethod(itemData)) {
							isVisible = true;
						}
					}
					if(!isVisible) {
						continue;
					}
				}

				ListViewItem item = new ListViewItem(itemData.Name);
				item.SubItems.Add(itemData.ID);
				listView.Items.Add(item);

				if(id == selectedId) {
					item.Selected = true;
				}
			}
			isRefreshingList = false;
			listView.EndUpdate();

			if(listView.SelectedItems.Count == 0 && !string.IsNullOrWhiteSpace(lastSelectedId)) {
				SelectedEntity = null;
				lastSelectedId = null;
				SelectedEntityChanged?.Invoke(this, new EntityArgs(null));
			}
		}

		private void textBoxSearch_TextChanged(object sender, EventArgs e) {
			ApplyFilter(textBoxSearch.Text);
		}

		private void listView_SelectedIndexChanged(object sender, EventArgs e) {
			if(isRefreshingList) {
				return;
			}
			
			if(listView.SelectedItems.Count == 0) {
				if(lastSelectedId != null) {
					lastSelectedId = null;
					SelectedEntityChanged?.Invoke(this, new EntityArgs(null));
				}
				enableDelete = false;
				return;
			}
			enableDelete = true;

			if(!AllowAddAndDelete) {
				buttonDelete.Enabled = false;
			}
			else {
				buttonDelete.Enabled = enableDelete;
			}

			string selectedId = listView.SelectedItems[0].SubItems[1].Text;
			if(selectedId != lastSelectedId) {
				lastSelectedId = selectedId;
				SelectedEntityChanged?.Invoke(this, new EntityArgs(GetEntity(selectedId)));
			}
		}

		private IEntity GetEntity(string id) {
			if(data == null) {
				return null;
			}
			
			for(int i = 0; i < data.Length; ++i) {
				if(data[i].ID == id) {
					return data[i];
				}
			}

			return null;
		}

		private void EntityListBox_Load(object sender, EventArgs e) {
			buttonDelete.Enabled = false;
			HeroServiceData.OnEntityUpdated += OnEntityUpdated;
			HeroServiceData.OnEntityDeleted += OnEntityDeleted;
			HeroServiceData.OnDataLoaded += OnDataLoaded;
			if(!DesignMode && ParentForm != null) {
				ParentForm.FormClosing += ParentForm_FormClosing;
			}
		}

		private void ParentForm_FormClosing(object sender, FormClosingEventArgs e) {
			HeroServiceData.OnEntityUpdated -= OnEntityUpdated;
			HeroServiceData.OnEntityDeleted -= OnEntityDeleted;
			HeroServiceData.OnDataLoaded -= OnDataLoaded;
		}

		private void buttonDelete_Click(object sender, EventArgs e) {
			MessageBox.Show("Unfortunately deleting entities is not currently supported.", "Oops", MessageBoxButtons.OK, MessageBoxIcon.Information);
			/*DialogResult result = MessageBox.Show("Are you sure you want to delete this entity?", "Delete Entity", MessageBoxButtons.YesNo);

			if(result == DialogResult.Yes) {
				HeroServiceData.DeleteEntity(lastSelectedId);
			}*/
		}

		private void buttonAdd_Click(object sender, EventArgs e) {
			IEntity entity = HeroServiceData.CreateEntity(dataType);
			listView.SelectedItems.Clear();
			lastSelectedId = entity.ID;
			SetData(dataType, filterMethod);
		}
	}

	public class EntityArgs : EventArgs {
		public IEntity EntityData { get; private set; }

		public EntityArgs(IEntity entityData) : base() {
			EntityData = entityData;
		}
	}
}
