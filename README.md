# Welcome to Heroes Tactics! #

Some helpful links:

* [Heroes Tactics: Concept](https://docs.google.com/document/d/1a8VnyLxhc3-KRo_QRJeoStqQFYD5K7cOGOSq6NcEMf8/edit?usp=sharing)
* [Heroes Tactics: High-Level Design](https://docs.google.com/document/d/1guRCbmbV6k3OZrWsBqGDt9kh2x2-vlwOYkvrLXoHLHU/edit#heading=h.ivbzmtce7qfx)
* [Dev Wiki](https://bitbucket.org/hexandheart/heroes/wiki/)
* [Asana Task Board](https://app.asana.com/0/353501854661317/board)
* [Hex and Heart Discord](https://discord.gg/xSx366k)
* [brainCloud Portal](https://portal.braincloudservers.com)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Getting Started With Development ##

Please see the [Development Guide](https://bitbucket.org/hexandheart/heroes/wiki/Development_Guide).

## Contact ##

Please contact @apocriva (apocriva#1984 on Discord) with any questions.