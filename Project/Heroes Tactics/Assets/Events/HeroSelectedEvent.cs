using System.IO;

namespace Events {

	public class HeroSelectedEvent : IEvent {

		public int ActorID { get; private set; }
		public int SelectorTeamID { get; private set; }

		public HeroSelectedEvent(int actorId, int selectorTeamId) {
			ActorID = actorId;
			SelectorTeamID = selectorTeamId;
		}

	}
	
}