using BattleMechanics;

namespace Events {

	public class BattleStateUpdatedEvent : IEvent {

		public BattleState BattleState { get; private set; }

		public BattleStateUpdatedEvent(BattleState battleState) {
			BattleState = battleState;
		}

	}
	
}