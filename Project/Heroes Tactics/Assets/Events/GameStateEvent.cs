using System.IO;

namespace Events {

	public class GameStateEvent : IEvent {

		public GameState GameState { get; private set; }
		public TransitionState TransitionState { get; private set; }

		public GameStateEvent(GameState gameState, TransitionState transitionState) {
			GameState = gameState;
			TransitionState = transitionState;
		}

	}
	
}