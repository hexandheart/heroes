using System.IO;

namespace Events {

	public class PawnHoveredEvent : IEvent {

		public Pawn Pawn { get; private set; }
		public bool IsHovering { get; private set; }

		public PawnHoveredEvent(Pawn pawn, bool isHovering) {
			Pawn = pawn;
			IsHovering = isHovering;
		}

	}
	
}