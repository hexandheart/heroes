using BattleMechanics;

namespace Events {

	public class TileClickedEvent : IEvent {

		public GridVector Position { get; private set; }

		public TileClickedEvent(GridVector position) {
			Position = position;
		}

	}
	
}