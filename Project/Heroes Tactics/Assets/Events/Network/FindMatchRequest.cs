using System.IO;

namespace Events {

	public class FindMatchRequest : IEvent {

		public struct ServerSettings {
			public string address;
			public int port;
			public string appId;
			public string version;
		}

		public MatchType MatchType { get; private set; }
		public string LoadoutID { get; private set; }
		public ServerSettings? ServerOverride { get; private set; }

		public FindMatchRequest(MatchType matchType, string loadoutId, ServerSettings? serverOverride) {
			MatchType = matchType;
			LoadoutID = loadoutId;
			ServerOverride = serverOverride;
		}

		private FindMatchRequest() { }

	}
	
}