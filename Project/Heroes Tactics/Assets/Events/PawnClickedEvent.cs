using System.IO;

namespace Events {

	public class PawnClickedEvent : IEvent {

		public Pawn Pawn { get; private set; }

		public PawnClickedEvent(Pawn pawn) {
			Pawn = pawn;
		}

	}
	
}