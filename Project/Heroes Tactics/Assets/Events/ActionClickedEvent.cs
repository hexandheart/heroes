using System.IO;
using BattleMechanics;

namespace Events {

	public class ActionClickedEvent : IEvent {

		public Actor Actor { get; private set; }
		public BattleAction Action { get; private set; }

		public ActionClickedEvent(Actor actor, BattleAction battleAction) {
			Actor = actor;
			Action = battleAction;
		}

	}
	
}