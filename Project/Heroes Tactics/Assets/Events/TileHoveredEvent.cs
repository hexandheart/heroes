using BattleMechanics;
using System.IO;

namespace Events {

	public class TileHoveredEvent : IEvent {

		public GridVector Position { get; private set; }
		public bool IsHovering { get; private set; }

		public TileHoveredEvent(GridVector position, bool isHovering) {
			Position = position;
			IsHovering = isHovering;
		}

	}
	
}