﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class ArenaCameraTarget : MonoBehaviour {

	private Camera cameraTarget;

	public Camera Camera {
		get {
			return cameraTarget;
		}
	}

	private void Start() {
		cameraTarget = GetComponent<Camera>();
	}

}
