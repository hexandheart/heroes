﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Events;

public class ArenaCameraManager : MonoBehaviour, IEventListener {

	public static readonly float TransitionTime = 0.5f;

	private static ArenaCameraManager instance;

	private Camera arenaCamera;
	private Dictionary<string, ArenaCameraTarget> cameraTargets;
	private Transform heroDetailFocus;

	private Coroutine transitionCoroutine;
	private ArenaCameraTarget currentTarget;

	private bool forceToTarget;

	private List<KeyValuePair<Object, string>> focusList;

	private void OnEnable() {
		AddListeners();
		forceToTarget = true;
		instance = this;
		SceneManager.sceneLoaded += OnSceneLoaded;
		SceneManager.sceneUnloaded += OnSceneUnloaded;
	}

	private void OnDisable() {
		RemoveListeners();
		instance = null;
		SceneManager.sceneLoaded -= OnSceneLoaded;
		SceneManager.sceneUnloaded -= OnSceneUnloaded;
	}

	public static void PushFocus(Object source, string target) {
		instance.PushFocusInternal(source, target);
	}

	public static void PopFocus(Object source) {
		instance.PopFocusInternal(source);
	}

	private void PushFocusInternal(Object source, string target) {
		/*if(target.StartsWith("HeroDetail:")) {
			string[] args = target.Substring("HeroDetail:".Length).Split(',');
			ArenaLevel level = FindObjectOfType<ArenaLevel>();
			int x = int.Parse(args[0]);
			int z = int.Parse(args[1]);
			Vector3 position = level.TileToWorld(new Point(x, z));
			position.y += level.HeightStep * 2;
			heroDetailFocus.position = position;
			target = "HeroDetail";
		}

		if(!cameraTargets.ContainsKey(target)) {
			Debug.LogErrorFormat(this, "[ArenaCameraManager]: Unable to find ArenaCameraTarget named {0}", target);
			return;
		}
		ArenaCameraTarget cameraTarget = cameraTargets[target];
		
		focusList.Add(new KeyValuePair<Object, string>(source, target));
		TransitionToTarget(cameraTarget, forceToTarget ? 0.01f : TransitionTime);
		forceToTarget = false;*/
	}

	private void PopFocusInternal(Object source) {
		/*int index = focusList.FindLastIndex(kv => kv.Key == source);
		if(index >= 0) {
			string currentTargetName = focusList[index].Value;
			focusList.RemoveAt(index);
			
			if(currentTarget != null && currentTarget.name == currentTargetName && focusList.Count > 0) {
				TransitionToTarget(cameraTargets[focusList[focusList.Count - 1].Value], TransitionTime);
			}
			else {
				currentTarget = null;
			}
		}*/
	}

	private void UpdateTargets() {
		arenaCamera = Camera.main;

		if(cameraTargets == null) {
			cameraTargets = new Dictionary<string, ArenaCameraTarget>();
		}
		cameraTargets.Clear();
		if(focusList == null) {
			focusList = new List<KeyValuePair<Object, string>>();
		}

		ArenaCameraTarget[] targets = FindObjectsOfType<ArenaCameraTarget>();
		for(int i = 0; i < targets.Length; ++i) {
			cameraTargets.Add(targets[i].name, targets[i]);
		}

		heroDetailFocus = null;
		GameObject focusGo = GameObject.Find("HeroDetailFocus");
		if(focusGo != null) {
			heroDetailFocus = focusGo.transform;
		}
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode) {
		UpdateTargets();
	}

	private void OnSceneUnloaded(Scene scene) {
		UpdateTargets();
	}

	private void TransitionToTarget(ArenaCameraTarget target, float duration) {
		if(transitionCoroutine != null) {
			StopCoroutine(transitionCoroutine);
		}
		transitionCoroutine = StartCoroutine(DoTransitionCoroutine(target, duration));
	}

	private IEnumerator DoTransitionCoroutine(ArenaCameraTarget target, float duration) {
		Vector3 startPos = arenaCamera.transform.position;
		Quaternion startRot = arenaCamera.transform.rotation;
		float startFov = arenaCamera.fieldOfView;
		float startNear = arenaCamera.nearClipPlane;
		float startFar = arenaCamera.farClipPlane;

		float currentTime = 0f;

		currentTarget = target;

		while(currentTime <= duration && currentTarget != null) {
			currentTime += Time.deltaTime;
			float t = currentTime / duration;
			if(t > 1f) {
				t = 1f;
			}
			float t2 = 1f - ((1f - t) * (1f - t));

			arenaCamera.transform.position = Vector3.Lerp(startPos, target.transform.position, t2);
			arenaCamera.transform.rotation = Quaternion.Lerp(startRot, target.transform.rotation, t2);
			arenaCamera.fieldOfView = Mathf.Lerp(startFov, target.Camera.fieldOfView, t2);
			arenaCamera.nearClipPlane = Mathf.Lerp(startNear, target.Camera.nearClipPlane, t2);
			arenaCamera.farClipPlane = Mathf.Lerp(startFar, target.Camera.farClipPlane, t2);

			yield return null;
		}

		if(currentTarget != null) {
			arenaCamera.transform.position = target.transform.position;
			arenaCamera.transform.rotation = target.transform.rotation;
			arenaCamera.fieldOfView = target.Camera.fieldOfView;
			arenaCamera.nearClipPlane = target.Camera.nearClipPlane;
			arenaCamera.farClipPlane = target.Camera.farClipPlane;
		}

		transitionCoroutine = null;
	}

	private void Update() {
		if(currentTarget != null && transitionCoroutine == null) {
			arenaCamera.transform.position = currentTarget.transform.position;
			arenaCamera.transform.rotation = currentTarget.transform.rotation;
			arenaCamera.fieldOfView = currentTarget.Camera.fieldOfView;
			arenaCamera.nearClipPlane = currentTarget.Camera.nearClipPlane;
			arenaCamera.farClipPlane = currentTarget.Camera.farClipPlane;
		}
	}

	#region Event listeners
	public void AddListeners() {
		EventManager.AddListener<GameStateEvent>(OnGameStateEvent);
	}

	public void RemoveListeners() {
		EventManager.RemoveListener<GameStateEvent>(OnGameStateEvent);
	}

	private void OnGameStateEvent(IEvent evt) {
		GameStateEvent gse = evt as GameStateEvent;

		if(gse.TransitionState == TransitionState.Entering) {
			forceToTarget = true;
		}
	}
	#endregion

}
