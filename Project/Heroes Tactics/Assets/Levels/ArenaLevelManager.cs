﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Events;

public class ArenaLevelManager : MonoBehaviour, IEventListener {

	[SerializeField]
	private string mainMenuScene;

	private void Start() {
		AddListeners();

		SceneManager.sceneLoaded += OnSceneLoaded;
		SceneManager.sceneUnloaded += OnSceneUnloaded;

		for(int i = 0; i < SceneManager.sceneCount; ++i) {
			Scene scene = SceneManager.GetSceneAt(i);
			OnSceneLoaded(scene, LoadSceneMode.Additive);
		}
	}

	private void Destroy() {
		RemoveListeners();

		SceneManager.sceneLoaded -= OnSceneLoaded;
		SceneManager.sceneUnloaded -= OnSceneUnloaded;
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode) {

#if UNITY_EDITOR
		// Rebuild lighting.
		if(UnityEditor.Lightmapping.giWorkflowMode == UnityEditor.Lightmapping.GIWorkflowMode.Iterative) {
			DynamicGI.UpdateEnvironment();
		}
#endif
	}

	private void OnSceneUnloaded(Scene scene) {
	}

	private IEnumerator LoadScene(string sceneName, LoadSceneMode loadSceneMode) {
		yield return SceneManager.LoadSceneAsync(sceneName, loadSceneMode);
	}

	#region Event listeners
	public void AddListeners() {
		EventManager.AddListener<GameStateEvent>(OnGameStateEvent);
	}

	public void RemoveListeners() {
		EventManager.RemoveListener<GameStateEvent>(OnGameStateEvent);
	}

	private void OnGameStateEvent(IEvent evt) {
		GameStateEvent gse = evt as GameStateEvent;

		if(gse.GameState == GameState.Hub && gse.TransitionState == TransitionState.Entering && !string.IsNullOrEmpty(mainMenuScene)) {
			GameStateManager.RegisterTransitionCoroutine(LoadScene(mainMenuScene, LoadSceneMode.Single));
		}
	}
	#endregion

}
