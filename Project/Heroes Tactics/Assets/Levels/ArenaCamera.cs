﻿using Arena;
using Arena.Photon;
using BattleMechanics;
using BattleMechanics.Breezes;
using BattleMechanics.Events;
using BattleMechanicsUtils;
using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class ArenaCamera : MonoBehaviour {

	[SerializeField]
	private Transform focus;

	[SerializeField]
	private Transform localPanTarget;

	[SerializeField]
	private CinemachineVirtualCamera cineCam;
	[SerializeField]
	private CinemachineFollowZoom cineZoom;

	[SerializeField]
	private float spinSensitivityYaw;
	[SerializeField]
	private float spinSensitivityPitch;
	[SerializeField]
	private float minPitch;
	[SerializeField]
	private float maxPitch;
	[SerializeField]
	private float spinThreshold;
	[SerializeField]
	private float spinFriction;

	[SerializeField]
	private float panThreshold;
	[SerializeField]
	private float panSensitivity;
	[SerializeField]
	private float panFriction;

	[SerializeField]
	private float zoomThreshold;
	[SerializeField]
	private float zoomSensitivity;
	[SerializeField]
	private float zoomMin;
	[SerializeField]
	private float zoomMax;
	[SerializeField]
	private float zoomFriction;

	private bool inputEnabled = false;

	private bool isOverSpinThreshold = false;
	private bool isOverPanThreshold = false;
	private bool isOverZoomThreshold = false;

	private bool isManualSpinInput = false;
	private bool isManualPanInput = false;
	private bool isManualZoomInput = false;

	private Vector3 lastSpinMousePos = Vector3.zero;
	private Vector3 lastPanMousePos = Vector3.zero;
	private float lastZoomValue = 0f;

	private Vector3 spinMomentumInput = Vector3.zero;
	private Vector3 panMomentumInput = Vector3.zero;
	private float zoomMomentumInput = 0f;

	private float mapWidth = 0f;
	private float mapHeight = 0f;

	private void Start() {
		AddListeners();
	}

	private void OnDestroy() {
		RemoveListeners();
	}

	public void SetFocus(Vector3 position, float width) {
		if(focus != null) {
			focus.position = position;
		}
		transform.position = position;
		cineZoom.m_Width = width;
	}

	public void ResetFocus() {
		SetFocus(
			new Vector3(BattleController.BattleState.Map.GridWidth / 2.0f - 0.5f, 0f, -BattleController.BattleState.Map.GridHeight / 2.0f + 0.5f),
			(BattleController.BattleState.Map.GridWidth + BattleController.BattleState.Map.GridHeight) / 2f);
		mapWidth = Mathf.Min(BattleController.BattleState.Map.GridWidth, BattleController.BattleState.Map.GridHeight);
		mapHeight = mapWidth;
	}

	public void Update() {
		UpdateAngles();
		UpdatePan();
		UpdateZoom();
	}

	private void UpdateAngles() {
		Vector3 targetAngles = transform.localEulerAngles;

		if(isManualSpinInput && inputEnabled) {
			// Have we stopped manual input?
			if(!Input.GetMouseButton(1)) {
				isManualSpinInput = false;
			}
			else {
				// Are we over the movement threshold?
				Vector3 delta = Input.mousePosition - lastSpinMousePos;
				if(!isOverSpinThreshold && delta.magnitude >= spinThreshold) {
					isOverSpinThreshold = true;
				}

				if(isOverSpinThreshold) {
					lastSpinMousePos = Input.mousePosition;
					
					targetAngles.x += spinSensitivityPitch * delta.y * Time.deltaTime;
					targetAngles.y += spinSensitivityYaw * delta.x * Time.deltaTime;

					spinMomentumInput.x = spinSensitivityPitch * delta.y * Time.deltaTime;
					spinMomentumInput.y = spinSensitivityYaw * delta.x * Time.deltaTime;
				}
			}
		}

		// This is not an else because maybe manual input stopped above.
		if(!isManualSpinInput) {
			// Did we start manual input?
			if(Input.GetMouseButton(1) && inputEnabled) {
				lastSpinMousePos = Input.mousePosition;
				isManualSpinInput = true;
				isOverSpinThreshold = false;
			}
			// Keep momentum going.
			else {
				float newMomentumPitch = spinMomentumInput.x - spinMomentumInput.x * spinFriction * Time.deltaTime;
				if(Mathf.Sign(newMomentumPitch) != Mathf.Sign(spinMomentumInput.x)) {
					spinMomentumInput.x = 0f;
				}
				else {
					spinMomentumInput.x = newMomentumPitch;
				}
				targetAngles.x += spinMomentumInput.x;

				float newMomentumYaw = spinMomentumInput.y - spinMomentumInput.y * spinFriction * Time.deltaTime;
				if(Mathf.Sign(newMomentumYaw) != Mathf.Sign(spinMomentumInput.y)) {
					spinMomentumInput.y = 0f;
				}
				else {
					spinMomentumInput.y = newMomentumYaw;
				}
				targetAngles.y += spinMomentumInput.y;
			}
		}

		targetAngles.x = Mathf.Max(minPitch, Mathf.Min(targetAngles.x, maxPitch));

		while(targetAngles.y >= 360f) {
			targetAngles.y -= 360f;
		}
		while(targetAngles.y < 0f) {
			targetAngles.y += 360f;
		}

		transform.localEulerAngles = targetAngles;
	}

	private void UpdatePan() {
		Vector3 targetPosition = transform.position;
		if(localPanTarget != null) {
			targetPosition = localPanTarget.position;
		}

		if(isManualPanInput && inputEnabled) {
			// Have we stopped manual input?
			if(!Input.GetMouseButton(0)) {
				isManualPanInput = false;
			}
			else {
				// Are we over the movement threshold?
				Vector3 delta = Input.mousePosition - lastPanMousePos;
				if(!isOverPanThreshold && delta.magnitude >= panThreshold) {
					isOverPanThreshold = true;
				}

				if(isOverPanThreshold) {
					lastPanMousePos = Input.mousePosition;
					
					Vector3 forward = localPanTarget != null ? localPanTarget.forward : transform.forward;
					forward.y = 0f;
					forward.Normalize();
					Vector3 right = localPanTarget != null ? localPanTarget.right : transform.right;
					right.y = 0f;
					right.Normalize();

					float localPanSensitivity = Mathf.Lerp(panSensitivity * 0.5f, panSensitivity, (cineZoom.m_Width - zoomMin) / (zoomMax - zoomMin));
					Vector3 targetDelta = (right * -delta.x * localPanSensitivity + forward * -delta.y * localPanSensitivity) * Time.deltaTime;

					targetPosition += targetDelta;
					panMomentumInput = targetDelta;
				}
			}
		}

		// This is not an else because maybe manual input stopped above.
		if(!isManualPanInput) {
			// Did we start manual input?
			if(Input.GetMouseButton(0) && inputEnabled) {
				lastPanMousePos = Input.mousePosition;
				isManualPanInput = true;
				isOverPanThreshold = false;
			}
			// Keep momentum going.
			else {
				float newMomentumX = panMomentumInput.x - panMomentumInput.x * panFriction * Time.deltaTime;
				if(Mathf.Sign(newMomentumX) != Mathf.Sign(panMomentumInput.x)) {
					panMomentumInput.x = 0f;
				}
				else {
					panMomentumInput.x = newMomentumX;
				}
				targetPosition.x += panMomentumInput.x;

				float newMomentumZ = panMomentumInput.z - panMomentumInput.z * panFriction * Time.deltaTime;
				if(Mathf.Sign(newMomentumZ) != Mathf.Sign(panMomentumInput.z)) {
					panMomentumInput.z = 0f;
				}
				else {
					panMomentumInput.z = newMomentumZ;
				}
				targetPosition.z += panMomentumInput.z;
			}
		}

		targetPosition.x = Mathf.Max(0f, Mathf.Min(targetPosition.x, mapWidth));
		targetPosition.z = Mathf.Min(0f, Mathf.Max(targetPosition.z, -mapHeight));

		if(localPanTarget != null) {
			localPanTarget.position = targetPosition;
		}
		else {
			transform.position = targetPosition;
		}
	}

	private void UpdateZoom() {
		float zoom = cineZoom.m_Width;

		if(!isManualZoomInput) {
			if(Mathf.Abs(Input.mouseScrollDelta.y) > 0f) {
				lastZoomValue = Input.mouseScrollDelta.y;
				isManualZoomInput = true;
				isOverZoomThreshold = false;
			}
			else {
				float newZoomMomentum = zoomMomentumInput - zoomMomentumInput * zoomFriction * Time.deltaTime;
				if(Mathf.Sign(newZoomMomentum) != Mathf.Sign(zoomMomentumInput)) {
					zoomMomentumInput = 0f;
				}
				else {
					zoomMomentumInput = newZoomMomentum;
				}
				zoom += zoomMomentumInput;
			}
		}

		if(isManualZoomInput && inputEnabled) {
			if(Mathf.Abs(Input.mouseScrollDelta.y) < 0.01f) {
				isManualZoomInput = false;
			}
			else {
				lastZoomValue += Input.mouseScrollDelta.y;
				if(!isOverZoomThreshold && Mathf.Abs(lastZoomValue) > zoomThreshold) {
					isOverZoomThreshold = true;
				}

				if(isOverZoomThreshold) {
					zoom += lastZoomValue * zoomSensitivity * Time.deltaTime;
					lastZoomValue = Input.mouseScrollDelta.y;
					zoomMomentumInput = lastZoomValue * zoomSensitivity * Time.deltaTime;
				}
			}
		}

		zoom = Mathf.Max(zoomMin, Mathf.Min(zoom, zoomMax));

		cineZoom.m_Width = zoom;
	}

	public void AddListeners() {
		PresentationManager.RegisterHandler(BattleEventType.BattlePhaseChanged, OnBattlePhaseChanged);
		PresentationManager.RegisterHandler(BattleEventType.BreezeUpdate, OnBreezeUpdate);
		PresentationManager.RegisterHandler(BattleEventType.TurnStart, OnTurnStart);
	}

	public void RemoveListeners() {
		PresentationManager.UnregisterHandler(BattleEventType.BattlePhaseChanged, OnBattlePhaseChanged);
		PresentationManager.UnregisterHandler(BattleEventType.BreezeUpdate, OnBreezeUpdate);
		PresentationManager.UnregisterHandler(BattleEventType.TurnStart, OnTurnStart);
	}

	private IEnumerator OnBattlePhaseChanged(IBattleEvent battleEvent) {
		BattlePhaseChangedEvent evt = battleEvent as BattlePhaseChangedEvent;

		if(evt.BattlePhase == ReadyState.Ready) {
			inputEnabled = true;
		}
		else {
			inputEnabled = false;
		}
		yield break;
	}

	private IEnumerator OnTurnStart(IBattleEvent battleEvent) {
		inputEnabled = true;
		yield break;
	}

	private IEnumerator OnBreezeUpdate(IBattleEvent battleEvent) {
		/*BreezeUpdateEvent evt = battleEvent as BreezeUpdateEvent;

		Team team = BattleController.BattleState.GetTeam(evt.TeamID);
		if(evt.BreezeType == BreezeType.ActorSelection) {
			Actor actor = BattleController.BattleState.GetActorByID(team.ActorSelectionBreeze.ActorID);
			if(actor == null) {
				ResetFocus();
			}
			else {
				Vector3 root = new Vector3(BattleController.BattleState.Map.GridWidth / 2.0f - 0.5f, 0f, -BattleController.BattleState.Map.GridHeight / 2.0f + 0.5f);
				focus.position = root * 0.7f + actor.Position.ToVector3() * 0.3f;
				cineZoom.m_Width = 7f;
			}
		}*/

		yield break;
	}

}
