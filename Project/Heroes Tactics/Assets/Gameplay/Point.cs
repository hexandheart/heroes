﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public struct Point {
	public int x;
	public int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public override int GetHashCode() {
		return (x << 8) + y;
	}

	public override bool Equals(object obj) {
		if(obj == null) {
			return false;
		}
		if(!(obj is Point)) {
			return false;
		}

		Point ap = (Point)obj;
		return x == ap.x && y == ap.y;
	}

	public override string ToString() {
		return string.Format("[Point ({0}, {1})]", x, y);
	}

	public static Point operator +(Point lhs, Point rhs) {
		return new Point(lhs.x + rhs.x, lhs.y + rhs.y);
	}

	public static Point operator -(Point lhs, Point rhs) {
		return new Point(lhs.x - rhs.x, lhs.y - rhs.y);
	}

	public static bool operator ==(Point lhs, Point rhs) {
		return lhs.Equals(rhs);
	}

	public static bool operator !=(Point lhs, Point rhs) {
		return !lhs.Equals(rhs);
	}

	public int Pack() {
		return (x << 8) + y;
	}

	public static Point Unpack(int packed) {
		return new Point((packed >> 8) & 0xff, packed & 0xff);
	}

	public static int[] Pack(List<Point> positions) {
		// Convert to array of ints...
		int[] packed = new int[positions.Count];
		for(int i = 0; i < positions.Count; ++i) {
			packed[i] = positions[i].Pack();
		}

		return packed;
	}

	public int Magnitude() {
		return Mathf.Abs(x) + Mathf.Abs(y);
	}
}
