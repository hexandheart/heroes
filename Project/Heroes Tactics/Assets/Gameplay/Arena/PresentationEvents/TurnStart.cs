﻿using System.Collections;
using System.Collections.Generic;
using Arena.Photon;
using BattleMechanics;
using BattleMechanics.Events;

namespace Arena.PresentationEvents {

	/// <summary>
	/// <para>Turn has just started.</para>
	/// </summary>
	public class TurnStart : PresentationEvent {
		public TurnStart(TurnStartEvent evt) : base(evt.Time) {
			this.evt = evt;
		}

		public override IEnumerator Process() {
			yield return null;
		}

		private TurnStartEvent evt;

	}

}