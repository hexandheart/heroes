﻿using System.Collections;
using BattleMechanics;

namespace Arena.PresentationEvents {

	/// <summary>
	/// <para>Triggered at the start of battle.</para>
	/// </summary>
	public class PawnSpawned : PresentationEvent {
		public PawnSpawned(int time, Actor actor, GridVector position, GridFacing facing) : base(time) {
			this.actor = actor;
			this.position = position;
			this.facing = facing;
		}

		public override IEnumerator Process() {
			bool done = false;
			PresentationManager.LoadPawnForActor(actor,
				(pawn) => {
					done = true;
				});
			while(!done) {
				yield return null;
			}

			// Do an animation for it or something!
			yield return null;
		}

		private Actor actor;
		private GridVector position;
		private GridFacing facing;
	}

}