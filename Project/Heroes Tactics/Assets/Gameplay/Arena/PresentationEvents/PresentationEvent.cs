﻿using System;
using System.Collections;

namespace Arena {

	/// <summary>
	/// <para>Base event type for "presentation events" -- ie battle events translated into local goings-on.</para>
	/// 
	/// <para>"Time" is the battle time at which this event occurred.</para>
	/// </summary>
	public abstract class PresentationEvent {
		public int Time { get; private set; }
		public abstract IEnumerator Process();

		public PresentationEvent(int time) {
			Time = time;
		}
	}

}
