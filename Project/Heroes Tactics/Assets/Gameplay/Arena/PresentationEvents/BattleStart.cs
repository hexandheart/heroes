﻿using System.Collections;
using System.Collections.Generic;
using Arena.Photon;
using BattleMechanics;

namespace Arena.PresentationEvents {

	/// <summary>
	/// <para>Triggered at the start of battle.</para>
	/// </summary>
	public class BattleStart : PresentationEvent {
		public BattleStart() : base(0) { }

		public override IEnumerator Process() {
			yield return null;
		}

	}

}