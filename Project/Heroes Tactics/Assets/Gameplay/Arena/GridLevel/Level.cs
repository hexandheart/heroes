﻿using System.Collections;
using UnityEngine;
using BattleMechanics;
using Events;

namespace Arena.GridLevel {

	public class Level : MonoBehaviour {

		[SerializeField]
		private TileSet tileSet;

		[SerializeField]
		private float tileAnimHeight;
		[SerializeField]
		private float tileAnimDuration;

		private GameObject tileRoot;
		private Tile[] tiles;
		private BattleState battleState;

		public IEnumerator LoadBattleState(BattleState battleState) {
			this.battleState = battleState;
			SpawnTiles();
			yield return null;
		}

		public IEnumerator CleanUp() {
			StopAllCoroutines();
			if(tileRoot != null) {
				tileRoot.SetActive(false);
				Destroy(tileRoot);
				tileRoot = null;
			}
			yield break;
		}

		private void SpawnTiles() {
			int gridWidth = battleState.Map.GridWidth;
			int gridHeight = battleState.Map.GridHeight;

			tileRoot = new GameObject("Level");
			DontDestroyOnLoad(tileRoot);

			tiles = new Tile[gridWidth * gridHeight];
			for(int y = 0; y < gridHeight; ++y) {
				for(int x = 0; x < gridWidth; ++x) {
					GridVector position = new GridVector(x, y);
					Tile tile = tileSet.InstantiateTile(transform, position, battleState.Map.GetTileAt(x, y).Flags);
					tile.transform.SetParent(tileRoot.transform, true);
					tile.gameObject.SetActive(false);
					tile.Initialize(position, OnTileClicked);
					tiles[y * gridWidth + x] = tile;
				}
			}
		}

		public IEnumerator LevelIntro() {
			yield return new WaitForSeconds(0.5f);
			for(int y = 0; y < battleState.Map.GridHeight; ++y) {
				for(int x = 0; x < battleState.Map.GridWidth; ++x) {
					StartCoroutine(AnimateTileIntro(x, y, true));
				}
			}

			yield return new WaitForSeconds((battleState.Map.GridWidth + battleState.Map.GridHeight * 2) * 0.05f + tileAnimDuration);
		}

		public IEnumerator LevelOutro() {
			yield return new WaitForSeconds(0.5f);
			for(int y = 0; y < battleState.Map.GridHeight; ++y) {
				for(int x = 0; x < battleState.Map.GridWidth; ++x) {
					StartCoroutine(AnimateTileIntro(x, y, false));
				}
			}

			yield return new WaitForSeconds((battleState.Map.GridWidth + battleState.Map.GridHeight * 2) * 0.05f + tileAnimDuration);
		}

		private IEnumerator AnimateTileIntro(int x, int y, bool forward) {
			float delayTime = ((battleState.Map.GridWidth - x) + ((battleState.Map.GridHeight - y) * 2)) * 0.05f;
			if(!forward) {
				delayTime = (x + y * 2) * 0.05f;
			}
			yield return new WaitForSeconds(delayTime);
			Tile tile = tiles[y * battleState.Map.GridWidth + x];
			float startHeight = tileAnimHeight;
			float endHeight = 0f;
			float currentTime = 0f;

			if(!forward) {
				startHeight = 0f;
				endHeight = tileAnimHeight;
			}

			tile.gameObject.SetActive(true);
			while(currentTime <= tileAnimDuration) {
				currentTime += Time.deltaTime;
				float percent = currentTime / tileAnimDuration;
				tile.transform.localPosition = new Vector3(x, Mathf.Lerp(startHeight, endHeight, percent * percent), -y);
				yield return null;
			}
			tile.transform.localPosition = new Vector3(x, endHeight, -y);

			if(!forward) {
				tile.gameObject.SetActive(false);
			}
		}

		public void ClearHighlights() {
			for(int i = 0; i < battleState.Map.GridWidth * battleState.Map.GridHeight; ++i) {
				tiles[i].ClearHighlight();
			}
		}

		public void ShowSourceHighlight(int x, int y) {
			tiles[y * battleState.Map.GridWidth + x].ShowSourceHighlight();
		}

		public void ShowHighlight(TargetSolverResult targetSolverResult, bool isInteractible) {
			tiles[targetSolverResult.TargetPosition.GridY * battleState.Map.GridWidth + targetSolverResult.TargetPosition.GridX].ShowHighlight(targetSolverResult.ResultType);
			tiles[targetSolverResult.TargetPosition.GridY * battleState.Map.GridWidth + targetSolverResult.TargetPosition.GridX].IsInteractible = isInteractible;
		}

		public void ClearActionTarget(GridVector position) {
			tiles[position.GridY * battleState.Map.GridWidth + position.GridX].ClearActionTarget();
		}

		public void ShowActionTargetForMovement(GridVector position) {
			tiles[position.GridY * battleState.Map.GridWidth + position.GridX].ShowActionTargetForMovement();
		}

		public void ShowActionTarget(GridVector position, BattleActionEffectType effectType) {
			tiles[position.GridY * battleState.Map.GridWidth + position.GridX].ShowActionTarget(effectType);
		}

		private void OnTileClicked(GridVector position) {
			EventManager.Queue(new TileClickedEvent(position));
		}

	}

}
