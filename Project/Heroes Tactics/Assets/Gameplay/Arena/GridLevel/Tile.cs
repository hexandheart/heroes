﻿using System;
using UnityEngine;
using UnityEngine.UI;
using BattleMechanics;
using Events;
using UnityEngine.EventSystems;

namespace Arena.GridLevel {

	[SelectionBase]
	public class Tile : MonoBehaviour {

		public bool IsInteractible {
			get {
				return tileButton.interactable;
			}
			set {
				raycaster.enabled = value;
				tileButton.interactable = value;
			}
		}

		public GridVector Position { get { return position; } }

		private Action<GridVector> onTileClicked;

		[SerializeField]
		private Image tileHighlight;
		[SerializeField]
		private Image actionTarget;

		[SerializeField]
		private Button tileButton;

		private GraphicRaycaster raycaster;
		private GridVector position;

		public void Initialize(GridVector position, Action<GridVector> onTileClicked) {
			raycaster = GetComponentInChildren<GraphicRaycaster>();
			raycaster.enabled = false;
			this.onTileClicked = onTileClicked;
			this.position = position;
		}

		public void ClearHighlight() {
			tileHighlight.transform.localScale = Vector3.one;
			tileHighlight.color = Color.clear;
			actionTarget.color = Color.clear;
			IsInteractible = false;
		}

		public void ShowHighlight(TargetSolverResultType type) {
			tileHighlight.transform.localScale = Vector3.one;
			switch(type) {
			case TargetSolverResultType.Valid:
				tileHighlight.color = Color.green;
				break;
			case TargetSolverResultType.Blocked:
				tileHighlight.color = Color.red;
				break;
			case TargetSolverResultType.VisionBlocked:
				tileHighlight.color = Color.black;
				break;
			default:
				tileHighlight.color = Color.clear;
				break;
			}
		}

		public void ShowSourceHighlight() {
			if(tileHighlight.color == Color.green) {
				tileHighlight.color = Color.cyan;
			}
			else {
				tileHighlight.color = Color.blue;
			}
			tileHighlight.transform.localScale = new Vector3(1.1f, 1f, 1.1f);
		}

		public void ClearActionTarget() {
			actionTarget.color = Color.clear;
		}

		public void ShowActionTargetForMovement() {
			actionTarget.color = Color.blue;
		}

		public void ShowActionTarget(BattleActionEffectType actionEffectType) {
			switch(actionEffectType) {
			case BattleActionEffectType.Help:
				actionTarget.color = Color.green;
				break;
			case BattleActionEffectType.Harm:
				actionTarget.color = Color.red;
				break;
			}
		}

		public void OnClicked() {
			onTileClicked?.Invoke(position);
		}

	}

}
