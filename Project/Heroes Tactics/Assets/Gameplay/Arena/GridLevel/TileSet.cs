﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleMechanics;

namespace Arena.GridLevel {

	[CreateAssetMenu(fileName = "GridTileSet", menuName = "HeroesTactics/GridTileSet", order = 1)]
	public class TileSet : ScriptableObject {

		[SerializeField]
		private Tile[] tilePrefabs;

		public Tile InstantiateTile(Transform parent, GridVector location, TileFlags tileFlags) {
			int tileIndex = (int)tileFlags;
			Tile tile = Instantiate(tilePrefabs[tileIndex]);
			tile.transform.localPosition = new Vector3(location.GridX, 0f, -location.GridY);
			tile.transform.SetParent(parent, true);

			return tile;
		}

	}

}
