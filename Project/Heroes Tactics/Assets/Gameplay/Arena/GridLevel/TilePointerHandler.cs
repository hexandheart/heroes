﻿using UnityEngine;
using UnityEngine.EventSystems;
using Events;
using Arena.GridLevel;

public class TilePointerHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	private Tile tile;

	private void Start() {
		tile = GetComponentInParent<Tile>();
	}

	public void OnPointerEnter(PointerEventData eventData) {
		EventManager.Queue(new TileHoveredEvent(tile.Position, true));
	}

	public void OnPointerExit(PointerEventData eventData) {
		EventManager.Queue(new TileHoveredEvent(tile.Position, false));
	}

}
