﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using BattleMechanics;

namespace Arena.GridLevel {

	[RequireComponent(typeof(Level))]
	public class LevelTester : MonoBehaviour {

		[SerializeField]
		[Multiline(15)]
		private string jsonBattleData;

		[SerializeField]
		private int solverSourceXMin;
		[SerializeField]
		private int solverSourceXMax;
		[SerializeField]
		private int solverSourceYMin;
		[SerializeField]
		private int solverSourceYMax;
		[SerializeField]
		private int solverMinRangeMin;
		[SerializeField]
		private int solverMinRangeMax;
		[SerializeField]
		private int solverAdditionalRangeMin;
		[SerializeField]
		private int solverAdditionalRangeMax;
		[SerializeField]
		private bool useLineOfSight;
		[SerializeField]
		private bool usePathing;

		private Level level;
		private BattleState battleState;
		private TargetSolver targetSolver;

		/*private void Start() {
			level = GetComponent<Level>();

			CreateTestBattleState();
			level.LoadBattleState(battleState);
			StartCoroutine(DoDemo());
		}

		private void CreateTestBattleState() { // Create the map.
			JsonData initData = JsonMapper.ToObject(jsonBattleData);

			JsonData arenaData = initData["arenaData"];
			int mapWidth = arenaData["width"].AsInt();
			int mapHeight = arenaData["height"].AsInt();
			BattleMechanics.Tile[] tiles = new BattleMechanics.Tile[mapWidth * mapHeight];

			JsonData tileData = arenaData["tiles"];
			for(int y = 0; y < mapHeight; ++y) {
				for(int x = 0; x < mapWidth; ++x) {
					int index = y * mapWidth + x;
					tiles[index] = new BattleMechanics.Tile((TileFlags)(int)tileData[index]);
				}
			}

			string scene = (string)initData["scenario"]["clientLevel"];

			JsonData spawnZonesData = arenaData["spawns"];
			List<SpawnZone> spawnZones = new List<SpawnZone>();
			for(int zoneIndex = 0; zoneIndex < spawnZonesData.Count; ++zoneIndex) {
				JsonData zoneData = spawnZonesData[zoneIndex];
				int teamId = zoneData["teamId"].AsInt();

				List<GridVector> spawnPoints = new List<GridVector>();
				List<GridFacing> spawnFacings = new List<GridFacing>();
				for(int pointIndex = 0; pointIndex < zoneData["spawnPoints"].Count; ++pointIndex) {
					int pointX = zoneData["spawnPoints"][pointIndex]["x"].AsInt();
					int pointY = zoneData["spawnPoints"][pointIndex]["y"].AsInt();
					spawnPoints.Add(new GridVector(pointX, pointY));
					spawnFacings.Add((GridFacing)System.Enum.Parse(typeof(GridFacing), zoneData["spawnPoints"][pointIndex]["facing"].ToString(), true));
				}

				spawnZones.Add(new SpawnZone(teamId, spawnPoints.ToArray(), spawnFacings.ToArray()));
			}

			Map map = new Map(mapWidth, mapHeight, tiles, scene, spawnZones);

			// Intiailize the teams in the battle.
			JsonData loadoutsData = initData["loadouts"];
			int numTeams = loadoutsData.Count;
			Team[] teams = new Team[numTeams];
			Loadout[] loadouts = new Loadout[numTeams];

			for(int loadoutIndex = 0; loadoutIndex < loadoutsData.Count; ++loadoutIndex) {
				JsonData loadoutData = loadoutsData[loadoutIndex];
				List<LoadoutSlot> loadoutSlots = new List<LoadoutSlot>();
				for(int slotIndex = 0; slotIndex < loadoutData["heroes"].Count; ++slotIndex) {
					JsonData slotData = loadoutData["heroes"][slotIndex];
					string heroId = (string)slotData["heroId"];
					int spawnSlot = (int)slotData["spawnSlot"];
					LoadoutSlot slot = new LoadoutSlot(heroId, spawnSlot);
					loadoutSlots.Add(slot);
				}
				loadouts[loadoutIndex] = new Loadout(loadoutSlots);
				teams[loadoutIndex] = new Team((int)loadoutData["teamId"], 0, loadouts[loadoutIndex], BattleMechanics.Objectives.ObjectiveType.DefeatEnemies);
			}

			// Initialize the battle.
			battleState = new BattleState(map, teams);
		}

		private IEnumerator DoDemo() {
			targetSolver = new TargetSolver(battleState);

			yield return StartCoroutine(level.LevelIntro());
			while(true) {
				solverSourceXMin = Mathf.Min(Mathf.Max(0, solverSourceXMin), battleState.Map.GridWidth - 1);
				solverSourceXMax = Mathf.Min(Mathf.Max(solverSourceXMin, solverSourceXMax), battleState.Map.GridWidth - 1);
				solverSourceYMin = Mathf.Min(Mathf.Max(0, solverSourceYMin), battleState.Map.GridHeight - 1);
				solverSourceYMax = Mathf.Min(Mathf.Max(solverSourceYMin, solverSourceYMax), battleState.Map.GridHeight - 1);

				solverMinRangeMin = Mathf.Max(0, solverMinRangeMin);
				solverMinRangeMax = Mathf.Max(0, solverMinRangeMax);
				solverAdditionalRangeMin = Mathf.Max(0, solverAdditionalRangeMin);
				solverAdditionalRangeMax = Mathf.Max(0, solverAdditionalRangeMax);

				int sourceX = Random.Range(solverSourceXMin, solverSourceXMax);
				int sourceY = Random.Range(solverSourceYMin, solverSourceYMax);
				GridFacing facing = (GridFacing)Random.Range(0, Enums.Length<GridFacing>());
				int minRange = Random.Range(solverMinRangeMin, solverMinRangeMax + 1);
				int maxRange = Random.Range(solverAdditionalRangeMin, solverAdditionalRangeMax + 1) + minRange;

				targetSolver.Reset();
				targetSolver.SetSource(new GridVector(sourceX, sourceY), facing, 0);
				targetSolver.SetRange(minRange, maxRange);
				if(usePathing) {
					targetSolver.UsePathing(TargetSolverBlockFlags.Everything);
				}
				if(useLineOfSight) {
					targetSolver.UseLineOfSight(TargetSolverBlockFlags.TileVision);
				}
				for(int y = 0; y < battleState.Map.GridHeight; ++y) {
					for(int x = 0; x < battleState.Map.GridWidth; ++x) {
						level.ShowHighlight(targetSolver.GetResultAtPosition(new GridVector(x, y)), false);
						if(x == sourceX && y == sourceY) {
							level.ShowSourceHighlight(x, y);
						}
					}
				}
				yield return new WaitForSeconds(2.5f);
				level.ClearHighlights();
				yield return new WaitForSeconds(0.1f);
			}
		}*/

	}

}
