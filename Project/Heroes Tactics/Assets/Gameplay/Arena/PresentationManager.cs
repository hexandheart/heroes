﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleMechanics;
using BattleMechanics.Breezes;
using BattleMechanics.Events;
using BattleMechanics.Events.Data;
using BattleMechanics.Requests;
using Arena.GridLevel;
using Arena.Photon;
using Events;
using BattleMechanicsUtils;

namespace Arena {

	public class PresentationManager : MonoBehaviour, IEventListener {

		public delegate IEnumerator BattleEventHandler(IBattleEvent battleEvent);

		public static PresentationManager Instance { get { return instance; } }

		/// <summary>
		/// Pushes a presentation event into the processing queue.
		/// </summary>
		/// <param name="e"></param>
		public static void PushEvent(IBattleEvent e) {
			instance.eventsToProcess.Enqueue(e);
		}

		public static void BeginBattle() {
			// We are currently in the loading screen. We do want to begin the battle sequence, but
			// we also want to reset our camera and stuff BEFORE we hide the loading screen.
			instance.arenaCamera.ResetFocus();

			instance.StartCoroutine(instance.BeginBattleSequence());
		}

		public static void RegisterHandler(BattleEventType eventType, BattleEventHandler handler) {
			// Make sure the handler list exists first.
			if(!instance.battleEventHandlers.ContainsKey(eventType)) {
				instance.battleEventHandlers.Add(eventType, new List<BattleEventHandler>());
			}

			// We shouldn't add duplicates!
			if(instance.battleEventHandlers[eventType].Contains(handler)) {
				Debug.LogWarningFormat(instance, "[PresentationManager]: Attempting to register duplicate event handler for {0}.", eventType);
				return;
			}

			instance.battleEventHandlers[eventType].Add(handler);
		}

		public static void UnregisterHandler(BattleEventType eventType, BattleEventHandler handler) {
			if(instance == null) {
				return;
			}

			// If the list doesn't exist, the handler isn't registered.
			if(!instance.battleEventHandlers.ContainsKey(eventType)) {
				return;
			}

			// Make sure the list doesn't contain the handler.
			instance.battleEventHandlers[eventType].Remove(handler);
		}

		public static void LoadPawnsForState(BattleState battleState, Action<bool> loadedCallback) {
			instance.StartCoroutine(instance.LoadPawnsForStateInternal(battleState, loadedCallback));
		}

		public static IEnumerator LoadPawnForActor(Actor actor, Action<Pawn> loadedCallback) {
			Pawn ret = null;
			if(!instance.pawns.ContainsKey(actor.ID)) {
				bool done = false;
				PawnLoader.LoadPawn(actor,
					(pawn) => {
						done = true;
						instance.pawns[actor.ID] = pawn;
						ret = pawn;
					},
					(reasonCode, message) => {
						Debug.LogError("[PresentationManager]: " + message);
						done = true;
						ret = null;
					});
				
				while(!done) {
					yield return null;
				}
			}

			loadedCallback?.Invoke(ret);
		}

		public static Pawn GetPawnForActor(Actor actor) {
			if(!instance.pawns.ContainsKey(actor.ID)) {
				return null;
			}
			return instance.pawns[actor.ID];
		}
		
		public static Level Level {
			get {
				return instance.level;
			}
		}

		public static Color GetTeamColor(int teamId) {
			if(teamId < 0 || teamId >= instance.teamColors.Length) {
				return Color.magenta;
			}
			return instance.teamColors[teamId];
		}

		/*******************************************************************************/
		#region Event listeners

		public void AddListeners() {
			EventManager.AddListener<GameStateEvent>(OnGameState);
			EventManager.AddListener<PawnClickedEvent>(OnPawnClicked);
			EventManager.AddListener<TileHoveredEvent>(OnTileHovered);
			EventManager.AddListener<TileClickedEvent>(OnTileClicked);
			EventManager.AddListener<ActionClickedEvent>(OnActionClicked);
		}

		public void RemoveListeners() {
			EventManager.RemoveListener<GameStateEvent>(OnGameState);
			EventManager.RemoveListener<PawnClickedEvent>(OnPawnClicked);
			EventManager.RemoveListener<TileHoveredEvent>(OnTileHovered);
			EventManager.RemoveListener<TileClickedEvent>(OnTileClicked);
			EventManager.RemoveListener<ActionClickedEvent>(OnActionClicked);
		}

		/// <summary>
		/// If we're exiting a state, we need to clean ourselves up.
		/// </summary>
		/// <param name="evt"></param>
		private void OnGameState(IEvent evt) {
			GameStateEvent gse = evt as GameStateEvent;
			if(gse.TransitionState == TransitionState.Exiting) {
				GameStateManager.RegisterTransitionCoroutine(CleanUp());
			}
		}

		private IEnumerator CleanUp() {
			StopAllCoroutines();
			eventsToProcess.Clear();

			// Level is never null!
			yield return StartCoroutine(level.CleanUp());

			if(pawns != null) {
				foreach(var kv in pawns) {
					kv.Value.gameObject.SetActive(false);
					Destroy(kv.Value.gameObject);
				}
				pawns = null;
			}
			
			eventProcessingCoroutine = StartCoroutine(ProcessEvents());
		}
		
		private void OnPawnClicked(IEvent evt) {
			// If it's not our turn, clicking does nothing.
			if(!BattleController.BattleState.CurrentTeam.IsLocalTeam()) {
				return;
			}

			PawnClickedEvent pce = evt as PawnClickedEvent;

			// Hey it's our turn! Let's select that actor!
			BattleController.GetLocalTeam().ActorSelectionBreeze.Select(pce.Pawn.Actor.ID);
		}

		private void OnTileHovered(IEvent evt) {
			// If it's not our turn, hovering does nothing.
			if(!BattleController.BattleState.CurrentTeam.IsLocalTeam()) {
				return;
			}

			// So the theory here is that we can update our highlighted tiles based on the hovered tile
			// being the "theoretical target".
			TileHoveredEvent the = evt as TileHoveredEvent;
			if(the.IsHovering) {
				UpdateSolverHighlights(the.Position);
				currentTargetPreview = the.Position;
			}
			else {
				ClearSolverPreview(the.Position);
				if(currentTargetPreview.HasValue && currentTargetPreview.Value == the.Position) {
					currentTargetPreview = null;
				}
				else if(currentTargetPreview.HasValue) {
					UpdateSolverHighlights(currentTargetPreview.Value);
				}
			}
		}

		private void OnTileClicked(IEvent evt) {
			// If it's not our turn, clicking does nothing.
			if(!BattleController.BattleState.CurrentTeam.IsLocalTeam()) {
				return;
			}

			// Theoretically, the only thing we're able to click on is a valid target...
			Team team = BattleController.GetLocalTeam();

			int actorId = team.ActorSelectionBreeze.ActorID;
			Actor actor = BattleController.BattleState.GetActorByID(actorId);
			BattleActionGroupType actionGroup = team.TargetingBreeze.ActionGroup;
			int actionSlot = team.TargetingBreeze.ActionSlot;

			BattleAction action = actor.GetAction(actionGroup, actionSlot);
			if(action == null) {
				return;
			}

			GridVector position = (evt as TileClickedEvent).Position;

			audioSource.PlayOneShot(selectTargetClip);
			BattleController.RaiseRequest(new PerformActionRequest(
				actorId, actionGroup, actionSlot,
				position, (position - actor.Position).GetFacing()));
		}

		private void OnActionClicked(IEvent evt) {
			// If it's not our turn, clicking does nothing.
			if(!BattleController.BattleState.CurrentTeam.IsLocalTeam()) {
				return;
			}

			// Show targeting!
			audioSource.PlayOneShot(selectTargetClip);
			ActionClickedEvent ace = evt as ActionClickedEvent;
			Team team = BattleController.GetLocalTeam();
			team.TargetingBreeze.ShowTargeting(ace.Actor.ID, ace.Action);
		}

		#endregion

		/*****************************************************************************************/

		#region Privates

		private IEnumerator LoadPawnsForStateInternal(BattleState battleState, Action<bool> loadedCallback) {
			bool errorOccurred = false;

			Dictionary<int, Pawn> pawns = new Dictionary<int, Pawn>();

			yield return null;

			this.pawns = pawns;

			loadedCallback?.Invoke(errorOccurred != true);
		}

		private IEnumerator BeginBattleSequence() {
			processingPaused = true;

			// Arbitrary time for loading screen to fade out.
			//yield return new WaitForSeconds(0.5f);

			// Play level intro!
			// Show the level intro!
			audioSource.PlayOneShot(battleStartClip);
			yield return StartCoroutine(level.LevelIntro());

			// Show the spawn animations for the initial pawns.
			List<Actor> actors = BattleController.BattleState.GetAllActors();

			// Theoretically have these sorted to show the teams in sequence. For now, show them all at once.
			int numLoads = 0;
			foreach(Actor actor in actors) {
				++numLoads;
				StartCoroutine(LoadPawnForActor(
					actor,
					(pawn) => {
						--numLoads;
					}));
			}
			
			while(numLoads > 0) {
				yield return null;
			}

			float delay = 0.2f;
			foreach(var pawn in pawns.Values) {
				StartCoroutine(pawn.SpawnSequence(BattleController.BattleState, pawnStepClip));
				yield return new WaitForSeconds(delay);
			}
			yield return new WaitForSeconds(Pawn.AnimDuration);

			processingPaused = false;
		}

		#region Battle event processing

		private void RegisterHandlers() {
			battleEventHandlers = new Dictionary<BattleEventType, List<BattleEventHandler>>();

			RegisterHandler(BattleEventType.SpawnActor, OnSpawnActorEvent);
			RegisterHandler(BattleEventType.SpawnMultipleActors, OnSpawnMultipleActorsEvent);
			RegisterHandler(BattleEventType.ActionPerformed, OnActionPerformedEvent);
			RegisterHandler(BattleEventType.BreezeUpdate, OnBreezeUpdateEvent);
			RegisterHandler(BattleEventType.TurnStart, OnTurnStartEvent);
			RegisterHandler(BattleEventType.BattleEnded, OnBattleEndedEvent);
		}

		private IEnumerator ProcessEvents() {
			while(true) {
				if(eventsToProcess.Count == 0 || processingPaused) {
					yield return null;
					continue;
				}

				IBattleEvent e = eventsToProcess.Dequeue();
				// Let the battle controller handle it first to get the underlying battle state up to date.
				BattleController.HandleEvent(e);
				if(battleEventHandlers.ContainsKey(e.EventType)) {
					// Allow visual handlers to handle it!
					CoroutineJoin join = new CoroutineJoin(this);
					foreach(BattleEventHandler handler in battleEventHandlers[e.EventType]) {
						join.Add(handler(e));
					}
					yield return join.WaitForAll();
				}
				yield return null;
			}
		}

		private IEnumerator OnSpawnMultipleActorsEvent(IBattleEvent battleEvent) {
			SpawnMultipleActorsEvent evt = battleEvent as SpawnMultipleActorsEvent;

			float spawnDelay = 0.2f;
			for(int i = 0; i < evt.NumSpawns; ++i) {
				// Purposely do not yield to start coroutines in parallel.
				StartCoroutine(OnSpawnActorEvent(evt.GetSpawn(i)));
				yield return new WaitForSeconds(spawnDelay);
			}

			yield return new WaitForSeconds(Pawn.AnimDuration); 
		}

		private IEnumerator OnSpawnActorEvent(IBattleEvent battleEvent) {
			SpawnActorEvent evt = battleEvent as SpawnActorEvent;

			bool loading = true;
			float waitTime = 0f;

			StartCoroutine(LoadPawnForActor(
				BattleController.BattleState.GetActorByID(evt.ActorID),
					(pawn) => {
					pawn.Hud.SetValues(
						pawn.Actor.CurrentEntity.LocID,
						pawn.Actor.CurrentEntity.Stats.Power,
						pawn.Actor.CurrentEntity.Stats.Health,
						pawn.Actor.CurrentEntity.Stats.MaxHealth,
						pawn.Actor.CurrentEntity.Stats.Armor);
					StartCoroutine(pawn.SpawnSequence(BattleController.BattleState, pawnStepClip));
					waitTime = Pawn.AnimDuration;
					loading = false;
				}));

			while(loading) {
				yield return null;
			}

			while(waitTime > 0f) {
				waitTime -= Time.deltaTime;
				yield return null;
			}
		}

		private IEnumerator OnActionPerformedEvent(IBattleEvent battleEvent) {
			ActionPerformedEvent evt = battleEvent as ActionPerformedEvent;
			
			yield return StartCoroutine(DoPawnMovement(evt));
			yield return StartCoroutine(DoActionResults(evt));
			yield return StartCoroutine(DoSpawnSummons(evt));

			BattleController.BattleState.PurgeDeadActors();

			yield break;
		}

		private IEnumerator DoPawnMovement(ActionPerformedEvent evt) {
			Actor actor = BattleController.BattleState.GetActorByID(evt.ActorID);
			BattleAction action = actor.GetAction(evt.ActionGroup, evt.ActionSlot);
			Pawn pawn = GetPawnForActor(actor);

			Vector3 source = evt.Source.ToVector3();
			Vector3 target = evt.Target.ToVector3();
			if(action.MovementEffect == null) {
				target = source;
			}

			Quaternion sourceRot = evt.SourceFacing.ToRotation();
			Quaternion targetRot = evt.TargetFacing.ToRotation();

			if(action.MovementEffect != null) {
				pawn.PlayAudio(pawnPickupClip);
			}

			float maxHeight = 2f;
			float duration = 0.5f;
			float time = 0;
			while(time <= duration) {
				float percent = time / duration;

				float currentHeight = Mathf.Sin(percent * (float)Math.PI) * maxHeight;
				if(action.MovementEffect == null) {
					currentHeight = 0;
				}

				Vector3 current = Vector3.Lerp(source, target, percent);
				current.y = currentHeight;
				Quaternion currentRot = Quaternion.Lerp(sourceRot, targetRot, (1f - (1f - percent) * (1f -percent)));

				pawn.transform.localPosition = current;
				pawn.transform.localRotation = currentRot;

				time += Time.deltaTime;
				yield return null;
			}

			if(action.MovementEffect != null) {
				pawn.PlayAudio(pawnStepClip);
			}

			pawn.transform.localPosition = target;
			pawn.transform.localRotation = targetRot;
		}

		private IEnumerator DoActionResults(ActionPerformedEvent evt) {
			if(evt.Results.Length > 0) {
				Actor sourceActor = BattleController.BattleState.GetActorByID(evt.ActorID);
				BattleAction sourceAction = sourceActor.GetAction(evt.ActionGroup, evt.ActionSlot);

				Pawn sourcePawn = GetPawnForActor(sourceActor);
				sourcePawn.Hud.ShowAction(sourceAction.LocID);

				sourcePawn.PlayAudio(abilityClip);

				yield return new WaitForSeconds(0.2f);
			}

			foreach(var result in evt.Results) {
				Actor targetActor = BattleController.BattleState.GetActorAtPosition(result.Target);
				if(targetActor == null) {
					Debug.LogFormat(this, "[PresentationManager]: Couldn't find actor at {0}", result.Target);
					continue;
				}
				Pawn pawn = GetPawnForActor(targetActor);

				if(result.Type == ActionResultType.Dodge) {
					pawn.Hud.ShowDodge();
					continue;
				}
				else if(result.Type != ActionResultType.Hit) {
					continue;
				}
				
				pawn.Hud.SetValues(
					targetActor.CurrentEntity.LocID,
					targetActor.CurrentEntity.Stats.Power,
					targetActor.CurrentEntity.Stats.Health,
					targetActor.CurrentEntity.Stats.MaxHealth,
					targetActor.CurrentEntity.Stats.Armor);
				
				if(result.HealthChange > 0) {
					pawn.PlayAudio(healClip);
					pawn.Hud.ShowHeal(result.HealthChange);
				}
				else {
					if(targetActor.IsAlive) {
						pawn.PlayAudio(hurtClip);
					}
					else {
						pawn.PlayAudio(deathClip);
					}
					pawn.Hud.ShowHit(result.HealthChange, targetActor.CurrentEntity.Stats.Armor);
				}
				yield return new WaitForSeconds(0.2f);

				if(!targetActor.IsAlive) {
					pawn.gameObject.SetActive(false);
				}
			}

			if(evt.Results.Length > 0) {
				yield return new WaitForSeconds(0.5f);
			}
		}

		private IEnumerator DoSpawnSummons(ActionPerformedEvent evt) {
			foreach(var result in evt.Results) {
				if(result.Type != ActionResultType.Summon) {
					continue;
				}

				SpawnActorEvent spawnEvent = SpawnActorEvent.Deserialize(result.Data);
				yield return StartCoroutine(OnSpawnActorEvent(spawnEvent));
			}
		}

		private IEnumerator OnBreezeUpdateEvent(IBattleEvent battleEvent) {
			BreezeUpdateEvent evt = battleEvent as BreezeUpdateEvent;

			Team team = BattleController.BattleState.GetTeam(evt.TeamID);
			if(evt.BreezeType == BreezeType.ActorSelection) {
				ActorSelectionBreeze breeze = team.ActorSelectionBreeze;

				if(breeze.ActorID != 0) {
					audioSource.PlayOneShot(selectActorClip);
				}
				EventManager.Queue(new HeroSelectedEvent(breeze.ActorID, breeze.Team.TeamID));
			}
			else if(evt.BreezeType == BreezeType.Targeting) {
				TargetingBreeze breeze = team.TargetingBreeze;
				// Execution intended to never come back.
				yield return OnTargetingBreezeUpdate(breeze);
			}

			yield break;
		}

		private IEnumerator OnTargetingBreezeUpdate(TargetingBreeze breeze) {
			Actor actor = BattleController.BattleState.GetActorByID(breeze.SourceActorID);
			BattleAction action = actor?.GetAction(breeze.ActionGroup, breeze.ActionSlot);
			if(actor == null || action == null) {
				ClearSolver();
				yield break;
			}

			// Different from our current actor/action?
			if(currentActor != actor ||
				currentAction != action) {
				currentActor = actor;
				currentAction = action;
				// What kind of targeting are we looking at here?
				currentSolver = new TargetSolver(BattleController.BattleState);
				currentSolver.PrepareForAction(currentActor, currentAction);
				currentSolver.CalculateResults();

				UpdateSolverHighlights(null);
			}

			yield break;
		}

		private void ClearSolver() {
			level.ClearHighlights();
			currentSolver = null;
			currentActor = null;
			currentAction = null;

			// Clear outlines!
			OutlineEffectManager.EnableTeamColors(false);
		}

		private void UpdateSolverHighlights(GridVector? previewTarget) {
			if(currentSolver == null) {
				return;
			}

			// Set up outlines!
			OutlineEffectManager.EnableTeamColors(true);
			foreach(var actor in BattleController.BattleState.GetAllActors()) {
				GetPawnForActor(actor).SetOutlineColorIndex(OutlineEffectManager.TeamColor(actor.TeamID));
			}

			if(!previewTarget.HasValue) {
				for(int y = 0; y < BattleController.BattleState.Map.GridHeight; ++y) {
					for(int x = 0; x < BattleController.BattleState.Map.GridWidth; ++x) {
						GridVector target = new GridVector(x, y);
						TargetSolverResult result = currentSolver.GetResultAtPosition(target);
						bool isInteractible = (currentActor.TeamID == BattleController.GetLocalTeam().TeamID) && result.ResultType == TargetSolverResultType.Valid;
						level.ShowHighlight(result, isInteractible);
					}
				}
			}
			else {
				TargetSolverResult result = currentSolver.GetResultAtPosition(previewTarget.Value);
				foreach(var tile in result.AffectedTiles) {
					if(currentAction.MovementEffect != null) {
						level.ShowActionTargetForMovement(tile);
					}
					if(currentAction.HealthEffect != null) {
						level.ShowActionTarget(tile, currentAction.HealthEffect.PotencyType);
					}
				}
			}

			level.ShowSourceHighlight(currentActor.Position.GridX, currentActor.Position.GridY);
		}

		private void ClearSolverPreview(GridVector previewTarget) {
			if(currentSolver == null) {
				return;
			}

			TargetSolverResult result = currentSolver.GetResultAtPosition(previewTarget);
			foreach(var tile in result.AffectedTiles) {
				level.ClearActionTarget(tile);
			}
		}

		private IEnumerator OnTurnStartEvent(IBattleEvent battleEvent) {
			TurnStartEvent evt = battleEvent as TurnStartEvent;

			if(evt.TeamID == BattleController.GetLocalTeam().TeamID) {
				audioSource.PlayOneShot(localTurnStartClip);
			}
			else {
				audioSource.PlayOneShot(enemyTurnStartClip);
			}

			yield break;
		}

		private IEnumerator OnBattleEndedEvent(IBattleEvent battleEvent) {
			BattleEndedEvent evt = battleEvent as BattleEndedEvent;

			if(evt.WinnerTeamID == BattleController.GetLocalTeam().TeamID) {
				audioSource.PlayOneShot(victoryClip);
			}
			else {
				audioSource.PlayOneShot(defeatClip);
			}

			yield break;
		}

		#endregion

		#region MonoBehaviour overrides
		/// <summary>
		/// <para>Initialize the singleton and start listening for game events.</para>
		/// </summary>
		private void Awake() {
			instance = this;

			audioSource = GetComponent<AudioSource>();

			AddListeners();
			RegisterHandlers();

			eventProcessingCoroutine = StartCoroutine(ProcessEvents());
		}

		/// <summary>
		/// <para>Unhook the singleton and stop listening for game events.</para>
		/// </summary>
		private void OnDestroy() {
			StopAllCoroutines();
			RemoveListeners();
			instance = null;
		}

		/// <summary>
		/// You know what it is!
		/// </summary>
		private void Update() {
			if(Input.GetKeyDown(KeyCode.Escape)) {
				if(currentActor != null && currentActor.Team.IsLocalTeam() &&
					currentAction != null) {
					currentActor.Team.TargetingBreeze.ClearTargeting();
				}
			}
		}

		/*****************************************************************************************/

		private static PresentationManager instance;

		private bool processingPaused = true;

		private Dictionary<int, Pawn> pawns;
		private Queue<IBattleEvent> eventsToProcess = new Queue<IBattleEvent>();
		private Dictionary<BattleEventType, List<BattleEventHandler>> battleEventHandlers;
		private Coroutine eventProcessingCoroutine;

		private TargetSolver currentSolver;
		private Actor currentActor;
		private BattleAction currentAction;
		private GridVector? currentTargetPreview;

		private AudioSource audioSource;

		[SerializeField]
		private Level level;

		[SerializeField]
		private Color[] teamColors;

		[SerializeField]
		private ArenaCamera arenaCamera;

		[SerializeField]
		private AudioClip pawnPickupClip;
		[SerializeField]
		private AudioClip pawnStepClip;
		[SerializeField]
		private AudioClip abilityClip;
		[SerializeField]
		private AudioClip hurtClip;
		[SerializeField]
		private AudioClip healClip;
		[SerializeField]
		private AudioClip deathClip;
		[SerializeField]
		private AudioClip battleStartClip;
		[SerializeField]
		private AudioClip victoryClip;
		[SerializeField]
		private AudioClip defeatClip;
		[SerializeField]
		private AudioClip localTurnStartClip;
		[SerializeField]
		private AudioClip enemyTurnStartClip;
		[SerializeField]
		private AudioClip selectActorClip;
		[SerializeField]
		private AudioClip selectTargetClip;

		#endregion

		#endregion
	}

}
