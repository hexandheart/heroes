﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using BattleMechanics;
using BattleMechanics.Events;

namespace Arena.Photon {

	public class HeroNetworkManager : MonoBehaviour, IEventListener {

		public static readonly string Version = "0.1";

		public static bool IsMatchmaking {
			get { return instance.isMatchmaking; }
		}

		public static string ProfileID { get; set; }

		/***********************************************************************************************/
		#region Event listeners

		public void AddListeners() {
			battleEventHandlers = new Dictionary<BattleEventType, Action<IDictionary>>() {
				{ BattleEventType.GameClosed, OnEvent_GameClosed },
				{ BattleEventType.PrepareBattle, OnEvent_PrepareBattle },
			};
			EventManager.AddListener<FindMatchRequest>(OnFindMatchRequest);
			EventManager.AddListener<LeaveBattleRequest>(OnLeaveBattleRequest);
			PhotonNetwork.OnEventCall += OnEvent;
		}

		public void RemoveListeners() {
			EventManager.RemoveListener<FindMatchRequest>(OnFindMatchRequest);
			EventManager.RemoveListener<LeaveBattleRequest>(OnLeaveBattleRequest);
			PhotonNetwork.OnEventCall -= OnEvent;
		}

		private void OnFindMatchRequest(IEvent evt) {
			FindMatchRequest fmr = evt as FindMatchRequest;
			StartCoroutine(DoMatchmaking(fmr.LoadoutID, fmr.MatchType, fmr.ServerOverride));
		}

		private void OnLeaveBattleRequest(IEvent evt) {
			Disconnect();
			GameStateManager.RequestState(GameState.Hub);
		}

		#endregion

		/***********************************************************************************************/
		#region Matchmaking

		private IEnumerator DoMatchmaking(string loadoutId, MatchType matchType, FindMatchRequest.ServerSettings? serverOverride) {
			this.loadoutId = loadoutId;
			this.matchType = matchType;

			isMatchmaking = true;

			EventManager.Queue(new MatchmakingStartedEvent());
			yield return new WaitForSeconds(0.5f);

			//PhotonNetwork.EnableLobbyStatistics = true;
			//PhotonNetwork.logLevel = PhotonLogLevel.Informational;
			if(!serverOverride.HasValue) {
				PhotonNetwork.ConnectUsingSettings(Version);
			}
			else {
				PhotonNetwork.ConnectToMaster(serverOverride.Value.address, serverOverride.Value.port, serverOverride.Value.appId, serverOverride.Value.version);
			}
		}

		private void CancelMatchmaking() {
			if(!isMatchmaking) {
				Debug.LogWarning("[HeroNetoworkManager]: CancelMatchmaking triggered, but matchmaking isn't active.", this);
				return;
			}
			
			isMatchmaking = false;
			EventManager.Queue(new MatchmakingStoppedEvent());
		}

		private void Disconnect() {
			if(isMatchmaking) {
				CancelMatchmaking();
			}
			if(PhotonNetwork.connectionState == ConnectionState.Connected) {
				PhotonNetwork.Disconnect();
			}
			BattleController.DestroyInstance();
		}

		#endregion

		/***********************************************************************************************/
		#region Photon callbacks

		private string GetLobbyName() {
			string lobbyName = matchType.ToString();
			string gameId = BrainCloudWrapper.GetBC().GameId;
			if(gameId != "11605") {
				lobbyName += "|" + gameId;
			}
			return lobbyName;
		}

		private void OnConnectedToMaster() {
			PhotonNetwork.SetPlayerCustomProperties(new ExitGames.Client.Photon.Hashtable() {
				{ "ProfileID", ProfileID },
				{ "LoadoutID", loadoutId },
				{ "ReadyState", ReadyState.PreGame },
			});
			// Join lobby based on match type.
			PhotonNetwork.JoinLobby(new TypedLobby(GetLobbyName(), LobbyType.Default));
		}

		private void OnFailedToConnectToPhoton(DisconnectCause cause) {
			// Network issues or invalid AppId.
			Disconnect();
			ShowError(TM._("CONNECTION ERROR"), string.Format(TM._("Couldn't connect to game server.\n\n({0})"), cause.ToString()));
		}

		private void OnConnectionFail(DisconnectCause cause) {
			// Invalid region (DisconnectCause.InvalidRegion).
			// CCU limit reached (DisconnectCause.MaxCcuReached). (Also calls OnPhotonMaxCccuReached())

			Disconnect();
			ShowError(TM._("CONNECTION ERROR"), string.Format(TM._("Couldn't connect to game server.\n\n({0})"), cause.ToString()));
		}

		private bool triedToJoin = false;
		private void OnJoinedLobby() {
			triedToJoin = false;
		}

		private void OnReceivedRoomListUpdate() {
			if(triedToJoin) {
				return;
			}
			// We're in a lobby, try to join a room!
			triedToJoin = true;
			PhotonNetwork.JoinRandomRoom();
		}

		private void OnPhotonRandomJoinFailed(object[] codeAndMsg) {
			triedToJoin = false;
			/*short errorCode = (short)codeAndMsg[0];
			string debugMsg = (string)codeAndMsg[1];

			Disconnect();
			ShowError(TM._("CONNECTION ERROR"), string.Format(TM._("Couldn't connect to game room.\n\n({0}: {1})"), errorCode.ToString(), debugMsg));*/

			// We failed to find a random room, so make one.
			int maxPlayers = 0;
			if(matchType == MatchType.solo) {
				maxPlayers = 1;
			}
			else if(matchType == MatchType.versus) {
				maxPlayers = 2;
			}
			PhotonNetwork.CreateRoom(
				null,
				new RoomOptions() {
					MaxPlayers = (byte)maxPlayers,
					Plugins = new string[] { "BattlePlugin" },
				},
				new TypedLobby(GetLobbyName(), LobbyType.Default));
		}

		private void OnJoinedRoom() {
		}

		private void OnEvent(byte eventCode, object content, int senderId) {
			var data = content as IDictionary;

			if(!battleEventHandlers.ContainsKey((BattleEventType)eventCode)) {
				//Debug.Log(string.Format("[HeroNetworkManager]: No handler registered for event {0} ({1})", (BattleEventType)eventCode, eventCode));
				return;
			}
			Debug.Log(string.Format("[HeroNetworkManager]: Handled battle event {0} ({1})", (BattleEventType)eventCode, eventCode));
			battleEventHandlers[(BattleEventType)eventCode].Invoke(data);
		}

		#endregion

		/***********************************************************************************************/
		#region Battle server events

		private void OnEvent_GameClosed(IDictionary data) {
			Debug.Log("Game closed event!");
			Disconnect();
		}

		private void OnEvent_PrepareBattle(IDictionary data) {
			PrepareBattleEvent evt = PrepareBattleEvent.FromData(data);

			GameStateManager.RequestState(GameState.Battle);
			GameStateManager.RegisterTransitionCoroutine(PrepareBattleLoading(evt));
		}

		private IEnumerator PrepareBattleLoading(PrepareBattleEvent evt) {
			yield return new WaitForSeconds(0.5f);
			BattleController battleController = Instantiate(battleControllerPrefab);
			yield return StartCoroutine(battleController.PrepareBattle(evt));
		}

		#endregion

		/***********************************************************************************************/
		#region MonoBehaviour overrides

		private void OnGUI() {
			GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
		}

		private void Start() {
			PhotonNetwork.autoJoinLobby = false;
			instance = this;
			AddListeners();
		}

		private void OnDestroy() {
			instance = null;
			RemoveListeners();
		}

		#endregion

		/***********************************************************************************************/
		#region Privates

		private static HeroNetworkManager instance;

		private Dictionary<BattleEventType, Action<IDictionary>> battleEventHandlers;

		// Matchmaking fields.
		private bool isMatchmaking = false;
		private string loadoutId;
		private MatchType matchType;

		[SerializeField]
		private int defaultBrainCloudApp;
		private string lobbyName;

		// Battle initialization.
		[SerializeField]
		private BattleController battleControllerPrefab;

		private void ShowError(string caption, string errorMessage) {
			Popup.ShowPopup(caption, errorMessage, Popup.Button.OK, null);
		}

		#endregion

	}

}
