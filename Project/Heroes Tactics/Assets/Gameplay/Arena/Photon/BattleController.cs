﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BattleMechanics;
using BattleMechanics.Events;
using BattleMechanics.Requests;
using BattleMechanicsUtils;
using Events;
using BattleMechanics.Breezes;

namespace Arena.Photon {

	public class BattleController : MonoBehaviour, IEventListener {

		public static BattleState BattleState { get { return instance.battleState; } }

		public static void DestroyInstance() {
			if(instance == null) {
				return;
			}
			instance.gameObject.SetActive(false);
			Destroy(instance.gameObject);
		}

		public IEnumerator PrepareBattle(PrepareBattleEvent evt) {
			battleState = evt.CreateBattle();
			//yield return SceneManager.LoadSceneAsync(battleState.Map.Scene, LoadSceneMode.Single);
			yield return new WaitForSeconds(1.0f);

			bool isDone = false;

			// Load map (if necessary?)
			yield return StartCoroutine(PresentationManager.Level.LoadBattleState(battleState));

			// Load initial pawns.
			isDone = false;
			PresentationManager.LoadPawnsForState(battleState, (error) => { isDone = true; });
			while(!isDone) {
				yield return null;
			}
			
			// Let the server know we're ready!
			PhotonNetwork.SetPlayerCustomProperties(new ExitGames.Client.Photon.Hashtable() {
				{ "ReadyState", ReadyState.Ready },
			});

			// Wait until the battle has started.
			while(battleState.BattlePhase == ReadyState.PreGame) {
				yield return null;
			}

			// Initialize our objectives.
			for(int i = 0; i < BattleState.NumTeams; ++i) {
				BattleState.GetTeamAtIndex(i).Objective.Initialize(BattleState);
			}

			// Let the PresentationManager loose!
			PresentationManager.BeginBattle();
		}

		public static void RaiseRequest(IRequest request) {
			// Requests are only raised to the server.
			PhotonNetwork.RaiseEvent((byte)request.RequestType, request.GetData(), true, RaiseEventOptions.Default);
		}

		public static void HandleEvent(IBattleEvent battleEvent) {
			Debug.Log(string.Format("[BattleController]: <color=blue>Handling battle event {0}</color>", battleEvent.ToString()));
			battleEvent.ApplyTo(BattleState);
		}

		public static Team GetLocalTeam() {
			for(int i = 0; i < instance.battleState.NumTeams; ++i) {
				Team team = instance.battleState.GetTeamAtIndex(i);
				if(team.IsLocalTeam()) {
					return team;
				}
			}
			return null;
		}

		/***********************************************************************************************/
		#region Event listeners

		public void AddListeners() {
			PhotonNetwork.OnEventCall += OnEvent;
			IBreeze.OnLocalBreezeUpdated += OnLocalBreezeUpdated;
		}

		public void RemoveListeners() {
			PhotonNetwork.OnEventCall -= OnEvent;
			IBreeze.OnLocalBreezeUpdated -= OnLocalBreezeUpdated;
		}

		private void OnEvent(byte eventCode, object content, int senderId) {
			var evt = BattleEventFactory.FromData(eventCode, (IDictionary)content);

			// Special case for breezes, because we've already handled our own. Unless it's forced.
			if(evt.EventType == BattleEventType.BreezeUpdate) {
				BreezeUpdateEvent bue = evt as BreezeUpdateEvent;
				if(bue.TeamID == GetLocalTeam().TeamID && !bue.IsForced) {
					return;
				}
			}

			if(battleState.BattlePhase == ReadyState.Ready) {
				// We store the event to handle whenever the presentation manager tells us to.
				Debug.Log(string.Format("[BattleController]: <color=#666666>Queued battle event {0}</color>", evt.ToString()));
				PresentationManager.PushEvent(evt);
			}
			else {
				// Presentation manager doesn't really do anything here, before or after the game.
				HandleEvent(evt);
			}
		}

		private void OnLocalBreezeUpdated(IBreeze breeze) {
			// Immediately handle local breezes.
			BreezeUpdateEvent evt = new BreezeUpdateEvent(battleState.Time, breeze.Team.TeamID, breeze.BreezeType, breeze.GetData(), false);

			Debug.Log(string.Format("[BattleController]: <color=#006666>Local breeze {0}</color>", evt.ToString()));
			PresentationManager.PushEvent(evt);

			RaiseRequest(new UpdateBreezeRequest(breeze));
		}

		#endregion

		/***********************************************************************************************/
		#region MonoBehaviour overrides

		private void Start() {
			instance = this;
			AddListeners();
		}

		private void OnDestroy() {
			instance = null;
			RemoveListeners();
		}

		#endregion

		/***********************************************************************************************/
		#region Privates

		private static BattleController instance;

		private BattleState battleState;

		#endregion

	}

}