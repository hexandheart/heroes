﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineJoin {

	public CoroutineJoin(MonoBehaviour owner) {
		this.owner = owner;
	}

	public void Add(IEnumerator coroutine) {
		subTasks.Add(false);
		owner.StartCoroutine(StartSubTask(subTasks.Count - 1, coroutine));
	}

	public Coroutine WaitForAll() {
		return owner.StartCoroutine(WaitForAllSubTasks());
	}

	/*******************************************************************************/
	#region Privates

	private IEnumerator StartSubTask(int index, IEnumerator coroutine) {
		yield return owner.StartCoroutine(coroutine);
		subTasks[index] = true;
	}

	private IEnumerator WaitForAllSubTasks() {
		while(true) {
			bool allAreComplete = true;
			foreach(bool isComplete in subTasks) {
				if(!isComplete) {
					allAreComplete = false;
					break;
				}
			}

			if(allAreComplete) {
				break;
			}
			else {
				yield return null;
			}
		}
	}

	private readonly MonoBehaviour owner;
	private List<bool> subTasks = new List<bool>();

	#endregion

}
