﻿using System.Collections.Generic;
using LitJson;

public class Scenario {

	public string ID { get; private set; }
	public string Arena { get; private set; }
	public ScenarioTeam[] Teams { get; private set; }
	public ScenarioTeam[] HumanTeams { get; private set; }
	public ScenarioTeam[] BotTeams { get; private set; }

	public static Scenario FromJson(JsonData data) {
		string id = "";
		if(data.Keys.Contains("id")) {
			id = (string)data["id"];
		}
		string arena = (string)data["arena"];
		ScenarioTeam[] teams = null;
		if(data.Keys.Contains("teams")) {
			teams = ScenarioTeam.FromJsonArray(data["teams"]);
		}

		return new Scenario(id, arena, teams);
	}

	private Scenario(string id, string arena, ScenarioTeam[] teams) {
		ID = id;
		Arena = arena;
		Teams = teams;

		if(teams != null) {
			HumanTeams = System.Array.FindAll(teams, t => t.Controller == ScenarioTeamController.Human);
			BotTeams = System.Array.FindAll(teams, t => t.Controller == ScenarioTeamController.Bot);
		}
	}

}

public enum ScenarioTeamController {
	Human,
	Bot,
}

public class ScenarioTeam {

	public ScenarioTeamController Controller { get; private set; }
	public int TeamID { get; private set; }
	public string[] HeroIDs { get; private set; }

	public static ScenarioTeam FromJson(JsonData data) {
		ScenarioTeamController controller = (ScenarioTeamController)System.Enum.Parse(typeof(ScenarioTeamController), (string)data["control"], true);
		int teamId = (int)data["teamId"];
		string[] botHeroIds = null;
		if(data.Keys.Contains("heroIds")) {
			botHeroIds = new string[data["heroIds"].Count];
			for(int i = 0; i < botHeroIds.Length; ++i) {
				if(data["heroIds"][i] == null) {
					botHeroIds[i] = null;
				}
				else {
					botHeroIds[i] = (string)data["heroIds"][i];
				}
			}
		}

		return new ScenarioTeam(controller, teamId, botHeroIds);
	}

	public static ScenarioTeam[] FromJsonArray(JsonData data) {
		if(!data.IsArray) {
			return new ScenarioTeam[0];
		}
		ScenarioTeam[] ret = new ScenarioTeam[data.Count];
		for(int i = 0; i < data.Count; ++i) {
			ret[i] = FromJson(data[i]);
		}
		return ret;
	}

	private ScenarioTeam(ScenarioTeamController controller, int teamId, string[] heroIds) {
		Controller = controller;
		TeamID = teamId;
		HeroIDs = heroIds;
	}

}
