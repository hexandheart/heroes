﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

public enum TransitionState {
	Entering,
	Active,
	Exiting,
}

public enum GameState {
	InitialLoading,
	Hub,
	Battle,
}

/// <summary>
/// <para>Support for transitioning between game states.</para>
/// </summary>
public class GameStateManager : MonoBehaviour {

	/// <summary>
	/// <para>The transition which is underway will not complete until the supplied coroutine
	/// has finished.</para>
	/// </summary>
	/// <param name="coroutine">Coroutine to wait for.</param>
	public static void RegisterTransitionCoroutine(IEnumerator coroutine) {
		++instance.waitCount;
		// TODO: Ideally, the coroutine should be able to signal an abort in the transition.
		instance.StartCoroutine(instance.Wrapper(coroutine));
	}

	/// <summary>
	/// <para>Request a transition to the specified state. Events will be triggered as the
	/// transition proceeds, in this order:</para>
	/// <para>1. Current State - Exiting</para>
	/// <para>2. New State - Entering</para>
	/// <para>3. New State - Active</para>
	/// <para>At any point before New State - Active, GameStateManager.RegisterTransitionCoroutine()
	/// can be called to force the transition to wait until the specified coroutine is complete.</para>
	/// </summary>
	/// <param name="newState"></param>
	public static void RequestState(GameState newState) {
		instance.StartCoroutine(instance.TransitionToState(newState));
	}

	/***********************************************************************************************/

	private static GameStateManager instance;

	[SerializeField]
	private GameState initialState = GameState.Hub;

	private GameState gameState = GameState.InitialLoading;
	private int waitCount = 0;
	private bool isTransitionQueued = false;

	/// <summary>
	/// Kick off the initial transition on app start.
	/// </summary>
	/// <returns></returns>
	private IEnumerator Start() {
		instance = this;
		yield return null;
		StartCoroutine(TransitionToState(initialState));
	}

	/// <summary>
	/// Internal wrapper for counting coroutine completion.
	/// </summary>
	/// <param name="coroutine"></param>
	/// <returns></returns>
	private IEnumerator Wrapper(IEnumerator coroutine) {
		yield return StartCoroutine(coroutine);
		OnCoroutineComplete();
	}

	/// <summary>
	/// Internal call to decrement counter on coroutine completion.
	/// </summary>
	private void OnCoroutineComplete() {
		--waitCount;
	}

	/// <summary>
	/// <para>Does the work of transitioning to a new state.</para>
	/// 
	/// <para>Automatically shows the LoadingOverlay for the duration of the transition, and
	/// automatically loads an appropriate menu once the transition is complete.</para>
	/// </summary>
	/// <param name="newState"></param>
	/// <returns></returns>
	private IEnumerator TransitionToState(GameState newState) {
		bool isSameStateTransition = (newState == gameState);
		
		if(waitCount != 0) {
			Debug.LogWarningFormat(this, "GameStateManager: Requesting transition to state {0}, but waiting on {1} coroutine(s).", newState, waitCount);
			isTransitionQueued = true;
			while(waitCount > 0) {
				yield return null;
			}
		}

		if(isTransitionQueued) {
			isTransitionQueued = false;
		}

		if(!isSameStateTransition) {
			if(!isTransitionQueued) {
				MenuManager.OpenOverlay<LoadingOverlay>();
			}

			yield return new WaitForSecondsRealtime(0.5f);

			MenuManager.OpenMenu<EmptyMenu>();
			MenuManager.CloseOverlay<NavOverlay>();
		}

		yield return StartCoroutine(WaitForTransition(gameState, TransitionState.Exiting));
		gameState = newState;
		yield return StartCoroutine(WaitForTransition(gameState, TransitionState.Entering));

		if(!isSameStateTransition && !isTransitionQueued) {
			MenuManager.CloseOverlay<LoadingOverlay>();
		}

		EventManager.Queue(new GameStateEvent(gameState, TransitionState.Active));

		// TODO: Don't do this here! This is kind of hacky, but will work for now.

		// Open game-state-appropriate menu.
		switch(gameState) {
		case GameState.Hub:
			if(BrainCloudWrapper.GetBC().Authenticated) {
				MenuManager.OpenOverlay<NavOverlay>(MainSubMenu.Play);
			}
			else {
				MenuManager.OpenMenu<AuthMenu>();
			}
			break;
		case GameState.Battle:
			MenuManager.OpenMenu<BattleMenu>();
			break;
		}
	}

	/// <summary>
	/// <para>TransitionToState() uses this coroutine to wait for registered coroutines.</para>
	/// </summary>
	/// <param name="state"></param>
	/// <param name="transition"></param>
	/// <returns></returns>
	private IEnumerator WaitForTransition(GameState state, TransitionState transition) {
		EventManager.Queue(new GameStateEvent(state, transition));

		// Give listeners a chance to react.
		yield return null;
		// Wait until provided transition coroutines are complete.
		while(waitCount > 0) {
			yield return null;
		}
	}

}
