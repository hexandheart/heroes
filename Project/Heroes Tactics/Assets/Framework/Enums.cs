﻿using System.Collections.Generic;
using System.Linq;
using LitJson;

public static class Enums {

	public static IEnumerable<T> Get<T>() {
		return System.Enum.GetValues(typeof(T)).Cast<T>();
	}

	public static System.Array AsArray<T>() {
		return System.Enum.GetValues(typeof(T));
	}

	public static int Length<T>() {
		return System.Enum.GetValues(typeof(T)).Length;
	}

	public static int AsInt(this JsonData jsonData) {
		if(jsonData.IsInt) {
			return (int)jsonData;
		}
		else if(jsonData.IsDouble) {
			return (int)((double)jsonData);
		}

		return 0;
	}

}