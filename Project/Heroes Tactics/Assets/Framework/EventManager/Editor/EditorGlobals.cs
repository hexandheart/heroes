using UnityEngine;
using UnityEditor;
using System.Collections;

public static class EditorGlobals {
	
	public static Object GetObject(string name)
	{
		int assetID = EditorPrefs.GetInt(name, -1);
		return (assetID != -1) ? EditorUtility.InstanceIDToObject(assetID) : null;
	}
		
}
