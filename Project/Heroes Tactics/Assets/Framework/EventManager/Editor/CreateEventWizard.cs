using System.IO;
using System.Threading;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Text.RegularExpressions;

namespace Events 
{	
	[InitializeOnLoad]
	public class CreateEventWizard : EditorWindow 
	{
		[MenuItem("Event Manager/Create New Event...")]
		public static void Settings()
		{
			EditorWindow.GetWindow<CreateEventWizard>(false, "New Event", true);
		}
		
		private static string EventName 
		{
			get { return EditorPrefs.GetString("EventManager ClassName", "NewEvent"); }
			set { EditorPrefs.SetString("EventManager ClassName", value); }
		}	
		
		private static bool IsCreatingEventClass 
		{
			get { return EditorPrefs.GetBool("EventManager IsCreatingScreen", false); }
			set { EditorPrefs.SetBool("EventManager IsCreatingScreen", value); }
		}
		
		private static bool IsScriptCreated 
		{
			get { return EditorPrefs.GetBool("EventManager IsScriptCreated", false); }
			set { EditorPrefs.SetBool("EventManager IsScriptCreated", value); }
		}
		
		public static TextAsset EventScriptTemplate 
		{
			get 
			{
				// TODO: Consider moving this EditorGlobals.GetObject method from Flow to something else, 
				// as its utility extends beyond Flow imo.
				// Or just have EventManager crate it's own EditorGlobals!
				TextAsset template = EditorGlobals.GetObject("EventManager EventScriptTemplate") as TextAsset;
				
				if (template == null) 
				{
					template = AssetDatabase.LoadAssetAtPath("Assets/Framework/EventManager/Template/EventScriptTemplate.txt", typeof(TextAsset)) as TextAsset;
				}
				
				return template;
			}
			set { EditorPrefs.SetInt("EventManager EventScriptTemplate", value != null ? value.GetInstanceID() : -1); }
		}
		
		static CreateEventWizard() 
		{
			EditorApplication.update += Update;
		}
		
		private void OnGUI() 
		{
			EditorGUIUtility.labelWidth = 80f;
						
			GUILayout.BeginHorizontal();
			EventName = EditorGUILayout.TextField("Name", EventName, GUILayout.Width(200f));
			GUILayout.Space(20f);
			GUILayout.Label("Name for the Event Class.\nNo spaces or special characters!");
			GUILayout.EndHorizontal();
						
			GUILayout.Space(12f);
			
			Color oldColor = GUI.color;
			Regex regex = new Regex("^[a-zA-Z][a-zA-Z0-9]*$");
			if (string.IsNullOrEmpty(EventName) ||
				!regex.IsMatch(EventName) ||
				IsCreatingEventClass) 
			{
				GUI.enabled = false;
				GUI.color = Color.red;
			}
			else 
			{
				GUI.color = Color.green;
			}
			bool create = GUILayout.Button("Create Event", GUILayout.Width(120f));
			GUI.enabled = true;
			GUI.color = oldColor;
			
			if (create) 
			{
				CreateEvent();
			}
		}
		
		private static void CreateEvent()
		{
			string scriptPath = EditorUtility.SaveFilePanelInProject("Create Event...", EventName, "cs", "Select the location to save the event class.");
			if (string.IsNullOrEmpty(scriptPath)) 
			{
				return;
			}
			
			IsCreatingEventClass = true;
			IsScriptCreated = false;

			GenerateScript(scriptPath, EventName);
		}
		
		private static void GenerateScript(string path, string name) 
		{
			if (File.Exists(path)) 
			{
				Debug.LogWarning("Script template generation failed: '" + path + "' already exists.");
				IsScriptCreated = true;
				return;
			}
			
			using (StreamWriter stream = new StreamWriter(path)) 
			{				
				string script = EventScriptTemplate.text.Replace("EVENT_NAME", name);
				stream.Write(script);
			}
			
			AssetDatabase.Refresh();
			IsScriptCreated = true;
		}
		
		private static void Update() 
		{
			if (IsCreatingEventClass)
			{
				GUI.enabled = false;
				if (!IsScriptCreated) 
				{
					EditorUtility.DisplayProgressBar("Creating New Event...", "Creating event...", 0.1f);
					return;
				}
				if (EditorApplication.isCompiling) 
				{
					EditorUtility.DisplayProgressBar("Creating New Event...", "Compiling, please wait...", 0.8f);
					return;
				}
				
				GUI.enabled = true;

				EditorUtility.ClearProgressBar();
				
				IsCreatingEventClass = false;
				IsScriptCreated = false;
			}
		}		
	}	
}
