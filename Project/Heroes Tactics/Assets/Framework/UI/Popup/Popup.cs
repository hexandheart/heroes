﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections;

public class Popup : Menu {

	private static Popup instance;

	[Flags]
	public enum Button {
		None = 0,
		OK = 1,
		Cancel = 2,
		Input = 4,
	}

	[SerializeField]
	private GameObject okButton;
	[SerializeField]
	private LabeledWidget okButtonLabel;

	[SerializeField]
	private GameObject cancelButton;
	[SerializeField]
	private LabeledWidget cancelButtonLabel;

	[SerializeField]
	private Text captionLabel;

	[SerializeField]
	private Text messageLabel;

	[SerializeField]
	private InputField inputField;

	private Button response = Button.None;
	private bool closePopup = false;
	private Action<Button> callback;
	private GameObject lastSelection = null;

	private IEnumerator currentPopup;

	public static string TextEntry {
		get {
			return instance.inputField.text;
		}
		set {
			instance.inputField.text = value;
		}
	}

	protected override void OnAwake() {
		instance = this;
	}

	public static void ShowPopup(string caption, string message, Button buttons, Action<Button> callback) {
		if(instance.currentPopup != null) {
			instance.StopCoroutine(instance.currentPopup);
			instance.currentPopup = null;
		}
		instance.currentPopup = instance.ShowCoroutine(caption, message, buttons, "", "", callback);
		instance.StartCoroutine(instance.currentPopup);
	}

	public static void ShowPopup(string caption, string message, Button buttons, string defaultText, string placeholderText, Action<Button> callback) {
		if(instance.currentPopup != null) {
			instance.StopCoroutine(instance.currentPopup);
			instance.currentPopup = null;
		}
		instance.currentPopup = instance.ShowCoroutine(caption, message, buttons, defaultText, placeholderText, callback);
		instance.StartCoroutine(instance.currentPopup);
	}

	public static void ClosePopup() {
		instance.closePopup = true;
	}

	private IEnumerator ShowCoroutine(string caption, string message, Button buttons, string defaultText, string placeholderText, Action<Button> callback) {
		captionLabel.text = caption;
		messageLabel.text = message;
		okButton.SetActive((buttons & Button.OK) != 0);
		okButtonLabel.Text = TM._("OK");
		cancelButton.SetActive((buttons & Button.Cancel) != 0);
		cancelButtonLabel.Text = TM._("CANCEL");
		inputField.text = defaultText;
		(inputField.placeholder as Text).text = placeholderText;
		response = Button.None;

		if((buttons & Button.Input) != 0) {
			messageLabel.alignment = TextAnchor.UpperCenter;
			inputField.gameObject.SetActive(true);
		}
		else {
			messageLabel.alignment = TextAnchor.MiddleCenter;
			inputField.gameObject.SetActive(false);
		}

		closePopup = false;
		lastSelection = EventSystem.current.currentSelectedGameObject;
		EventSystem.current.SetSelectedGameObject(null);
		Open();
		while(response == Button.None && !closePopup) {
			yield return true;
		}
		Close();
		//EventSystem.current.SetSelectedGameObject(null);

		if(callback != null) {
			callback(response);
		}
	}

	public void OnOkClicked() {
		this.response = Button.OK;
	}

	public void OnCancelClicked() {
		this.response = Button.Cancel;
	}

	protected override void OnTransitionInComplete() {
		base.OnTransitionInComplete();

		if(okButton.activeSelf) {
			EventSystem.current.SetSelectedGameObject(okButton);
		}
	}

	protected override void OnTransitionOutComplete() {
		base.OnTransitionOutComplete();

		EventSystem.current.SetSelectedGameObject(lastSelection);
	}

}
