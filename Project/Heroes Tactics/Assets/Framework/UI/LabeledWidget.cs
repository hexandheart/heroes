﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[SelectionBase]
[ExecuteInEditMode]
[DisallowMultipleComponent]
public class LabeledWidget : MonoBehaviour {

	[SerializeField]
	private Text[] labels = new Text[0];
	[SerializeField]
	private string text;

	public string Text {
		get {
			return text;
		}
		set {
			text = value;
		}
	}

	private string oldText;

	private void Update() {
		if(text != oldText && text != null) {
			oldText = text;
			SetLabels();
		}
	}

	private void SetLabels() {
		for(int i = 0; i < labels.Length; ++i) {
			labels[i].text = text;
		}
	}

}
