﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Menu : MonoBehaviour {

	public bool IsOpen {
		get {
			return animator.GetBool("IsOpen");
		}
		private set {
			if(value == false) {
				canvasGroup.interactable = false;
			}
			animator.SetBool("IsOpen", value);
		}
	}

	private Animator animator;
	private CanvasGroup canvasGroup;

	[SerializeField]
	protected GameObject defaultSelection;

	private void Awake() {
		animator = GetComponent<Animator>();
		canvasGroup = GetComponent<CanvasGroup>();

		if(animator == null || canvasGroup == null) {
			gameObject.SetActive(false);
			return;
		}

		RectTransform rect = GetComponent<RectTransform>();
		rect.offsetMin = Vector2.zero;
		rect.offsetMax = Vector2.zero;

		OnAwake();
	}

	protected virtual void OnAwake() {}

	private void Update() {
		if(animator.GetCurrentAnimatorStateInfo(0).IsName("Open")) {
			canvasGroup.blocksRaycasts = true;
		}
		else {
			canvasGroup.blocksRaycasts = false;
		}

		if(IsOpen) {
			OnUpdate();
		}
	}

	protected virtual void OnUpdate() {}

	public void Open(params object[] args) {
		if(!IsOpen) {
			OnShow(args);
		}
		IsOpen = true;
	}

	public void Close() {
		IsOpen = false;
	}

	protected virtual void OnShow(params object[] args) {}

	protected virtual void OnTransitionInComplete() {
		canvasGroup.interactable = true;
		if(defaultSelection != null) {
			EventSystem.current.SetSelectedGameObject(defaultSelection);
		}
	}

	protected virtual void OnTransitionOutComplete() {
		canvasGroup.interactable = false;
	}

}
