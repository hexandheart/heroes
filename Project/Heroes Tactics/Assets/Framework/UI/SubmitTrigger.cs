﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SubmitTrigger : MonoBehaviour {

	[SerializeField]
	private KeyCode triggerKey;
	[SerializeField]
	private UnityEvent onTrigger;

	private void Update() {
		if(!Input.GetKeyDown(triggerKey)) {
			return;
		}

		if(onTrigger.GetPersistentEventCount() == 0) {
			return;
		}

		if(EventSystem.current == null) {
			return;
		}

		GameObject selected = EventSystem.current.currentSelectedGameObject;
		if(selected != gameObject) {
			return;
		}

		onTrigger.Invoke();
	}

}
