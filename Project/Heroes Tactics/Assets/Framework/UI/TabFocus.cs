﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TabFocus : MonoBehaviour {

	[SerializeField]
	private Selectable onTab;

	private void Update() {
		if(!Input.GetKeyDown(KeyCode.Tab)) {
			return;
		}

		if(onTab == null) {
			return;
		}

		if(EventSystem.current == null) {
			return;
		}

		GameObject selected = EventSystem.current.currentSelectedGameObject;
		if(selected != gameObject) {
			return;
		}

		onTab.Select();
	}

}
