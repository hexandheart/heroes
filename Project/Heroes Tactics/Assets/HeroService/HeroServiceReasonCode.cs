﻿public partial class HeroService {

	public enum ReasonCode {

		NO_REASON = 0,

		// Hero reason codes
		INVALID_HERO_ID = 40000,

		// Loadout reason codes
		MAX_LOADOUTS_REACHED = 40100,
		INVALID_LOADOUT_ID = 40101,

		// Job reason codes
		INVALID_JOB_ID = 40200,

		// Scenario reason codes
		INVALID_SCENARIO_PARAMS = 40300,
		INVALID_SCENARIO_ID = 40301,

	}

	public static string GetReasonMessage(ReasonCode reasonCode) {
		switch(reasonCode) {
		case ReasonCode.NO_REASON: return TM._("NO_REASON");

		// Hero reason codes
		case ReasonCode.INVALID_HERO_ID: return TM._("INVALID_HERO_ID");

		// Loadout reason codes
		case ReasonCode.MAX_LOADOUTS_REACHED: return TM._("MAX_LOADOUTS_REACHED");
		case ReasonCode.INVALID_LOADOUT_ID: return TM._("INVALID_LOADOUT_ID");

		// Scenario reason codes
		case ReasonCode.INVALID_SCENARIO_PARAMS: return TM._("INVALID_SCENARIO_PARAMS");
		case ReasonCode.INVALID_SCENARIO_ID: return TM._("INVALID_SCENARIO_ID");
		}

		return TM._("NO_REASON");
	}

}