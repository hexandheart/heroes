﻿using UnityEngine;
using LitJson;

public class LoadoutEntity {

	public static readonly string NewLoadoutID = "NEW";
	private static readonly int Size = 7;

	public bool IsDetailed { get { return Heroes != null; } }

	public string ID { get; set; }
	public string Name { get; set; }
	public string[] HeroIDs { get; private set; }
	public HeroEntity[] Heroes { get; private set; }

	public static LoadoutEntity FromJson(JsonData data) {
		string id = "";
		if(data.Keys.Contains("name")) {
			id = (string)data["id"];
		}
		string name = "";
		if(data.Keys.Contains("name")) {
			name = (string)data["name"];
		}

		return new LoadoutEntity(id, name, new string[0]);

		// Partial loadout.
		/*if(data.Keys.Contains("heroIds")) {
			string[] heroIds = new string[data["heroIds"].Count];
			for(int i = 0; i < data["heroIds"].Count; ++i) {
				heroIds[i] = (string)data["heroIds"][i];
			}
			return new LoadoutEntity(id, name, heroIds);
		}
		// Full loadout
		else if(data.Keys.Contains("heroes")) {
			HeroEntity[] heroes = HeroEntity.FromJsonArray(data["heroes"]);
			return new LoadoutEntity(id, name, heroes);
		}

		Debug.LogError("[Loadout]: Provided data doesn't contain heroes.");
		return null;*/
	}

	public LoadoutEntity() {
		ID = NewLoadoutID;
		Name = TM._("NEW LOADOUT");
		HeroIDs = new string[Size];
		Heroes = null;
	}

	private LoadoutEntity(string id, string name, string[] heroIds) {
		ID = id;
		Name = name;
		HeroIDs = heroIds;
		Heroes = null;
	}

	private LoadoutEntity(string id, string name, HeroEntity[] heroes) {
		ID = id;
		Name = name;
		Heroes = heroes;
		HeroIDs = new string[heroes.Length];

		for(int i = 0; i < heroes.Length; ++i) {
			if(heroes[i] == null) {
				continue;
			}
			HeroIDs[i] = heroes[i].ID;
		}
	}

}
