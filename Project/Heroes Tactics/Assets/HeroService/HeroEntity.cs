﻿using LitJson;

public class HeroEntity {

	public string ID { get; private set; }
	public string AssetID { get; private set; }

	public static HeroEntity FromJson(JsonData data) {
		string id = (string)data["heroId"];
		string assetId = (string)data["assetId"];

		return new HeroEntity(id, assetId);
	}

	public static HeroEntity[] FromJsonArray(JsonData data) {
		if(!data.IsArray) {
			return new HeroEntity[0];
		}
		HeroEntity[] ret = new HeroEntity[data.Count];
		for(int i = 0; i < data.Count; ++i) {
			ret[i] = FromJson(data[i]);
		}
		return ret;
	}

	private HeroEntity(string id, string assetId) {
		ID = id;
		AssetID = assetId;
	}

}
