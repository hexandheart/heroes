﻿using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class CollectiblesEntity {

	public Dictionary<string, int> Heroes { get { return heroes; } }

	public static CollectiblesEntity FromJson(JsonData data) {
		Dictionary<string, int> heroes = ParseGroup(data["heroes"]);

		return new CollectiblesEntity(heroes);
	}

	#region Privates

	private Dictionary<string, int> heroes = new Dictionary<string, int>();

	private CollectiblesEntity(Dictionary<string, int> heroes) {
		this.heroes = heroes;
	}

	private static Dictionary<string, int> ParseGroup(JsonData data) {
		if(!data.IsArray) {
			return new Dictionary<string, int>();
		}
		Dictionary<string, int> ret = new Dictionary<string, int>(data.Count);
		for(int i = 0; i < data.Count; ++i) {
			ret[(string)data[i]["id"]] = (int)data[i]["count"];
		}
		return ret;
	}

	#endregion

}
