﻿using UnityEngine;
using BattleMechanics;

namespace BattleMechanicsUtils {

	public static class GridExtensions {

		public static Vector3 ToVector3(this GridVector gv) {
			return new Vector3(gv.GridX, 0f, -gv.GridY);
		}

		public static Quaternion ToRotation(this GridFacing facing) {
			return FacingRotations[(int)facing];
		}

		private static readonly Quaternion[] FacingRotations = new Quaternion[] {
			Quaternion.Euler(0f, 0f, 0f),	// North
			Quaternion.Euler(0f, 90f, 0f),	// East
			Quaternion.Euler(0f, 180f, 0f),	// South
			Quaternion.Euler(0f, 270f, 0f),	// West
		};

	}

	public static class BattleMechanicsExtensions {

		public static bool IsLocalTeam(this Team team) {
			return team.ClientID == PhotonNetwork.player.ID;
		}

	}

}
