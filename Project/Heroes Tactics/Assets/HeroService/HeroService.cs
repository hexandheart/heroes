﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrainCloud;
using LitJson;

public partial class HeroService : MonoBehaviour {

	private static HeroService instance;

	public delegate void ErrorHandler(int status, int reasonCode, string statusMessage);
	public delegate void HeroInfoHandler(HeroEntity[] hero);
	public delegate void CollectionHandler(CollectiblesEntity collectibles);
	public delegate void LoadoutListHandler(List<LoadoutEntity> loadouts, int maxLoadouts);
	public delegate void CreateLoadoutHandler(string loadoutId);
	public delegate void BattleDetailsHandler(Scenario scenario, List<LoadoutEntity> loadouts);
	public delegate void GeneralHandler();

	private void Start() {
		instance = this;
	}

	///////////////////////////////////////////////////////////////////////////
	#region GetHeroInfo
	private event HeroInfoHandler OnHeroInfo;
	private event ErrorHandler OnHeroInfoError;

	private Dictionary<string, HeroEntity> cachedHeroes = new Dictionary<string, HeroEntity>();

	public static HeroEntity GetCachedHeroInfo(string id) {
		if(!instance.cachedHeroes.ContainsKey(id)) {
			return null;
		}
		return instance.cachedHeroes[id];
	}

	public static void GetHeroInfo(string[] ids, HeroInfoHandler callback, ErrorHandler errorCallback) {
		instance.GetHeroInfoInternal(ids, callback, errorCallback);
	}

	private string GetHeroInfoParams(string[] ids) {
		if(ids == null || ids.Length == 0) {
			return "{}";
		}

		string ret = "{\"ids\":[";
		bool first = true;
		for(int i = 0; i < ids.Length; ++i) {
			if(first) {
				first = false;
			}
			else {
				ret += ",";
			}
			if(string.IsNullOrEmpty(ids[i])) {
				ret += "\"\"";
			}
			else {
				ret += string.Format("\"{0}\"", ids[i]);
			}
		}
		ret += "]}";
		return ret;
	}

	private void GetHeroInfoInternal(string[] ids, HeroInfoHandler callback, ErrorHandler errorCallback = null) {
		BrainCloudClient bc = BrainCloudWrapper.GetBC();
		if(!bc.IsAuthenticated()) {
			if(errorCallback != null) {
				errorCallback(400, ReasonCodes.INVALID_REQUEST, "Not signed in!");
			}
			return;
		}

		OnHeroInfo += callback;
		OnHeroInfoError += errorCallback;

		// Null ids means no additional processing. We're requesting the full list.
		if(ids == null || ids.Length == 0) {
			bc.ScriptService.RunScript("GetHeroInfo", GetHeroInfoParams(null), HeroInfoCallback, HeroInfoFailureCallback);
			return;
		}

		// If we're not actually requesting any heroes, let's just early out.
		bool containsIds = false;
		for(int i = 0; i < ids.Length; ++i) {
			if(!string.IsNullOrEmpty(ids[i])) {
				containsIds = true;
				break;
			}
		}
		if(!containsIds) {
			StartCoroutine(HeroInfoNoResults(ids.Length));
			return;
		}

		// Check to see if we've already cached these heroes.
		bool allCached = true;
		for(int i = 0; i < ids.Length; ++i) {
			if(string.IsNullOrEmpty(ids[i])) {
				continue;
			}
			if(!cachedHeroes.ContainsKey(ids[i])) {
				allCached = false;
				break;
			}
		}

		if(allCached) {
			HeroInfoCachedResults(ids);
			return;
		}

		string args = GetHeroInfoParams(ids);
		bc.ScriptService.RunScript("GetHeroInfo", args, HeroInfoCallback, HeroInfoFailureCallback);
	}

	private IEnumerator HeroInfoNoResults(int numResults) {
		yield return null;
		if(OnHeroInfo != null) {
			OnHeroInfo(new HeroEntity[numResults]);
		}
		ClearHeroInfoCallbacks();
	}

	private IEnumerator HeroInfoCachedResults(string[] ids) {
		yield return null;
		if(OnHeroInfo != null) {
			HeroEntity[] ret = new HeroEntity[ids.Length];
			for(int i = 0; i < ids.Length; ++i) {
				if(string.IsNullOrEmpty(ids[i])) {
					continue;
				}
				ret[i] = cachedHeroes[ids[i]];
			}
		}
		ClearHeroInfoCallbacks();
	}

	private void HeroInfoCallback(string jsonResponse, object cbObject) {
		JsonData response = JsonMapper.ToObject(jsonResponse)["data"]["response"];
		if((bool)response["success"] == false) {
			if(OnHeroInfoError != null) {
				OnHeroInfoError((int)response["status"], (int)response["reasonCode"], (string)response["status_message"]);
			}
			ClearHeroInfoCallbacks();
			return;
		}

		HeroEntity[] heroes = HeroEntity.FromJsonArray(response["heroes"]);
		for(int i = 0; i < heroes.Length; ++i) {
			if(heroes[i] == null) {
				continue;
			}
			cachedHeroes[heroes[i].ID] = heroes[i];
		}
		if(OnHeroInfo != null) {
			OnHeroInfo(heroes);
		}
		ClearHeroInfoCallbacks();
	}

	private void HeroInfoFailureCallback(int status, int reasonCode, string jsonError, object cbObject) {
		Debug.LogError(string.Format("Error retrieving hero info: status={0} reasonCode={1}\n{2}", status, reasonCode, jsonError));
		if(OnHeroInfoError != null) {
			OnHeroInfoError(status, reasonCode, jsonError);
		}
		ClearHeroInfoCallbacks();
	}

	private void ClearHeroInfoCallbacks() {
		OnHeroInfo = null;
		OnHeroInfoError = null;
	}
	#endregion

	///////////////////////////////////////////////////////////////////////////
	#region GetCollection
	private event CollectionHandler OnCollection;
	private event ErrorHandler OnCollectionError;

	public static void GetCollection(CollectionHandler callback, ErrorHandler errorCallback) {
		instance.GetCollectionInternal(callback, errorCallback);
	}

	private string GetCollectionParams() {
		return "";
	}

	private void GetCollectionInternal(CollectionHandler callback, ErrorHandler errorCallback = null) {
		BrainCloudClient bc = BrainCloudWrapper.GetBC();
		if(!bc.IsAuthenticated()) {
			if(errorCallback != null) {
				errorCallback(400, ReasonCodes.INVALID_REQUEST, "Not signed in!");
			}
			return;
		}

		string args = "";
		OnCollection += callback;
		OnCollectionError += errorCallback;
		bc.ScriptService.RunScript("GetCollection", args, CollectionCallback, CollectionFailureCallback);
	}

	private void CollectionCallback(string jsonResponse, object cbObject) {
		JsonData response = JsonMapper.ToObject(jsonResponse)["data"]["response"];
		if((bool)response["success"] == false) {
			if(OnCollectionError != null) {
				OnCollectionError((int)response["status"], ReasonCodes.INVALID_REQUEST, (string)response["status_message"]);
			}
			ClearCollectionCallbacks();
			return;
		}

		CollectiblesEntity collectibles = CollectiblesEntity.FromJson(response["data"]);
		if(OnCollection != null) {
			OnCollection(collectibles);
		}
		ClearCollectionCallbacks();
	}

	private void CollectionFailureCallback(int status, int reasonCode, string jsonError, object cbObject) {
		Debug.LogError(string.Format("Error retrieving collection: status={0} reasonCode={1}\n{2}", status, reasonCode, jsonError));
		if(OnCollectionError != null) {
			OnCollectionError(status, reasonCode, jsonError);
		}
		ClearCollectionCallbacks();
	}

	private void ClearCollectionCallbacks() {
		OnCollection = null;
		OnCollectionError = null;
	}
	#endregion

	///////////////////////////////////////////////////////////////////////////
	#region GetLoadoutList
	private event LoadoutListHandler OnLoadoutList;
	private event ErrorHandler OnLoadoutListError;

	public static void GetLoadoutList(LoadoutListHandler callback, ErrorHandler errorCallback) {
		instance.GetLoadoutListInternal(callback, errorCallback);
	}

	private string GetLoadoutListParams() {
		return "";
	}

	private void GetLoadoutListInternal(LoadoutListHandler callback, ErrorHandler errorCallback = null) {
		BrainCloudClient bc = BrainCloudWrapper.GetBC();
		if(!bc.IsAuthenticated()) {
			if(errorCallback != null) {
				errorCallback(400, ReasonCodes.INVALID_REQUEST, "Not signed in!");
			}
			return;
		}

		string args = "";
		OnLoadoutList += callback;
		OnLoadoutListError += errorCallback;
		bc.ScriptService.RunScript("GetLoadoutList", args, LoadoutListCallback, LoadoutListFailureCallback);
	}

	private void LoadoutListCallback(string jsonResponse, object cbObject) {
		JsonData response = JsonMapper.ToObject(jsonResponse)["data"]["response"];
		if((bool)response["success"] == false) {
			if(OnLoadoutListError != null) {
				OnLoadoutListError((int)response["status"], (int)response["reasonCode"], GetReasonMessage((ReasonCode)(int)response["reasonCode"]));
			}
			ClearLoadoutListCallbacks();
			return;
		}

		int maxLoadouts = (int)response["maxLoadouts"];
		List<LoadoutEntity> loadouts = new List<LoadoutEntity>();
		for(int i = 0; i < response["loadouts"].Count; ++i) {
			LoadoutEntity loadout = LoadoutEntity.FromJson(response["loadouts"][i]);
			loadouts.Add(loadout);
		}
		if(OnLoadoutList != null) {
			OnLoadoutList(loadouts, maxLoadouts);
		}
		ClearLoadoutListCallbacks();
	}

	private void LoadoutListFailureCallback(int status, int reasonCode, string jsonError, object cbObject) {
		Debug.LogError(string.Format("Error retrieving collection: status={0} reasonCode={1}\n{2}", status, reasonCode, jsonError));
		if(OnLoadoutListError != null) {
			OnLoadoutListError(status, reasonCode, jsonError);
		}
		ClearLoadoutListCallbacks();
	}

	private void ClearLoadoutListCallbacks() {
		OnLoadoutList = null;
		OnLoadoutListError = null;
	}
	#endregion

	///////////////////////////////////////////////////////////////////////////
	#region CreateLoadout
	private event CreateLoadoutHandler OnCreateLoadout;
	private event ErrorHandler OnCreateLoadoutError;

	public static void CreateLoadout(LoadoutEntity loadout, CreateLoadoutHandler callback, ErrorHandler errorCallback) {
		instance.CreateLoadoutInternal(loadout, callback, errorCallback);
	}

	private string GetCreateLoadoutParams(LoadoutEntity loadout) {
		string args = "{ \"name\": \"" + loadout.Name + "\", ";
		if(loadout.ID == LoadoutEntity.NewLoadoutID) {
			args += "\"heroIds\": [";
		}
		else {
			args += "\"loadoutId\": \"" + loadout.ID + "\", \"heroIds\": [";
		}

		for(int i = 0; i < loadout.HeroIDs.Length; ++i) {
			if(i > 0) {
				args += ",";
			}
			if(string.IsNullOrEmpty(loadout.HeroIDs[i])) {
				args += "\"\"";
			}
			else {
				args += string.Format("\"{0}\"", loadout.HeroIDs[i]);
			}
		}
		args += "] }";

		return args;
	}

	private void CreateLoadoutInternal(LoadoutEntity loadout, CreateLoadoutHandler callback, ErrorHandler errorCallback = null) {
		BrainCloudClient bc = BrainCloudWrapper.GetBC();
		if(!bc.IsAuthenticated()) {
			if(errorCallback != null) {
				errorCallback(400, ReasonCodes.INVALID_REQUEST, "Not signed in!");
			}
			return;
		}

		string args = GetCreateLoadoutParams(loadout);
		OnCreateLoadout += callback;
		OnCreateLoadoutError += errorCallback;
		bc.ScriptService.RunScript("CreateLoadout", args, CreateLoadoutCallback, CreateLoadoutFailureCallback);
	}

	private void CreateLoadoutCallback(string jsonResponse, object cbObject) {
		JsonData response = JsonMapper.ToObject(jsonResponse)["data"]["response"];
		if((bool)response["success"] == false) {
			if(OnCreateLoadoutError != null) {
				OnCreateLoadoutError((int)response["status"], (int)response["reasonCode"], GetReasonMessage((ReasonCode)(int)response["reasonCode"]));
			}
			ClearCreateLoadoutCallbacks();
			return;
		}

		string loadoutId = (string)response["loadoutId"];
		if(OnCreateLoadout != null) {
			OnCreateLoadout(loadoutId);
		}
		ClearCreateLoadoutCallbacks();
	}

	private void CreateLoadoutFailureCallback(int status, int reasonCode, string jsonError, object cbObject) {
		Debug.LogError(string.Format("Error retrieving collection: status={0} reasonCode={1}\n{2}", status, reasonCode, jsonError));
		if(OnCreateLoadoutError != null) {
			OnCreateLoadoutError(status, reasonCode, jsonError);
		}
		ClearCreateLoadoutCallbacks();
	}

	private void ClearCreateLoadoutCallbacks() {
		OnCreateLoadout = null;
		OnCreateLoadoutError = null;
	}
	#endregion

	///////////////////////////////////////////////////////////////////////////
	#region DeleteLoadout
	private event GeneralHandler OnDeleteLoadout;
	private event ErrorHandler OnDeleteLoadoutError;

	public static void DeleteLoadout(LoadoutEntity loadout, GeneralHandler callback, ErrorHandler errorCallback) {
		instance.DeleteLoadoutInternal(loadout, callback, errorCallback);
	}

	private string GetDeleteLoadoutParams(LoadoutEntity loadout) {
		string args = "{ \"loadoutId\": \"" + loadout.ID + "\" }";
		return args;
	}

	private void DeleteLoadoutInternal(LoadoutEntity loadout, GeneralHandler callback, ErrorHandler errorCallback = null) {
		BrainCloudClient bc = BrainCloudWrapper.GetBC();
		if(!bc.IsAuthenticated()) {
			if(errorCallback != null) {
				errorCallback(400, ReasonCodes.INVALID_REQUEST, "Not signed in!");
			}
			return;
		}

		string args = GetDeleteLoadoutParams(loadout);
		OnDeleteLoadout += callback;
		OnDeleteLoadoutError += errorCallback;
		bc.ScriptService.RunScript("DeleteLoadout", args, DeleteLoadoutCallback, DeleteLoadoutFailureCallback);
	}

	private void DeleteLoadoutCallback(string jsonResponse, object cbObject) {
		JsonData response = JsonMapper.ToObject(jsonResponse)["data"]["response"];
		if((bool)response["success"] == false) {
			if(OnDeleteLoadoutError != null) {
				OnDeleteLoadoutError((int)response["status"], (int)response["reasonCode"], GetReasonMessage((ReasonCode)(int)response["reasonCode"]));
			}
			ClearDeleteLoadoutCallbacks();
			return;
		}
		
		if(OnDeleteLoadout != null) {
			OnDeleteLoadout();
		}
		ClearDeleteLoadoutCallbacks();
	}

	private void DeleteLoadoutFailureCallback(int status, int reasonCode, string jsonError, object cbObject) {
		Debug.LogError(string.Format("Error retrieving collection: status={0} reasonCode={1}\n{2}", status, reasonCode, jsonError));
		if(OnDeleteLoadoutError != null) {
			OnDeleteLoadoutError(status, reasonCode, jsonError);
		}
		ClearDeleteLoadoutCallbacks();
	}

	private void ClearDeleteLoadoutCallbacks() {
		OnDeleteLoadout = null;
		OnDeleteLoadoutError = null;
	}
	#endregion
	
	///////////////////////////////////////////////////////////////////////////
	#region GetBattleDetails
	private event BattleDetailsHandler OnBattleDetails;
	private event ErrorHandler OnBattleDetailsError;

	public static void GetBattleDetails(string scenarioId, BattleDetailsHandler callback, ErrorHandler errorCallback) {
		instance.GetBattleDetailsInternal(scenarioId, callback, errorCallback);
	}

	private string GetBattleDetailsParams(string scenarioId) {
		/*if(Arena.Unet.TeamController.All == null ||
			Arena.Unet.TeamController.All.Count == 0) {
			Debug.LogError("[HeroService]: Can't call GetBattleDetails() unless we're already connected with a battle.", this);
			return "{}";
		}

		bool first = true;
		string data = "{ \"scenarioId\":\"" + scenarioId + "\",\"loadouts\":[";
		for(int i = 0; i < Arena.Unet.TeamController.All.Count; ++i) {
			Arena.Unet.TeamController tc = Arena.Unet.TeamController.All[i];
			
			if(tc.IsHuman) {
				data += string.Format("{0}{{\"teamId\":{1},\"owner\":\"{2}\",\"loadoutId\":\"{3}\"}}", first ? "" : ",", tc.TeamID, tc.ProfileID, tc.LoadoutID);
			}

			first = false;
		}
		data += "]}";

		return data;*/
		return "{}";
	}

	private void GetBattleDetailsInternal(string scenarioId, BattleDetailsHandler callback, ErrorHandler errorCallback = null) {
		BrainCloudClient bc = BrainCloudWrapper.GetBC();
		if(!bc.IsAuthenticated()) {
			if(errorCallback != null) {
				errorCallback(400, ReasonCodes.INVALID_REQUEST, "Not signed in!");
			}
			return;
		}

		string args = GetBattleDetailsParams(scenarioId);
		OnBattleDetails += callback;
		OnBattleDetailsError += errorCallback;
		bc.ScriptService.RunScript("GetBattleDetails", args, BattleDetailsCallback, BattleDetailsFailureCallback);
	}

	private void BattleDetailsCallback(string jsonResponse, object cbObject) {
		JsonData response = JsonMapper.ToObject(jsonResponse)["data"]["response"];
		if((bool)response["success"] == false) {
			BattleDetailsFailureCallback((int)response["status"], (int)response["reasonCode"], GetReasonMessage((ReasonCode)(int)response["reasonCode"]), null);
			ClearBattleDetailsCallbacks();
			return;
		}
		
		List<LoadoutEntity> loadouts = new List<LoadoutEntity>();
		for(int i = 0; i < response["data"]["loadouts"].Count; ++i) {
			LoadoutEntity loadout = LoadoutEntity.FromJson(response["data"]["loadouts"][i]);
			loadouts.Add(loadout);
		}
		Scenario scenario = Scenario.FromJson(response["data"]["scenario"]);
		if(OnBattleDetails != null) {
			OnBattleDetails(scenario, loadouts);
		}
		ClearBattleDetailsCallbacks();
	}

	private void BattleDetailsFailureCallback(int status, int reasonCode, string jsonError, object cbObject) {
		Debug.LogError(string.Format("Error retrieving battle details: status={0} reasonCode={1}\n{2}", status, reasonCode, jsonError));
		if(OnBattleDetailsError != null) {
			OnBattleDetailsError(status, reasonCode, jsonError);
		}
		ClearBattleDetailsCallbacks();
	}

	private void ClearBattleDetailsCallbacks() {
		OnBattleDetails = null;
		OnBattleDetailsError = null;
	}
	#endregion

}
