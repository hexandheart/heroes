﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine.UI;

public class LoadoutsMenu : Menu {

	[SerializeField]
	private LoadoutListView loadoutListView;
	[SerializeField]
	private BuildLoadoutView buildLoadoutView;

	protected override void OnAwake() {
		base.OnAwake();
		loadoutListView.OnLoadoutSelected += ShowLoadout;
	}

	private void OnDestroy() {
		loadoutListView.OnLoadoutSelected -= ShowLoadout;
	}

	protected override void OnShow(params object[] args) {
		MenuManager.OpenOverlay<NavOverlay>(MainSubMenu.Loadouts);
		
		ShowLoadoutList();
		ArenaCameraManager.PushFocus(this, "LoadoutMenu");
	}

	protected override void OnTransitionOutComplete() {
		ArenaCameraManager.PopFocus(this);
		base.OnTransitionOutComplete();
	}

	public void ShowLoadoutList() {
		loadoutListView.Show(true);
		buildLoadoutView.Hide();
	}

	public void ShowLoadout(LoadoutEntity loadout) {
		loadoutListView.Hide();
		buildLoadoutView.Show(loadout);
	}

}
