﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;

public class BuildLoadoutView : MonoBehaviour, IEventListener {

	[SerializeField]
	private LabeledWidget backLabel;

	[SerializeField]
	private CollectionView collectionView;

	[SerializeField]
	private LoadoutInfoPanel infoPanel;

	private bool isDirty = false;

	private LoadoutEntity loadout;
	private int selectedSlot;
	private HeroEntity[] heroes;
	private Pawn[] pawns;
	private PawnOverlay[] pawnOverlays;
	private bool pawnOverlaysVisible = false;
	
	public void Show(LoadoutEntity loadout) {
		AddListeners();

		Localize();
		this.loadout = loadout;
		isDirty = false;

		ShowLoadoutInfo();

		LoadHeroes();

		collectionView.gameObject.SetActive(false);
		collectionView.RefreshData();
		gameObject.SetActive(true);

		ArenaCameraManager.PushFocus(this, "LoadoutMenu");

		SetupPawnOverlays();
	}

	public void Hide() {
		RemoveListeners();

		HideLoadoutInfo();

		ArenaCameraManager.PopFocus(this);
		HidePawnOverlays();

		collectionView.gameObject.SetActive(false);
		gameObject.SetActive(false);
	}

	private void Localize() {
		backLabel.Text = TM._("BACK");
	}

	private void Update() {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			OnBackClicked();
		}
	}

	private void ShowLoadoutInfo() {
		string loadoutName = string.Format("{0}{1}", loadout.Name, isDirty ? "*" : "");
		infoPanel.ShowInfo(loadoutName, isDirty);
		infoPanel.gameObject.SetActive(true);
	}

	private void HideLoadoutInfo() {
		infoPanel.gameObject.SetActive(false);
	}

	private void SetupPawnOverlays() {
		if(pawnOverlays != null) {
			for(int i = 0; i < pawnOverlays.Length; ++i) {
				Destroy(pawnOverlays[i]);
			}
		}

		pawnOverlays = new PawnOverlay[0];
		/*ArenaLevel level = FindObjectOfType<ArenaLevel>();
		int spawnPointCount = level.GetSpawnPointCount(0);
		pawnOverlays = new PawnOverlay[spawnPointCount];
		for(int i = 0; i < spawnPointCount; ++i) {
			SpawnPoint spawnPoint = level.GetSpawnPoint(0, i);
			int slotIndex = i;
			pawnOverlays[i] = PawnLoader.LoadPawnOverlay();
			pawnOverlays[i].transform.position = new Vector3(spawnPoint.transform.position.x, spawnPoint.transform.position.y + 0.5f, spawnPoint.transform.position.z);
			pawnOverlays[i].Hide();
			pawnOverlays[i].OnClick.AddListener(delegate { OnSelectHeroSlot(slotIndex); });
		}*/
	}

	private void HidePawnOverlays() {
		if(pawnOverlays == null) {
			return;
		}
		pawnOverlaysVisible = false;
		for(int i = 0; i < pawnOverlays.Length; ++i) {
			pawnOverlays[i].Hide();
		}
	}

	private void ShowPawnOverlays() {
		for(int i = 0; i < pawnOverlays.Length; ++i) {
			ShowPawnOverlay(i);
		}
	}

	private void ShowPawnOverlay(int index) {
		if(heroes[index] == null) {
			pawnOverlays[index].ShowAddButton();
			return;
		}
		pawnOverlays[index].Hide();
	}

	public void OnBackClicked() {
		if(collectionView.gameObject.activeInHierarchy) {
			CloseCollection();
		}
		else {
			if(isDirty) {
				Popup.ShowPopup(TM._("DISCARD CHANGES"), TM._("ARE YOU SURE YOU WANT TO LEAVE WITHOUT SAVING CHANGES?"), Popup.Button.OK | Popup.Button.Cancel, OnConfirmBack);
				return;
			}
			OnConfirmBack(Popup.Button.OK);
		}
	}

	public void OnSelectHeroSlot(int slot) {
		HideLoadoutInfo();
		HidePawnOverlays();
		if(selectedSlot != -1) {
			ArenaCameraManager.PopFocus(this);
		}
		selectedSlot = slot;
		collectionView.ResetFilters();
		collectionView.ApplyFilter(HeroInUseFilter);
		collectionView.SetShowNoHeroItem(true);
		collectionView.Refresh();
		collectionView.gameObject.SetActive(true);
		/*ArenaLevel level = FindObjectOfType<ArenaLevel>();
		Point tile = level.WorldToTile(pawnOverlays[slot].transform.position);
		ArenaCameraManager.PushFocus(this, string.Format("HeroDetail:{0},{1}", tile.x, tile.y));*/

		if(heroes[selectedSlot] != null) {
			EventManager.Queue(new HeroSelectedEvent(selectedSlot, 1/*, heroes[selectedSlot]*/));
		}
	}

	private void CloseCollection() {
		selectedSlot = -1;
		collectionView.gameObject.SetActive(false);
		ArenaCameraManager.PopFocus(this);
		ShowPawnOverlays();
		EventManager.Queue(new HeroSelectedEvent(0, 1/*, null*/));

		ShowLoadoutInfo();
	}

	private void OnConfirmBack(Popup.Button result) {
		if(result == Popup.Button.OK) {
			GetComponentInParent<LoadoutsMenu>().ShowLoadoutList();
			MenuManager.Instance.StartCoroutine(DestroyPawns());
		}
	}

	public void OnHeroSelected(HeroEntity hero) {
		if(!isDirty && heroes[selectedSlot] != hero) {
			isDirty = true;
		}
		
		heroes[selectedSlot] = hero;
		if(hero == null) {
			loadout.HeroIDs[selectedSlot] = null;
		}
		else {
			loadout.HeroIDs[selectedSlot] = hero.ID;
		}
		collectionView.Refresh();
		MenuManager.Instance.StartCoroutine(ReloadPawn(selectedSlot));
	}

	private void LoadHeroes() {
		HeroService.GetHeroInfo(loadout.HeroIDs, OnHeroInfo, OnHeroInfoError);
	}

	private void OnHeroInfo(HeroEntity[] heroes) {
		this.heroes = heroes;
		for(int i = 0; i < pawnOverlays.Length; ++i) {
			ShowPawnOverlay(i);
		}
		MenuManager.Instance.StartCoroutine(LoadPawns());
	}

	private void OnHeroInfoError(int status, int reasonCode, string message) {
		MessageLog.Log(MessageLogLevel.Debug, "Error getting heroes: " + message);
	}

	private IEnumerator LoadPawns() {
		yield return MenuManager.Instance.StartCoroutine(DestroyPawns());

		pawns = new Pawn[heroes.Length];
		for(int i = 0; i < heroes.Length; ++i) {
			if(heroes[i] == null) {
				continue;
			}

			yield return MenuManager.Instance.StartCoroutine(LoadPawn(i));
		}
	}

	private IEnumerator LoadPawn(int index) {
		if(heroes[index] == null) {
			yield break;
		}

		/*Pawn pawn = null;
		int reasonCode = 0;
		string message = null;
		PawnLoader.LoadPawn(
			index,
			heroes[index],
			resultPawn => {
				pawn = resultPawn;
			},
			(resultReasonCode, resultMessage) => {
				reasonCode = resultReasonCode;
				message = resultMessage;
			});

		while(pawn == null && reasonCode == 0) {
			yield return null;
		}

		if(reasonCode != 0) {
			MessageLog.Log(MessageLogLevel.Debug, string.Format("[BuildLoadoutView]: " + message));
			yield break;
		}

		pawns[index] = pawn;*/

		// Put the pawn in a good spot!
		/*ArenaLevel level = FindObjectOfType<ArenaLevel>();
		int spawnPointCount = level.GetSpawnPointCount(0);
		if(index >= spawnPointCount) {
			yield break;
		}

		SpawnPoint spawnPoint = level.GetSpawnPoint(0, index);
		pawn.transform.position = level.TileToWorld(spawnPoint.Tile);
		pawn.transform.rotation = spawnPoint.transform.localRotation;*/
	}

	private IEnumerator DestroyPawns() {
		if(pawns == null) {
			yield break;
		}

		// Yield on animate-out?

		for(int i = 0; i < pawns.Length; ++i) {
			yield return MenuManager.Instance.StartCoroutine(DestroyPawn(i));
		}

		pawns = null;
	}

	private IEnumerator DestroyPawn(int index) {
		if(pawns[index] == null) {
			yield break;
		}

		Destroy(pawns[index].gameObject);
		yield return null;
	}

	private IEnumerator ReloadPawn(int index) {
		yield return MenuManager.Instance.StartCoroutine(DestroyPawn(index));
		yield return MenuManager.Instance.StartCoroutine(LoadPawn(index));
		EventManager.Queue(new HeroSelectedEvent(index, 1/*, heroes[index]*/));
	}

	private bool HeroInUseFilter(HeroEntity hero) {
		return System.Array.Find(heroes, arrayHero => arrayHero != null && arrayHero.ID == hero.ID) != null;
	}

	public void OnSaveLoadoutClicked() {
		ShowLoadoutInfo();

		infoPanel.ShowSaving(true);
		HeroService.CreateLoadout(loadout, OnCreateLoadout_Success, OnCreateLoadout_Error);
	}

	private void OnCreateLoadout_Success(string loadoutId) {
		loadout.ID = loadoutId;
		isDirty = false;
		infoPanel.ShowSaving(false);
		ShowLoadoutInfo();
	}

	private void OnCreateLoadout_Error(int status, int reasonCode, string statusMessage) {
		MessageLog.Log(MessageLogLevel.Debug, "[Loadout]: " + statusMessage);
		Debug.LogError(string.Format("[BuildLoadoutView]: Error {0} ({1}) {2}", status, reasonCode, statusMessage));
		infoPanel.ShowSaving(false);
	}

	public void OnRenameClicked() {
		Popup.ShowPopup(TM._("RENAME LOADOUT"), TM._("ENTER A NAME FOR THE LOADOUT"), Popup.Button.OK | Popup.Button.Cancel | Popup.Button.Input, loadout.Name, TM._("Enter loadout name"), result => {
			if(result == Popup.Button.Cancel ||
				string.IsNullOrEmpty(Popup.TextEntry) ||
				Popup.TextEntry == loadout.Name) {
				return;
			}
			loadout.Name = Popup.TextEntry;
			isDirty = true;
			ShowLoadoutInfo();
		});
	}

	#region Event listeners
	public void AddListeners() {
		EventManager.AddListener<PawnClickedEvent>(OnPawnClickedEvent);
	}

	public void RemoveListeners() {
		EventManager.RemoveListener<PawnClickedEvent>(OnPawnClickedEvent);
	}

	private void OnPawnClickedEvent(IEvent evt) {
		PawnClickedEvent pce = evt as PawnClickedEvent;

		// Figure out which slot it is.
		for(int i = 0; i < pawns.Length; ++i) {
			if(pawns[i] != pce.Pawn) {
				continue;
			}
			// Don't do anything if we're already selected on this slot.
			if(selectedSlot == i) {
				break;
			}
			OnSelectHeroSlot(i);
			break;
		}
	}
	#endregion

}
