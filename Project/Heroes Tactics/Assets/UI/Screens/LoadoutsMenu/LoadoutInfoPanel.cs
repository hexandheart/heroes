﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadoutInfoPanel : MonoBehaviour {

	[SerializeField]
	private LabeledWidget saveButtonLabel;

	[SerializeField]
	private Button saveButton;

	[SerializeField]
	private LabeledWidget loadoutNameLabel;

	[SerializeField]
	private GameObject loadingSpinner;

	private void OnEnable() {
		Localize();
	}

	private void Localize() {
		saveButtonLabel.Text = TM._("SAVE");
	}

	public void ShowInfo(string loadoutName, bool canSave) {
		loadoutNameLabel.Text = loadoutName;
		saveButton.interactable = canSave;
	}

	public void ShowSaving(bool saving) {
		if(saving) {
			loadingSpinner.SetActive(true);
			saveButtonLabel.Text = TM._("SAVING...");
		}
		else {
			loadingSpinner.SetActive(false);
			saveButtonLabel.Text = TM._("SAVE");
		}
	}

}
