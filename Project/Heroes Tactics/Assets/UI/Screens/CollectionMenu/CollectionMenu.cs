﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine.UI;

public class CollectionMenu : Menu {

	[SerializeField]
	private CollectionView view;

	protected override void OnShow(params object[] args) {
		MenuManager.OpenOverlay<NavOverlay>(MainSubMenu.Collection);

		ArenaCameraManager.PushFocus(this, "Gameplay");
		view.RefreshData();
	}

	protected override void OnTransitionOutComplete() {
		base.OnTransitionOutComplete();
		ArenaCameraManager.PopFocus(this);
	}

}
