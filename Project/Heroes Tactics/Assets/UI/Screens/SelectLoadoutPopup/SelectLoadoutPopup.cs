﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections;

public class SelectLoadoutPopup : Menu {

	private static SelectLoadoutPopup instance;

	[SerializeField]
	private GameObject okButton;
	[SerializeField]
	private LabeledWidget okButtonLabel;

	[SerializeField]
	private GameObject cancelButton;
	[SerializeField]
	private LabeledWidget cancelButtonLabel;

	[SerializeField]
	private Text captionLabel;

	[SerializeField]
	private LoadoutListView loadoutListView;

	private LoadoutEntity response = null;
	private bool closePopup = false;
	private Action<LoadoutEntity> callback;
	private GameObject lastSelection = null;

	private IEnumerator currentPopup;

	protected override void OnAwake() {
		loadoutListView.OnLoadoutSelected += OnLoadoutSelected;
		instance = this;
	}

	private void OnDestroy() {
		loadoutListView.OnLoadoutSelected -= OnLoadoutSelected;
	}

	public static void ShowPopup(Action<LoadoutEntity> callback) {
		if(instance.currentPopup != null) {
			instance.StopCoroutine(instance.currentPopup);
			instance.currentPopup = null;
		}
		instance.currentPopup = instance.ShowCoroutine(callback);
		instance.StartCoroutine(instance.currentPopup);
	}

	public static void ClosePopup() {
		instance.closePopup = true;
	}

	private IEnumerator ShowCoroutine(Action<LoadoutEntity> callback) {
		captionLabel.text = TM._("SELECT LOADOUT");

		// For now don't show okay, only cancel.
		okButton.SetActive(false);
		okButtonLabel.Text = TM._("OK");
		cancelButton.SetActive(true);
		cancelButtonLabel.Text = TM._("CANCEL");
		response = null;

		closePopup = false;
		lastSelection = EventSystem.current.currentSelectedGameObject;
		EventSystem.current.SetSelectedGameObject(null);

		loadoutListView.Show(false);

		Open();
		while(response == null && !closePopup) {
			yield return true;
		}
		Close();
		//EventSystem.current.SetSelectedGameObject(null);

		if(callback != null) {
			callback(response);
		}
	}

	public void OnCancelClicked() {
		closePopup = true;
	}

	protected override void OnTransitionInComplete() {
		base.OnTransitionInComplete();

		// TODO: Select a loadout item.
	}

	protected override void OnTransitionOutComplete() {
		base.OnTransitionOutComplete();

		EventSystem.current.SetSelectedGameObject(lastSelection);
	}

	private void OnLoadoutSelected(LoadoutEntity loadout) {
		response = loadout;
	}

}
