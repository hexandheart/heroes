﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Events;

public class PlayMenu : Menu, IEventListener {

	[SerializeField]
	private Button playVsAIButton;
	[SerializeField]
	private LabeledWidget playVsAILabel;

	[SerializeField]
	private Button playQuickButton;
	[SerializeField]
	private LabeledWidget playQuickLabel;

	[SerializeField]
	private DebugServerOverride debugServerOverride;

	protected override void OnShow(params object[] args) {
		AddListeners();
		MenuManager.OpenOverlay<NavOverlay>(MainSubMenu.Play);
		EnableUI(Arena.Photon.HeroNetworkManager.IsMatchmaking != true);

		ArenaCameraManager.PushFocus(this, "PlayMenu");

		if(Application.isEditor) {
			debugServerOverride.UseOverride = true;
		}
	}

	protected override void OnTransitionOutComplete() {
		base.OnTransitionOutComplete();
		RemoveListeners();
		ArenaCameraManager.PopFocus(this);
	}

	public void OnPlayVsAIClicked() {
		SelectLoadoutPopup.ShowPopup(loadout => {
			if(loadout == null) {
				return;
			}
			// Kick off Play VS AI!!
			EventManager.Queue(new FindMatchRequest(MatchType.solo, loadout.ID, debugServerOverride.OverrideSettings));
		});
	}

	public void OnPlayQuickClicked() {
		SelectLoadoutPopup.ShowPopup(loadout => {
			if(loadout == null) {
				return;
			}
			// Kick off Quick Match!!
			EventManager.Queue(new FindMatchRequest(MatchType.versus, loadout.ID, debugServerOverride.OverrideSettings));
		});
	}

	private void Localize() {
		playVsAILabel.Text = TM._("PLAY VS AI");
		playQuickLabel.Text = TM._("QUICK MATCH");
	}

	private void EnableUI(bool enable) {
		playVsAIButton.interactable = enable;
		playQuickButton.interactable = enable;

		if(enable) {
			playVsAIButton.Select();
		}
	}

	#region Event listeners
	public void AddListeners() {
		EventManager.AddListener<MatchmakingStartedEvent>(OnMatchmakingStartedEvent);
		EventManager.AddListener<MatchmakingStoppedEvent>(OnMatchmakingStoppedEvent);
	}

	public void RemoveListeners() {
		EventManager.RemoveListener<MatchmakingStartedEvent>(OnMatchmakingStartedEvent);
		EventManager.RemoveListener<MatchmakingStoppedEvent>(OnMatchmakingStoppedEvent);
	}

	private void OnMatchmakingStartedEvent(IEvent evt) {
		EnableUI(false);
	}

	private void OnMatchmakingStoppedEvent(IEvent evt) {
		EnableUI(true);
	}
	#endregion

}
