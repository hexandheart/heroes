﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugServerOverride : MonoBehaviour {

	[SerializeField]
	private Toggle useOverride;

	[SerializeField]
	private CanvasGroup settingsGroup;

	[SerializeField]
	private InputField inputAddress;
	[SerializeField]
	private InputField inputPort;
	[SerializeField]
	private InputField inputAppId;
	[SerializeField]
	private InputField inputGameVersion;

	public void OnOverrideToggled(bool isOn) {
		settingsGroup.interactable = isOn;
	}

	public bool UseOverride {
		get {
			return useOverride.isOn;
		}
		set {
			useOverride.isOn = value;
			OnOverrideToggled(value);
		}
	}

	public Events.FindMatchRequest.ServerSettings? OverrideSettings {
		get {
			if(!useOverride.isOn) {
				return null;
			}

			return new Events.FindMatchRequest.ServerSettings() {
				address = inputAddress.text,
				port = int.Parse(inputPort.text),
				appId = inputAppId.text,
				version = inputGameVersion.text,
			};
		}
	}

}
