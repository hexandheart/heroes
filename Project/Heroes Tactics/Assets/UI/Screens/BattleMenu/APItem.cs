﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APItem : MonoBehaviour {

	[SerializeField]
	private GameObject backing;
	[SerializeField]
	private GameObject full;

	public void Hide() {
		backing.SetActive(false);
		full.SetActive(false);
	}

	public void ShowEmpty() {
		backing.SetActive(true);
		full.SetActive(false);
	}

	public void ShowFull() {
		backing.SetActive(true);
		full.SetActive(true);
	}

}
