﻿using Arena;
using Arena.Photon;
using BattleMechanics;
using BattleMechanics.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChampionPanel : MonoBehaviour {

	[SerializeField]
	private bool isLocalDisplay;

	[SerializeField]
	private PawnHudIndicator healthIndicator;
	[SerializeField]
	private GameObject armorBase;
	[SerializeField]
	private SuperTextMesh armorValue;
	[SerializeField]
	private SuperTextMesh championNameLabel;

	private Actor champion;

	private void Start() {
		healthIndicator.gameObject.SetActive(false);
		championNameLabel.gameObject.SetActive(false);
		AddListeners();
	}

	private void OnDestroy() {
		RemoveListeners();
	}

	public void AddListeners() {
		PresentationManager.RegisterHandler(BattleEventType.TurnStart, OnTurnStartEvent);
		PresentationManager.RegisterHandler(BattleEventType.ActionPerformed, OnActionPerformedEvent);
		PresentationManager.RegisterHandler(BattleEventType.BattleEnded, OnBattleEndedEvent);
	}

	public void RemoveListeners() {
		PresentationManager.UnregisterHandler(BattleEventType.TurnStart, OnTurnStartEvent);
		PresentationManager.UnregisterHandler(BattleEventType.ActionPerformed, OnActionPerformedEvent);
		PresentationManager.UnregisterHandler(BattleEventType.BattleEnded, OnBattleEndedEvent);
	}

	private IEnumerator OnTurnStartEvent(IBattleEvent battleEvent) {
		UpdatePanel();
		yield break;
	}

	private IEnumerator OnActionPerformedEvent(IBattleEvent battleEvent) {
		UpdatePanel();
		yield break;
	}

	private IEnumerator OnBattleEndedEvent(IBattleEvent battleEvent) {
		champion = null;
		healthIndicator.gameObject.SetActive(false);
		championNameLabel.gameObject.SetActive(false);
		yield break;
	}

	private void UpdatePanel() {
		if(champion == null) {
			Team localTeam = BattleController.GetLocalTeam();
			for(int i = 0; i < BattleController.BattleState.NumTeams; ++i) {
				Team team = BattleController.BattleState.GetTeamAtIndex(i);
				if((team == localTeam) != isLocalDisplay) {
					continue;
				}
				champion = team.Champion;
				break;
			}

			if(champion == null) {
				healthIndicator.gameObject.SetActive(false);
				championNameLabel.gameObject.SetActive(false);
				return;
			}
		}

		healthIndicator.gameObject.SetActive(true);
		championNameLabel.gameObject.SetActive(true);

		championNameLabel.Text = champion.CurrentEntity.LocID;

		healthIndicator.SetValuesWithColor(champion.CurrentEntity.Stats.Health, champion.CurrentEntity.Stats.MaxHealth);
		healthIndicator.IsVisible = true;
		healthIndicator.IsHighlighted = true;

		if(champion.CurrentEntity.Stats.Armor != 0) {
			armorValue.Text = champion.CurrentEntity.Stats.Armor.ToString();
			armorBase.SetActive(true);
		}
		else {
			armorBase.SetActive(false);
		}
	}

}
