﻿using UnityEngine;
using Events;
using Arena;
using BattleMechanics.Events;
using System.Collections;
using Arena.Photon;
using BattleMechanics;

public class APList : MonoBehaviour {

	[SerializeField]
	private bool isLocalDisplay;

	[SerializeField]
	private SuperTextMesh apValueLabel;

	private APItem[] apItems;

	private void Start() {
		apValueLabel.gameObject.SetActive(false);
		apItems = GetComponentsInChildren<APItem>(true);
		for(int i = 0; i < apItems.Length; ++i) {
			apItems[i].Hide();
		}
		AddListeners();
	}

	private void OnDestroy() {
		RemoveListeners();
	}

	public void AddListeners() {
		PresentationManager.RegisterHandler(BattleEventType.TurnStart, OnTurnStartEvent);
		PresentationManager.RegisterHandler(BattleEventType.ActionPerformed, OnActionPerformedEvent);
	}

	public void RemoveListeners() {
		PresentationManager.UnregisterHandler(BattleEventType.TurnStart, OnTurnStartEvent);
		PresentationManager.UnregisterHandler(BattleEventType.ActionPerformed, OnActionPerformedEvent);
	}

	private IEnumerator OnTurnStartEvent(IBattleEvent battleEvent) {
		TurnStartEvent evt = battleEvent as TurnStartEvent;
		if((evt.TeamID == BattleController.GetLocalTeam().TeamID) != isLocalDisplay) {
			yield break;
		}

		UpdateAP(evt.TeamAPData.AP, evt.TeamAPData.MaxAP);
	}

	private IEnumerator OnActionPerformedEvent(IBattleEvent battleEvent) {
		ActionPerformedEvent evt = battleEvent as ActionPerformedEvent;
		Actor actor = BattleController.BattleState.GetActorByID(evt.ActorID);
		Team myTeam = BattleController.GetLocalTeam();
		if((actor.Team == myTeam) != isLocalDisplay) {
			yield break;
		}

		UpdateAP(actor.Team.AP, actor.Team.MaxAP);
	}

	private void UpdateAP(int full, int max) {
		apValueLabel.gameObject.SetActive(true);
		apValueLabel.Text = string.Format("{0}/{1}", full, max);

		int i = 0;
		for(; i < full; ++i) {
			apItems[i].ShowFull();
		}
		for(; i < max; ++i) {
			apItems[i].ShowEmpty();
		}
		for(; i < apItems.Length; ++i) {
			apItems[i].Hide();
		}
	}

}
