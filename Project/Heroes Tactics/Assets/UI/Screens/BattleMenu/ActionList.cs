﻿using UnityEngine;
using BattleMechanics;
using Events;
using Arena;
using Arena.Photon;
using BattleMechanics.Events;
using BattleMechanicsUtils;
using System.Collections;

public class ActionList : MonoBehaviour, IEventListener {

	[SerializeField]
	private GameObject noneItem;

	private Actor displayedActor;
	private ActionItem[] actionItems;

	public void ShowActions(Actor actor) {
		displayedActor = actor;

		int currentAction = 0;
		if(actor != null) {
			foreach(var actionGroup in actor.CurrentEntity.ActionMap.ActionGroups) {
				foreach(var action in actionGroup.Actions.Values) {
					actionItems[currentAction].ShowAction(actor, action);
					++currentAction;
					if(currentAction >= actionItems.Length) {
						break;
					}
				}
				if(currentAction >= actionItems.Length) {
					break;
				}
			}
		}

		noneItem.SetActive(currentAction == 0);

		for(; currentAction < actionItems.Length; ++currentAction) {
			actionItems[currentAction].ShowAction(actor, null);
		}
	}

	private void Start() {
		actionItems = GetComponentsInChildren<ActionItem>(true);
		ShowActions(null);
		AddListeners();
	}

	private void OnDestroy() {
		RemoveListeners();
	}

	public void AddListeners() {
		EventManager.AddListener<HeroSelectedEvent>(OnHeroSelected);
		EventManager.AddListener<ActionClickedEvent>(OnActionClicked);

		PresentationManager.RegisterHandler(BattleEventType.ActionPerformed, OnActionPerformed);
	}

	public void RemoveListeners() {
		EventManager.RemoveListener<HeroSelectedEvent>(OnHeroSelected);
		EventManager.RemoveListener<ActionClickedEvent>(OnActionClicked);

		PresentationManager.UnregisterHandler(BattleEventType.ActionPerformed, OnActionPerformed);
	}

	private void OnHeroSelected(IEvent evt) {
		HeroSelectedEvent hse = evt as HeroSelectedEvent;
		Actor actor = BattleController.BattleState.GetActorByID(hse.ActorID);

		if(actor == null || !actor.Team.IsLocalTeam()) {
			ShowActions(null);
		}
		else {
			ShowActions(actor);
		}
	}

	private void OnActionClicked(IEvent evt) {
		ActionClickedEvent ace = evt as ActionClickedEvent;

		if(ace.Actor == null || !ace.Actor.Team.IsLocalTeam()) {
			ShowActions(null);
		}
		else {
			ShowActions(ace.Actor);
		}
	}

	private IEnumerator OnActionPerformed(IBattleEvent battleEvent) {
		if(displayedActor == null) yield break;

		ActionPerformedEvent evt = battleEvent as ActionPerformedEvent;

		if(evt.ActorID == displayedActor.ID) {
			ShowActions(displayedActor);
		}

		yield break;
	}

}
