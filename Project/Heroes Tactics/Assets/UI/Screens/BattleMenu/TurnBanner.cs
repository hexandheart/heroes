﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Arena.Photon;
using BattleMechanics;
using BattleMechanics.Events;
using BattleMechanicsUtils;

public class TurnBanner : MonoBehaviour {

	[SerializeField]
	private Text label;

	public IEnumerator ShowForTeam(int teamId) {
		Team team = BattleController.BattleState.GetTeam(teamId);
		if(team.IsLocalTeam()) {
			label.text = "YOUR TURN";
		}
		else {
			label.text = "ENEMY'S TURN";
		}

		if(team.TeamID == (int)PawnMaterialIndex.BlueTeam) {
			label.color = Color.blue;
		}
		else if(team.TeamID == (int)PawnMaterialIndex.RedTeam) {
			label.color = Color.red;
		}
		else {
			label.color = Color.white;
		}

		gameObject.SetActive(true);

		yield return new WaitForSeconds(1.0f);

		gameObject.SetActive(false);
	}

	public IEnumerator ShowBattleEnd(BattleEndedEvent evt) {
		Team team = BattleController.BattleState.GetTeam(evt.WinnerTeamID);
		if(team.IsLocalTeam()) {
			label.text = "VICTORY";
			label.color = Color.green;
		}
		else {
			label.text = "DEFEAT";
			label.color = Color.yellow;
		}

		gameObject.SetActive(true);
		yield return new WaitForSeconds(1.0f);
		gameObject.SetActive(false);
	}

}
