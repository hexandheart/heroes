﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Events;
using BattleMechanics;
using BattleMechanics.Events;
using BattleMechanicsUtils;
using Arena;
using Arena.Photon;

public class BattleMenu : Menu, IEventListener {

	[SerializeField]
	private TurnBanner turnBanner;

	private bool listenersAdded = false;

	protected override void OnShow(params object[] args) {
		turnBanner.gameObject.SetActive(false);
		AddListeners();
	}

	protected override void OnTransitionOutComplete() {
		base.OnTransitionOutComplete();
		RemoveListeners();
	}

	#region Event listeners
	public void AddListeners() {
		if(listenersAdded) {
			return;
		}
		PresentationManager.RegisterHandler(BattleEventType.TurnStart, OnTurnStart);
		PresentationManager.RegisterHandler(BattleEventType.BattleEnded, OnBattleEnded);
		listenersAdded = true;
	}

	public void RemoveListeners() {
		if(!listenersAdded) {
			return;
		}
		PresentationManager.UnregisterHandler(BattleEventType.TurnStart, OnTurnStart);
		PresentationManager.UnregisterHandler(BattleEventType.BattleEnded, OnBattleEnded);
		listenersAdded = false;
	}

	private IEnumerator OnTurnStart(IBattleEvent battleEvent) {
		TurnStartEvent evt = battleEvent as TurnStartEvent;
		Debug.LogFormat("T {0}> Turn Start: team {1}", evt.Time, evt.TeamID);
		yield return StartCoroutine(turnBanner.ShowForTeam(evt.TeamID));
	}

	private IEnumerator OnBattleEnded(IBattleEvent battleEvent) {
		BattleEndedEvent evt = battleEvent as BattleEndedEvent;
		yield return StartCoroutine(turnBanner.ShowBattleEnd(evt));
	}
	#endregion

}
