﻿using UnityEngine;
using UnityEngine.UI;
using BattleMechanics;

public class ActionItem : MonoBehaviour {

	public Actor Actor { get; private set; }
	public BattleAction Action { get; private set; }

	[SerializeField]
	private Text nameLabel;
	[SerializeField]
	private Text costLabel;

	[SerializeField]
	private GameObject activeHandle;

	private TooltipDataProvider tooltipDataProvider;
	private Button itemButton;

	private void Awake() {
		itemButton = GetComponent<Button>();
		tooltipDataProvider = GetComponent<TooltipDataProvider>();
	}

	public void ShowAction(Actor actor, BattleAction action) {
		Actor = actor;
		Action = action;

		activeHandle.SetActive(false);
		if(action == null) {
			gameObject.SetActive(false);
			return;
		}

		string actionNameString = action.LocID.ToUpper() + "\n";
		nameLabel.text = action.LocID.ToUpper();

		string rangeString = "";
		if(action.Targeting.RangeMax == 0) {
			rangeString = "RANGE: Self";
		}
		else if(action.Targeting.RangeMin == 1 && action.Targeting.RangeMax == 1) {
			rangeString = "RANGE: Melee";
		}
		else if(action.Targeting.RangeMax == -1) {
			rangeString = string.Format("RANGE: {0}-{1}", action.Targeting.RangeMin, actor.CurrentEntity.Stats.Step);
		}
		else {
			rangeString = string.Format("RANGE: {0}-{1}", action.Targeting.RangeMin, action.Targeting.RangeMax);
		}

		string effectString = "";
		if(action.Summon != null) {
			effectString = "(Insert hero stats here)";
			if(action.Summon.ActiveActorID == BattleActionSummon.InactiveID) {
				actionNameString = string.Format("SUMMON HERO:\n{0}\n", action.LocID.ToUpper());
			}
			else {
				actionNameString = string.Format("{0}\n(Active)\n", action.LocID.ToUpper());
				rangeString = "";
			}
		}
		else if(action.HealthEffect != null) {
			string target = "target";
			if(action.Area != null && action.Area.GetShape(GridFacing.North).Length > 1) {
				target = "targets in affected area";
			}

			int potency = 0;
			int evasionChance = 0;
			action.GetExpectedPotency(actor, null, out potency, out evasionChance);
			if(action.HealthEffect.PotencyType == BattleActionEffectType.Harm) {
				if(action.HealthEffect.MitigationType == MitigationType.Normal) {
					effectString = string.Format("Deal {0} damage to {1}, reduced by armor.", -potency, target);
				}
				else {
					effectString = string.Format("Deal {0} damage to {1}, unaffected by armor.", -potency, target);
				}

				if(action.HealthEffect.EvasionType == EvasionType.Dodge) {
					effectString += " May be dodged.";
				}
				else {
					effectString += " Cannot be dodged.";
				}
			}
			else {
				effectString = string.Format("Restore {0} health to {1}.", potency, target);
			}
		}

		string costString = "";
		if(action.Cost.AP > 0) {
			costLabel.text = string.Format("{0} AP", action.Cost.AP);
			costString = string.Format("COST: {0} AP", action.Cost.AP);
		}
		else if(action.Cost.Primary > 0) {
			costLabel.text = "PRIMARY";
			costString = string.Format("Primary Action", action.Cost.AP);
		}
		else if(action.Cost.Minor > 0) {
			costLabel.text = "MINOR";
			costString = string.Format("Minor Action", action.Cost.AP);
		}
		else {
			costLabel.text = "";
		}
		gameObject.SetActive(true);

		if(actor != null) {
			if(actor.Team.TargetingBreeze.ActionGroup == action.ActionGroup &&
				actor.Team.TargetingBreeze.ActionSlot == action.ActionSlot) {
				activeHandle.SetActive(true);
			}
			else {
				activeHandle.SetActive(false);
			}
			itemButton.interactable = actor.CanAffordAction(action);
		}

		tooltipDataProvider.SetTooltipData(
			actionNameString + "\n" +
			rangeString + "\n" +
			costString +
			(string.IsNullOrEmpty(effectString) ? "" : "\n\n" + effectString));
	}

}
