﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Arena;
using Arena.Photon;
using BattleMechanics;
using BattleMechanicsUtils;
using BattleMechanics.Events;
using BattleMechanics.Requests;
using Events;

public class DebugRequests : MonoBehaviour {

	[SerializeField]
	private Button endTurnButton;
	[SerializeField]
	private Button concedeButton;
	[SerializeField]
	private Button leaveButton;

	[SerializeField]
	private Text statusMessage;

	private void Start() {
		endTurnButton.interactable = false;
		concedeButton.interactable = true;
		leaveButton.interactable = false;
		PresentationManager.RegisterHandler(BattleEventType.BattlePhaseChanged, OnBattlePhaseChanged);
		PresentationManager.RegisterHandler(BattleEventType.TurnStart, OnTurnStart);
		PresentationManager.RegisterHandler(BattleEventType.BattleEnded, OnBattleEnded);

		statusMessage.text = "STARTING...";
	}

	private void OnDestroy() {
		PresentationManager.UnregisterHandler(BattleEventType.BattlePhaseChanged, OnBattlePhaseChanged);
		PresentationManager.UnregisterHandler(BattleEventType.TurnStart, OnTurnStart);
		PresentationManager.UnregisterHandler(BattleEventType.BattleEnded, OnBattleEnded);
	}

	public void OnEndTurnClicked() {
		endTurnButton.interactable = false;
		EndTurnRequest req = new EndTurnRequest();
		BattleController.RaiseRequest(req);

		statusMessage.text = "ENDING TURN";
	}

	public void OnConcedeClicked() {
		endTurnButton.interactable = false;
		concedeButton.interactable = false;
		ConcedeMatchRequest req = new ConcedeMatchRequest();
		BattleController.RaiseRequest(req);

		statusMessage.text = "CONCEDING";
	}

	public void OnLeaveClicked() {
		EventManager.Queue(new LeaveBattleRequest());
	}
	
	public void OnActionItemClicked(ActionItem item) {
		EventManager.Queue(new ActionClickedEvent(item.Actor, item.Action));
	}

	private IEnumerator OnBattlePhaseChanged(IBattleEvent battleEvent) {
		BattlePhaseChangedEvent evt = battleEvent as BattlePhaseChangedEvent;
		if(evt.BattlePhase == ReadyState.Ready) {
			endTurnButton.interactable = false;
			concedeButton.interactable = true;
			leaveButton.interactable = false;
			statusMessage.text = "BATTLE START!";
		}
		yield break;
	}

	private IEnumerator OnTurnStart(IBattleEvent battleEvent) {
		TurnStartEvent evt = battleEvent as TurnStartEvent;
		concedeButton.interactable = true;

		Team team = BattleController.BattleState.GetTeam(evt.TeamID);
		if(team.IsLocalTeam()) {
			endTurnButton.interactable = true;
			statusMessage.text = "YOUR TURN";
		}
		else {
			statusMessage.text = "ENEMY TURN";
		}
		yield break;
	}

	private IEnumerator OnBattleEnded(IBattleEvent battleEvent) {
		endTurnButton.interactable = false;
		concedeButton.interactable = false;
		leaveButton.interactable = true;
		statusMessage.text = "BATTLE OVER!";
		yield break;
	}

}
