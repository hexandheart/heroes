﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Events;

public class MatchmakingOverlay : MonoBehaviour, IEventListener {

	[SerializeField]
	private LabeledWidget statusLabel;
	[SerializeField]
	private Button cancelButton;
	[SerializeField]
	private LabeledWidget cancelLabel;

	private void Start() {
		AddListeners();
		Localize();
		gameObject.SetActive(false);
	}

	private void OnDestroy() {
		RemoveListeners();
	}

	private void Localize() {
		cancelLabel.Text = "CANCEL";
	}

	public void OnCancelClicked() {
		EventManager.Queue(new CancelMatchmakingRequest());
	}

	#region Event listeners
	public void AddListeners() {
		EventManager.AddListener<MatchmakingStartedEvent>(OnMatchmakingStartedEvent);
		EventManager.AddListener<MatchmakingStoppedEvent>(OnMatchmakingStoppedEvent);
		EventManager.AddListener<MatchFoundEvent>(OnMatchFoundEvent);
	}

	public void RemoveListeners() {
		EventManager.RemoveListener<MatchmakingStartedEvent>(OnMatchmakingStartedEvent);
		EventManager.RemoveListener<MatchmakingStoppedEvent>(OnMatchmakingStoppedEvent);
		EventManager.RemoveListener<MatchFoundEvent>(OnMatchFoundEvent);
	}

	private void OnMatchmakingStartedEvent(IEvent evt) {
		statusLabel.Text = TM._("FINDING MATCH...");
		cancelButton.interactable = true;
		gameObject.SetActive(true);
	}

	private void OnMatchmakingStoppedEvent(IEvent evt) {
		cancelButton.interactable = true;
		gameObject.SetActive(false);
	}

	private void OnMatchFoundEvent(IEvent evt) {
		statusLabel.Text = TM._("JOINING MATCH...");
		cancelButton.interactable = false;
	}
	#endregion

}
