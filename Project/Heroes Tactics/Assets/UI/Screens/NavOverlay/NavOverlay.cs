﻿using UnityEngine;
using System.Collections;
using Events;
using UnityEngine.UI;

public enum MainSubMenu {
	Play,
	Loadouts,
	Collection,
}

public class NavOverlay : Menu {

	[SerializeField]
	private MenuStrip menuStrip;

	[SerializeField]
	private MenuStripItem playItem;
	[SerializeField]
	private MenuStripItem loadoutsItem;
	[SerializeField]
	private MenuStripItem collectionItem;

	private MainSubMenu currentSubMenu = MainSubMenu.Play;

	protected override void OnShow(params object[] args) {
		// Always make sure the system menu is up!
		MenuManager.OpenOverlay<SystemOverlay>();

		MainSubMenu subMenu = currentSubMenu;
		if(args.Length > 0) {
			subMenu = (MainSubMenu)args[0];
		}

		if(!IsOpen) {
			playItem.SetLabel(TM._("PLAY"));
			loadoutsItem.SetLabel(TM._("LOADOUTS"));
			collectionItem.SetLabel(TM._("COLLECTION"));

			menuStrip.UpdateLayout();
		}
		menuStrip.SelectItem((int)subMenu);
		currentSubMenu = subMenu;
	}

	public void OnPlayTabClicked(bool isOn) {
		if(!isOn) {
			return;
		}
		MenuManager.OpenMenu<PlayMenu>();
	}

	public void OnLoadoutsTabClicked(bool isOn) {
		if(!isOn) {
			return;
		}
		MenuManager.OpenMenu<LoadoutsMenu>();
	}

	public void OnCollectionTabClicked(bool isOn) {
		if(!isOn) {
			return;
		}
		MenuManager.OpenMenu<CollectionMenu>();
	}

}
