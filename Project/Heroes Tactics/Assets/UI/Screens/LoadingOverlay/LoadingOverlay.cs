﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingOverlay : Menu {

	[SerializeField]
	private Text loadingLabel;

	protected override void OnShow(params object[] args) {
		base.OnShow(args);
		Localize();
	}

	private void Localize() {
		loadingLabel.text = TM._("LOADING...");
	}

}
