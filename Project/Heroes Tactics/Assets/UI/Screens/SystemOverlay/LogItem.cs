﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogItem : MonoBehaviour {

	private static readonly string MessageFormat = "<color=cccccc>[{0}]:</color> {1}";
	private static readonly string MessageFormatDebug = "<i><color=#cccc00>[{0}]:</color> {1}</i>";
	private static readonly float FadeTime = 1.5f;

	[SerializeField]
	private Text label;

	private float lifeTime;
	private DateTime time;
	private MessageLogLevel logLevel;
	private string message;

	public void SetInfo(DateTime time, MessageLogLevel logLevel, float lifeTime, string message) {
		this.lifeTime = lifeTime;
		this.logLevel = logLevel;
		switch(logLevel) {
			case MessageLogLevel.Normal:
				label.text = string.Format(MessageFormat, time.ToString("t"), message);
				break;
			case MessageLogLevel.Debug:
				label.text = string.Format(MessageFormatDebug, time.ToString("t"), message);
				break;
		}
	}

	private void Update() {
		if(lifeTime > 0f) {
			lifeTime -= Time.deltaTime;
			if(lifeTime < 0f) {
				label.CrossFadeAlpha(0f, FadeTime, true);
			}
		}
		else {
			lifeTime -= Time.deltaTime;
			if(lifeTime < -FadeTime) {
				GetComponentInParent<MessageLog>().UpdateLayout();
				transform.SetParent(null);
				Destroy(gameObject);
				return;
			}
		}
	}

}
