﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MessageLogLevel {
	Normal,
	Debug,
	MatchDebug,
}

public class MessageLog : MonoBehaviour {

	private static MessageLog instance;

	[SerializeField]
	private LogItem logItemPrefab;

	[SerializeField]
	private ScrollRect scrollRect;
	[SerializeField]
	private Transform logContentRoot;
	[SerializeField]
	private VerticalLayoutGroup itemLayout;

	[SerializeField]
	private int maxMessages;
	[SerializeField]
	private float messageLife;

	[SerializeField]
	private MessageLogLevel logLevel;

	private bool updateLayout = false;

	public static void Log(string message) {
		instance.AddMessage(DateTime.Now, MessageLogLevel.Normal, message);
	}

	public static void Log(MessageLogLevel logLevel, string message) {
		instance.AddMessage(DateTime.Now, logLevel, message);
	}

	public static void Log(DateTime time, string message) {
		instance.AddMessage(time, MessageLogLevel.Normal, message);
	}

	public static void Log(DateTime time, MessageLogLevel logLevel, string message) {
		instance.AddMessage(time, logLevel, message);
	}

	private void Start() {
		instance = this;
	}

	public void AddMessage(DateTime time, MessageLogLevel logLevel, string message) {
		LogItem item = Instantiate<LogItem>(logItemPrefab, logContentRoot);
		item.SetInfo(time, logLevel, messageLife, message);

		PruneOldMessages();

		updateLayout = true;
	}

	public void UpdateLayout() {
		updateLayout = true;
	}

	private void PruneOldMessages() {
		int childrenToDelete = logContentRoot.childCount - maxMessages;
		while(childrenToDelete > 0) {
			Transform child = logContentRoot.GetChild(0);
			child.SetParent(null);
			Destroy(child.gameObject);
			--childrenToDelete;
		}
	}

	private void LateUpdate() {
		if(updateLayout) {
			float positionBefore = scrollRect.verticalNormalizedPosition;
			float sizeBefore = scrollRect.content.sizeDelta.y;

			//StartCoroutine(UpdateLayout());
			Canvas.ForceUpdateCanvases();
			itemLayout.CalculateLayoutInputHorizontal();
			itemLayout.CalculateLayoutInputVertical();
			itemLayout.SetLayoutHorizontal();
			itemLayout.SetLayoutVertical();
			itemLayout.GetComponent<ContentSizeFitter>().SetLayoutVertical();

			if(positionBefore <= 0.01f) {
				scrollRect.verticalNormalizedPosition = 0f;
			}
			else {
				float position = 1f - positionBefore;
				float size = scrollRect.content.sizeDelta.y;
				
				scrollRect.verticalNormalizedPosition = Mathf.Min(1f - (position * sizeBefore / size), 1f);
			}

			updateLayout = false;
		}
	}

}
