﻿using UnityEngine;
using System.Collections;
using Events;
using UnityEngine.UI;
using LitJson;

public class AuthMenu : Menu {

	[SerializeField]
	private InputField emailField;
	[SerializeField]
	private Text emailLabel;
	[SerializeField]
	private InputField passwordField;
	[SerializeField]
	private Text passwordLabel;

	[SerializeField]
	private Button signInButton;
	[SerializeField]
	private LabeledWidget signInLabel;

	[SerializeField]
	private Toggle rememberToggle;
	[SerializeField]
	private Text rememberLabel;

	[SerializeField]
	private AuthServerOverride authServerOverride;

	private bool attemptedLogin = false;

	protected override void OnShow(params object[] args) {
		if(BrainCloudWrapper.GetBC().Authenticated) {
			MenuManager.OpenOverlay<NavOverlay>(MainSubMenu.Play);
			return;
		}

		Localize();

		if(PlayerPrefs.HasKey("Email")) {
			emailField.text = PlayerPrefs.GetString("Email", "email@email.com");
			defaultSelection = passwordField.gameObject;
			rememberToggle.isOn = true;
		}
		else {
			rememberToggle.isOn = false;
		}

		ArenaCameraManager.PushFocus(this, "AuthMenu");
		MenuManager.OpenOverlay<SystemOverlay>();

		EnableUI(true);

		if(Application.isEditor) {
			authServerOverride.UseOverride = true;
		}
	}

	protected override void OnTransitionOutComplete() {
		base.OnTransitionOutComplete();
		ArenaCameraManager.PopFocus(this);
	}

	public void OnExitClicked() {
		Application.Quit();
	}

	public void OnSignInClicked() {
		EnableUI(false);
		// TODO: Don't force create!
		if(authServerOverride.UseOverride) {
			BrainCloudWrapper.Initialize(
				"https://sharedprod.braincloudservers.com/dispatcherv2",
				authServerOverride.OverrideSettings.secret,
				authServerOverride.OverrideSettings.appId,
				authServerOverride.OverrideSettings.version);
		}
		else {
			BrainCloudWrapper.Initialize();
		}
		BrainCloudWrapper.GetInstance().AuthenticateEmailPassword(emailField.text, passwordField.text, true, OnSuccess_Authenticate, OnError, null);
	}

	private void Localize() {
		emailLabel.text = TM._("EMAIL ADDRESS");
		passwordLabel.text = TM._("PASSWORD");
		rememberLabel.text = TM._("REMEMBER EMAIL");

		signInLabel.Text = TM._("SIGN IN");
	}

	private void EnableUI(bool enable) {
		emailField.interactable = enable;
		passwordField.interactable = enable;
		signInButton.interactable = enable;

		if(enable) {
			emailField.Select();
		}
	}

	private void OnSuccess_Authenticate(string responseData, object cbObject) {
		if(rememberToggle.isOn) {
			PlayerPrefs.SetString("Email", emailField.text);
		}
		else {
			PlayerPrefs.DeleteKey("Email");
		}
		PlayerPrefs.Save();

		// Read our initial player state.
		BrainCloudWrapper.GetBC().PlayerStateService.ReadPlayerState(OnSuccess_ReadPlayerState, OnError);
	}

	private void OnError(int statusCode, int reasonCode, string statusMessage, object cbObject) {
		if(/*reasonCode == BrainCloud.ReasonCodes.SWITCHING_PROFILES &&*/
			!attemptedLogin) {
			attemptedLogin = true;
			// Maybe it's SWITCHING_PROFILES, we have to reset the stored profile.
			BrainCloudWrapper.GetInstance().ResetStoredProfileId();
			BrainCloudWrapper.GetInstance().AuthenticateEmailPassword(emailField.text, passwordField.text, true, OnSuccess_Authenticate, OnError, null);
			return;
		}
		attemptedLogin = false;

		MessageLog.Log(MessageLogLevel.Debug, string.Format("BrainCloud authentication error.\nstatusCode: {0}\nreasonCode: {1}\nstatusMesage: {2}", statusCode, reasonCode, statusMessage));
		EnableUI(true);
	}

	private void OnSuccess_ReadPlayerState(string responseData, object cbObject) {
		JsonData data = JsonMapper.ToObject(responseData);
		//Arena.Unet.BattleNetworkManager.LocalProfileID = (string)data["data"]["profileId"];
		Arena.Photon.HeroNetworkManager.ProfileID = (string)data["data"]["profileId"];

		HeroService.GetHeroInfo(
			null,
			(HeroEntity[] heroes) => {
				OnSuccess_Complete();
			},
			(int status, int reasonCode, string statusMessage) => {
				OnError(status, reasonCode, statusMessage, null);
			});
	}

	private void OnSuccess_Complete() {
		MenuManager.OpenOverlay<NavOverlay>(MainSubMenu.Play);
	}

}
