﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuthServerOverride : MonoBehaviour {

	public enum AuthServer {
		Live,
		Dev,
	}

	[System.Serializable]
	public class AuthServerSettings {
		public AuthServer server;
		public string appId;
		public string secret;
		public string version;
	}

	[SerializeField]
	private Toggle useOverride;

	[SerializeField]
	private CanvasGroup settingsGroup;

	[SerializeField]
	private Dropdown serverDropdown;
	[SerializeField]
	private InputField inputGameVersion;

	[SerializeField]
	private AuthServerSettings[] authSettings;

	public void OnOverrideToggled(bool isOn) {
		settingsGroup.interactable = isOn;
	}

	public bool UseOverride {
		get { return useOverride.isOn; }
		set {
			useOverride.isOn = value;
			OnOverrideToggled(value);
		}
	}

	public AuthServerSettings OverrideSettings {
		get {
			return authSettings[serverDropdown.value];
		}
	}

}
