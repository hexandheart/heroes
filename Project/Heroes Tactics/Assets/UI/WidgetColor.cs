﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class WidgetColor : MonoBehaviour {

	[SerializeField]
	private int colorIndex;
	private int oldColorIndex = -1;

	[SerializeField]
	private WidgetPalette widgetPalette;

	private Graphic graphic;

	public void OnPaletteChanged() {
		graphic.color = widgetPalette.GetColor(colorIndex);
	}

	private void OnEnable() {
		graphic = GetComponent<Graphic>();
		if(widgetPalette == null) {
			widgetPalette = GetComponentInParent<WidgetPalette>();
		}
		if(widgetPalette == null) {
			return;
		}
		widgetPalette.Register(this);
	}

	private void OnDisable() {
		if(widgetPalette == null) {
			return;
		}
		widgetPalette.Unregister(this);
	}

	private void Update() {
		if(widgetPalette == null) {
			return;
		}
		if(colorIndex != oldColorIndex) {
			oldColorIndex = colorIndex;
			graphic.color = widgetPalette.GetColor(colorIndex);
		}
	}

}
