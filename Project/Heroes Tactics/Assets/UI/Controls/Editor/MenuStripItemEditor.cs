﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEditor.UI;

[CustomEditor(typeof(MenuStripItem), true)]
[CanEditMultipleObjects]
public class MenuStripItemEditor : ToggleEditor {

	SerializedProperty labelProperty;

	protected override void OnEnable() {
		base.OnEnable();

		labelProperty = serializedObject.FindProperty("label");
	}

	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		EditorGUILayout.Space();

		serializedObject.Update();
		EditorGUILayout.PropertyField(labelProperty);

		serializedObject.ApplyModifiedProperties();
	}

}
