﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadoutListView : MonoBehaviour {

	public delegate void LoadoutSelectedHandler(LoadoutEntity loadout);
	public event LoadoutSelectedHandler OnLoadoutSelected;

	[SerializeField]
	private LoadoutsGrid loadoutsGrid;
	[SerializeField]
	private GameObject newLoadoutButton;
	[SerializeField]
	private GameObject loadingPanel;

	[SerializeField]
	private LabeledWidget newLoadoutLabel;

	private bool allowNewLoadout;
	private List<LoadoutEntity> loadouts;

	public void Show(bool allowNewLoadout) {
		this.allowNewLoadout = allowNewLoadout;
		loadingPanel.SetActive(true);
		loadoutsGrid.gameObject.SetActive(false);
		gameObject.SetActive(true);

		HeroService.GetLoadoutList(OnLoadoutList, OnLoadoutListError);
	}

	public void Hide() {
		gameObject.SetActive(false);
	}

	private void Localize() {
		newLoadoutLabel.Text = TM._("NEW LOADOUT");
	}

	private void OnLoadoutList(List<LoadoutEntity> loadouts, int maxLoadouts) {
		this.loadouts = loadouts;
		loadingPanel.SetActive(false);

		if(loadouts.Count < maxLoadouts && allowNewLoadout) {
			newLoadoutButton.SetActive(true);
		}
		else {
			newLoadoutButton.SetActive(false);
		}

		loadoutsGrid.ShowLoadouts(loadouts, allowNewLoadout);
		loadoutsGrid.gameObject.SetActive(true);
	}

	private void OnLoadoutListError(int status, int reasonCode, string statusMessage) {
		loadingPanel.SetActive(false);
		MessageLog.Log(MessageLogLevel.Debug, string.Format("Error retrieving loadout list: {0} {1}", reasonCode, statusMessage));
	}

	public void OnNewLoadoutClicked() {
		LoadoutEntity loadout = new LoadoutEntity();
		if(OnLoadoutSelected != null) {
			OnLoadoutSelected(loadout);
		}
	}

	private void OnCreateLoadout(string loadoutId) {
		Show(allowNewLoadout);
	}

	public void OnLoadoutClicked(LoadoutEntity loadout) {
		if(OnLoadoutSelected != null) {
			OnLoadoutSelected(loadout);
		}
	}

	public void OnDeleteLoadoutClicked(LoadoutEntity loadout) {
		loadingPanel.SetActive(true);
		loadoutsGrid.gameObject.SetActive(false);
		gameObject.SetActive(true);

		HeroService.DeleteLoadout(loadout, OnDeleteLoadout, OnLoadoutListError);
	}

	public void OnDeleteLoadout() {
		Show(allowNewLoadout);
	}

}
