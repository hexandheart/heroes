﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadoutsGrid : MonoBehaviour {

	private LoadoutItem[] loadoutItems;

	public void ShowLoadouts(List<LoadoutEntity> loadouts, bool allowDelete) {
		loadoutItems = GetComponentsInChildren<LoadoutItem>(true);
		int i = 0;
		for(; i < loadouts.Count; ++i) {
			loadoutItems[i].ShowLoadout(loadouts[i], allowDelete);
		}
		for(; i < loadoutItems.Length; ++i) {
			loadoutItems[i].ShowLoadout(null, false);
		}
	}

}
