﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadoutItem : MonoBehaviour {

	[SerializeField]
	private Text guidLabel;

	[SerializeField]
	private GameObject deleteButton;

	private LoadoutEntity loadout;

	public void ShowLoadout(LoadoutEntity loadout, bool allowDelete) {
		if(loadout == null) {
			gameObject.SetActive(false);
			return;
		}

		deleteButton.SetActive(allowDelete);

		this.loadout = loadout;
		guidLabel.text = loadout.Name;
		gameObject.SetActive(true);
	}

	public void OnDeleteClicked() {
		Popup.ShowPopup(TM._("DELETE LOADOUT"), TM._("ARE YOU SURE YOU WANT TO DELETE THIS LOADOUT?"), Popup.Button.OK | Popup.Button.Cancel, OnConfirm);
	}

	private void OnConfirm(Popup.Button button) {
		if(button == Popup.Button.Cancel) {
			return;
		}

		GetComponentInParent<LoadoutListView>().OnDeleteLoadoutClicked(loadout);
	}

	public void OnClicked() {
		GetComponentInParent<LoadoutListView>().OnLoadoutClicked(loadout);
	}

}
