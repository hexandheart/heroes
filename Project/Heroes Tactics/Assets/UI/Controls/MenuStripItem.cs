﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[SelectionBase]
[DisallowMultipleComponent]
public class MenuStripItem : Toggle {

	[SerializeField]
	private LabeledWidget label;

	public void SetLabel(string text) {
		label.Text = text;
	}

	public void Update() {
		if(Application.isPlaying) {
			animator.SetBool("IsOn", isOn);
		}
	}

}
