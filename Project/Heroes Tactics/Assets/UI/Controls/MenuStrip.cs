﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuStrip : MonoBehaviour {

	[SerializeField]
	private HorizontalLayoutGroup itemLayout;
	[SerializeField]
	private MenuStripItem[] menuStripItems;

	private int selectedIndex = -1;
	
	public void SelectItem(int index) {
		menuStripItems[index].isOn = false;
		menuStripItems[index].isOn = true;
	}

	public void UpdateLayout() {
		Canvas.ForceUpdateCanvases();
		itemLayout.CalculateLayoutInputHorizontal();
		itemLayout.CalculateLayoutInputVertical();
		itemLayout.SetLayoutHorizontal();
		itemLayout.SetLayoutVertical();
	}

}
