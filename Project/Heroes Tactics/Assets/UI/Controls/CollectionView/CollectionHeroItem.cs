﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionHeroItem : MonoBehaviour {

	[SerializeField]
	private Text nameLabel;
	[SerializeField]
	private Text jobLabel;

	private HeroEntity hero;

	public void ShowHero(HeroEntity hero) {
		if(hero == null) {
			gameObject.SetActive(false);
			return;
		}
		
		this.hero = hero;
		nameLabel.text = hero.AssetID;
		jobLabel.text = hero.AssetID;
		gameObject.SetActive(true);
	}

	public void ShowNoHero() {
		hero = null;

		nameLabel.text = TM._("NONE");
		jobLabel.text = "";
		gameObject.SetActive(true);
	}

	public void OnClicked() {
		GetComponentInParent<CollectionView>().HeroSelected(hero);
	}

}
