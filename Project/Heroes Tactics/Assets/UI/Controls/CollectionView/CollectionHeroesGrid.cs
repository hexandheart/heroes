﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionHeroesGrid : MonoBehaviour {
	
	private CollectionHeroItem[] heroItems;

	public int ShowHeroes(CollectiblesEntity collectibles, List<CollectionFilterMethod> filters, bool showNoHeroItem) {
		heroItems = GetComponentsInChildren<CollectionHeroItem>(true);
		int itemIndex = 0;
		int totalHeroCount = 0;
		if(showNoHeroItem) {
			heroItems[itemIndex].ShowNoHero();
			++itemIndex;
		}

		foreach(var kv in collectibles.Heroes) {
			HeroEntity hero = HeroService.GetCachedHeroInfo(kv.Key);
			if(IsHeroFiltered(hero, filters)) {
				continue;
			}
			heroItems[itemIndex].ShowHero(hero);
			++itemIndex;
			++totalHeroCount;
		}
		for(; itemIndex < heroItems.Length; ++itemIndex) {
			heroItems[itemIndex].ShowHero(null);
		}

		return totalHeroCount;
	}

	private bool IsHeroFiltered(HeroEntity hero, List<CollectionFilterMethod> filters) {
		for(int i = 0; i < filters.Count; ++i) {
			bool filtered = filters[i].Invoke(hero);
			if(filtered) {
				return true;
			}
		}

		return false;
	}

}
