﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionViewCount : MonoBehaviour {

	[SerializeField]
	private Text filterLabel;
	[SerializeField]
	private Text countLabel;

	public void ClearInfo() {
		gameObject.SetActive(false);
	}

	public void SetInfo(int currentCount, int maxCount) {
		if(currentCount == maxCount) {
			filterLabel.text = TM._("TOTAL");
			countLabel.text = maxCount.ToString();
		}
		else {
			filterLabel.text = TM._("VISIBLE");
			countLabel.text = currentCount.ToString();
		}

		gameObject.SetActive(true);
	}

}
