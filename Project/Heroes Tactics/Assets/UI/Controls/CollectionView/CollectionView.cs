﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public delegate bool CollectionFilterMethod(HeroEntity hero);

public class CollectionView : MonoBehaviour {

	[System.Serializable]
	public class CollectionHeroSelectedEvent : UnityEvent<HeroEntity> {
	}

	[SerializeField]
	private CollectionViewCount viewCount;
	[SerializeField]
	private GameObject loadingPanel;

	[SerializeField]
	private CollectionHeroesGrid heroesGrid;

	[SerializeField]
	private CollectionHeroSelectedEvent onHeroSelected;

	private CollectiblesEntity collection;
	private bool showNoHeroItem;
	private List<CollectionFilterMethod> filters = new List<CollectionFilterMethod>();

	private void Start() {
		viewCount.ClearInfo();
		loadingPanel.SetActive(false);
	}

	public void ResetFilters() {
		filters.Clear();
	}

	public void ApplyFilter(CollectionFilterMethod filter) {
		filters.Add(filter);
	}

	public void SetShowNoHeroItem(bool show) {
		showNoHeroItem = show;
	}

	public void RefreshData() {
		viewCount.ClearInfo();
		heroesGrid.gameObject.SetActive(false);
		loadingPanel.SetActive(true);
		HeroService.GetCollection(OnCollection, OnCollectionError);
	}

	public void Refresh() {
		OnCollection(collection);
	}

	private void OnCollection(CollectiblesEntity collection) {
		this.collection = collection;
		loadingPanel.SetActive(false);

		int heroCount = heroesGrid.ShowHeroes(collection, filters, showNoHeroItem);
		viewCount.SetInfo(heroCount, collection.Heroes.Count);
		heroesGrid.gameObject.SetActive(true);
	}

	private void OnCollectionError(int status, int reasonCode, string statusMessage) {
		loadingPanel.SetActive(false);
		MessageLog.Log(MessageLogLevel.Debug, string.Format("Error retrieving collection: {0}", statusMessage));
	}

	public void HeroSelected(HeroEntity hero) {
		onHeroSelected.Invoke(hero);
	}

}
