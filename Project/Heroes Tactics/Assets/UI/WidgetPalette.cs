﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WidgetPalette : MonoBehaviour {

	[SerializeField]
	private Color[] colors = new Color[0];

	private Color[] oldColors = null;
	private List<WidgetColor> widgets = new List<WidgetColor>();

	public void Register(WidgetColor widget) {
		widgets.Add(widget);
		widget.OnPaletteChanged();
	}

	public void Unregister(WidgetColor widget) {
		widgets.Remove(widget);
	}

	public Color GetColor(int index) {
		if(index < 0 || index >= colors.Length) {
			return Color.magenta;
		}
		return colors[index];
	}

	private void Update() {
		bool updated = false;
		if(oldColors == null || oldColors.Length != colors.Length) {
			oldColors = new Color[colors.Length];
			updated = true;
		}
		else {
			for(int i = 0; i < colors.Length; ++i) {
				if(colors[i] != oldColors[i]) {
					updated = true;
					break;
				}
			}
		}

		if(updated) {
			for(int i = 0; i < widgets.Count; ++i) {
				widgets[i].OnPaletteChanged();
			}
		}
	}

}
