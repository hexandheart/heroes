﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;
using Events;

[RequireComponent(typeof(OutlineEffect))]
public class OutlineEffectManager : MonoBehaviour {

	private static OutlineEffectManager instance;

	[SerializeField]
	private Color hoverColor = Color.white;

	[SerializeField]
	private Color clickColor = Color.white;

	[SerializeField]
	private Color selectedColor1 = Color.white;
	[SerializeField]
	private Color selectedColor2 = Color.white;
	[SerializeField]
	private float selectedPulseTime = 1f;

	[SerializeField]
	private Color[] teamColors;

	private OutlineEffect outlineEffect;
	private bool teamColorsEnabled = false;

	public static bool IsTeamColorEnabled {
		get { return instance.teamColorsEnabled; }
	}

	private void Start() {
		instance = this;
		outlineEffect = GetComponent<OutlineEffect>();
	}

	public static int Hover() {
		if(instance.teamColorsEnabled) {
			return -1;
		}

		instance.outlineEffect.lineColor0 = instance.hoverColor;
		return 0;
	}

	public static int Click() {
		if(instance.teamColorsEnabled) {
			return -1;
		}

		instance.outlineEffect.lineColor0 = instance.clickColor;
		return 0;
	}

	public static int Selected() {
		return 1;
	}

	public static void EnableTeamColors(bool enable) {
		instance.teamColorsEnabled = enable;
		if(enable) {
			instance.outlineEffect.lineColor0 = instance.teamColors[(int)PawnMaterialIndex.BlueTeam];
			instance.outlineEffect.lineColor2 = instance.teamColors[(int)PawnMaterialIndex.RedTeam];
		}
		EventManager.Queue(new OutlinesUpdatedEvent());
	}

	public static int TeamColor(int teamId) {
		switch((PawnMaterialIndex)teamId) {
		case PawnMaterialIndex.BlueTeam: return 0;
		case PawnMaterialIndex.RedTeam: return 2;
		}

		return -1;
	}

	private void Update() {
		if(outlineEffect == null) {
			return;
		}

		float t = Mathf.PingPong(Time.unscaledTime, selectedPulseTime);
		outlineEffect.lineColor1 = Color.Lerp(selectedColor1, selectedColor2, t);
	}

}
