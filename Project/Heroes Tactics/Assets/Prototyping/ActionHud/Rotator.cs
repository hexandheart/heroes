﻿using UnityEngine;

public class Rotator : MonoBehaviour {

	[SerializeField]
	private float rate;

	public void Update() {
		transform.Rotate(Vector3.up, rate * Time.deltaTime);
	}

}
