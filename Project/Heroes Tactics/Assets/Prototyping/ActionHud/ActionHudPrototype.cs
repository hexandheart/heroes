﻿using System.Collections.Generic;
using UnityEngine;
using Events;
using BattleMechanics;

public class ActionHudPrototype : MonoBehaviour
#if false
	, IEventListener
#endif
	{

	[SerializeField]
	private string[] pawnAssets;

	[SerializeField]
	private PawnHud pawnHudPrefab;

	private Dictionary<Pawn, PawnHud> pawnHuds = new Dictionary<Pawn, PawnHud>();

#if false
	private void Start() {
		for(int i = 0; i < pawnAssets.Length; ++i) {
			Entity entity = new Entity(
				pawnAssets[i], pawnAssets[i], EntityType.Hero,
				new Stats(10, 10, 2, 0, 5, 2, 1, 0, 1, 0),
				new BattleActionMap(
					null,
					new List<BattleActionGroup>() {
					new BattleActionGroup(BattleActionGroupType.Basic, 1, new Dictionary<int, BattleAction>() {
						{ 0, new BattleAction(
							"n/a", "move", "move", CounterType.None,
							new BattleActionTargeting(1, 2, false, true, false, true, false),
							new BattleActionCost(0, 0, 1),
							new BattleActionMovementEffect(),
							null, null, null, null) }
					})
				}));
			Actor actor = new Actor(i, 0, new GridVector(i, 0), GridFacing.North, entity);
			//pawns[i].SetActor(actor);
			PawnLoader.LoadPawn(actor,
				(pawn) => {
					pawn.gameObject.SetActive(true);
					PawnHud pawnHud = Instantiate(pawnHudPrefab);
					pawnHud.transform.SetParent(pawn.transform, false);
					pawnHud.SetValues(
						pawn.Actor.CurrentEntity.Stats.Power,
						pawn.Actor.CurrentEntity.Stats.Health,
						pawn.Actor.CurrentEntity.Stats.MaxHealth,
						pawn.Actor.CurrentEntity.Stats.Armor);
					pawnHud.SetPowerVisible(true);
					pawnHud.SetHealthVisible(true);
					pawnHud.SetActionVisible(pawnHuds.Count > 0, pawnHuds.Count > 1);

					pawnHuds[pawn] = pawnHud;
				},
				(errorCode, message) => {
					Debug.LogErrorFormat("Load pawn error {0}: {1}", errorCode, message);
				});
		}

		AddListeners();
	}

	private void OnDestroy() {
		RemoveListeners();
	}

	public void AddListeners() {
		EventManager.AddListener<PawnClickedEvent>(OnPawnClicked);
		EventManager.AddListener<PawnHoveredEvent>(OnPawnHovered);
	}

	public void RemoveListeners() {
		EventManager.RemoveListener<PawnClickedEvent>(OnPawnClicked);
		EventManager.RemoveListener<PawnHoveredEvent>(OnPawnHovered);
	}

	private void OnPawnClicked(IEvent evt) {
		PawnClickedEvent pce = evt as PawnClickedEvent;

		Debug.Log("Pawn clicked: " + pce.Pawn.Actor);
	}

	private void OnPawnHovered(IEvent evt) {
		PawnHoveredEvent phe = evt as PawnHoveredEvent;

		pawnHuds[phe.Pawn].SetPowerHighlighted(phe.IsHovering);
		pawnHuds[phe.Pawn].SetHealthHighlighted(phe.IsHovering);
		pawnHuds[phe.Pawn].SetActionHighlighted(phe.IsHovering);
	}
#endif

}
