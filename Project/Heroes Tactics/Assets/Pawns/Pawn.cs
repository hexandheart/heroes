﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;
using Events;
using BattleMechanics;
using Arena.Photon;
using Arena;
using BattleMechanics.Events;

public enum PawnMaterialIndex {
	None,
	BlueTeam,
	RedTeam,
}

[ExecuteInEditMode]
[SelectionBase]
public class Pawn : MonoBehaviour, IEventListener {

	public static readonly float AnimHeight = 7.0f;
	public static readonly float AnimDuration = 0.5f;

	public Actor Actor { get; private set; }
	public PawnHud Hud { get; private set; }

	[SerializeField]
	private PawnMaterialIndex materialIndex;
	private PawnMaterialIndex currentMaterialIndex = PawnMaterialIndex.None;

	[SerializeField]
	private bool forceUpdate = false;

	[SerializeField]
	private int outlineColorIndex = 0;
	private int currentOutlineColorIndex = 0;

	private Outline[] outlines;

	[SerializeField]
	private bool outlineEnabled = false;
	private bool currentOutlineEnabled = false;

	[SerializeField]
	private Transform modelRoot;

	private AudioSource audioSource;
	private PawnBase pawnBase;

	private int selectorTeams = 0;
	private bool isSelected = false;
	private bool isHovered = false;
	private bool isHoveredByTile = false;
	private bool isButtonDown = false;

	private void Start() {
		audioSource = GetComponent<AudioSource>();
		if(Application.isPlaying) {
			outlineEnabled = false;
		}
	}

	public void PlayAudio(AudioClip clip) {
		audioSource.PlayOneShot(clip);
	}

	public void SetActor(Actor actor) {
		Actor = actor;

		switch(actor.TeamID) {
		case 1: SetMaterialIndex(PawnMaterialIndex.BlueTeam); break;
		case 2: SetMaterialIndex(PawnMaterialIndex.RedTeam); break;
		default: SetMaterialIndex(PawnMaterialIndex.None); break;
		}
	}

	public void SetBase(PawnBase pawnBase) {
		this.pawnBase = pawnBase;
		this.pawnBase.SetPawn(this);

		if(modelRoot != null) {
			modelRoot.transform.localPosition = new Vector3(0f, pawnBase.Height, 0f);
		}
	}

	public void SetHud(PawnHud pawnHud) {
		Hud = pawnHud;
		Hud.SetTeam(Actor.TeamID);
	}

	private void OnEnable() {
		outlines = GetComponentsInChildren<Outline>(true);
		SetMaterialIndex(materialIndex);
		SetOutlineColorIndex(outlineColorIndex);
		EnableOutline(outlineEnabled);
		if(Application.isPlaying) {
			AddListeners();
		}
	}

	private void OnDisable() {
		if(Application.isPlaying) {
			RemoveListeners();
		}
	}

	public void SetMaterialIndex(PawnMaterialIndex index) {
		materialIndex = index;
		currentMaterialIndex = index;
		if(index == PawnMaterialIndex.None) {
			return;
		}

		PawnMaterialPalette[] materialPalettes = GetComponentsInChildren<PawnMaterialPalette>(true);
		for(int i = 0; i < materialPalettes.Length; ++i) {
			materialPalettes[i].SetMaterialIndex((int)index - 1);
		}
	}

	public void SetOutlineColorIndex(int colorIndex) {
		if(colorIndex < 0) {
			EnableOutline(false);
			return;
		}
		EnableOutline(true);
		outlineColorIndex = colorIndex;
		currentOutlineColorIndex = colorIndex;
		for(int i = 0; i < outlines.Length; ++i) {
			outlines[i].color = colorIndex;
		}
		if(pawnBase != null) {
			pawnBase.SetOutlineColorIndex(colorIndex);
		}
	}

	public void EnableOutline(bool enable) {
		outlineEnabled = enable;
		currentOutlineEnabled = enable;
		for(int i = 0; i < outlines.Length; ++i) {
			outlines[i].enabled = enable;
		}
		if(pawnBase != null) {
			pawnBase.EnableOutline(enable);
		}
	}

	public IEnumerator SpawnSequence(BattleState battleState, AudioClip pawnStepClip) {
		bool forward = true;
		float startHeight = AnimHeight;
		float endHeight = 0f;
		float currentTime = 0f;

		if(!forward) {
			startHeight = 0f;
			endHeight = AnimHeight;
		}
		
		while(currentTime <= AnimDuration) {
			currentTime += Time.deltaTime;
			float percent = currentTime / AnimDuration;
			transform.localPosition = new Vector3(Actor.Position.GridX, Mathf.Lerp(startHeight, endHeight, percent * percent), -Actor.Position.GridY);
			yield return null;
		}
		transform.localPosition = new Vector3(Actor.Position.GridX, endHeight, -Actor.Position.GridY);

		audioSource.PlayOneShot(pawnStepClip);

		Hud.SetValues(
			Actor.CurrentEntity.LocID,
			Actor.CurrentEntity.Stats.Power,
			Actor.CurrentEntity.Stats.Health,
			Actor.CurrentEntity.Stats.MaxHealth,
			Actor.CurrentEntity.Stats.Armor);
		Hud.SetPowerVisible(true);
		Hud.SetHealthVisible(true);

		if(!forward) {
			//gameObject.SetActive(false);
		}
	}

	private void Update() {
		if(materialIndex != currentMaterialIndex || forceUpdate) {
			SetMaterialIndex(materialIndex);
		}

		if(outlineColorIndex != currentOutlineColorIndex || forceUpdate) {
			SetOutlineColorIndex(outlineColorIndex);
		}

		if(outlineEnabled != currentOutlineEnabled || forceUpdate) {
			EnableOutline(outlineEnabled);
		}

		if(forceUpdate) {
			forceUpdate = false;
		}
	}

	private void UpdateOutline() {
		if(!OutlineEffectManager.IsTeamColorEnabled) {
			if(isButtonDown && (isHovered || isHoveredByTile)) {
				SetOutlineColorIndex(OutlineEffectManager.Click());
			}
			else if(isSelected) {
				SetOutlineColorIndex(OutlineEffectManager.Selected());
			}
			else if(isHovered || isHoveredByTile) {
				SetOutlineColorIndex(OutlineEffectManager.Hover());
			}
			else {
				EnableOutline(false);
			}
		}
		else if(isSelected) {
			SetOutlineColorIndex(OutlineEffectManager.Selected());
		}

		Hud.SetNameVisible(isHovered || isHoveredByTile || isSelected);
		Hud.SetPowerHighlighted(isHovered || isHoveredByTile || isSelected);
		Hud.SetHealthHighlighted(isHovered || isHoveredByTile || isSelected);
		Hud.SetActionHighlighted(isHovered || isHoveredByTile || isSelected);
	}

	public void OnMouseEnter() {
		if(UnityEngine.EventSystems.EventSystem.current != null &&
			!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) {
			isHovered = true;
			EventManager.Queue(new PawnHoveredEvent(this, isHovered));
		}
		UpdateOutline();
	}

	public void OnMouseExit() {
		isHovered = false;
		EventManager.Queue(new PawnHoveredEvent(this, isHovered));
		UpdateOutline();
	}

	public void OnMouseOver() {
		if(UnityEngine.EventSystems.EventSystem.current != null &&
			UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) {
			isHovered = false;
		}
		else {
			isHovered = true;
		}
		UpdateOutline();
	}

	public void OnMouseDown() {
		if(UnityEngine.EventSystems.EventSystem.current != null &&
			!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) {
			isButtonDown = true;
		}
		UpdateOutline();
	}

	public void OnMouseUp() {
		if(UnityEngine.EventSystems.EventSystem.current != null &&
			!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() &&
			(isHovered || isHoveredByTile)) {
			// This really is a button press, but maybe we're over the base now or something.
			OnMouseUpAsButton();
		}
		isButtonDown = false;
		UpdateOutline();
	}

	public void OnMouseUpAsButton() {
		// This may have already been handled by OnMouseUp.
		if(isButtonDown == false) {
			return;
		}
		isButtonDown = false;
		if(UnityEngine.EventSystems.EventSystem.current != null &&
			!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() &&
			(isHovered || isHoveredByTile)) {
			EventManager.Queue(new PawnClickedEvent(this));
			UpdateOutline();
		}
	}

	#region Event listeners

	public void AddListeners() {
		EventManager.AddListener<HeroSelectedEvent>(OnHeroSelectedEvent);
		EventManager.AddListener<PawnHoveredEvent>(OnPawnHovered);
		EventManager.AddListener<TileHoveredEvent>(OnTileHovered);
		EventManager.AddListener<OutlinesUpdatedEvent>(OnOutlinesUpdated);

		PresentationManager.RegisterHandler(BattleEventType.TurnStart, OnTurnStart);
		PresentationManager.RegisterHandler(BattleEventType.ActionPerformed, OnActionPerformed);
	}

	public void RemoveListeners() {
		EventManager.RemoveListener<HeroSelectedEvent>(OnHeroSelectedEvent);
		EventManager.RemoveListener<PawnHoveredEvent>(OnPawnHovered);
		EventManager.RemoveListener<TileHoveredEvent>(OnTileHovered);
		EventManager.RemoveListener<OutlinesUpdatedEvent>(OnOutlinesUpdated);

		PresentationManager.UnregisterHandler(BattleEventType.TurnStart, OnTurnStart);
		PresentationManager.UnregisterHandler(BattleEventType.ActionPerformed, OnActionPerformed);

	}

	private void OnHeroSelectedEvent(IEvent evt) {
		HeroSelectedEvent hse = evt as HeroSelectedEvent;

		if(hse.ActorID == Actor.ID) {
			selectorTeams |= hse.SelectorTeamID;
		}
		else {
			selectorTeams &= ~hse.SelectorTeamID;
		}

		isSelected = selectorTeams != 0;

		UpdateOutline();
	}

	private void OnPawnHovered(IEvent evt) {
		PawnHoveredEvent phe = evt as PawnHoveredEvent;
		if(phe.Pawn != this) {
			return;
		}

		UpdateOutline();
	}

	private void OnTileHovered(IEvent evt) {
		TileHoveredEvent the = evt as TileHoveredEvent;
		if(the.Position != Actor.Position) {
			return;
		}

		isHoveredByTile = the.IsHovering;
		UpdateOutline();
	}

	private void OnOutlinesUpdated(IEvent evt) {
		UpdateOutline();
	}

	private IEnumerator OnTurnStart(IBattleEvent battleEvent) {
		TurnStartEvent evt = battleEvent as TurnStartEvent;

		if(evt.TeamID == Actor.TeamID) {
			Hud.SetActionVisible(Actor.CanActMinor || Actor.CanActPrimary, Actor.CanActMinor);
			Hud.SetPowerVisible(true);
		}
		else {
			Hud.SetActionVisible(false, false);
			Hud.SetPowerVisible(Actor.GetCounterAction() != null);
		}
		yield break;
	}

	private IEnumerator OnActionPerformed(IBattleEvent battleEvent) {
		ActionPerformedEvent evt = battleEvent as ActionPerformedEvent;
		if(evt.ActorID == Actor.ID) {
			Hud.SetActionVisible(Actor.CanActMinor || Actor.CanActPrimary, Actor.CanActMinor);
		}
		yield break;
	}

	#endregion

}
