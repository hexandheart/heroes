﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BattleMechanics;
using BattleMechanicsUtils;
using AssetBundles;

public class PawnLoader : MonoBehaviour {

	private static readonly string CommonBaseName = "CommonBase";

	[SerializeField]
	private PawnOverlay pawnOverlayPrefab;

	[SerializeField]
	private PawnHud pawnHudPrefab;

	[SerializeField]
	private bool useStreamingAssetsInEditor;

	[SerializeField]
	private float globalPawnScale;

	public delegate void LoadPawnSucessCallback(Pawn pawn);
	public delegate void LoadPawnErrorCallback(int reasonCode, string message);

	public static void LoadPawn(Actor actor, LoadPawnSucessCallback onSuccess, LoadPawnErrorCallback onError) {
		instance.StartCoroutine(instance.LoadPawnFromAssetBundle(actor, onSuccess, onError));
	}

	public static PawnOverlay LoadPawnOverlay() {
		return Instantiate(instance.pawnOverlayPrefab);
	}

	#region Privates
	private static PawnLoader instance;

	private void Awake() {
		instance = this;
		StartCoroutine(Initialize());
	}

	private IEnumerator Initialize() {
		if(Application.isEditor && useStreamingAssetsInEditor) {
			AssetBundleManager.SetSourceAssetBundleURL(Application.dataPath + "/StreamingAssets/");
		}
		else {
			AssetBundleManager.SetSourceAssetBundleURL("http://hexandheart.com/ht/dev/");
		}

		var request = AssetBundleManager.Initialize();
		if(request != null) {
			yield return StartCoroutine(request);
		}
	}

	private IEnumerator LoadPawnInternal(Actor actor, LoadPawnSucessCallback onSuccess, LoadPawnErrorCallback onError) {
		var request = Resources.LoadAsync<Pawn>("Pawn/" + actor.CurrentEntity.AssetID);
		yield return request;

		if(request.asset == null) {
			// Load default!
			request = Resources.LoadAsync<Pawn>("Pawn/default");
			yield return request;

			if(request.asset == null) {
				// Still nothing? Wow that's super bad!
				if(onError != null) {
					onError((int)HeroService.ReasonCode.INVALID_JOB_ID, "No asset for job asset ID: " + actor.CurrentEntity.AssetID);
					yield break;
				}
			}
		}

		Pawn pawnPrefab = request.asset as Pawn;

		request = Resources.LoadAsync<PawnBase>("PawnBase/" + CommonBaseName);
		yield return request;

		if(request.asset == null) {
			if(onError != null) {
				onError((int)HeroService.ReasonCode.INVALID_JOB_ID, "No base for job asset ID: " + actor.CurrentEntity.AssetID);
				yield break;
			}
		}

		PawnBase basePrefab = request.asset as PawnBase;

		if(onSuccess != null) {
			// Instantiate the pawn!
			string pawnName = string.Format("Pawn {0}:{1} (Team {2})", actor.ID, actor.CurrentEntity.AssetID, actor.TeamID);
			onSuccess(InstantiatePawn(pawnName, actor, pawnPrefab, basePrefab));
		}
	}

	private IEnumerator LoadPawnFromAssetBundle(Actor actor, LoadPawnSucessCallback onSuccess, LoadPawnErrorCallback onError) {
		var request = AssetBundleManager.LoadAssetAsync("pawns", actor.CurrentEntity.AssetID, typeof(GameObject));
		if(request == null) {
			onError?.Invoke((int)HeroService.ReasonCode.INVALID_JOB_ID, "No asset for job asset ID: " + actor.CurrentEntity.AssetID);
			yield break;
		}
		yield return StartCoroutine(request);
		Pawn pawnPrefab = null;
		if(request.GetAsset<GameObject>() == null) {
			// Load default!
			var resourceRequest = Resources.LoadAsync<Pawn>("Pawn/default");
			yield return resourceRequest;

			if(resourceRequest.asset == null) {
				// Still nothing? Wow that's super bad!
				if(onError != null) {
					onError((int)HeroService.ReasonCode.INVALID_JOB_ID, "No asset for job asset ID: " + actor.CurrentEntity.AssetID);
					yield break;
				}
			}

			pawnPrefab = resourceRequest.asset as Pawn;
		}
		else {
			pawnPrefab = request.GetAsset<GameObject>().GetComponent<Pawn>();
		}

		var baseRequest = Resources.LoadAsync<PawnBase>("PawnBase/" + CommonBaseName);
		yield return request;

		if(baseRequest.asset == null) {
			if(onError != null) {
				onError((int)HeroService.ReasonCode.INVALID_JOB_ID, "No base for job asset ID: " + actor.CurrentEntity.AssetID);
				yield break;
			}
		}

		PawnBase basePrefab = baseRequest.asset as PawnBase;

		if(onSuccess != null) {
			// Instantiate the pawn!
			string pawnName = string.Format("Pawn {0}:{1} (Team {2})", actor.ID, actor.CurrentEntity.AssetID, actor.TeamID);
			onSuccess(InstantiatePawn(pawnName, actor, pawnPrefab, basePrefab));
		}
	}

	private Pawn InstantiatePawn(string name, Actor actor, Pawn prefab, PawnBase basePrefab) {
		Pawn pawn = Instantiate(prefab);
		pawn.name = name;

		Vector3 spawnPosition = actor.Position.ToVector3();
		spawnPosition.y = -10000f;
		pawn.transform.localPosition = spawnPosition;
		pawn.transform.localRotation = actor.Facing.ToRotation();
		pawn.transform.localScale = Vector3.one * globalPawnScale;
		pawn.SetActor(actor);

		PawnBase pawnBase = Instantiate(basePrefab, pawn.transform);
		pawn.SetBase(pawnBase);

		// Pawn hud!
		pawn.SetHud(Instantiate(pawnHudPrefab, pawn.transform));

		return pawn;
	}
	#endregion

}
