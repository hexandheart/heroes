﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnMaterialPalette : MonoBehaviour {

	[SerializeField]
	private Material[] materialPalette = new Material[0];

	[SerializeField]
	private Renderer[] renderers = new Renderer[0];

	public void SetMaterialIndex(int index) {
		if(materialPalette.Length == 0) {
			return;
		}
		if(index < 0 || index >= materialPalette.Length) {
			index = 0;
		}

		for(int i = 0; i < renderers.Length; ++i) {
			renderers[i].sharedMaterial = materialPalette[index];
		}
	}

}
