﻿using UnityEngine;
using UnityEngine.UI;

public class PawnHudHitIndicator : MonoBehaviour {

	[SerializeField]
	private SuperTextMesh hitValueLabel;

	[SerializeField]
	private GameObject armorBase;
	[SerializeField]
	private SuperTextMesh armorValueLabel;

	private Animator animator;

	private void Awake() {
		animator = GetComponent<Animator>();
		hitValueLabel.Text = "";
	}

	public void ShowHit(int potency, int armor) {
		if(armor > 0) {
			armorValueLabel.Text = armor.ToString();
			armorBase.gameObject.SetActive(true);
		}
		else {
			armorBase.gameObject.SetActive(false);
		}
		hitValueLabel.Text = potency.ToString("+#;-#;0");
		animator.SetTrigger("ShowHit");
	}

	public void ShowHeal(int potency) {
		armorBase.gameObject.SetActive(false);
		hitValueLabel.Text = potency.ToString("+#;-#;0");
		animator.SetTrigger("ShowHeal");
	}

	public void ShowDodge() {
		armorBase.gameObject.SetActive(false);
		hitValueLabel.Text = "DODGE!";
		animator.SetTrigger("ShowDodge");
	}

	public void ShowAction(string actionName) {
		armorBase.gameObject.SetActive(false);
		hitValueLabel.Text = actionName;
		animator.SetTrigger("ShowDodge");
	}

}
