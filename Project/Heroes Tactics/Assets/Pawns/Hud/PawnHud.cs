﻿using Arena;
using UnityEngine;
using UnityEngine.UI;

public class PawnHud : MonoBehaviour {

	[SerializeField]
	private PawnHudIndicator powerIndicator;
	[SerializeField]
	private PawnHudIndicator healthIndicator;
	[SerializeField]
	private PawnHudActionIndicator actionIndicator;
	[SerializeField]
	private PawnHudHitIndicator hitIndicator;

	[SerializeField]
	private GameObject armorIndicator;
	[SerializeField]
	private SuperTextMesh armorLabel;

	[SerializeField]
	private PawnHudNameIndicator nameIndicator;

	[SerializeField]
	private GameObject[] overlayAdjustingObjects;

	public void SetTeam(int teamId) {
		actionIndicator.SetBannerColor(PresentationManager.GetTeamColor(teamId));
	}

	public void SetValues(string name, int power, int health, int maxHealth, int armor) {
		nameIndicator.SetName(name);
		powerIndicator.SetValue(power);
		healthIndicator.SetValuesWithColor(health, maxHealth);
		if(armor == 0) {
			armorIndicator.SetActive(false);
		}
		else {
			armorLabel.Text = armor.ToString();
			armorIndicator.SetActive(true);
		}
	}

	public void SetNameVisible(bool isVisible) {
		nameIndicator.Show(isVisible);
	}

	public void SetPowerVisible(bool isVisible) {
		powerIndicator.IsVisible = isVisible;
		UpdateLayer();
	}

	public void SetPowerHighlighted(bool isHighlighted) {
		powerIndicator.IsHighlighted = isHighlighted;
		UpdateLayer();
	}

	public void SetHealthVisible(bool isVisible) {
		healthIndicator.IsVisible = isVisible;
		UpdateLayer();
	}

	public void SetHealthHighlighted(bool isHighlighted) {
		healthIndicator.IsHighlighted = isHighlighted;
		UpdateLayer();
	}

	public void SetActionVisible(bool isVisible, bool isIconVisible) {
		actionIndicator.IsVisible = isVisible;
		actionIndicator.IsIconVisible = isIconVisible;
		UpdateLayer();
	}

	public void SetActionHighlighted(bool isHighlighted) {
		actionIndicator.IsHighlighted = isHighlighted;
		UpdateLayer();
	}

	public void ShowHit(int potency, int armor) {
		hitIndicator.ShowHit(potency, armor);
	}

	public void ShowHeal(int potency) {
		hitIndicator.ShowHeal(potency);
	}

	public void ShowDodge() {
		hitIndicator.ShowDodge();
	}

	public void ShowAction(string actionName) {
		hitIndicator.ShowAction(actionName);
	}

	private void UpdateLayer() {
		bool isOverlay =
			powerIndicator.IsHighlighted ||
			healthIndicator.IsHighlighted ||
			actionIndicator.IsHighlighted;
		int layer = LayerMask.NameToLayer("Default");

		if(isOverlay) {
			layer = LayerMask.NameToLayer("PawnOverlay");
		}

		foreach(var go in overlayAdjustingObjects) {
			foreach(Transform t in go.transform.GetComponentsInChildren<Transform>(true)) {
				t.gameObject.layer = layer;
			}
		}
	}

}
