﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnHudNameIndicator : MonoBehaviour {

	[SerializeField]
	private SuperTextMesh nameLabel;

	private Animator animator;

	private void Start() {
		animator = GetComponent<Animator>();
	}

	public void SetName(string name) {
		nameLabel.Text = name;
	}

	public void Show(bool show) {
		animator.SetBool("Show", show);
	}

}
