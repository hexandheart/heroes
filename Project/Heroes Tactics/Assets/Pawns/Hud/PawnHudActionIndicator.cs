﻿using UnityEngine;
using UnityEngine.UI;

public class PawnHudActionIndicator : PawnHudIndicator {

	[SerializeField]
	private Image[] bannerParts;

	public void SetBannerColor(Color color) {
		for(int i = 0; i < bannerParts.Length; ++i) {
			bannerParts[i].color = color;
		}
	}

}
