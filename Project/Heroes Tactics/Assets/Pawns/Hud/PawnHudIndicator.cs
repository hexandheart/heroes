﻿using UnityEngine;

public class PawnHudIndicator : MonoBehaviour {

	public bool IsVisible {
		get {
			return animator.GetBool("Visible");
		}
		set {
			animator.SetBool("Visible", value);
		}
	}

	public bool IsHighlighted {
		get {
			return animator.GetBool("Highlighted");
		}
		set {
			animator.SetBool("Highlighted", value);
		}
	}

	public bool IsIconVisible {
		get {
			return animator.GetBool("IconVisible");
		}
		set {
			animator.SetBool("IconVisible", value);
		}
	}

	public void SetValue(int current) {
		valueLabel.Text = current.ToString();
		if(maxValueLabel != null) {
			maxValueLabel.Text = "";
		}
	}

	public void SetValuesWithColor(int current, int max) {
		Color color = Color.white;
		if(current < max / 2) {
			// Critical health!
			color = new Color(1f, 0.75f, 0.5f);
		}
		else if(current < max) {
			color = new Color(1f, 1f, 0f);
		}
		valueLabel.Text = string.Format("<c={0}>{1}</c>", ColorUtility.ToHtmlStringRGB(color), current);
		if(maxValueLabel != null) {
			maxValueLabel.Text = string.Format("/ {0}", max);
		}
	}

	[SerializeField]
	private SuperTextMesh valueLabel;
	[SerializeField]
	private SuperTextMesh maxValueLabel;

	private Animator animator;

	private void Awake() {
		animator = GetComponent<Animator>();
		if(valueLabel != null) {
			valueLabel.Text = "-";
		}
		if(maxValueLabel != null) {
			maxValueLabel.Text = "/ -";
		}
		IsVisible = false;
		IsHighlighted = false;
		IsIconVisible = false;
	}

}
