﻿using UnityEngine;

public enum BillboardMode {
	Flat,
	Vertical,
}

public class Billboard : MonoBehaviour {

	[SerializeField]
	private BillboardMode mode = BillboardMode.Flat;

	private void LateUpdate() {
		Camera main = Camera.main;
		if(main == null) {
			return;
		}

		if(mode == BillboardMode.Flat) {
			transform.LookAt(transform.position + main.transform.rotation * Vector3.forward, main.transform.rotation * Vector3.up);
		}
		else {
			Vector3 target = main.transform.position - main.transform.rotation * Vector3.forward;
			target.y = 0f;

			transform.LookAt(transform.position - target);
		}
	}

}
