﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

public class PawnBase : MonoBehaviour {

	[SerializeField]
	private bool forceUpdate = false;

	[SerializeField]
	private int outlineColorIndex = 0;
	private int currentOutlineColorIndex = 0;

	private Outline[] outlines;

	[SerializeField]
	private bool outlineEnabled = false;
	private bool currentOutlineEnabled = false;

	[SerializeField]
	private float height;

	private Pawn pawn;

	public float Height { get { return height; } }

	public void SetPawn(Pawn pawn) {
		this.pawn = pawn;
	}

	private void OnEnable() {
		outlines = GetComponentsInChildren<Outline>(true);
		SetOutlineColorIndex(outlineColorIndex);
		EnableOutline(outlineEnabled);
	}

	public void SetOutlineColorIndex(int colorIndex) {
		outlineColorIndex = colorIndex;
		currentOutlineColorIndex = colorIndex;
		for(int i = 0; i < outlines.Length; ++i) {
			outlines[i].color = colorIndex;
		}
	}

	public void EnableOutline(bool enable) {
		outlineEnabled = enable;
		currentOutlineEnabled = enable;
		for(int i = 0; i < outlines.Length; ++i) {
			outlines[i].enabled = enable;
		}
	}

	private void Update() {
		if(outlineColorIndex != currentOutlineColorIndex || forceUpdate) {
			SetOutlineColorIndex(outlineColorIndex);
		}

		if(outlineEnabled != currentOutlineEnabled || forceUpdate) {
			EnableOutline(outlineEnabled);
		}

		if(forceUpdate) {
			forceUpdate = false;
		}
	}

	public void OnMouseEnter() {
		if(pawn == null) {
			return;
		}
		pawn.OnMouseEnter();
	}

	public void OnMouseExit() {
		if(pawn == null) {
			return;
		}
		pawn.OnMouseExit();
	}

	public void OnMouseOver() {
		if(pawn == null) {
			return;
		}
		pawn.OnMouseOver();
	}

	public void OnMouseDown() {
		if(pawn == null) {
			return;
		}
		pawn.OnMouseDown();
	}

	public void OnMouseUp() {
		if(pawn == null) {
			return;
		}
		pawn.OnMouseUp();
	}

	public void OnMouseUpAsButton() {
		if(pawn == null) {
			return;
		}
		pawn.OnMouseUpAsButton();
	}

}
