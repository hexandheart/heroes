﻿using System;
using System.Collections.Generic;
using BattleMechanics;
using LitJson;

namespace HeroesTacticsPlugins {

	internal static class EntityExtensions {

		public static Entity FromJson(JsonData data) {
			string id = (string)data["heroId"];
			string assetId = (string)data["assetId"];
			string locId = data.Keys.Contains("locId") ? (string)data["locId"] : assetId;
			EntityType type = data.Keys.Contains("type") ?
				(EntityType)Enum.Parse(typeof(EntityType), (string)data["type"], true) :
				EntityType.Hero;
			Stats stats = StatsFromJson(data["stats"]);

			BattleActionMap actionMap = null;
			if(data.Keys.Contains("actionMap")) {
				actionMap = BattleActionMapFromJson(data["actionMap"]);
			}
			else {
				actionMap = BattleActionMap.Default;
			}
			
			return new Entity(
				id, assetId, locId,
				type,
				stats,
				actionMap);
		}

		public static Stats StatsFromJson(JsonData data) {
			int maxHealth = (int)data["health"];
			int power = (int)data["power"];
			int armor = (int)data["armor"];

			int dodge = (int)data["dodge"];
			int step = (int)data["step"];

			int maxPrimaryActions = 1;
			if(data.Keys.Contains("maxPrimaryActions")) {
				maxPrimaryActions = (int)data["maxPrimaryActions"];
			}
			int maxMinorActions = 1;
			if(data.Keys.Contains("maxMinorActions")) {
				maxMinorActions = (int)data["maxMinorActions"];
			}

			return new Stats(maxHealth, maxHealth,
				power,
				armor, dodge,
				step,
				maxPrimaryActions, 0,
				maxMinorActions, 0);
		}

		public static BattleActionTargeting BattleActionTargetingFromJson(JsonData data) {
			int rangeMin = data.Keys.Contains("rangeMin") ? (int)data["rangeMin"] : 1;
			int rangeMax = data.Keys.Contains("rangeMax") ? (int)data["rangeMax"] : 1;

			bool requireLos = data.Keys.Contains("requireLos") ? (bool)data["requireLos"] : true;
			bool requirePath = data.Keys.Contains("requirePath") ? (bool)data["requirePath"] : false;

			bool canTargetActor = data.Keys.Contains("canTargetActor") ? (bool)data["canTargetActor"] : true;
			bool canTargetSpace = data.Keys.Contains("canTargetSpace") ? (bool)data["canTargetSpace"] : false;
			bool canTargetNonWalkableTile = data.Keys.Contains("canTargetNonWalkableTile") ? (bool)data["canTargetNonWalkableTile"] : false;

			return new BattleActionTargeting(
				rangeMin, rangeMax,
				requireLos, requirePath,
				canTargetActor, canTargetSpace, canTargetNonWalkableTile);
		}

		public static BattleActionCost BattleActionCostFromJson(JsonData data) {
			int ap = data.Keys.Contains("ap") ? (int)data["ap"] : 0;
			int primary = data.Keys.Contains("primary") ? (int)data["primary"] : 1;
			int minor = data.Keys.Contains("minor") ? (int)data["minor"] : 0;

			return new BattleActionCost(
				ap,
				primary, minor);
		}

		public static BattleActionMovementEffect BattleActionMovementEffectFromJson(JsonData data) {
			return new BattleActionMovementEffect();
		}

		public static BattleActionHealthEffect BattleActionHealthEffectFromJson(JsonData data) {
			EvasionType evasionType = data.Keys.Contains("evasion") ?
				(EvasionType)Enum.Parse(typeof(EvasionType), (string)data["evasion"], true) :
				EvasionType.None;
			MitigationType mitigationType = data.Keys.Contains("mitigation") ?
				(MitigationType)Enum.Parse(typeof(MitigationType), (string)data["mitigation"], true) :
				MitigationType.Normal;

			JsonData potencyData = data["potency"];
			BattleActionEffectType potencyType = potencyData.Keys.Contains("type") ?
				(BattleActionEffectType)Enum.Parse(typeof(BattleActionEffectType), (string)potencyData["type"], true) :
				BattleActionEffectType.Harm;
			int potencyValue = potencyData.Keys.Contains("value") ? (int)potencyData["value"] : 0;
			bool potencyIncludePower = potencyData.Keys.Contains("includePower") ? (bool)potencyData["includePower"] : false;

			return new BattleActionHealthEffect(
				evasionType, mitigationType,
				potencyType, potencyValue, potencyIncludePower);
		}

		public static BattleActionArea BattleActionAreaFromJson(JsonData data) {
			bool alignToFacing = data.Keys.Contains("alignToFacing") ? (bool)data["alignToFacing"] : false;

			// Grid showing affected tiles.
			//   t = Selected target, tile not affected
			//   T = Selected target, tile affected
			//   . = Not affected
			//   Any other character = Affected

			// "..x..",
			// ".xxx.", /* ---> */
			// "xxTxx", /* ---> alignToFacing direction */
			// ".xxx.", /* ---> */
			// "..x..",

			// We'll loop through each item in this grid of characters. We'll start
			// by treating the target tile as 0,0 and if/when we find the actual target
			// we can loop back through and readjust the tiles we've already read.
			List<GridVector> shape = new List<GridVector>();
			JsonData shapeData = data["shape"];
			GridVector target = GridVector.Zero;
			for(int y = 0; y < shapeData.Count; ++y) {
				string shapeRow = (string)shapeData[y];
				for(int x = 0; x < shapeRow.Length; ++x) {
					// Target?
					if(shapeRow[x] == 'T' || shapeRow[x] == 't') {
						target = new GridVector(x, y);
						// Transform what we have already.
						for(int i = 0; i < shape.Count; ++i) {
							shape[i] = shape[i] - target;
						}
					}

					// Affected?
					if(shapeRow[x] == 'T' || shapeRow[x] != '.') {
						shape.Add(new GridVector(x - target.GridX, y - target.GridY));
					}
				}
			}

			return new BattleActionArea(
				alignToFacing,
				shape.ToArray());
		}

		public static BattleActionSummon BattleActionSummonFromJson(JsonData data) {
			string heroId = (string)data["heroId"];

			return new BattleActionSummon(heroId, BattleActionSummon.InactiveID);
		}

		public static BattleActionCounter BattleActionCounterFromJson(JsonData data) {
			CounterType type = data.Keys.Contains("type") ?
				(CounterType)Enum.Parse(typeof(CounterType), (string)data["type"], true) :
				CounterType.Standard;

			return new BattleActionCounter(type);
		}

		public static BattleAction BattleActionFromJson(BattleActionGroupType actionGroup, int actionSlot, JsonData data) {
			if(data == null) {
				return null;
			}

			string id = data.Keys.Contains("id") ?
				(string)data["id"] :
				string.Empty;
			string locId = data.Keys.Contains("locId") ?
				(string)data["locId"] :
				string.Empty;
			string fxId = data.Keys.Contains("fxId") ?
				(string)data["fxId"] :
				string.Empty;
			CounterType counterableType = data.Keys.Contains("counterableType") ?
				(CounterType)Enum.Parse(typeof(CounterType), (string)data["counterableType"], true) :
				CounterType.None;

			BattleActionTargeting targeting = BattleActionTargetingFromJson(data["targeting"]);
			BattleActionCost cost = BattleActionCostFromJson(data["cost"]);

			BattleActionMovementEffect movementEffect = data.Keys.Contains("movementEffect") ?
				BattleActionMovementEffectFromJson(data["movementEffect"]) :
				null;
			BattleActionHealthEffect healthEffect = data.Keys.Contains("healthEffect") ?
				BattleActionHealthEffectFromJson(data["healthEffect"]) :
				null;
			BattleActionArea area = data.Keys.Contains("area") ?
				BattleActionAreaFromJson(data["area"]) :
				null;
			BattleActionSummon summon = data.Keys.Contains("summon") ?
				BattleActionSummonFromJson(data["summon"]) :
				null;
			BattleActionCounter counter = data.Keys.Contains("counter") ?
				BattleActionCounterFromJson(data["counter"]) :
				null;

			return new BattleAction(
				actionGroup, actionSlot,
				id, locId, fxId, counterableType,
				targeting, cost,
				movementEffect,
				healthEffect,
				area,
				summon,
				counter);
		}

		public static BattleActionGroup BattleActionGroupFromJson(JsonData data) {
			BattleActionGroupType groupType = (BattleActionGroupType)Enum.Parse(typeof(BattleActionGroupType), (string)data["groupType"], true);
			int numSlots = (int)data["numSlots"];

			Dictionary<int, BattleAction> actions = new Dictionary<int, BattleAction>();
			JsonData actionsData = data["actions"];
			for(int i = 0; i < actionsData.Count; ++i) {
				int slot = (int)actionsData[i]["slot"];
				actions[slot] = BattleActionFromJson(groupType, slot, actionsData[i]["action"]);
			}

			return new BattleActionGroup(
				groupType, numSlots,
				actions);
		}

		public static BattleActionMap BattleActionMapFromJson(JsonData data) {
			BattleAction counterAction = data.Keys.Contains("counter") ?
				BattleActionFromJson(BattleActionGroupType.None, 0, data["counter"]) :
				null;

			JsonData groupsData = data["actionGroups"];
			List<BattleActionGroup> actionGroups = new List<BattleActionGroup>(groupsData.Count);
			for(int i = 0; i < groupsData.Count; ++i) {
				actionGroups.Add(BattleActionGroupFromJson(groupsData[i]));
			}

			return new BattleActionMap(
				counterAction,
				actionGroups);
		}

	}

}
