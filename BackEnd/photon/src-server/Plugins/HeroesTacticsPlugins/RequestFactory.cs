﻿using System.Collections.Generic;
using BattleMechanics.Requests;
using Photon.Hive.Plugin;

namespace HeroesTacticsPlugins {

	internal static class RequestFactory {

		public static IRequest FromEvent(IRaiseEventRequest request) {
			byte eventCode = request.EvCode;
			Dictionary<byte, object> data = (Dictionary<byte, object>)request.Data;

			switch((RequestType)eventCode) {

			case RequestType.EndTurn: return EndTurnRequest.FromData(data);
			case RequestType.ConcedeMatch: return ConcedeMatchRequest.FromData(data);

			case RequestType.UpdateBreeze: return UpdateBreezeRequest.FromData(data);

			case RequestType.PerformAction: return PerformActionRequest.FromData(data);

			}

			return null;
		}

	}

}
