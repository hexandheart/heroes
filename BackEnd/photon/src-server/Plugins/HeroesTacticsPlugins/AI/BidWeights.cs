﻿using LitJson;
using System.Collections.Generic;

namespace HeroesTacticsPlugins.AI {

	internal enum BidWeightType {
		Victory,
		Defeat,
		EnemyKO,
		AllyKO,
		EnemyDamage,
		AllyDamage,
		EnemyHeal,
		AllyHeal,
		AllyHealCritical,
		Summon,
	}

	/// <summary>
	/// Storage for the bidWeights configuration from the battle details.
	/// </summary>
	internal class BidWeights {

		/// <summary>
		/// Read the BidWeights from the provided JSON.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static BidWeights FromJson(JsonData data) {
			Dictionary<BidWeightType, int> weights = new Dictionary<BidWeightType, int>();
			
			foreach(string key in data.Keys) {
				if(!System.Enum.TryParse(key, true, out BidWeightType type)) {
					// Should probably error?
					continue;
				}
				weights[type] = (int)data[key];
			}

			return new BidWeights(weights);
		}

		/// <summary>
		/// Get the weight for the specified weight type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public int GetWeight(BidWeightType type) {
			return weights[type];
		}
		
		/*******************************************************************************/
		#region Privates

		private Dictionary<BidWeightType, int> weights;

		private BidWeights(Dictionary<BidWeightType, int> weights) {
			this.weights = weights;
		}

		#endregion

	}

}
