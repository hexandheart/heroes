﻿using BattleMechanics;

namespace HeroesTacticsPlugins.AI {

	/// <summary>
	/// Part of a Bid that describes an action to take.
	/// </summary>
	internal class BidAction {

		/// <summary>
		/// The Actor to perform the action.
		/// </summary>
		public Actor Actor { get; private set; }

		/// <summary>
		/// The action to perform.
		/// </summary>
		public BattleAction Action { get; private set; }

		/// <summary>
		/// The action's target.
		/// </summary>
		public GridVector Target { get; private set; }

		/// <summary>
		/// The facing of the target.
		/// </summary>
		public GridFacing TargetFacing { get; private set; }

		/// <summary>
		/// The weight of this action.
		/// </summary>
		public int Weight { get; private set; }

		/*******************************************************************************/
		#region Constructor

		public BidAction(Actor actor, BattleAction action, GridVector target, GridFacing targetFacing, int weight) {
			Actor = actor;
			Action = action;
			Target = target;
			TargetFacing = targetFacing;
			Weight = weight;
		}

		#endregion

	}

}
