﻿using BattleMechanics;
using System.Collections.Generic;

namespace HeroesTacticsPlugins.AI {

	/// <summary>
	/// Brain logic for BrainType.Brawler.
	/// </summary>
	internal class BrawlerBrain : IBrain {

		/*******************************************************************************/
		#region IBrain implementation

		/// <summary>
		/// Constructor. Doesn't use additional parameters.
		/// </summary>
		/// <param name="brainParams"></param>
		/// <param name="myActor"></param>
		public BrawlerBrain(Dictionary<string, object> brainParams, Actor myActor) : base(brainParams, myActor) {
			// No additional parameters.
		}

		/// <summary>
		/// Prepare a potential pair of bids:
		/// - Move towards enemies.
		/// - Perform a primary action.
		/// </summary>
		/// <param name="bidWeights"></param>
		/// <param name="battleState"></param>
		/// <returns></returns>
		public override Bid RequestBid(BidWeights bidWeights, BattleState battleState) {
			// Can we act?
			if(!MyActor.IsAlive || !MyActor.CanActMinor && !MyActor.CanActPrimary) {
				return null;
			}

			Bid bid = new Bid();
			PrepareChampionBid(bid, bidWeights, battleState);
			CalculateMovementBid(bid, bidWeights, battleState);
			CalculatePrimaryBid(bid, bidWeights, battleState);
			return bid;
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		/// <summary>
		/// Calculates the movement portion of the Brawler's bid. Brawler wants to move close to enemies.
		/// </summary>
		/// <param name="bidWeights"></param>
		/// <param name="battleState"></param>
		/// <returns></returns>
		private void CalculateMovementBid(Bid bid, BidWeights bidWeights, BattleState battleState) {
			BattleAction action = MyActor.GetDefaultMovementAction();
			if(action == null || !MyActor.CanAffordAction(action)) {
				return;
			}

			TargetSolver solver = new TargetSolver(battleState);
			solver.PrepareForAction(MyActor, action);

			// Find a place to go.
			List<GridVector> validMoves = new List<GridVector>();
			for(int y = 0; y < battleState.Map.GridHeight; ++y) {
				for(int x = 0; x < battleState.Map.GridWidth; ++x) {
					GridVector target = new GridVector(x, y);
					TargetSolverResult result = solver.GetResultAtPosition(target);
					if(result.ResultType == TargetSolverResultType.Valid) {
						validMoves.Add(target);
					}
				}
			}

			// If we didn't find a valid move, we'll just do nothing and that's fine.
			BidAction bidAction = null;
			if(validMoves.Count > 0) {
				// Include our current tile, because we might already be in a good place!
				validMoves.Add(MyActor.Position);
				// Movement heuristic!
				FindPreferredMovementTarget(
					battleState, validMoves,
					out GridVector preferredTarget, out GridFacing preferredFacing);

				if(preferredTarget != MyActor.Position) {
					bidAction = new BidAction(
						MyActor, action,
						preferredTarget, preferredFacing,
						0); // No weight to movement.
				}
			}

			bid.AddAction(bidAction);
		}

		/// <summary>
		/// Finds the preferred target from a list of valid moves.
		/// </summary>
		/// <param name="battleState"></param>
		/// <param name="testTargets"></param>
		/// <param name="preferredTarget"></param>
		/// <param name="preferredFacing"></param>
		private void FindPreferredMovementTarget(BattleState battleState, List<GridVector> testTargets, out GridVector preferredTarget, out GridFacing preferredFacing) {
			int[] rangeToEnemy = new int[testTargets.Count];
			for(int i = 0; i < testTargets.Count; ++i) {
				rangeToEnemy[i] = int.MaxValue;
			}

			int shortestRange = int.MaxValue;
			List<int> shortestRangeIndices = new List<int>();
			foreach(var actor in battleState.GetAllActors()) {
				// We don't care about our teammates.
				if(actor.TeamID == MyActor.TeamID) {
					continue;
				}

				// If this actor is closer to any of these targets than what's currently stored, track it.
				for(int testIndex = 0; testIndex < testTargets.Count; ++testIndex) {
					int range = (actor.Position - testTargets[testIndex]).Magnitude();

					if(range < rangeToEnemy[testIndex]) {
						rangeToEnemy[testIndex] = range;

						if(range == shortestRange) {
							shortestRangeIndices.Add(testIndex);
						}
						else if(range < shortestRange) {
							shortestRange = range;
							shortestRangeIndices.Clear();
							shortestRangeIndices.Add(testIndex);
						}
					}
				}
			}

			// If we didn't find anything, drop out!
			if(shortestRangeIndices.Count == 0) {
				preferredTarget = GridVector.Zero;
				preferredFacing = MyActor.Facing;
				return;
			}

			// shortestRange is now filled with the indices of what we'd consider to be good
			// moves from our BrawlerBrain standpoint. Pick one at random.
			preferredTarget = testTargets[shortestRangeIndices[rng.Next(shortestRangeIndices.Count)]];
			preferredFacing = (preferredTarget - MyActor.Position).GetFacing();
		}

		/// <summary>
		/// Calculates the primary action portion of a bid. Brawler wants to brawl!
		/// </summary>
		/// <param name="bidWeights"></param>
		/// <param name="battleState"></param>
		/// <returns></returns>
		private void CalculatePrimaryBid(Bid bid, BidWeights bidWeights, BattleState battleState) {
			// Pick a random action for now.
			// Override battle state team AP for current bid.
			int oldAP = MyActor.Team.AP;
			MyActor.Team.AP -= bid.GetAPCost();
			List<BattleAction> primaryActions = MyActor.FindAffordablePrimaryActions();
			MyActor.Team.AP = oldAP;
			if(primaryActions.Count == 0) {
				return;
			}
			BattleAction action = primaryActions[rng.Next(primaryActions.Count)];

			TargetSolver solver = new TargetSolver(battleState);
			solver.PrepareForAction(MyActor, action);

			// Find a valid target.
			List<TargetSolverResult> validTargets = new List<TargetSolverResult>();
			for(int y = 0; y < battleState.Map.GridHeight; ++y) {
				for(int x = 0; x < battleState.Map.GridWidth; ++x) {
					GridVector target = new GridVector(x, y);
					TargetSolverResult result = solver.GetResultAtPosition(target);
					if(result.ResultType == TargetSolverResultType.Valid) {
						validTargets.Add(result);
					}
				}
			}

			// If we didn't find a valid action, we'll just do nothing and that's fine.
			BidAction bidAction = null;
			if(validTargets.Count > 0) {
				FindPreferredActionTarget(
					bidWeights,
					battleState,
					action,
					validTargets,
					out GridVector preferredTarget, out GridFacing preferredFacing,
					out int weight);

				if(weight > 0) {
					bidAction = new BidAction(
						MyActor, action,
						preferredTarget, preferredFacing,
						weight);
				}
			}

			bid.AddAction(bidAction);
		}

		/// <summary>
		/// Finds the preferred primary action target from a list of valid target.
		/// </summary>
		/// <param name="battleState"></param>
		/// <param name="testResults"></param>
		/// <param name="preferredTarget"></param>
		/// <param name="preferredFacing"></param>
		private void FindPreferredActionTarget(BidWeights bidWeights, BattleState battleState, BattleAction action, List<TargetSolverResult> testResults, out GridVector preferredTarget, out GridFacing preferredFacing, out int weight) {
			int[] weightOfTarget = new int[testResults.Count];
			for(int i = 0; i < testResults.Count; ++i) {
				weightOfTarget[i] = int.MaxValue;
			}
			
			// We will end up ignoring any action that has weight <= 0.
			int highestWeight = 1;
			List<int> highestWeightIndices = new List<int>();

			// This is probably super expensive!

			// Loop through each of the test targets...
			for(int testIndex = 0; testIndex < testResults.Count; ++testIndex) {
				int testTargetWeight = 0;
				// ... and through each of the affected tiles at that target ...
				foreach(var affectedTarget in testResults[testIndex].AffectedTiles) {
					// Is there an actor here?
					Actor target = battleState.GetActorAtPosition(affectedTarget);
					if(target == null) {
						continue;
					}

					// Check for each of the BidWeights!
					action.GetExpectedPotency(MyActor, target, out int expectedPotency, out int expectedEvasion);
					
					// Victory/Defeat
					// TODO!!!
					
					// Enemy checks
					if(target.TeamID != MyActor.TeamID) {
						// Hurt
						if(expectedPotency < 0) {
							expectedPotency = -expectedPotency;
							// EnemyKO
							if(expectedPotency >= target.CurrentEntity.Stats.Health) {
								testTargetWeight += bidWeights.GetWeight(BidWeightType.EnemyKO);
							}
							// EnemyDamage
							else {
								testTargetWeight += bidWeights.GetWeight(BidWeightType.EnemyDamage) * System.Math.Min(expectedPotency, target.CurrentEntity.Stats.Health);
							}
						}
						// EnemyHeal
						else if(expectedPotency > 0) {
							// Don't count overhealing.
							int healingAmount = target.CurrentEntity.Stats.MaxHealth - System.Math.Min(target.CurrentEntity.Stats.Health + expectedPotency, target.CurrentEntity.Stats.MaxHealth);
							if(healingAmount > 0) {
								testTargetWeight += bidWeights.GetWeight(BidWeightType.EnemyHeal) * healingAmount;
							}
						}
					}
					// Ally checks
					else if(target.TeamID == MyActor.TeamID) {
						// Hurt
						if(expectedPotency < 0) {
							expectedPotency = -expectedPotency;
							// AllyKO
							if(expectedPotency >= target.CurrentEntity.Stats.Health) {
								testTargetWeight += bidWeights.GetWeight(BidWeightType.AllyKO);
							}
							// AllyDamage
							else {
								testTargetWeight += bidWeights.GetWeight(BidWeightType.AllyDamage) * System.Math.Min(expectedPotency, target.CurrentEntity.Stats.Health);
							}
						}
						// Heal
						else if(expectedPotency > 0) {
							// Don't count overhealing.
							int healingAmount = target.CurrentEntity.Stats.MaxHealth - System.Math.Min(target.CurrentEntity.Stats.Health + expectedPotency, target.CurrentEntity.Stats.MaxHealth);
							if(healingAmount > 0) {
								// AllyHealCritical
								if(target.CurrentEntity.Stats.Health <= target.CurrentEntity.Stats.MaxHealth / 2) {
									testTargetWeight += bidWeights.GetWeight(BidWeightType.AllyHealCritical) * healingAmount;
								}
								else {
									testTargetWeight += bidWeights.GetWeight(BidWeightType.AllyHeal) * healingAmount;
								}
							}
						}
					}
				}

				// testTargetWeight is the weight of this testResult.
				weightOfTarget[testIndex] = testTargetWeight;

				if(testTargetWeight >= highestWeight) {
					if(testTargetWeight > highestWeight) {
						highestWeight = testTargetWeight;
						highestWeightIndices.Clear();
					}
					highestWeightIndices.Add(testIndex);
				}
			}

			// If we didn't find anything, drop out!
			if(highestWeightIndices.Count == 0) {
				preferredTarget = GridVector.Zero;
				preferredFacing = MyActor.Facing;
				weight = 0;
				return;
			}

			// highestWeightIndices is now filled with the indices of what we'd consider to be good
			// moves from our BrawlerBrain standpoint. Pick one at random.
			preferredTarget = testResults[highestWeightIndices[rng.Next(highestWeightIndices.Count)]].TargetPosition;
			preferredFacing = (preferredTarget - MyActor.Position).GetFacing();
			weight = highestWeight;
		}

		/*******************************************************************************/

		#endregion

	}

}
