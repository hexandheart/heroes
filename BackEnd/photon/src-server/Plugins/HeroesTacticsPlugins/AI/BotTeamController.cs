﻿using System;
using System.Collections;
using System.Collections.Generic;
using BattleMechanics;
using BattleMechanics.Breezes;
using BattleMechanics.Events;
using BattleMechanics.Requests;

namespace HeroesTacticsPlugins.AI {

	internal class BotTeamController : TeamController {

		public void OnEvent(int sender, IBattleEvent battleEvent) {
			battleEventQueue.Enqueue(battleEvent);
		}

		/*******************************************************************************/
		public BotTeamController(BattlePlugin battlePlugin, int teamId, int clientId) :
			base(battlePlugin, teamId, clientId) {
			battleEventQueue = new Queue<IBattleEvent>();
			RegisterEventHandlers();
			IBreeze.OnLocalBreezeUpdated += OnLocalBreezeUpdated;

			brains = new Dictionary<int, IBrain>();

			// Start our thinker.
			thinkTimer = battlePlugin.PluginHost.CreateTimer(Think, 0, ThinkIntervalMs);
		}

		/*******************************************************************************/
		#region Thinky bits

		private void RegisterEventHandlers() {
			battleEventHandlers = new Dictionary<BattleEventType, BattleEventHandler>() {
				{ BattleEventType.GameClosed, OnGameClosed },
				{ BattleEventType.BattleEnded, OnBattleEnded },
				{ BattleEventType.BattlePhaseChanged, OnBattlePhaseChanged },

				{ BattleEventType.PrepareBattle, OnPrepareBattle },
				{ BattleEventType.SpawnActor, OnSpawnActor },
				{ BattleEventType.SpawnMultipleActors, OnSpawnMultipleActors },

				{ BattleEventType.BreezeUpdate, OnBreezeUpdate },

				{ BattleEventType.TurnStart, OnTurnStart },
				{ BattleEventType.ActionPerformed, OnActionPerformed },
			};
		}

		private void Think() {
			if(BattleController == null) {
				return;
			}
			
			// If we have a thought, let's keep thinking about it instead of processing events.
			if(currentThought != null) {
				bool stillThinking = currentThought.MoveNext();
				if(!stillThinking) {
					currentThought = null;
				}
				return;
			}

			// If it's still our turn and we have no thought, we should pass the turn.
			if(currentThought == null &&
				BattleController.BattleState.CurrentTeam == GetMyTeam() &&
				battleEventQueue.Count == 0) {
				BattleController.RaiseRequest(GetMyTeam(), new EndTurnRequest());
				return;
			}

			if(battleEventQueue.Count == 0) {
				return;
			}
			ProcessEvents();
		}

		private void ProcessEvents() {
			int eventsProcessed = 0;
			while(battleEventQueue.Count > 0 && eventsProcessed < MaxEventsToProcess) {
				IBattleEvent battleEvent = battleEventQueue.Dequeue();

				if(battleEventHandlers.ContainsKey(battleEvent.EventType)) {
					battleEventHandlers[battleEvent.EventType].Invoke(battleEvent);
				}

				// We skip breeze updates because we don't want to be held hostage by a player
				// spamming, even though they shouldn't be able to do it.
				if(battleEvent.EventType != BattleEventType.BreezeUpdate) {
					++eventsProcessed;
				}
			}
		}

		private void OnGameClosed(IBattleEvent battleEvent) { }

		private void OnBattleEnded(IBattleEvent battleEvent) {
			// Stop thinking now that the game has ended.
			IBreeze.OnLocalBreezeUpdated -= OnLocalBreezeUpdated;
			BattlePlugin.PluginHost.StopTimer(thinkTimer);
		}

		private void OnBattlePhaseChanged(IBattleEvent battleEvent) { }

		private void OnPrepareBattle(IBattleEvent battleEvent) { }

		private void OnSpawnActor(IBattleEvent battleEvent) {
			SpawnActorEvent evt = battleEvent as SpawnActorEvent;
			if(evt.Owner != GetMyTeam().TeamID) {
				return;
			}

			// If it's one of ours, give it a brain.
			Actor actor = BattleController.BattleState.GetActorByID(evt.ActorID);
			brains.Add(evt.ActorID, BrainFactory.Create(BattleController.EntityCache.GetBrainParams(evt.Entity.ID), actor));
		}

		private void OnSpawnMultipleActors(IBattleEvent battleEvent) {
			SpawnMultipleActorsEvent evt = battleEvent as SpawnMultipleActorsEvent;
			for(int i = 0; i < evt.NumSpawns; ++i) {
				OnSpawnActor(evt.GetSpawn(i));
			}
		}

		private void OnBreezeUpdate(IBattleEvent battleEvent) { }

		private void OnActionPerformed(IBattleEvent battleEvent) {
			ActionPerformedEvent evt = battleEvent as ActionPerformedEvent;

			foreach(var result in evt.Results) {
				if(result.Type != ActionResultType.Summon) {
					continue;
				}

				// Handle the spawn!
				SpawnActorEvent spawnEvent = SpawnActorEvent.Deserialize(result.Data);
				OnSpawnActor(spawnEvent);
			}
		}

		/*******************************************************************************/
		#region OnTurnStart -- meat of AI

		private void OnTurnStart(IBattleEvent battleEvent) {
			TurnStartEvent evt = battleEvent as TurnStartEvent;

			if(evt.TeamID != TeamID) return;

			currentThought = TeamBiddingThought();
		}

		private IEnumerator TeamBiddingThought() {
			Team myTeam = GetMyTeam();
			int bidsProcessed = 0;

			// Collect bids.
			do {
				int topBidWeight = int.MinValue;
				Bid topBid = null;
				foreach(var brain in brains.Values) {
					Bid bid = brain.RequestBid(BattleController.BidWeights, BattleController.BattleState);

					// Only care about bids that have actions.
					if(bid == null || bid.Size == 0) {
						continue;
					}

					int weight = bid.CalculateWeight();
					if(weight > topBidWeight) {
						topBid = bid;
						topBidWeight = weight;
					}
				}

				if(topBid == null) {
					break;
				}

				++bidsProcessed;

				// We have a winner!
				foreach(var bidAction in topBid.Actions) {
					// Do the thing!
					myTeam.ActorSelectionBreeze.Select(
						bidAction.Actor.ID);
					yield return null;
					BattleController.RaiseRequest(myTeam, new PerformActionRequest(
						bidAction.Actor.ID,
						bidAction.Action.ActionGroup,
						bidAction.Action.ActionSlot,
						bidAction.Target,
						bidAction.TargetFacing));
					yield return null;
				}

				yield return null;
			} while(true);

			if(bidsProcessed == 0) {
				// If we didn't process any bids at all, our team has effectively given up.
				BattleController.RaiseRequest(myTeam, new ConcedeMatchRequest());
			}
			else {
				BattleController.RaiseRequest(myTeam, new EndTurnRequest());
			}
		}

		private IEnumerator MoveActorThought() {
			// Pick a random actor from what we have available.
			List<Actor> actors = BattleController.BattleState.GetAllActors();
			List<Actor> myActors = actors.FindAll(m => m.TeamID == TeamID);

			if(myActors.Count == 0) yield break;

			Random rng = new Random();
			Team myTeam = GetMyTeam();

			// Move each of our actors!
			for(int i = 0; i < myActors.Count; ++i) {
				Actor actor = myActors[i];

				myTeam.ActorSelectionBreeze.Select(actor.ID);
				yield return null;

				BattleAction action = actor.GetDefaultMovementAction();

				if(!actor.CanAffordAction(action)) {
					// Oh you can't move.
					continue;
				}

				// Let's try and move it!
				/*TargetSolver solver = new TargetSolver(BattleController.BattleState);
				solver.SetSource(actor.Position, actor.Facing, actor.TeamID);
				solver.SetRange(1, actor.CurrentEntity.Stats.Step);
				solver.UsePathing(TargetSolverBlockFlags.TileGroundMovement | TargetSolverBlockFlags.Enemies);*/

				TargetSolver solver = new TargetSolver(BattleController.BattleState);
				solver.PrepareForAction(actor, action);
				solver.CalculateResults();

				// Find a place to go.
				List<GridVector> validMoves = new List<GridVector>();
				for(int y = 0; y < BattleController.BattleState.Map.GridHeight; ++y) {
					for(int x = 0; x < BattleController.BattleState.Map.GridWidth; ++x) {
						GridVector target = new GridVector(x, y);
						TargetSolverResult result = solver.GetResultAtPosition(target);
						if(result.ResultType == TargetSolverResultType.Valid) {
							validMoves.Add(target);
						}
					}
				}

				// If we didn't find a valid move, we'll just do nothing and that's fine.
				if(validMoves.Count > 0) {
					GridVector preferredTarget = validMoves[rng.Next(validMoves.Count - 1)];
					GridFacing preferredFacing = (preferredTarget - actor.Position).GetFacing();

					BattleController.RaiseRequest(myTeam, new PerformActionRequest(actor.ID, action.ActionGroup, action.ActionSlot, preferredTarget, preferredFacing));

					yield return null;
				}
			}
			
			BattleController.RaiseRequest(myTeam, new EndTurnRequest());
		}

		#endregion

		/*******************************************************************************/
		#region Breeze handling

		private void OnLocalBreezeUpdated(IBreeze breeze) {
			// Due to the quirk that there may be multiple bot teams on this one client, we have to actually
			// check that this is for our team. Otherwise we raise the request multiple times!
			if(breeze.Team.TeamID != TeamID) {
				return;
			}
			// On the client, we'd throttle these, but we can be "confident" that the AI isn't going to spam.
			BattleController.RaiseRequest(GetMyTeam(), new UpdateBreezeRequest(breeze));
		}

		#endregion

		#endregion

		/*******************************************************************************/
		#region Privates

		private delegate void BattleEventHandler(IBattleEvent battleEvent);

		private static readonly int ThinkIntervalMs = 500;
		private static readonly int MaxEventsToProcess = 1;

		private object thinkTimer;
		private IEnumerator currentThought;
		private Queue<IBattleEvent> battleEventQueue;
		private Dictionary<BattleEventType, BattleEventHandler> battleEventHandlers;

		private Dictionary<int, IBrain> brains;

		private Team GetMyTeam() {
			return BattleController.BattleState.GetTeam(TeamID);
		}

		#endregion

	}

}
