﻿using BattleMechanics;
using LitJson;
using System.Collections.Generic;

namespace HeroesTacticsPlugins.AI {

	public enum BrainType {
		Brawler,
		RangedSupport,
	}

	/// <summary>
	/// Interface for braaaains
	/// </summary>
	internal abstract class IBrain {

		/// <summary>
		/// The Actor owning this brain.
		/// </summary>
		public Actor MyActor { get; private set; }

		/// <summary>
		/// Called by the Brain's manager (BotTeamController) to place a bid for an action.
		/// </summary>
		/// <param name="bidWeights"></param>
		/// <param name="battleState"></param>
		/// <returns></returns>
		public abstract Bid RequestBid(BidWeights bidWeights, BattleState battleState);

		/*******************************************************************************/
		#region Constructor

		/// <summary>
		/// Construct the brain!
		/// </summary>
		/// <param name="brainParams">brainConfig properties from brainCloud.</param>
		/// <param name="myActor"></param>
		protected IBrain(Dictionary<string, object> brainParams, Actor myActor) {
			rng = new System.Random();
			MyActor = myActor;
		}

		#endregion

		/*******************************************************************************/
		#region Champion utility

		/// <summary>
		/// If we're a champion, this will prepare our champion-specific bids. (ie summons)
		/// </summary>
		/// <param name="bid"></param>
		/// <param name="bidWeights"></param>
		/// <param name="battleState"></param>
		protected void PrepareChampionBid(Bid bid, BidWeights bidWeights, BattleState battleState) {
			if(MyActor.CurrentEntity.Type != EntityType.Champion) {
				return;
			}
			
			int oldAP = MyActor.Team.AP;
			MyActor.Team.AP -= bid.GetAPCost();
			// Let's summon all the things! Priority given to things earlier in the summon list.
			foreach(var actionGroup in MyActor.CurrentEntity.ActionMap.ActionGroups) {
				foreach(var action in actionGroup.Actions.Values) {
					if(action.Summon == null) {
						continue;
					}

					// Can we summon this?
					if(action.Summon.ActiveActorID == BattleActionSummon.InactiveID &&
						MyActor.CanAffordAction(action)) {
						// Prepare to summon!
						bool canSummon = PrepareSummon(bid, action, bidWeights, battleState);
						if(canSummon) {
							MyActor.Team.AP -= action.Cost.AP;
						}
					}
				}
			}
			MyActor.Team.AP = oldAP;
		}

		/// <summary>
		/// Prepare a summon in an action bid.
		/// </summary>
		/// <param name="bid"></param>
		/// <param name="action"></param>
		/// <param name="bidWeights"></param>
		/// <param name="battleState"></param>
		/// <returns>true if the summon could be prepared, false otherwise</returns>
		protected bool PrepareSummon(Bid bid, BattleAction action, BidWeights bidWeights, BattleState battleState) {
			TargetSolver solver = new TargetSolver(battleState);
			solver.PrepareForAction(MyActor, action);
			solver.CalculateResults();

			List<TargetSolverResult> validResults = new List<TargetSolverResult>();
			for(int y = 0; y < battleState.Map.GridHeight; ++y) {
				for(int x = 0; x < battleState.Map.GridWidth; ++x) {
					GridVector target = new GridVector(x, y);
					TargetSolverResult result = solver.GetResultAtPosition(target);

					if(result.ResultType != TargetSolverResultType.Valid) {
						continue;
					}

					validResults.Add(result);
				}
			}

			if(validResults.Count == 0) {
				return false;
			}

			TargetSolverResult selectedResult = validResults[rng.Next(validResults.Count)];
			bid.AddAction(new BidAction(
				MyActor, action,
				selectedResult.TargetPosition, (selectedResult.TargetPosition - MyActor.Position).GetFacing(),
				bidWeights.GetWeight(BidWeightType.Summon)));

			return true;
		}

		#endregion

		/*******************************************************************************/

		protected readonly System.Random rng;

	}

	/// <summary>
	/// Factory to create brains.
	/// </summary>
	internal static class BrainFactory {

		/// <summary>
		/// Get a set of brainConfig parameters from the provided JSON.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static Dictionary<string, object> BrainParamsFromJson(JsonData data) {
			Dictionary<string, object> brainParams = new Dictionary<string, object>();

			foreach(string key in data.Keys) {
				object value = data[key];
				if(key == "type") {
					if(System.Enum.TryParse((string)data[key], true, out BrainType type)) {
						brainParams.Add(key, type);
					}
					else {
						brainParams.Add(key, BrainType.Brawler);
					}
				}
				else {
					brainParams.Add(key, value);
				}
			}

			return brainParams;
		}

		/// <summary>
		/// Create a brain!
		/// </summary>
		/// <param name="brainParams"></param>
		/// <param name="myActor"></param>
		/// <returns></returns>
		public static IBrain Create(Dictionary<string, object> brainParams, Actor myActor) {
			switch((BrainType)brainParams["type"]) {
			case BrainType.Brawler:
				return new BrawlerBrain(brainParams, myActor);
			/*case BrainType.RangedSupport:
				return new RangedSupportBrain(brainParams, myActor);*/
			default:
				return new BrawlerBrain(brainParams, myActor);
			}
		}

	}

}
