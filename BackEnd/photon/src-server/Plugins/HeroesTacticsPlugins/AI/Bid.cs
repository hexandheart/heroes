﻿using System.Collections.Generic;
using BattleMechanics;

namespace HeroesTacticsPlugins.AI {

	/// <summary>
	/// A Brain's bid for a turn's action.
	/// </summary>
	internal class Bid {

		/// <summary>
		/// How many actions are in the bid?
		/// </summary>
		public int Size {
			get {
				return actions.Count;
			}
		}

		/// <summary>
		/// Enumerator for the actions in the bid.
		/// </summary>
		public IEnumerable<BidAction> Actions { get { return actions; } }

		/// <summary>
		/// Add an action to the bid.
		/// </summary>
		/// <param name="action"></param>
		public void AddAction(BidAction action) {
			if(action == null) {
				return;
			}
			actions.Add(action);
		}

		/// <summary>
		/// Calculate the weight of the bid based on its actions.
		/// </summary>
		/// <returns></returns>
		public int CalculateWeight() {
			int weight = 0;
			foreach(var action in actions) {
				weight += action.Weight;
			}
			return weight;
		}

		/// <summary>
		/// Gets the current AP cost of the bid.
		/// </summary>
		/// <returns></returns>
		public int GetAPCost() {
			int ap = 0;
			foreach(var action in actions) {
				ap += action.Action.Cost.AP;
			}
			return ap;
		}

		/*******************************************************************************/
		#region Privates

		private List<BidAction> actions = new List<BidAction>();

		#endregion

	}

}
