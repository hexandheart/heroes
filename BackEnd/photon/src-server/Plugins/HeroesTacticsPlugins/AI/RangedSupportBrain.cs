﻿using BattleMechanics;
using System.Collections.Generic;

namespace HeroesTacticsPlugins.AI {

	/// <summary>
	/// Brain logic for BrainType.RangedSupport.
	/// </summary>
	internal class RangedSupportBrain : IBrain {

		/*******************************************************************************/
		#region IBrain implementation

		/// <summary>
		/// Constructor. Additional parameters:
		/// - supportRange
		/// - enemyRange
		/// </summary>
		/// <param name="brainParams"></param>
		/// <param name="myActor"></param>
		public RangedSupportBrain(Dictionary<string, object> brainParams, Actor myActor) : base(brainParams, myActor) {
			SupportRange = (int)brainParams["supportRange"];
			EnemyRange = (int)brainParams["enemyRange"];
		}

		/// <summary>
		/// Prepare a potential pair of bids:
		/// - Move away from enemies (at least EnemyRange) and towards allies (within SupportRange).
		/// - Perform a primary action.
		/// </summary>
		/// <param name="bidWeights"></param>
		/// <param name="battleState"></param>
		/// <returns></returns>
		public override Bid RequestBid(BidWeights bidWeights, BattleState battleState) {
			Bid bid = null;

			return bid;
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private readonly int SupportRange;
		private readonly int EnemyRange;

		#endregion

	}

}
