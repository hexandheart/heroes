﻿using Photon.Hive.Operations;
using Photon.Hive.Plugin;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LitJson;
using System.Collections;

using BattleMechanics;
using BattleMechanics.Events;
using BattleMechanics.Requests;

namespace HeroesTacticsPlugins {

	/// <summary>
	/// Low-level player/connection/room management for battle.
	/// </summary>
	class BattlePlugin : PluginBase {

		/*******************************************************************************/

		/// <summary>
		/// Broadcast a battle event to clients.
		/// </summary>
		public void BroadcastBattleEvent(int source, IBattleEvent battleEvent) {
			var data = new Dictionary<byte, object>() {
				{ (byte)ParameterKey.Data, battleEvent.GetData() },
				{ (byte)ParameterKey.ActorNr, source }
			};
			BroadcastEvent((byte)battleEvent.EventType, data);

			// Also forward the battle event to our bots.
			foreach(TeamController tc in teamControllers) {
				if(tc.IsHuman) {
					continue;
				}
				AI.BotTeamController btc = tc as AI.BotTeamController;
				btc.OnEvent(source, battleEvent);
			}
		}

		/// <summary>
		/// Gets the team controller associated with a specific client.
		/// </summary>
		/// <param name="teamId"></param>
		/// <returns></returns>
		public TeamController GetTeamControllerForClient(int clientId) {
			return teamControllers.Find(c => c.ClientID == clientId);
		}

		/// <summary>
		/// Gets the team controller associated with a team ID.
		/// </summary>
		/// <param name="teamId"></param>
		/// <returns></returns>
		public TeamController GetTeamControllerForTeamID(int teamId) {
			return teamControllers.Find(c => c.TeamID == teamId);
		}

		/*******************************************************************************/
		#region PluginBase overrides

		/// <summary>
		/// Plugin name.
		/// </summary>
		public override string Name {
			get {
				return "BattlePlugin";
			}
		}

		/// <summary>
		/// Called when a game (room) is created. We use the opportunity to get a scenario from BrainCloud.
		/// </summary>
		/// <param name="info"></param>
		public override void OnCreateGame(ICreateGameCallInfo info) {
			info.Continue();
			string matchType = PluginHost.GetSerializableGameState().LobbyId;
			if(matchType.Contains("|")) {
				string[] split = matchType.Split('|');
				matchType = split[0];
				gameId = int.Parse(split[1]);
			}
			CallBrainCloudScript("GetRandomScenario", "{ \"scenarioType\" : \"" + matchType + "\" }", info, GetRandomScenario_Complete);
		}

		/// <summary>
		/// Called when a client joins (not including host). Initialize the battle if we have enough players.
		/// </summary>
		/// <param name="info"></param>
		public override void OnJoin(IJoinGameCallInfo info) {
			info.Continue();

			// If we have enough players now, we can initialize the battle!
			if(IsGameFull()) {
				InitializeBattle();
			}
		}

		/// <summary>
		/// Called when a client leaves for whatever reason!
		/// </summary>
		/// <param name="info"></param>
		public override void OnLeave(ILeaveGameCallInfo info) {
			info.Continue();

			// TODO: Exit gracefully!
			CloseGame();
		}

		/// <summary>
		/// Called when the game is closed. Does anything have to be cleaned up? iunno
		/// </summary>
		/// <param name="info"></param>
		public override void OnCloseGame(ICloseGameCallInfo info) {
			PluginHost.LogInfo("[BattlePlugin]: Game closed.");
			info.Continue();
		}

		/// <summary>
		/// Respond to clients setting properties.
		/// </summary>
		/// <param name="info"></param>
		public override void OnSetProperties(ISetPropertiesCallInfo info) {
			info.Continue();

			// If everyone is ready, we can actually kick things off!
			// TODO: Safety check, we shouldn't trigger the battle again if one of the clients
			// sets a property and the battle is already going on!
			if(IsEveryoneReady()) {
				battleController.StartBattle();
			}
		}

		public override void OnRaiseEvent(IRaiseEventCallInfo info) {
			//info.Continue();
			info.Cancel();

			TeamController tc = GetTeamControllerForClient(info.ActorNr);
			Team team = battleController.BattleState.GetTeam(tc.TeamID);
			IRequest request = RequestFactory.FromEvent(info.Request);
			battleController.RaiseRequest(team, request);
		}

		#endregion

		/*******************************************************************************/
		#region BrainCloud

		/// <summary>
		/// URL to which to make BrainCloud requests.
		/// </summary>
		private static readonly string BrainCloudUrl = "https://sharedprod.braincloudservers.com/s2sdispatcher";

		/// <summary>
		/// Format for JSON data that BrainCloud is expecting.
		/// </summary>
		private static readonly string BrainCloudDataFormat = "{{" +
				"\"gameId\": \"{0}\", " +
				"\"serverName\": \"{1}\", " +
				"\"gameSecret\": \"{2}\", " +
				"\"service\": \"script\", " +
				"\"operation\": \"RUN\", " +
				"\"data\": {{ " +
					"\"scriptName\": \"{3}\", " +
					"\"scriptData\": {4} " +
				"}} " +
			"}}";

		/// <summary>
		/// Call a BrainCloud script! Script must be flagged S2S in the BrainCloud dashboard.
		/// </summary>
		/// <param name="scriptName">Name of the script to run.</param>
		/// <param name="jsonData">Data to pass to the script.</param>
		/// <param name="info">Photon call to defer (if deferrable).</param>
		/// <param name="callback">Callback to call when the BrainCloud script completes.</param>
		private void CallBrainCloudScript(string scriptName, string jsonData, ICallInfo info, HttpRequestCallback callback) {
			string serverName = "BattleServerDev";
			string gameSecret = "eb8b1749-76c2-45e1-a8ab-e9a841687282";

			MemoryStream stream = new MemoryStream();
			byte[] data = Encoding.UTF8.GetBytes(string.Format(BrainCloudDataFormat, gameId, serverName, gameSecret, scriptName, jsonData));
			stream.Write(data, 0, data.Length);
			HttpRequest request = new HttpRequest() {
				Callback = callback,
				Url = BrainCloudUrl,
				DataStream = stream,
				Method = "POST",
				ContentType = "application/json",
				UserState = info
			};
			PluginHost.HttpRequest(request);
		}

		/// <summary>
		/// "GetRandomScenario" BrainCloud script has completed!
		/// </summary>
		/// <param name="httpResponse">BrainCloud response JSON.</param>
		/// <param name="userState">Deferred Photon call (if applicable).</param>
		private void GetRandomScenario_Complete(IHttpResponse httpResponse, object userState) {
			PluginHost.LogInfo("[BattlePlugin]: GetRandomScenario response=" + httpResponse.ResponseText);

			ICallInfo info = userState as ICallInfo;
			if(info != null && info.IsDeferred) {
				info.Continue();
			}

			if(string.IsNullOrEmpty(httpResponse.ResponseText)) {
				CloseGame();
				throw new System.Exception("GetRandomScenario did not return a response.");
			}

			JsonData responseData = JsonMapper.ToObject(httpResponse.ResponseText)["response"];

			// Response looks like this:
			// 	"response":{
			// 		"scenario":{ }, <- this is what we're interested in!!
			// 		"success":true,
			// 		"reasonCode":0,
			// 		"status":200
			// 	}

			int status = responseData["status"].AsInt();
			if(status != 200) {
				CloseGame();
				return;
			}

			//	"scenario":{
			//		"id": "scenario ID",
			//		"arena": "arena ID",
			//		"teams": [
			//			{
			//				"control" : "human" or "bot",
			//				"teamId" : 1,
			//				"heroIds" : [] <- array of hero IDs
			//			}
			//		]
			//	}
			scenarioData = responseData["scenario"];

			// Set number of open slots in the game.
			numHumanSlots = 0;
			numBotSlots = 0;

			var teamsData = scenarioData["teams"];
			for(int i = 0; i < teamsData.Count; ++i) {
				if((string)teamsData[i]["control"] == "human") {
					++numHumanSlots;
				}
				else {
					++numBotSlots;
				}
			}

			PluginHost.LogInfo(string.Format("[BattlePlugin]: Scenario {0} has {1} human player(s), {2} AI player(s)", (string)scenarioData["id"], numHumanSlots, numBotSlots));
			/*PluginHost.GetSerializableGameState().MaxPlayers = (byte)numHumanSlots;
			PluginHost.SetProperties(0, new System.Collections.Hashtable() {
				{ GameParameter.MaxPlayers, (byte)numHumanSlots }
			}, null, true);*/

			// The client should have requested the room be created with the right number of players.
			int requestedSlots = PluginHost.GetSerializableGameState().MaxPlayers;
			if(requestedSlots != numHumanSlots) {
				PluginHost.LogInfo(string.Format("[BattlePlugin]: Client requested {0} slot(s) but scenario expects {1}.", requestedSlots, numHumanSlots));
				CloseGame();
				return;
			}

			// If it's just the one player that's needed (or none??) we can kick the battle off now.
			if(IsGameFull()) {
				InitializeBattle();
			}
		}
		
		/// <summary>
		/// "GetBattleDetails" BrainCloud script has completed!
		/// </summary>
		/// <param name="httpResponse">BrainCloud response JSON.</param>
		/// <param name="userState">Deferred Photon call (if applicable).</param>
		private void GetBattleDetails_Complete(IHttpResponse httpResponse, object userState) {
			ICallInfo info = userState as ICallInfo;
			if(info != null && info.IsDeferred) {
				info.Continue();
			}

			PluginHost.LogInfo("[BattlePlugin]: GetBattleDetails response=" + httpResponse.ResponseText);

			// Drop out if there was an error.
			JsonData responseData = JsonMapper.ToObject(httpResponse.ResponseText)["response"];
			int status = responseData["status"].AsInt();
			if(status != 200) {
				CloseGame();
				return;
			}

			// Build the battle!
			JsonData battleData = responseData["data"];
			battleController = new BattleController(this, battleData);

			// Now we have to broadcast the important initialization bits to the clients.
			// After this week wait until all clients are in the Ready game state.
			PrepareBattleEvent evt = PrepareBattleEvent.FromBattleState(battleController.BattleState);
			BroadcastBattleEvent(0, evt);
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private JsonData scenarioData;
		private int numHumanSlots = 0;
		private int numBotSlots = 0;

		private static readonly int LiveGameID = 11605;
		private int gameId = LiveGameID;

		private BattleController battleController;
		private List<TeamController> teamControllers;

		/// <summary>
		/// Close the game and wrap everything up.
		/// </summary>
		private void CloseGame() {
			PluginHost.LogInfo("[BattlePlugin]: Closing game...");

			// Make this room invisible.
			PluginHost.SetProperties(0, new System.Collections.Hashtable() {
				{ GameParameter.IsVisible, false },
				{ GameParameter.IsOpen, false }
			}, null, true);
			/*PluginHost.GetSerializableGameState().IsOpen = false;
			PluginHost.GetSerializableGameState().IsVisible = false;*/

			// Broadcast the "game closed" event to all connected clients.
			var data = new Dictionary<byte, object>() {
				{ (byte)ParameterKey.Data, null },
				{ (byte)ParameterKey.ActorNr, 0 }
			};
			BroadcastEvent((byte)BattleEventType.GameClosed, data);
		}

		/// <summary>
		/// Is the game full? Full is defined as "we have all the humans we expect".
		/// </summary>
		/// <returns></returns>
		private bool IsGameFull() {
			return PluginHost.GameActorsActive.Count >= numHumanSlots;
		}

		/// <summary>
		/// Called when we have enough players to launch the game. Calls GetBattleDetails.
		/// </summary>
		private void InitializeBattle() {
			// Close the room.
			PluginHost.SetProperties(0, new System.Collections.Hashtable() {
				{ GameParameter.IsVisible, false },
				{ GameParameter.IsOpen, false }
			}, null, true);
			/*PluginHost.GetSerializableGameState().IsOpen = false;
			PluginHost.GetSerializableGameState().IsVisible = false;*/

			// Get battle details.

			// We create the team controllers here, which act as the mapping between
			// users and teams.
			teamControllers = new List<TeamController>();

			// TODO: Randomize humans!
			int currentHumanIndex = 0;
			JsonData loadouts = new JsonData();
			loadouts.SetJsonType(JsonType.Array);
			var teamsData = scenarioData["teams"];
			for(int i = 0; i < teamsData.Count; ++i) {
				int teamId = teamsData[i]["teamId"].AsInt();
				int controllerId = TeamController.BotClientID;
				TeamController teamController = null;

				// Humans have to pass in team ID, owner and loadout ID.
				if((string)teamsData[i]["control"] == "human") {
					IActor human = PluginHost.GameActorsActive[currentHumanIndex];
					controllerId = human.ActorNr;
					loadouts.Add(new JsonData() {
						["teamId"] = teamsData[i]["teamId"],
						["owner"] = human.Properties.GetProperty("ProfileID").Value.ToString(),
						["loadoutId"] = human.Properties.GetProperty("LoadoutID").Value.ToString(),
					});
					++currentHumanIndex;

					teamController = new TeamController(this, teamId, controllerId);
				}
				// For bots, we just have to know the team ID.
				else {
					loadouts.Add(new JsonData() {
						["teamId"] = teamsData[i]["teamId"],
					});

					teamController = new AI.BotTeamController(this, teamId, controllerId);
				}

				teamControllers.Add(teamController);
			}
			JsonData data = new JsonData {
				["scenarioId"] = scenarioData["id"],
				["loadouts"] = loadouts,
			};

			CallBrainCloudScript("GetBattleDetails", data.ToJson(), null, GetBattleDetails_Complete);
		}

		/// <summary>
		/// Is everyone in the game ready? (ie client is fully loaded)
		/// </summary>
		/// <returns></returns>
		private bool IsEveryoneReady() {
			//PluginHost.LogInfo("[BattlePlugin]: Is everyone ready?");
			foreach(var actor in PluginHost.GameActorsActive) {
				if(actor.Properties.GetProperty("ReadyState").Value == null ||
					(ReadyState)actor.Properties.GetProperty("ReadyState").Value != ReadyState.Ready) {
					//PluginHost.LogInfo("[BattlePlugin]: " + actor.UserId + " is not ready! (" + actor.Properties.GetProperty("ReadyState").Value + ")");
					return false;
				}
			}
			//PluginHost.LogInfo("[BattlePlugin]: Yep!");
			return true;
		}

		#endregion

	}

}
