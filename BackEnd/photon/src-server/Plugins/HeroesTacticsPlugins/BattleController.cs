﻿using System;
using System.Collections;
using BattleMechanics;
using BattleMechanics.Breezes;
using BattleMechanics.Events;
using BattleMechanics.Events.Data;
using BattleMechanics.Requests;
using LitJson;
using System.Collections.Generic;
using HeroesTacticsPlugins.AI;
using BattleMechanics.Objectives;

namespace HeroesTacticsPlugins {

	internal class BattleController {

		public BattleState BattleState { get; private set; }

		/*******************************************************************************/

		/// <summary>
		/// Spawn the initial actors and determine who goes first.
		/// </summary>
		/// <param name="battle"></param>
		public void StartBattle() {
			SpawnBattle();
			AdvanceTurn();
		}

		/// <summary>
		/// Client has raised a request. Deal with it!
		/// </summary>
		/// <param name="source"></param>
		/// <param name="request"></param>
		public void RaiseRequest(Team team, IRequest request) {
			if(!requestHandlers.ContainsKey(request.RequestType)) {
				// Oh no!
				return;
			}

			requestHandlers[request.RequestType].Invoke(team, request);
		}
		
		public EntityCache EntityCache;
		public BidWeights BidWeights { get; private set; }
		public Random RNG { get; private set; }

		/*******************************************************************************/
		#region Constructor

		private BattleController() { }

		internal BattleController(BattlePlugin battlePlugin, JsonData initData) {
			this.battlePlugin = battlePlugin;
			RNG = new Random();

			RegisterRequestHandlers();

			EntityCache = new EntityCache();

			JsonData brainConfigData = initData["brainConfig"];
			BidWeights = BidWeights.FromJson(brainConfigData["bidWeights"]);

			// Create the map.
			JsonData arenaData = initData["arenaData"];
			int mapWidth = arenaData["width"].AsInt();
			int mapHeight = arenaData["height"].AsInt();
			Tile[] tiles = new Tile[mapWidth * mapHeight];

			JsonData tileData = arenaData["tiles"];
			for(int y = 0; y < mapHeight; ++y) {
				for (int x = 0; x < mapWidth; ++x) {
					int index = y * mapWidth + x;
					tiles[index] = new Tile((TileFlags)(int)tileData[index]);
				}
			}

			string scene = (string)initData["scenario"]["clientLevel"];

			JsonData spawnZonesData = arenaData["spawns"];
			List<SpawnZone> spawnZones = new List<SpawnZone>();
			for(int zoneIndex = 0; zoneIndex < spawnZonesData.Count; ++zoneIndex) {
				JsonData zoneData = spawnZonesData[zoneIndex];
				int teamId = zoneData["teamId"].AsInt();

				List<GridVector> spawnPoints = new List<GridVector>();
				List<GridFacing> spawnFacings = new List<GridFacing>();
				for (int pointIndex = 0; pointIndex < zoneData["spawnPoints"].Count; ++pointIndex) {
					int pointX = zoneData["spawnPoints"][pointIndex]["x"].AsInt();
					int pointY = zoneData["spawnPoints"][pointIndex]["y"].AsInt();
					spawnPoints.Add(new GridVector(pointX, pointY));
					spawnFacings.Add((GridFacing)Enum.Parse(typeof(GridFacing), zoneData["spawnPoints"][pointIndex]["facing"].ToString(), true));
				}

				spawnZones.Add(new SpawnZone(teamId, spawnPoints.ToArray(), spawnFacings.ToArray()));
			}

			Map map = new Map(mapWidth, mapHeight, tiles, scene, spawnZones);

			// Intiailize the teams in the battle.
			JsonData loadoutsData = initData["loadouts"];
			int numTeams = loadoutsData.Count;
			Team[] teams = new Team[numTeams];
			Loadout[] loadouts = new Loadout[numTeams];

			for(int loadoutIndex = 0; loadoutIndex < loadoutsData.Count; ++loadoutIndex) {
				JsonData loadoutData = loadoutsData[loadoutIndex];
				List<LoadoutSlot> loadoutSlots = new List<LoadoutSlot>();
				for(int slotIndex = 0; slotIndex < loadoutData["spawnHeroes"].Count; ++slotIndex) {
					JsonData slotData = loadoutData["spawnHeroes"][slotIndex];
					string heroId = (string)slotData["heroId"];
					int spawnSlot = (int)slotData["spawnSlot"];
					LoadoutSlot slot = new LoadoutSlot(heroId, spawnSlot);
					loadoutSlots.Add(slot);
				}
				loadouts[loadoutIndex] = new Loadout(loadoutSlots);
				// Get the team controller for this team so we can figure out its client ID.
				int teamId = (int)loadoutData["teamId"];
				ObjectiveType objectiveType = (ObjectiveType)Enum.Parse(typeof(ObjectiveType), (string)loadoutData["objectiveType"], true);
				TeamController tc = battlePlugin.GetTeamControllerForTeamID(teamId);
				tc.SetBattleController(this);
				teams[loadoutIndex] = new Team(tc.TeamID, tc.ClientID, loadouts[loadoutIndex], objectiveType);
			}

			// Get the entity data to cache for this battle.
			JsonData entityData = initData["entityData"];
			// Each entry in this is indexed by its ID.
			foreach(string entityId in entityData.Keys) {
				Entity entity = EntityExtensions.FromJson(entityData[entityId]);
				EntityCache.RegisterEntity(entity, BrainFactory.BrainParamsFromJson(entityData[entityId]["brain"]));
			}

			// Initialize the battle.
			BattleState = new BattleState(map, teams);
		}

		#endregion

		/*******************************************************************************/
		#region Gameplay meat!

		/// <summary>
		/// Spawns all the initial stuff in the battle.
		/// </summary>
		private void SpawnBattle() {
			// Spawn the initial actors!
			List<SpawnActorEvent> spawnActorEvents = new List<SpawnActorEvent>();

			for(int teamIndex = 0; teamIndex < BattleState.NumTeams; ++teamIndex) {
				Team team = BattleState.GetTeamAtIndex(teamIndex);

				SpawnZone spawnZone = BattleState.Map.GetSpawnZoneForTeam(team.TeamID);

				for(int spawnSlot = 0; spawnSlot <= spawnZone.GetNumSpawnPoints(); ++spawnSlot) {
					LoadoutSlot loadoutSlot = team.Loadout.GetDataForSpawnSlot(spawnSlot);
					// If there's nothing in this slot, there's nothing to spawn!
					if(loadoutSlot == null) {
						continue;
					}

					int actorId = nextActorId++;
					string heroId = loadoutSlot.HeroID;

					Actor actor = new Actor(
						actorId, team.TeamID,
						spawnZone.GetSpawnPoint(spawnSlot),
						spawnZone.GetSpawnFacing(spawnSlot),
						EntityCache.GetEntity(heroId));
					BattleState.RegisterActor(actor);

					SpawnActorEvent e = new SpawnActorEvent(BattleState.Time, actor);
					spawnActorEvents.Add(e);
				}
			}

			// Now that everything's spawned, we can initialize our objectives.
			for(int i = 0; i < BattleState.NumTeams; ++i) {
				BattleState.GetTeamAtIndex(i).Objective.Initialize(BattleState);
			}

			SpawnMultipleActorsEvent smae = new SpawnMultipleActorsEvent(BattleState.Time, spawnActorEvents);
			battlePlugin.BroadcastBattleEvent(0, smae);

			BattlePhaseChangedEvent bpce = new BattlePhaseChangedEvent(BattleState.Time, ReadyState.Ready);
			bpce.ApplyTo(BattleState);
			battlePlugin.BroadcastBattleEvent(0, bpce);
		}

		/// <summary>
		/// Signals the start of the next turn! We are essentially now waiting for the team to do
		/// something. But if it's a bot team, we actually need to do something ourselves!
		/// </summary>
		private void AdvanceTurn() {
			if(BattleState.BattlePhase == ReadyState.PostGame) {
				return;
			}

			// Advance the active team.
			currentTeamIndex = (currentTeamIndex + 1) % BattleState.NumTeams;

			// The very first time we advance turn, we need to determine who goes first.
			if(BattleState.Time == 0) {
				currentTeamIndex = RNG.Next(BattleState.NumTeams);
			}

			int time = BattleState.Time + TimeStep;
			Team team = BattleState.GetTeamAtIndex(currentTeamIndex);

			// Update and refresh team's AP.
			int newMaxAP = team.MaxAP + 1;
			if(newMaxAP > APCap) {
				newMaxAP = APCap;
			}
			TeamAPData teamApData = new TeamAPData(team.TeamID, newMaxAP, newMaxAP);

			// Send turn start notification to clients.
			TurnStartEvent e = new TurnStartEvent(time, team.TeamID, teamApData);
			e.ApplyTo(BattleState);
			battlePlugin.BroadcastBattleEvent(0, e);
		}

		#endregion

		/*******************************************************************************/
		#region Request handling

		/// <summary>
		/// Register the handlers for the various requests that teams will raise.
		/// </summary>
		private void RegisterRequestHandlers() {
			requestHandlers = new Dictionary<RequestType, Action<Team, IRequest>> {
				{ RequestType.EndTurn, OnEndTurnRequest },
				{ RequestType.ConcedeMatch, OnConcedeMatchRequest },

				{ RequestType.UpdateBreeze, OnUpdateBreezeRequest },
				{ RequestType.PerformAction, OnPerformActionRequest },
			};
			IBreeze.OnBreezeUpdatedNoNotify += ServerBreezeUpdate;
		}

		/// <summary>
		/// Team wants to end their turn.
		/// </summary>
		/// <param name="team"></param>
		/// <param name="request"></param>
		private void OnEndTurnRequest(Team team, IRequest request) {
			if(BattleState.BattlePhase == ReadyState.PostGame) {
				return;
			}

			if(team.TeamID != BattleState.CurrentTeam.TeamID) {
				// Why is a non-active team requesting a turn end? Be patient!!
				return;
			}

			team.ActorSelectionBreeze.SelectNone(false);

			AdvanceTurn();
		}

		/// <summary>
		/// Team wants to concede the match. 
		/// </summary>
		/// <param name="team"></param>
		/// <param name="request"></param>
		private void OnConcedeMatchRequest(Team team, IRequest request) {
			if(BattleState.BattlePhase == ReadyState.PostGame) {
				return;
			}

			int winnerTeamId = -1;

			// The team that isn't the conceding team wins. This would have to change for
			// games with more than two teams.
			for(int i = 0; i < BattleState.NumTeams; ++i) {
				int teamId = BattleState.GetTeamAtIndex(i).TeamID;
				if(teamId != team.TeamID) {
					winnerTeamId = teamId;
					break;
				}
			}

			team.ActorSelectionBreeze.SelectNone(false);

			BattleEndedEvent battleEndedEvent = new BattleEndedEvent(BattleState.Time, winnerTeamId);
			battleEndedEvent.ApplyTo(BattleState);
			battlePlugin.BroadcastBattleEvent(0, battleEndedEvent);
		}

		/// <summary>
		/// Team wants to perform an action.
		/// </summary>
		/// <param name="team"></param>
		/// <param name="request"></param>
		private void OnPerformActionRequest(Team team, IRequest request) {
			// Ignore invalid requests.
			if(BattleState.BattlePhase == ReadyState.PostGame ||
				team.TeamID != BattleState.CurrentTeamID) {
				return;
			}

			PerformActionRequest req = request as PerformActionRequest;
			Actor actor = BattleState.GetActorByID(req.ActorID);
			BattleAction action = actor.GetAction(req.ActionGroup, req.ActionSlot);

			// It had better be a real action!
			if(action == null) {
				return;
			}

			// Have to be able to afford.
			if(!actor.CanAffordAction(action)) {
				// Nope!
				// TODO: Send the offending team some kind of error.
				return;
			}

			// Is it a valid action?
			if(!IsValidAction(actor, action, req.Target, req.TargetFacing, out TargetSolver solver)) {
				// Nope!
				// TODO: Send the offending team some kind of error.
				return;
			}

			// Resolve!
			ActionResultData[] results = ResolveAction(actor, action, req.Target, req.TargetFacing, solver);

			// Action should be okay to perform!
			ActionPerformedEvent evt = new ActionPerformedEvent(
				BattleState.Time,
				req.ActorID, action.ActionGroup, action.ActionSlot,
				actor.Position, actor.Facing,
				req.Target, req.TargetFacing,
				results); // <- That should be a list of action results!
			evt.ApplyTo(BattleState);
			BattleState.PurgeDeadActors();

			// Update the team's breezes.
			team.TargetingBreeze.ClearTargeting(false);

			battlePlugin.BroadcastBattleEvent(BattleState.CurrentTeam.ClientID, evt);

			// After each action, we've got to check whether the game is over.
			for(int i = 0; i < BattleState.NumTeams; ++i) {
				Team checkTeam = BattleState.GetTeamAtIndex(i);
				if(!checkTeam.Objective.IsMet(BattleState)) {
					continue;
				}

				// Oh no someone won!
				// We don't currently support draws.
				BattleEndedEvent battleEndEvt = new BattleEndedEvent(BattleState.Time, checkTeam.TeamID);
				battleEndEvt.ApplyTo(BattleState);
				battlePlugin.BroadcastBattleEvent(0, battleEndEvt);
			}
		}

		/// <summary>
		/// Team is updating a breeze.
		/// </summary>
		/// <param name="team"></param>
		/// <param name="request"></param>
		private void OnUpdateBreezeRequest(Team team, IRequest request) {
			// Ignore invalid requests.
			if(BattleState.BattlePhase == ReadyState.PostGame) {
				return;
			}

			UpdateBreezeRequest req = request as UpdateBreezeRequest;

			BreezeUpdateEvent evt = new BreezeUpdateEvent(BattleState.Time, team.TeamID, req.BreezeType, req.BreezeData, false);
			evt.ApplyTo(BattleState);
			battlePlugin.BroadcastBattleEvent(team.ClientID, evt);
		}

		private void ServerBreezeUpdate(IBreeze breeze) {
			BreezeUpdateEvent evt = new BreezeUpdateEvent(BattleState.Time, breeze.Team.TeamID, breeze.BreezeType, breeze.GetData(), true);
			evt.ApplyTo(BattleState);
			battlePlugin.BroadcastBattleEvent(breeze.Team.ClientID, evt);
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		/// <summary>
		/// Is the action valid as prepared?
		/// </summary>
		/// <param name="actor"></param>
		/// <param name="action"></param>
		/// <param name="target"></param>
		/// <param name="targetFacing"></param>
		/// <returns></returns>
		private bool IsValidAction(
			Actor actor, BattleAction action,
			GridVector target, GridFacing targetFacing,
			out TargetSolver solver) {
			solver = new TargetSolver(BattleState);
			solver.PrepareForAction(actor, action);
			solver.CalculateResults();

			// Is this a valid target?
			TargetSolverResult result = solver.GetResultAtPosition(target);
			if(result == null || result.ResultType != TargetSolverResultType.Valid) {
				return false;
			}

			// If it's a summon, it can't already be active.
			if(action.Summon != null) {
				if(action.Summon.ActiveActorID != BattleActionSummon.InactiveID) {
					return false;
				}
			}

			return true;
		}
		
		/// <summary>
		/// Resolve the action!
		/// </summary>
		/// <param name="actor"></param>
		/// <param name="action"></param>
		/// <param name="target"></param>
		/// <param name="targetFacing"></param>
		/// <param name="solver"></param>
		/// <returns></returns>
		private ActionResultData[] ResolveAction(
			Actor actor, BattleAction action,
			GridVector target, GridFacing targetFacing,
			TargetSolver solver) {
			List<ActionResultData> results = new List<ActionResultData>();

			// If the action has a HealthEffect, we have to resolve it.
			if(action.HealthEffect != null) {
				int potency = action.HealthEffect.PotencyValue;
				if(action.HealthEffect.PotencyIncludePower) {
					potency += actor.CurrentEntity.Stats.Power;
				}
				if(action.HealthEffect.PotencyType == BattleActionEffectType.Harm) {
					potency = -potency;
				}

				TargetSolverResult solverResult = solver.GetResultAtPosition(target);
				foreach(GridVector tile in solverResult.AffectedTiles) {
					Actor targetActor = BattleState.GetActorAtPosition(tile);
					if(targetActor == null) {
						// If there isn't an actor at the target, we still add the tile to
						// results. This way, the clients will know to show spell effects
						// there if necessary.
						results.Add(new ActionResultData(tile, ActionResultType.Empty, potency, null));
						continue;
					}

					// Target gets a chance to evade.
					int evasionChance = 0;
					if(action.HealthEffect.EvasionType == EvasionType.Dodge) {
						evasionChance = targetActor.CurrentEntity.Stats.Dodge;
					}

					int roll = RNG.Next(100);
					if(roll < evasionChance) {
						// Evade!
						results.Add(new ActionResultData(tile, ActionResultType.Dodge, 0, null));
						continue;
					}

					// Hit! Apply mitigation, if applicable. Only applies to harmful effects.
					if(action.HealthEffect.PotencyType == BattleActionEffectType.Harm &&
						action.HealthEffect.MitigationType == MitigationType.Normal) {
						// Potency is negative, so we add target's armor to reduce potency.
						potency += targetActor.CurrentEntity.Stats.Armor;
						if(potency > 0) {
							potency = 0;
						}
					}

					results.Add(new ActionResultData(tile, ActionResultType.Hit, potency, null));

					// Target gets a chance to counter!
					BattleAction counter = targetActor.GetCounterAction();
					if(counter != null &&
						action.CounterableType == counter.Counter.Type) {
						// Valid counter! Is it in range?
						if(IsValidAction(targetActor, counter, actor.Position, targetActor.Facing, out TargetSolver counterSolver)) {
							ActionResultData[] counterResults = ResolveAction(targetActor, counter, actor.Position, targetActor.Facing, counterSolver);
							results.AddRange(counterResults);
						}
					}
				}
			}
			// If there's a summon, we resolve that too!
			if(action.Summon != null) {
				int spawnActorId = nextActorId++;
				action.Summon.ActiveActorID = spawnActorId;

				SpawnActorEvent spawnEvent = new SpawnActorEvent(
					BattleState.Time,
					spawnActorId, actor.TeamID,
					target, actor.Facing,
					EntityCache.GetEntity(action.Summon.HeroID));

				results.Add(new ActionResultData(target, ActionResultType.Summon, 0, spawnEvent.Serialize()));
			}

			return results.ToArray();
		}

		/*******************************************************************************/
		private static readonly int TimeStep = 1;
		private static readonly int APCap = 10;

		private BattlePlugin battlePlugin;
		private int nextActorId = 1;

		private int currentTeamIndex = 0;

		private Dictionary<RequestType, Action<Team, IRequest>> requestHandlers;

		#endregion

	}

}
