﻿using System;
using System.Collections.Generic;
using Photon.Hive.Plugin;

namespace HeroesTacticsPlugins {

	public class PluginFactory : IPluginFactory {

		public IGamePlugin Create(IPluginHost gameHost, string pluginName, Dictionary<string, string> config, out string errorMsg) {
			var plugin = new BattlePlugin();
			if(plugin.SetupInstance(gameHost, config, out errorMsg)) {
				return plugin;
			}
			return null;
		}

	}

}
