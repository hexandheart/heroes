﻿namespace HeroesTacticsPlugins {

	internal class TeamController {

		public static readonly int BotClientID = 0;

		public int TeamID { get; private set; }
		public int ClientID { get; private set; }

		protected BattlePlugin BattlePlugin { get; private set; }
		public BattleController BattleController { get; private set; }

		public bool IsHuman {
			get {
				return ClientID != BotClientID;
			}
		}

		public void SetBattleController(BattleController battleController) {
			BattleController = battleController;
		}

		/*******************************************************************************/
		#region Constructor

		private TeamController() { }

		public TeamController(BattlePlugin battlePlugin, int teamId, int clientId) {
			BattlePlugin = battlePlugin;
			TeamID = teamId;
			ClientID = clientId;
		}

		#endregion

	}
}
