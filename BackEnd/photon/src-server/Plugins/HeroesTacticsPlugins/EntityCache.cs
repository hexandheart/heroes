﻿using System.Collections.Generic;
using BattleMechanics;

namespace HeroesTacticsPlugins {

	internal class EntityCache {

		public void RegisterEntity(Entity entity, Dictionary<string, object> brainParams) {
			if(!cachedEntities.ContainsKey(entity.ID)) {
				cachedEntities.Add(entity.ID, entity);
			}
			if(!cachedBrainParams.ContainsKey(entity.ID)) {
				cachedBrainParams.Add(entity.ID, brainParams);
			}
		}

		public Entity GetEntity(string id) {
			if(!cachedEntities.ContainsKey(id)) {
				return null;
			}
			return cachedEntities[id];
		}

		public Dictionary<string, object> GetBrainParams(string id) {
			if(!cachedBrainParams.ContainsKey(id)) {
				return null;
			}
			return cachedBrainParams[id];
		}

		/*******************************************************************************/
		#region Privates

		private Dictionary<string, Entity> cachedEntities = new Dictionary<string, Entity>();
		private Dictionary<string, Dictionary<string, object>> cachedBrainParams = new Dictionary<string, Dictionary<string, object>>();

		#endregion

	}

}
