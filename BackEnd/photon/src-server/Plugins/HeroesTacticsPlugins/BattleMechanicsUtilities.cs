﻿using System;
using System.IO;
using BattleMechanics;
using BattleMechanics.Events;
using LitJson;
using System.Collections.Generic;

namespace HeroesTacticsPlugins {

	public static class BattleMechanicsUtilities {

		/// <summary>
		/// Retrieve a JsonData value as an int.
		/// </summary>
		/// <param name="jsonData"></param>
		/// <returns></returns>
		public static int AsInt(this JsonData jsonData) {
			if(jsonData.IsInt) {
				return (int)jsonData;
			}
			else if(jsonData.IsDouble) {
				return (int)((double)jsonData);
			}

			return 0;
		}

	}

}
