﻿namespace BattleMechanics {

	public enum GridFacing {
		North,
		East,
		South,
		West,
	}

}
