﻿namespace BattleMechanics.Objectives {

	public enum ObjectiveType {
		DefeatEnemies,
	}

	public abstract class IObjective {

		public abstract ObjectiveType Type { get; }
		public int TeamID { get; }
		public abstract bool IsMet(BattleState battleState);

		public IObjective(int teamId) {
			TeamID = teamId;
		}

		public abstract void Initialize(BattleState battleState);

	}

	public static class ObjectiveFactory {

		public static IObjective Create(ObjectiveType type, int teamId) {
			switch(type) {
			case ObjectiveType.DefeatEnemies: return new DefeatEnemiesObjective(teamId);
			}
			return null;
		}

	}

}
