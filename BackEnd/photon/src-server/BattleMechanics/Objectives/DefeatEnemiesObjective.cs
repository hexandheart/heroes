﻿using System.Collections.Generic;

namespace BattleMechanics.Objectives {

	public class DefeatEnemiesObjective : IObjective {

		public override ObjectiveType Type { get { return ObjectiveType.DefeatEnemies; } }

		public override void Initialize(BattleState battleState) {
			// Does the enemy have a champion?
			for(int i = 0; i < battleState.NumTeams; ++i) {
				Team team = battleState.GetTeamAtIndex(i);
				if(team.TeamID == TeamID) {
					continue;
				}

				Actor champion = team.Champion;
				if(champion == null) {
					continue;
				}

				enemyChampions.Add(champion);
			}
		}

		public override bool IsMet(BattleState battleState) {
			if(enemyChampions.Count > 0) {
				foreach(var champion in enemyChampions) {
					if(champion.IsAlive) {
						return false;
					}
				}
			}
			else {
				foreach(var actor in battleState.GetAllActors()) {
					if(actor.TeamID != TeamID && actor.IsAlive) {
						return false;
					}
				}
			}
			return true;
		}

		/*******************************************************************************/
		#region Constructor

		public DefeatEnemiesObjective(int teamId) : base(teamId) { }

		#endregion

		/*******************************************************************************/
		#region Privates

		private List<Actor> enemyChampions = new List<Actor>();

		#endregion

	}

}
