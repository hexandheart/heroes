﻿using System.IO;

namespace BattleMechanics {

	public class BattleActionCounter {

		public CounterType Type { get; private set; }

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write((int)Type);
		}

		public static BattleActionCounter ReadFrom(BinaryReader reader) {
			CounterType type = (CounterType)reader.ReadInt32();

			return new BattleActionCounter(
				type);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static BattleActionCounter Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private BattleActionCounter() { }

		public BattleActionCounter(
			CounterType type) {
			Type = type;
		}

		public BattleActionCounter(BattleActionCounter o) {
			Type = o.Type;
		}

		#endregion

	}

}
