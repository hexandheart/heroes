﻿using System.IO;

namespace BattleMechanics {

	public class LoadoutSlot {

		public string HeroID { get; private set; }
		public int SpawnSlot { get; private set; }

		/*******************************************************************************/
		public LoadoutSlot(string heroId, int spawnSlot) {
			HeroID = heroId;
			SpawnSlot = spawnSlot;
		}

		/*******************************************************************************/
		#region Binary serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write(SpawnSlot);
			writer.Write(HeroID);
		}

		public static LoadoutSlot ReadFrom(BinaryReader reader) {
			int spawnSlot = reader.ReadInt32();
			string heroId = reader.ReadString();

			LoadoutSlot ret = new LoadoutSlot() {
				HeroID = heroId,
				SpawnSlot = spawnSlot,
			};

			return ret;
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private LoadoutSlot() { }

		#endregion

	}

}
