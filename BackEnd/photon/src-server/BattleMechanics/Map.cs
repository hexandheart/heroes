﻿using System.Collections.Generic;
using System.IO;

namespace BattleMechanics {

	/// <summary>
	/// Contains information about the physical battle space (grid).
	/// </summary>
	public class Map {

		/// <summary>
		/// Width of the map grid;
		/// </summary>
		public int GridWidth { get; private set; }

		/// <summary>
		/// Height of the map grid.
		/// </summary>
		public int GridHeight { get; private set; }

		/// <summary>
		/// Retrieve the tile at the specified grid position.
		/// </summary>
		/// <param name="gridX"></param>
		/// <param name="gridY"></param>
		/// <returns></returns>
		public Tile GetTileAt(int gridX, int gridY) {
			if(gridX < 0 || gridX >= GridWidth ||
				gridY < 0 || gridY >= GridHeight) {
				return Tile.Invalid;
			}

			return tiles[gridY * GridWidth + gridX];
		}

		public Tile GetTileAt(GridVector position) {
			return GetTileAt(position.GridX, position.GridY);
		}

		/// <summary>
		/// Client-side scene used for this map.
		/// </summary>
		public string Scene { get; private set; }

		/// <summary>
		/// Gets the spawn zone for the specified team.
		/// </summary>
		/// <param name="teamId"></param>
		/// <returns></returns>
		public SpawnZone GetSpawnZoneForTeam(int teamId) {
			if(spawnZones == null) {
				// You shouldn't be trying to get this. What are you doing?
				return null;
			}
			return spawnZones.Find(sz => sz.TeamID == teamId);
		}

		/*******************************************************************************/
		#region Constructors

		/// <summary>
		/// Constructs a Map with blank tiles (TileFlags.None).
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="tiles"></param>
		/// <param name="scene"></param>
		/// <param name="spawnZones"></param>
		public Map(int width, int height, Tile[] tiles, string scene, List<SpawnZone> spawnZones) {
			GridWidth = width;
			GridHeight = height;
			this.tiles = tiles;
			Scene = scene;
			this.spawnZones = spawnZones;
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					writer.Write(Scene);
					writer.Write(GridWidth);
					writer.Write(GridHeight);

					for(int y = 0; y < GridHeight; ++y) {
						for(int x = 0; x < GridWidth; ++x) {
							writer.Write(GetTileAt(x, y));
						}
					}

					return stream.ToArray();
				}
			}
		}

		public static Map From(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					string scene = reader.ReadString();
					int gridWidth = reader.ReadInt32();
					int gridHeight = reader.ReadInt32();
					Tile[] tiles = new Tile[gridWidth * gridHeight];

					for(int y = 0; y < gridHeight; ++y) {
						for(int x = 0; x < gridWidth; ++x) {
							tiles[y * gridWidth + x] = reader.ReadTile();
						}
					}

					// We intentionally don't pass spawn zones to the client.
					Map ret = new Map(gridWidth, gridHeight, tiles, scene, null);
					return ret;
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		/// <summary>
		/// Tile map!
		/// </summary>
		private Tile[] tiles;

		/// <summary>
		/// SERVER-SIDE ONLY. List of spawn zones.
		/// </summary>
		private List<SpawnZone> spawnZones;

		#endregion

	}

}
