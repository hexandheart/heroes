﻿using System.Collections.Generic;

namespace BattleMechanics {

	/// <summary>
	/// SERVER-SIDE ONLY. Describes the zone in which a team will spawn.
	/// </summary>
	public class SpawnZone {

		/// <summary>
		/// Zone's team ID.
		/// </summary>
		public int TeamID { get; private set; }

		/// <summary>
		/// Number of spawn points in the zone.
		/// </summary>
		/// <returns></returns>
		public int GetNumSpawnPoints() {
			return spawnPoints.Length;
		}

		/// <summary>
		/// Retrieves the location of the indexed spawn point.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public GridVector GetSpawnPoint(int index) {
			return spawnPoints[index];
		}

		/// <summary>
		/// Retrieves the facing of the indexed spawn point.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public GridFacing GetSpawnFacing(int index) {
			return spawnFacings[index];
		}

		/*******************************************************************************/

		#region Constructors

		public SpawnZone(int teamId, GridVector[] spawnPoints, GridFacing[] spawnFacings) {
			TeamID = teamId;
			this.spawnPoints = spawnPoints;
			this.spawnFacings = spawnFacings;
		}

		#endregion

		/*******************************************************************************/

		#region Privates

		private GridVector[] spawnPoints;
		private GridFacing[] spawnFacings;

		#endregion

	}

}
