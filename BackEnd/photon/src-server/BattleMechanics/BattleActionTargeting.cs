﻿using System.IO;

namespace BattleMechanics {

	public class BattleActionTargeting {

		public static BattleActionTargeting Default {
			get {
				return new BattleActionTargeting(
					1, 1,
					true, false,
					true, false, false
					);
			}
		}

		public int RangeMin { get; private set; }
		public int RangeMax { get; private set; }

		public bool RequireLOS { get; private set; }
		public bool RequirePath { get; private set; }

		public bool CanTargetActor { get; private set; }
		public bool CanTargetSpace { get; private set; }
		public bool CanTargetNonWalkableTile { get; private set; }

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write(RangeMin);
			writer.Write(RangeMax);

			writer.Write(RequireLOS);
			writer.Write(RequirePath);

			writer.Write(CanTargetActor);
			writer.Write(CanTargetSpace);
			writer.Write(CanTargetNonWalkableTile);
		}

		public static BattleActionTargeting ReadFrom(BinaryReader reader) {
			int rangeMin = reader.ReadInt32();
			int rangeMax = reader.ReadInt32();

			bool requireLos = reader.ReadBoolean();
			bool requirePath = reader.ReadBoolean();

			bool canTargetActor = reader.ReadBoolean();
			bool canTargetSpace = reader.ReadBoolean();
			bool canTargetNonWalkable = reader.ReadBoolean();

			return new BattleActionTargeting(
				rangeMin, rangeMax,
				requireLos, requirePath,
				canTargetActor, canTargetSpace, canTargetNonWalkable);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static BattleActionTargeting Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private BattleActionTargeting() { }

		public BattleActionTargeting(
			int rangeMin, int rangeMax,
			bool requireLos, bool requirePath,
			bool canTargetActor, bool canTargetSpace, bool canTargetNonWalkableTile) {
			RangeMin = rangeMin;
			RangeMax = rangeMax;

			RequireLOS = requireLos;
			RequirePath = requirePath;

			CanTargetActor = canTargetActor;
			CanTargetSpace = canTargetSpace;
			CanTargetNonWalkableTile = canTargetNonWalkableTile;
		}

		public BattleActionTargeting(BattleActionTargeting o) {
			RangeMin = o.RangeMin;
			RangeMax = o.RangeMax;

			RequireLOS = o.RequireLOS;
			RequirePath = o.RequirePath;

			CanTargetActor = o.CanTargetActor;
			CanTargetSpace = o.CanTargetSpace;
			CanTargetNonWalkableTile = o.CanTargetNonWalkableTile;
		}

		#endregion

	}

}
