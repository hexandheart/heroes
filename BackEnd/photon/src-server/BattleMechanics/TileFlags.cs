﻿namespace BattleMechanics {

	/// <summary>
	/// Flags which describe the mechanics-related properties of a tile.
	/// </summary>
	[System.Flags]
	public enum TileFlags {
		/// <summary>
		/// Invalid tile (notably the result of a tile being off the map).
		/// </summary>
		Invalid = 0xFF,

		/// <summary>
		/// Tile is passable.
		/// </summary>
		None = 0x00,

		/// <summary>
		/// Tile blocks ground movement.
		/// </summary>
		BlocksGroundMovement = 0x01,

		/// <summary>
		/// Tile blocks vision.
		/// </summary>
		BlocksVision = 0x02,

	}

}
