﻿using System.IO;

namespace BattleMechanics {

	/// <summary>
	/// Describes the properties of a single tile on the battle grid.
	/// </summary>
	public struct Tile {

		public static readonly Tile Default = new Tile();
		public static readonly Tile Invalid = new Tile(TileFlags.Invalid);

		/// <summary>
		/// Flags of the tile, describing passability/vision etc.
		/// </summary>
		public TileFlags Flags { get; private set; }
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="flags">Flags to assign to the tile.</param>
		public Tile(TileFlags flags) {
			Flags = flags;
		}

	}

}
