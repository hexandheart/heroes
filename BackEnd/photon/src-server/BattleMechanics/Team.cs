﻿using System.Collections.Generic;
using BattleMechanics.Breezes;
using BattleMechanics.Objectives;

namespace BattleMechanics {

	/// <summary>
	/// Describes a team (ie a group of actors controlled by a single individual, either human or AI).
	/// </summary>
	public class Team {

		public static readonly int BotClientID = 0;

		/// <summary>
		/// ID of the team.
		/// </summary>
		public int TeamID { get; private set; }

		/// <summary>
		/// Client in control of this team.
		/// </summary>
		public int ClientID { get; private set; }

		/// <summary>
		/// Is this a human team?
		/// </summary>
		public bool IsHuman {
			get {
				return ClientID != BotClientID;
			}
		}

		/// <summary>
		/// The team's loadout. Can change over time as aspects become visible to the client.
		/// </summary>
		public Loadout Loadout { get; private set; }

		/// <summary>
		/// Amount of AP currently available to the team.
		/// </summary>
		public int AP { get; set; }

		/// <summary>
		/// Maximum amount of AP currently available to the team.
		/// </summary>
		public int MaxAP { get; set; }

		/// <summary>
		/// The team's victory objective.
		/// </summary>
		public IObjective Objective { get; private set; }

		/// <summary>
		/// The team's champion.
		/// </summary>
		public Actor Champion { get; set; }

		/// <summary>
		/// Shows the actor a team has selected.
		/// </summary>
		public ActorSelectionBreeze ActorSelectionBreeze { get; private set; }

		/// <summary>
		/// Shows the targeting the team is previewing.
		/// </summary>
		public TargetingBreeze TargetingBreeze { get; private set; }

		/// <summary>
		/// Initialize our Breezes once we have the battle state available.
		/// </summary>
		/// <param name="battleState"></param>
		public void InitializeBreezes(BattleState battleState) {
			ActorSelectionBreeze = new ActorSelectionBreeze(battleState, this);
			TargetingBreeze = new TargetingBreeze(battleState, this);
		}

		/*******************************************************************************/
		#region Constructor

		public Team(int teamId, int clientId, ObjectiveType objectiveType) {
			TeamID = teamId;
			ClientID = clientId;
			Loadout = null;
			AP = 0;
			MaxAP = 0;
			Objective = ObjectiveFactory.Create(objectiveType, teamId);
		}

		public Team(int teamId, int clientId, Loadout loadout, ObjectiveType objectiveType) {
			TeamID = teamId;
			ClientID = clientId;
			Loadout = loadout;
			AP = 0;
			MaxAP = 0;
			Objective = ObjectiveFactory.Create(objectiveType, teamId);
		}

		#endregion

	}

}
