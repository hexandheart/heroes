﻿using System;
using System.IO;

namespace BattleMechanics {

	public class BattleActionArea {

		public bool AlignToFacing { get; private set; }

		public GridVector[] GetShape(GridFacing targetFacing) {
			if(!AlignToFacing) {
				return Shape;
			}

			GridVector[] ret = new GridVector[Shape.Length];
			for(int i = 0; i < Shape.Length; ++i) {
				ret[i] = FacingTranslations[(int)targetFacing](Shape[i]);
			}
			return ret;
		}

		/*******************************************************************************/
		#region Privates

		private GridVector[] Shape { get; set; }

		private static readonly Func<GridVector, GridVector>[] FacingTranslations = new Func<GridVector, GridVector>[] {
			// North: (-y, -x)
			(v) => { return new GridVector(-v.GridY, -v.GridX); },
			// East:  ( x,  y) (unchanged)
			(v) => { return v; },
			// South: ( y,  x)
			(v) => { return new GridVector( v.GridY,  v.GridX); },
			// West:  (-x, -y)
			(v) => { return new GridVector(-v.GridX, -v.GridY); },
		};

		#endregion

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write(AlignToFacing);

			writer.Write(Shape.Length);
			for(int i = 0; i < Shape.Length; ++i) {
				Shape[i].WriteTo(writer);
			}
		}

		public static BattleActionArea ReadFrom(BinaryReader reader) {
			bool alignToFacing = reader.ReadBoolean();

			int count = reader.ReadInt32();
			GridVector[]  shape = new GridVector[count];
			for(int i = 0; i < count; ++i) {
				shape[i] = GridVector.ReadFrom(reader);
			}

			return new BattleActionArea(
				alignToFacing,
				shape);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static BattleActionArea Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private BattleActionArea() { }

		public BattleActionArea(
			bool alignToFacing,
			GridVector[] shape) {
			AlignToFacing = alignToFacing;
			Shape = shape;
		}

		public BattleActionArea(BattleActionArea o) {
			AlignToFacing = o.AlignToFacing;

			Shape = new GridVector[o.Shape.Length];
			for(int i = 0; i < o.Shape.Length; ++i) {
				Shape[i] = new GridVector(o.Shape[i]);
			}
		}

		#endregion

	}

}
