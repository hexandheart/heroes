﻿using System.Collections.Generic;
using System.IO;

namespace BattleMechanics {

	public enum BattleActionGroupType {
		None,
		Basic,
		Summons,
	}

	public class BattleActionGroup {

		public BattleActionGroupType GroupType { get; private set; }
		public int NumSlots { get; private set; }
		public Dictionary<int, BattleAction> Actions { get; private set; }

		public BattleAction FindActionByID(string actionId) {
			foreach(var action in Actions.Values) {
				if(action.ID == actionId) {
					return action;
				}
			}
			return null;
		}

		public BattleAction FindSummonAction(int actorId) {
			foreach(var action in Actions.Values) {
				if(action.Summon == null) {
					continue;
				}

				if(action.Summon.ActiveActorID == actorId) {
					return action;
				}
			}

			return null;
		}

		public BattleAction GetActionBySlot(int actionSlot) {
			if(!Actions.ContainsKey(actionSlot)) {
				return null;
			}
			return Actions[actionSlot];
		}

		/*******************************************************************************/
		#region Serialization
		
		public void WriteTo(BinaryWriter writer) {
			writer.Write((int)GroupType);
			writer.Write(NumSlots);

			writer.Write(Actions.Count);
			foreach(var kv in Actions) {
				writer.Write(kv.Key);
				Actions[kv.Key].WriteTo(writer);
			}
		}

		public static BattleActionGroup ReadFrom(BinaryReader reader) {
			BattleActionGroupType groupType = (BattleActionGroupType)reader.ReadInt32();
			int numSlots = reader.ReadInt32();

			int count = reader.ReadInt32();
			Dictionary<int, BattleAction> actions = new Dictionary<int, BattleAction>();
			for(int i = 0; i < count; ++i) {
				int slot = reader.ReadInt32();
				actions[slot] = BattleAction.ReadFrom(reader);
			}

			return new BattleActionGroup(
				groupType, numSlots,
				actions);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static BattleActionGroup Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private BattleActionGroup() { }

		public BattleActionGroup(
			BattleActionGroupType groupType,
			int numSlots,
			Dictionary<int, BattleAction> actions) {
			GroupType = groupType;
			NumSlots = numSlots;
			Actions = actions;
		}

		public BattleActionGroup(BattleActionGroup o) {
			GroupType = o.GroupType;
			NumSlots = o.NumSlots;

			// Deep copy of actions.
			Actions = new Dictionary<int, BattleAction>();
			foreach(var kv in o.Actions) {
				Actions[kv.Key] = new BattleAction(o.Actions[kv.Key]);
			}
		}

		#endregion

	}

}
