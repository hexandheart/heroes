﻿using System.IO;

namespace BattleMechanics {

	/// <summary>
	/// Describes a point on a 2D integral grid, used for battle positions and whatnot.
	/// </summary>
	public struct GridVector {

		public static readonly GridVector Zero = new GridVector(0, 0);

		/// <summary>
		/// X coordinate on the grid.
		/// </summary>
		public int GridX { get; private set; }

		/// <summary>
		/// Y coordinate on the grid.
		/// </summary>
		public int GridY { get; private set; }

		public GridVector(int x, int y) {
			GridX = x;
			GridY = y;
		}

		public GridVector(GridVector o) {
			GridX = o.GridX;
			GridY = o.GridY;
		}

		/// <summary>
		/// Get the Manhattan distance between two grid points.
		/// </summary>
		/// <param name="gx1"></param>
		/// <param name="gy1"></param>
		/// <param name="gx2"></param>
		/// <param name="gy2"></param>
		/// <returns></returns>
		public static int Magnitude(int gx1, int gy1, int gx2, int gy2) {
			return System.Math.Abs(gx2 - gx1) + System.Math.Abs(gy2 - gy1);
		}

		/// <summary>
		/// Get the Manhattan magnitude of this vector.
		/// </summary>
		/// <returns></returns>
		public int Magnitude() {
			return System.Math.Abs(GridX) + System.Math.Abs(GridY);
		}

		/// <summary>
		/// Gets a vector representing the suppled facing.
		/// </summary>
		/// <param name="facing"></param>
		/// <returns></returns>
		public static GridVector FromFacing(GridFacing facing) {
			return FacingVectors[(int)facing];
		}

		/// <summary>
		/// Returns the facing for a specified vector.
		/// </summary>
		/// <param name="baseFacing"></param>
		/// <returns></returns>
		public GridFacing GetFacing() {
			if(GridX == 0 && GridY == 0) {
				// This probably isn't what you want. Check if your magnitude is zero!
				return GridFacing.North;
			}

			//  -2-1 0 1 2
			//-2|N N N N N
			//-1|W N N N E
			// 0|W W O E E
			// 1|W S S S E
			// 2|S S S S S
			if(GridY < 0 && -GridY >= System.Math.Abs(GridX)) {
				return GridFacing.North;
			}
			else if(GridX > 0 && GridX > System.Math.Abs(GridY)) {
				return GridFacing.East;
			}
			else if(GridY > 0 && GridY >= System.Math.Abs(GridX)) {
				return GridFacing.South;
			}
			else if(GridX < 0 && -GridX > System.Math.Abs(GridY)) {
				return GridFacing.West;
			}

			// This should never be a thing.
			return GridFacing.North;
		}

		/*******************************************************************************/
		#region Serialization

		/// <summary>
		/// Writes the GridVector to a binary stream.
		/// </summary>
		/// <param name="writer"></param>
		public void WriteTo(BinaryWriter writer) {
			writer.Write(GridX);
			writer.Write(GridY);
		}

		/// <summary>
		/// Reads a GridVector from a binary stream.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public static GridVector ReadFrom(BinaryReader reader) {
			int gridX = reader.ReadInt32();
			int gridY = reader.ReadInt32();

			return new GridVector(gridX, gridY);
		}

		/// <summary>
		/// Serialize the GridVector to a byte buffer.
		/// </summary>
		/// <returns></returns>
		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					writer.Write(GridX);
					writer.Write(GridY);
					return stream.ToArray();
				}
			}
		}

		/// <summary>
		/// Deserialize a GridVector from a byte buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <returns></returns>
		public static GridVector Deserialize(byte[] buffer) {
			using (var stream = new MemoryStream(buffer)) {
				using (var reader = new BinaryReader(stream)) {
					int gridX = reader.ReadInt32();
					int gridY = reader.ReadInt32();

					return new GridVector(gridX, gridY);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Object overrides

		/// <summary>
		/// Get a hash code.
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode() {
			return (GridX << 8) + GridY;
		}

		/// <summary>
		/// Do we equal a thing?
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj) {
			if(obj == null) {
				return false;
			}
			if(!(obj is GridVector)) {
				return false;
			}
			GridVector gv = (GridVector)obj;
			return GridX == gv.GridX && GridY == gv.GridY;
		}

		/// <summary>
		/// It's a GridVector! It's a string!
		/// </summary>
		/// <returns></returns>
		public override string ToString() {
			return string.Format("({0},{1})", GridX, GridY);
		}

		#endregion

		/*******************************************************************************/
		#region Operator overloads

		/// <summary>
		/// Add two GridVectors.
		/// </summary>
		/// <param name="lhs"></param>
		/// <param name="rhs"></param>
		/// <returns></returns>
		public static GridVector operator +(GridVector lhs, GridVector rhs) {
			return new GridVector(lhs.GridX + rhs.GridX, lhs.GridY + rhs.GridY);
		}

		/// <summary>
		/// Subtract two GridVectos.
		/// </summary>
		/// <param name="lhs"></param>
		/// <param name="rhs"></param>
		/// <returns></returns>
		public static GridVector operator -(GridVector lhs, GridVector rhs) {
			return new GridVector(lhs.GridX - rhs.GridX, lhs.GridY - rhs.GridY);
		}

		/// <summary>
		/// Are the two GridVectors equal?
		/// </summary>
		/// <param name="lhs"></param>
		/// <param name="rhs"></param>
		/// <returns></returns>
		public static bool operator ==(GridVector lhs, GridVector rhs) {
			return lhs.Equals(rhs);
		}

		/// <summary>
		/// Are the two GridVectors different?
		/// </summary>
		/// <param name="lhs"></param>
		/// <param name="rhs"></param>
		/// <returns></returns>
		public static bool operator !=(GridVector lhs, GridVector rhs) {
			return !lhs.Equals(rhs);
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly GridVector[] FacingVectors = {
			new GridVector(0, -1),	// North
			new GridVector(1, 0),	// East
			new GridVector(0, 1),	// South
			new GridVector(-1, 0),	// West
		};

		#endregion

	}

}
