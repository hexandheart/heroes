﻿namespace BattleMechanics {

	public enum MitigationType {
		Normal, // Affected by armor
		Piercing, // Not affected by armor
	}

}
