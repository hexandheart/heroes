﻿namespace BattleMechanics {

	/// <summary>
	/// What blocks line of sight or pathing?
	/// </summary>
	[System.Flags]
	public enum TargetSolverBlockFlags {
		Nothing = 0x00,
		Everything = 0xFF,

		TileVision = 0x01,
		TileGroundMovement = 0x02,

		Allies = 0x10,
		Enemies = 0x20,
	}

}
