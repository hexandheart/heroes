﻿using System.Collections.Generic;
using System.IO;

namespace BattleMechanics {

	public class BattleActionMap {

		public static BattleActionMap Default {
			get {
				return new BattleActionMap(
					null,
					new List<BattleActionGroup>());
			}
		}
		
		public BattleAction CounterAction { get; private set; }

		public List<BattleActionGroup> ActionGroups { get; private set; }

		public BattleAction FindActionByID(string actionId) {
			foreach(var actionGroup in ActionGroups) {
				BattleAction action = actionGroup.FindActionByID(actionId);
				if(action != null) {
					return action;
				}
			}

			return null;
		}

		public BattleAction FindSummonAction(int actorId) {
			foreach(var actionGroup in ActionGroups) {
				BattleAction action = actionGroup.FindSummonAction(actorId);
				if(action != null) {
					return action;
				}
			}

			return null;
		}

		public BattleAction GetAction(BattleActionGroupType actionGroup, int actionSlot) {
			foreach(var ag in ActionGroups) {
				if(ag.GroupType != actionGroup) {
					continue;
				}
				
				BattleAction action = ag.GetActionBySlot(actionSlot);
				if(action != null) {
					return action;
				}
			}

			return null;
		}

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write(CounterAction != null);
			if(CounterAction != null) {
				CounterAction.WriteTo(writer);
			}
			writer.Write(ActionGroups.Count);
			foreach(var ag in ActionGroups) {
				ag.WriteTo(writer);
			}
		}

		public static BattleActionMap ReadFrom(BinaryReader reader) {
			BattleAction counter = reader.ReadBoolean() ? BattleAction.ReadFrom(reader) : null;
			int count = reader.ReadInt32();
			List<BattleActionGroup> actionGroups = new List<BattleActionGroup>(count);
			for(int i = 0; i < count; ++i) {
				actionGroups.Add(BattleActionGroup.ReadFrom(reader));
			}

			return new BattleActionMap(
				counter,
				actionGroups);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static BattleActionMap Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private BattleActionMap() { }

		public BattleActionMap(
			BattleAction counter,
			List<BattleActionGroup> actionGroups) {
			CounterAction = counter;
			ActionGroups = actionGroups;
		}

		public BattleActionMap(BattleActionMap o) {
			if(o.CounterAction != null) {
				CounterAction = new BattleAction(o.CounterAction);
			}
			ActionGroups = new List<BattleActionGroup>();
			foreach(var ag in o.ActionGroups) {
				ActionGroups.Add(new BattleActionGroup(ag));
			}
		}

		#endregion

	}

}
