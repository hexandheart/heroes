﻿using System.IO;

namespace BattleMechanics {

	public class BattleActionCost {

		public int AP { get; private set; }
		public int Primary { get; private set; }
		public int Minor { get; private set; }

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write(AP);
			writer.Write(Primary);
			writer.Write(Minor);
		}

		public static BattleActionCost ReadFrom(BinaryReader reader) {
			int ap = reader.ReadInt32();
			int primary = reader.ReadInt32();
			int minor = reader.ReadInt32();

			return new BattleActionCost(
				ap,
				primary, minor);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static BattleActionCost Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private BattleActionCost() { }

		public BattleActionCost(
			int ap,
			int primary, int minor) {
			AP = ap;
			Primary = primary;
			Minor = minor;
		}

		public BattleActionCost(BattleActionCost o) {
			AP = o.AP;
			Primary = o.Primary;
			Minor = o.Minor;
		}

		#endregion

	}

}
