﻿using System.Collections;
using System.Collections.Generic;

namespace BattleMechanics.Breezes {

	/// <summary>
	/// Team is previewing potential targets.
	/// </summary>
	public class TargetingBreeze : IBreeze {

		public static readonly int SourceActorIDKey = 0;
		public int SourceActorID { get; private set; }
		public static readonly int ActionGroupKey = 1;
		public BattleActionGroupType ActionGroup { get; private set; }
		public static readonly int ActionSlotKey = 2;
		public int ActionSlot { get; private set; }

		public TargetingBreeze(BattleState battleState, Team team) : base(battleState, team) { }

		public void ShowTargeting(int sourceActorId, BattleAction action, bool notify = true) {
			SourceActorID = sourceActorId;
			ActionGroup = action.ActionGroup;
			ActionSlot = action.ActionSlot;
			
			BreezeUpdated(notify);
		}

		public void ClearTargeting(bool notify = true) {
			SourceActorID = -1;
			ActionGroup = BattleActionGroupType.None;
			ActionSlot = -1;
			
			BreezeUpdated(notify);
		}

		public override string ToString() {
			return string.Format("[TargetingBreeze:{0} - {1}-{2}]", SourceActorID, ActionGroup, ActionSlot);
		}

		/*******************************************************************************/
		#region IBreeze implementation

		public override BreezeType BreezeType { get { return BreezeType.Targeting; } }

		public override IDictionary GetData() {
			return new Dictionary<int, object>() {
				{ SourceActorIDKey, SourceActorID },
				{ ActionGroupKey, (int)ActionGroup },
				{ ActionSlotKey, ActionSlot },
			};
		}

		public override void UpdateFrom(IDictionary data) {
			SourceActorID = (int)data[SourceActorIDKey];
			ActionGroup = (BattleActionGroupType)(int)data[ActionGroupKey];
			ActionSlot = (int)data[ActionSlotKey];
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private TargetingBreeze() : base(null, null) { }

		#endregion

	}

}
