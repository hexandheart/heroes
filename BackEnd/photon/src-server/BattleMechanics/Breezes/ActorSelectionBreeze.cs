﻿using System.Collections;
using System.Collections.Generic;

namespace BattleMechanics.Breezes {

	/// <summary>
	/// Team's actor selection.
	/// </summary>
	public class ActorSelectionBreeze : IBreeze {

		public static readonly int None = -1;

		public static readonly int ActorIDKey = 0;
		public int ActorID { get; private set; }

		public ActorSelectionBreeze(BattleState battleState, Team team) : base(battleState, team) { }

		public void Select(int actorId, bool notify = true) {
			ActorID = actorId;

			// When an actor is selected, we always also show their movement...
			// but only if they have minor actions available.
			Actor actor = BattleState.GetActorByID(ActorID);
			if(actor != null) {
				BattleAction movement = actor.GetDefaultMovementAction();
				if(actor.CanAffordAction(movement)) {
					Team.TargetingBreeze.ShowTargeting(actorId, movement, notify);
				}
				else {
					Team.TargetingBreeze.ClearTargeting(notify);
				}
			}
			
			BreezeUpdated(notify);
		}

		public void SelectNone(bool notify = true) {
			ActorID = None;

			// Always make sure we stop showing targeting if we've deselected an actor.
			Team.TargetingBreeze.ClearTargeting(notify);
			
			BreezeUpdated(notify);
		}

		public override string ToString() {
			return string.Format("[ActionSelectionBreeze:{0}]", ActorID);
		}

		/*******************************************************************************/
		#region IBreeze implementation

		public override BreezeType BreezeType { get { return BreezeType.ActorSelection; } }

		public override IDictionary GetData() {
			return new Dictionary<int, object>() {
				{ ActorIDKey, ActorID },
			};
		}

		public override void UpdateFrom(IDictionary data) {
			ActorID = (int)data[ActorIDKey];
		}

		#endregion

		/*******************************************************************************/
		#region Privates
			
		private ActorSelectionBreeze() : base(null, null) { }

		#endregion

	}

}
