﻿using System.Collections;

namespace BattleMechanics.Breezes {

	public enum BreezeType {
		ActorSelection,
		Targeting,
	}

	public delegate void UpdateBreezeHandler(IBreeze breeze);

	public abstract class IBreeze {
		public static event UpdateBreezeHandler OnBreezeUpdatedNoNotify;
		public static event UpdateBreezeHandler OnLocalBreezeUpdated;

		public IBreeze(BattleState battleState, Team team) {
			BattleState = battleState;
			Team = team;
		}

		protected void BreezeUpdated(bool notify) {
			if(notify) {
				OnLocalBreezeUpdated?.Invoke(this);
			}
			else {
				OnBreezeUpdatedNoNotify?.Invoke(this);
			}
		}

		public abstract BreezeType BreezeType { get; }
		public Team Team { get; private set; }
		protected BattleState BattleState { get; private set; }

		public abstract IDictionary GetData();
		public abstract void UpdateFrom(IDictionary data);

		private IBreeze() { }
	}

}
