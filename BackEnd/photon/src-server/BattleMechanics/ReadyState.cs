﻿namespace BattleMechanics {
	public enum ReadyState {
		PreGame,
		Ready,
		PostGame,
	}
}
