﻿using System.IO;

namespace BattleMechanics {

	public class Entity {
		
		/// <summary>
		/// The entity's back-end ID.
		/// </summary>
		public string ID { get; private set; }

		/// <summary>
		/// Asset name used by client for showing pawns.
		/// </summary>
		public string AssetID { get; private set; }

		/// <summary>
		/// Localization ID.
		/// </summary>
		public string LocID { get; private set; }

		/// <summary>
		/// The entity's current base stats.
		/// </summary>
		public Stats Stats { get; private set; }

		/// <summary>
		/// The entity's type.
		/// </summary>
		public EntityType Type { get; private set; }

		/// <summary>
		/// The entity's available actions.
		/// </summary>
		public BattleActionMap ActionMap { get; private set; }

		public BattleAction FindActionByID(string actionId) {
			return ActionMap.FindActionByID(actionId);
		}

		public BattleAction FindSummonAction(int actorId) {
			return ActionMap.FindSummonAction(actorId);
		}

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write(ID);
			writer.Write(AssetID);
			writer.Write(LocID);
			writer.Write((int)Type);
			Stats.WriteTo(writer);
			ActionMap.WriteTo(writer);
		}

		public static Entity ReadFrom(BinaryReader reader) {
			string id = reader.ReadString();
			string assetId = reader.ReadString();
			string locId = reader.ReadString();
			EntityType type = (EntityType)reader.ReadInt32();
			Stats stats = Stats.ReadFrom(reader);
			BattleActionMap actionMap = BattleActionMap.ReadFrom(reader);

			return new Entity(id, assetId, locId, type, stats, actionMap);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static Entity Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private Entity() { }

		public Entity(
			string id, string assetId, string locId,
			EntityType type,
			Stats stats,
			BattleActionMap actionMap) {
			ID = id;
			AssetID = assetId;
			LocID = locId;
			Type = type;
			Stats = stats;
			ActionMap = actionMap;
		}

		public Entity(Entity o) {
			ID = o.ID;
			AssetID = o.AssetID;
			LocID = o.LocID;
			Type = o.Type;
			Stats = new Stats(o.Stats);
			ActionMap = new BattleActionMap(o.ActionMap);
		}

		#endregion

	}

}
