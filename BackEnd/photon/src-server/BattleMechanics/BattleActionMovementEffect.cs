﻿using System.IO;

namespace BattleMechanics {

	public class BattleActionMovementEffect {

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
		}

		public static BattleActionMovementEffect ReadFrom(BinaryReader reader) {
			return new BattleActionMovementEffect();
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static BattleActionMovementEffect Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		public BattleActionMovementEffect() {
			// Default constructor should be private.
		}

		public BattleActionMovementEffect(BattleActionMovementEffect o) {
			// Deep copy.
		}

		#endregion

	}

}
