﻿using System.Collections;

namespace BattleMechanics.Events {

	/// <summary>
	/// Triggered when the match has closed.
	/// </summary>
	public class GameClosedEvent : IBattleEvent {

		/*******************************************************************************/
		public GameClosedEvent(int time) {
			Time = time;
		}

		public void ApplyTo(BattleState battleState) {
		}

		/*******************************************************************************/
		#region IBattleEvent implementation

		public BattleEventType EventType { get { return BattleEventType.BattleEnded; } }

		public IDictionary GetData() {
			Hashtable ret = new Hashtable() {
				{ TimeKey, Time },
			};
			return ret;
		}

		public override string ToString() {
			return string.Format("[GameClosedEvent:T{0}]", Time);
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		public static GameClosedEvent FromData(IDictionary data) {
			int time = (int)data[TimeKey];
			return new GameClosedEvent(time);
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly int TimeKey = 0;
		public int Time { get; private set; }

		#endregion

	}

}
