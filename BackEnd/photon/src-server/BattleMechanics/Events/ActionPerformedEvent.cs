﻿using System.Collections;
using BattleMechanics;
using BattleMechanics.Events.Data;

namespace BattleMechanics.Events {

	public class ActionPerformedEvent : IBattleEvent {

		public ActionPerformedEvent(
			int time,
			int actorId, BattleActionGroupType actionGroup, int actionSlot,
			GridVector source, GridFacing sourceFacing,
			GridVector target, GridFacing targetFacing,
			ActionResultData[] results) {
			Time = time;
			ActorID = actorId;
			ActionGroup = actionGroup;
			ActionSlot = actionSlot;

			Source = source;
			SourceFacing = sourceFacing;

			Target = target;
			TargetFacing = targetFacing;

			Results = results;
			if(results == null) {
				Results = new ActionResultData[0];
			}
		}

		/*******************************************************************************/
		#region IBattleEvent implementation

		public BattleEventType EventType { get { return BattleEventType.ActionPerformed; } }

		public IDictionary GetData() {
			Hashtable ret = new Hashtable {
				{ TimeKey, Time },
				{ ActorIDKey, ActorID },
				{ ActionGroupKey, ActionGroup },
				{ ActionSlotKey, ActionSlot },
				{ SourceKey, Source.Serialize() },
				{ SourceFacingKey, (int)SourceFacing },
				{ TargetKey, Target.Serialize() },
				{ TargetFacingKey, (int)TargetFacing },
				{ ResultsKey, ActionResultData.SerializeArray(Results) },
			};
			return ret;
		}

		public void ApplyTo(BattleState battleState) {
			battleState.Time = Time;

			Actor actor = battleState.GetActorByID(ActorID);
			BattleAction action = actor.GetAction(ActionGroup, ActionSlot);

			ApplyMovement(actor, action, battleState);
			ApplyCost(actor, action, battleState);

			ApplyResults(actor, action, battleState);
		}

		public override string ToString() {
			return string.Format("[ActionPerformedEvent:Actor {0}:{1}-{2}:({3}->{4})]", ActorID, ActionGroup, ActionSlot, Source, Target);
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		public static ActionPerformedEvent FromData(IDictionary data) {
			ActionPerformedEvent ret = new ActionPerformedEvent() {
				Time = (int)data[TimeKey],
				ActorID = (int)data[ActorIDKey],
				ActionGroup = (BattleActionGroupType)(int)data[ActionGroupKey],
				ActionSlot = (int)data[ActionSlotKey],
				Source = GridVector.Deserialize((byte[])data[SourceKey]),
				SourceFacing = (GridFacing)(int)data[SourceFacingKey],
				Target = GridVector.Deserialize((byte[])data[TargetKey]),
				TargetFacing = (GridFacing)(int)data[TargetFacingKey],
				Results = ActionResultData.DeserializeArray((byte[])data[ResultsKey]),
			};
			return ret;
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly int TimeKey = 0;
		public int Time { get; private set; }

		private static readonly int ActorIDKey = 1;
		public int ActorID { get; private set; }

		private static readonly int ActionGroupKey = 2;
		public BattleActionGroupType ActionGroup { get; private set; }

		private static readonly int ActionSlotKey = 3;
		public int ActionSlot { get; private set; }

		private static readonly int SourceKey = 4;
		public GridVector Source { get; private set; }

		private static readonly int SourceFacingKey = 5;
		public GridFacing SourceFacing { get; private set; }

		private static readonly int TargetKey = 6;
		public GridVector Target { get; private set; }

		private static readonly int TargetFacingKey = 7;
		public GridFacing TargetFacing { get; private set; }

		private static readonly int ResultsKey = 8;
		public ActionResultData[] Results { get; private set; }

		private ActionPerformedEvent() { }

		/*******************************************************************************/
		#region Effect calculations

		private void ApplyMovement(Actor actor, BattleAction action, BattleState battleState) {
			if(action.MovementEffect != null) {
				actor.Position = Target;
			}
			actor.Facing = TargetFacing;
		}

		private void ApplyCost(Actor actor, BattleAction action, BattleState battleState) {
			// Consume the cost!
			actor.Team.AP -= action.Cost.AP;
			if(actor.Team.AP < 0) {
				actor.Team.AP = 0;
			}
			actor.CurrentEntity.Stats.PrimaryActionsTaken += action.Cost.Primary;
			actor.CurrentEntity.Stats.MinorActionsTaken += action.Cost.Minor;

			// Disable attack-and-move.
			if(action.Cost.Primary > 0) {
				actor.CurrentEntity.Stats.MinorActionsTaken = actor.CurrentEntity.Stats.MaxMinorActions;
			}
		}

		private void ApplyResults(Actor actor, BattleAction action, BattleState battleState) {
			for(int i = 0; i < Results.Length; ++i) {
				ActionResultData result = Results[i];
				Actor targetActor = battleState.GetActorAtPosition(result.Target);

				if(targetActor != null) {
					result.ApplyTo(battleState, targetActor);
				}

				if(action.Summon != null) {
					ApplySummon(actor, action, result, battleState);
				}
			}
		}

		private void ApplySummon(Actor actor, BattleAction action, ActionResultData result, BattleState battleState) {
			SpawnActorEvent spawnEvent = SpawnActorEvent.Deserialize(result.Data);
			spawnEvent.ApplyTo(battleState);
			action.Summon.ActiveActorID = spawnEvent.ActorID;
		}

		#endregion

		#endregion

	}

}
