﻿using System.Collections;
using System.Collections.Generic;

namespace BattleMechanics.Events {

	public class EntityUpdateEvent : IBattleEvent {

		public EntityUpdateEvent(int time, Actor actor) {
			Time = time;
			ActorID = actor.ID;
			EntityData = actor.CurrentEntity.Serialize();
		}

		/*******************************************************************************/
		#region IBattleEvent implementation

		public BattleEventType EventType { get { return BattleEventType.EntityUpdate; } }

		public IDictionary GetData() {
			IDictionary ret = new Dictionary<int, object>() {
				{ TimeKey, Time },
				{ ActorIDKey, ActorID },
				{ EntityDataKey, EntityData },
			};
			return ret;
		}

		public void ApplyTo(BattleState battleState) {
			battleState.Time = Time;

			Actor actor = battleState.GetActorByID(ActorID);
			actor.CurrentEntity = Entity.Deserialize(EntityData);
		}

		public override string ToString() {
			return string.Format("[EntityModifierUpdateEvent:T{0}:Actor {1}]", Time, ActorID);
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		public static EntityUpdateEvent FromData(IDictionary data) {
			EntityUpdateEvent ret = new EntityUpdateEvent() {
				Time = (int)data[TimeKey],
				ActorID = (int)data[ActorIDKey],
				EntityData = (byte[])data[EntityDataKey],
			};
			return ret;
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly int TimeKey = 0;
		public int Time { get; private set; }

		private static readonly int ActorIDKey = 1;
		public int ActorID { get; private set; }

		private static readonly int EntityDataKey = 2;
		public byte[] EntityData { get; private set; }

		private EntityUpdateEvent() { }

		#endregion

	}

}
