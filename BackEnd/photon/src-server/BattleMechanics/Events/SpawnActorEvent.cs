﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace BattleMechanics.Events {

	public class SpawnActorEvent : IBattleEvent {

		public SpawnActorEvent(int time, Actor actor) {
			Time = time;
			ActorID = actor.ID;
			Owner = actor.TeamID;
			Position = actor.Position;
			Facing = actor.Facing;
			Entity = actor.BaseEntity;
		}

		public SpawnActorEvent(int time, int actorId, int teamId, GridVector position, GridFacing facing, Entity entity) {
			Time = time;
			ActorID = actorId;
			Owner = teamId;
			Position = position;
			Facing = facing;
			Entity = entity;
		}

		/*******************************************************************************/
		#region IBattleEvent implementation

		public BattleEventType EventType { get { return BattleEventType.SpawnActor; } }

		public IDictionary GetData() {
			Dictionary<int, object> ret = new Dictionary<int, object>() {
				{ TimeKey, Time },
				{ ActorIDKey, ActorID },
				{ OwnerKey, Owner },
				{ PositionKey, Position.Serialize() },
				{ FacingKey, Facing },
				{ EntityKey, Entity.Serialize() },
			};
			return ret;
		}

		public void ApplyTo(BattleState battleState) {
			battleState.Time = Time;

			Actor actor = new Actor(
				 ActorID, Owner,
				 Position, Facing,
				 Entity);
			battleState.RegisterActor(actor);
			if(Time > 0) {
				actor.CurrentEntity.Stats.PrimaryActionsTaken = actor.CurrentEntity.Stats.MaxPrimaryActions;
				actor.CurrentEntity.Stats.MinorActionsTaken = actor.CurrentEntity.Stats.MaxMinorActions;
			}
		}

		public override string ToString() {
			return string.Format("[SpawnActorEvent:T{0}:ID {1}, Team {2}]", Time, ActorID, Owner);
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		public static SpawnActorEvent FromData(IDictionary data) {
			SpawnActorEvent ret = new SpawnActorEvent() {
				Time = (int)data[TimeKey],
				ActorID = (int)data[ActorIDKey],
				Owner = (int)data[OwnerKey],
				Position = GridVector.Deserialize((byte[])data[PositionKey]),
				Facing = (GridFacing)data[FacingKey],
				Entity = Entity.Deserialize((byte[])data[EntityKey]),
			};
			return ret;
		}

		public void WriteTo(BinaryWriter writer) {
			writer.Write(Time);
			writer.Write(ActorID);
			writer.Write(Owner);
			Position.WriteTo(writer);
			writer.Write((int)Facing);
			Entity.WriteTo(writer);
		}

		public static SpawnActorEvent ReadFrom(BinaryReader reader) {
			int time = reader.ReadInt32();
			int actorId = reader.ReadInt32();
			int owner = reader.ReadInt32();
			GridVector position = GridVector.ReadFrom(reader);
			GridFacing facing = (GridFacing)reader.ReadInt32();
			Entity entity = Entity.ReadFrom(reader);

			return new SpawnActorEvent() {
				Time = time,
				ActorID = actorId,
				Owner = owner,
				Position = position,
				Facing = facing,
				Entity = entity,
			};
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static SpawnActorEvent Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly int TimeKey = 0;
		public int Time { get; private set; }

		private static readonly int ActorIDKey = 1;
		public int ActorID { get; private set; }

		private static readonly int OwnerKey = 2;
		public int Owner { get; private set; }

		private static readonly int PositionKey = 3;
		public GridVector Position { get; private set; }

		private static readonly int FacingKey = 4;
		public GridFacing Facing { get; private set; }

		private static readonly int EntityKey = 5;
		public Entity Entity { get; private set; }

		private SpawnActorEvent() { }

		#endregion

	}

}
