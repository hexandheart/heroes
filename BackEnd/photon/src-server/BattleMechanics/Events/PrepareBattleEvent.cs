﻿using BattleMechanics.Objectives;
using System.Collections;
using System.IO;

namespace BattleMechanics.Events {

	public class PrepareBattleEvent : IBattleEvent {

		/*******************************************************************************/
		#region IBattleEvent implementation

		public BattleEventType EventType { get { return BattleEventType.PrepareBattle; } }

		public int Time { get { return 0; } }

		public IDictionary GetData() {
			Hashtable ret = new Hashtable {
				{ MapKey, MapData },
				{ TeamsKey, TeamsData },
			};
			return ret;
		}

		public void ApplyTo(BattleState battleState) {
			// Does nothing. You need to call CreateBattle()!
		}

		public override string ToString() {
			return string.Format("[PrepareBattleEvent:T{0}]", Time);
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		public static PrepareBattleEvent FromBattleState(BattleState battle) {
			PrepareBattleEvent ret = new PrepareBattleEvent() {
				MapData = battle.Map.Serialize(),
				TeamsData = SerializeTeams(battle),
			};
			return ret;
		}

		#endregion

		/*******************************************************************************/
		#region Deserialization

		public static PrepareBattleEvent FromData(IDictionary data) {
			PrepareBattleEvent ret = new PrepareBattleEvent() {
				MapData = (byte[])data[MapKey],
				TeamsData = (byte[])data[TeamsKey],
			};
			return ret;
		}

		public BattleState CreateBattle() {
			Map map = Map.From(MapData);
			Team[] teams = DeserializeTeams(TeamsData);

			return new BattleState(map, teams);
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly byte MapKey = 0;
		private byte[] MapData { get; set; }

		private static readonly byte TeamsKey = 1;
		private byte[] TeamsData { get; set; }

		private PrepareBattleEvent() { }

		private static byte[] SerializeTeams(BattleState battle) {
			using (var stream = new MemoryStream()) {
				using (var writer = new BinaryWriter(stream)) {
					writer.Write(battle.NumTeams);

					for(int i = 0; i < battle.NumTeams; ++i) {
						Team team = battle.GetTeamAtIndex(i);
						writer.Write(team.TeamID);
						writer.Write(team.ClientID);
						writer.Write((int)team.Objective.Type);

						bool includeLoadout = false;
						writer.Write(includeLoadout);
					}

					return stream.ToArray();
				}
			}
		}

		private static Team[] DeserializeTeams(byte[] buffer) {
			using (var stream = new MemoryStream(buffer)) {
				using (var reader = new BinaryReader(stream)) {
					int numTeams = reader.ReadInt32();
					Team[] teams = new Team[numTeams];

					for (int i = 0; i < numTeams; ++i) {
						int teamId = reader.ReadInt32();
						int clientId = reader.ReadInt32();
						ObjectiveType objectiveType = (ObjectiveType)reader.ReadInt32();
						bool includeLoadout = reader.ReadBoolean();

						if(includeLoadout) {
							Loadout loadout = Loadout.ReadFrom(reader);
							teams[i] = new Team(teamId, clientId, loadout, objectiveType);
						}
						else {
							teams[i] = new Team(teamId, clientId, objectiveType);
						}
					}

					return teams;
				}
			}
		}

		#endregion

	}

}
