﻿using System.Collections;
using BattleMechanics.Breezes;

namespace BattleMechanics.Events {

	public class BreezeUpdateEvent : IBattleEvent {

		public BreezeUpdateEvent(int time, int teamId, BreezeType breezeType, IDictionary breezeData, bool isForced) {
			Time = time;
			TeamID = teamId;
			BreezeType = breezeType;
			BreezeData = breezeData;
			IsForced = isForced;
		}
		
		/*******************************************************************************/
		#region IBattleEvent implementation

		public BattleEventType EventType { get { return BattleEventType.BreezeUpdate; } }

		public IDictionary GetData() {
			Hashtable ret = new Hashtable {
				{ TimeKey, Time },
				{ TeamIDKey, TeamID },
				{ BreezeTypeKey, (int)BreezeType },
				{ BreezeDataKey, BreezeData },
				{ IsForcedKey, IsForced },
			};
			return ret;
		}

		public void ApplyTo(BattleState battleState) {
			Team team = battleState.GetTeam(TeamID);

			switch(BreezeType) {
			case BreezeType.ActorSelection: team.ActorSelectionBreeze.UpdateFrom(BreezeData); break;
			case BreezeType.Targeting: team.TargetingBreeze.UpdateFrom(BreezeData); break;
			}
		}

		public override string ToString() {
			switch(BreezeType) {
			case BreezeType.ActorSelection:
				return string.Format("[BreezeUpdateEvent:T{0}:Team {1}, {2} @ Actor {3}]", Time, TeamID, BreezeType, BreezeData[ActorSelectionBreeze.ActorIDKey]);
			case BreezeType.Targeting:
				return string.Format("[BreezeUpdateEvent:T{0}:Team {1}, {2} @ SourceActor {3}]", Time, TeamID, BreezeType, BreezeData[TargetingBreeze.SourceActorIDKey]);
			}
			return string.Format("[BreezeUpdateEvent:T{0}:Team {1}, {2}]", Time, TeamID, BreezeType);
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		public static BreezeUpdateEvent FromData(IDictionary data) {
			BreezeUpdateEvent ret = new BreezeUpdateEvent() {
				Time = (int)data[TimeKey],
				TeamID = (int)data[TeamIDKey],
				BreezeType = (BreezeType)(int)data[BreezeTypeKey],
				BreezeData = (IDictionary)data[BreezeDataKey],
				IsForced = (bool)data[IsForcedKey],
			};
			return ret;
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly int TimeKey = 0;
		public int Time { get; private set; }

		private static readonly int TeamIDKey = 1;
		public int TeamID { get; private set; }

		private static readonly int BreezeTypeKey = 2;
		public BreezeType BreezeType { get; private set; }

		private static readonly int BreezeDataKey = 3;
		public IDictionary BreezeData { get; private set; }

		private static readonly int IsForcedKey = 4;
		public bool IsForced { get; private set; }

		private BreezeUpdateEvent() { }

		#endregion

	}

}
