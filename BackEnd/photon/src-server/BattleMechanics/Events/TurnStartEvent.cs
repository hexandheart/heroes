﻿using System.Collections;
using System.Collections.Generic;
using BattleMechanics.Events.Data;

namespace BattleMechanics.Events {

	public class TurnStartEvent : IBattleEvent {

		/*******************************************************************************/
		public TurnStartEvent(int time, int teamId, TeamAPData teamApData) {
			Time = time;
			TeamID = teamId;
			TeamAPData = teamApData;
		}

		/*******************************************************************************/
		#region IBattleEvent implementation

		public BattleEventType EventType { get { return BattleEventType.TurnStart; } }

		public IDictionary GetData() {
			Hashtable ret = new Hashtable {
				{ TimeKey, Time },
				{ TeamIDKey, TeamID },
				{ TeamAPDataKey, TeamAPData.Serialize() },
			};
			return ret;
		}

		public void ApplyTo(BattleState battleState) {
			battleState.Time = Time;
			battleState.CurrentTeamID = TeamID;

			Team team = battleState.GetTeam(TeamID);
			team.MaxAP = TeamAPData.MaxAP;
			team.AP = TeamAPData.AP;

			// All actors on this team get their actions refreshed.
			List<Actor> actors = battleState.GetAllActors().FindAll(a => a.TeamID == TeamID);
			foreach(Actor actor in actors) {
				actor.CurrentEntity.Stats.PrimaryActionsTaken = 0;
				actor.CurrentEntity.Stats.MinorActionsTaken = 0;
			}
		}

		public override string ToString() {
			return string.Format("[TurnStartEvent:T{0}:Team {1}]", Time, TeamID);
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		public static TurnStartEvent FromData(IDictionary data) {
			TurnStartEvent ret = new TurnStartEvent() {
				Time = (int)data[TimeKey],
				TeamID = (int)data[TeamIDKey],
				TeamAPData = TeamAPData.Deserialize(data[TeamAPDataKey]),
			};
			return ret;
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly int TimeKey = 0;
		public int Time { get; private set; }

		private static readonly int TeamIDKey = 1;
		public int TeamID { get; private set; }

		private static readonly int TeamAPDataKey = 2;
		public TeamAPData TeamAPData { get; private set; }

		private TurnStartEvent() { }

		#endregion

	}

}
