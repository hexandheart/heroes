﻿using System.Collections;

namespace BattleMechanics.Events {

	public enum BattleEventType {
		GameClosed,
		BattleEnded,
		BattlePhaseChanged,

		PrepareBattle,
		SpawnActor,
		SpawnMultipleActors,

		BreezeUpdate,
		
		TurnStart,
		ActionPerformed,
		EntityUpdate,
	}

	public interface IBattleEvent {
		int Time { get; }
		BattleEventType EventType { get; }
		IDictionary GetData();
		void ApplyTo(BattleState state);

		string ToString();
	}

	public static class BattleEventFactory {

		public static IBattleEvent FromData(byte eventCode, IDictionary data) {
			switch((BattleEventType)eventCode) {
			case BattleEventType.GameClosed: return GameClosedEvent.FromData(data);
			case BattleEventType.BattleEnded: return BattleEndedEvent.FromData(data);
			case BattleEventType.BattlePhaseChanged: return BattlePhaseChangedEvent.FromData(data);

			case BattleEventType.PrepareBattle: return PrepareBattleEvent.FromData(data);
			case BattleEventType.SpawnActor: return SpawnActorEvent.FromData(data);
			case BattleEventType.SpawnMultipleActors: return SpawnMultipleActorsEvent.FromData(data);

			case BattleEventType.BreezeUpdate: return BreezeUpdateEvent.FromData(data);

			case BattleEventType.TurnStart: return TurnStartEvent.FromData(data);
			case BattleEventType.ActionPerformed: return ActionPerformedEvent.FromData(data);
			case BattleEventType.EntityUpdate: return EntityUpdateEvent.FromData(data);
			}

			throw new System.NotImplementedException(string.Format("IBattleEvent type {0} has not been added to BattleEventFactory.", (BattleEventType)eventCode));
		}

	}

}
