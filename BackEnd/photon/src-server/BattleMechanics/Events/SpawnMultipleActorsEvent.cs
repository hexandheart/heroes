﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace BattleMechanics.Events {

	public class SpawnMultipleActorsEvent : IBattleEvent {

		public int NumSpawns {
			get {
				return SpawnActorEvents.Count;
			}
		}

		public SpawnActorEvent GetSpawn(int index) {
			return SpawnActorEvents[index];
		}

		/*******************************************************************************/
		public SpawnMultipleActorsEvent(int time, List<SpawnActorEvent> spawnActorEvents) {
			Time = time;
			SpawnActorEvents = spawnActorEvents;
		}

		/*******************************************************************************/
		#region IBattleEvent implementation

		public BattleEventType EventType { get { return BattleEventType.SpawnMultipleActors; } }

		public IDictionary GetData() {
			Hashtable ret = new Hashtable {
				{ TimeKey, Time },
				{ SpawnEventsKey, SerializeSpawnEvents() },
			};
			return ret;
		}

		public void ApplyTo(BattleState battleState) {
			for(int i = 0; i < NumSpawns; ++i) {
				GetSpawn(i).ApplyTo(battleState);
			}
		}

		public override string ToString() {
			return string.Format("[SpawnMultipleActorsEvent:T{0}:{1} spawns]", Time, NumSpawns);
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		private byte[] SerializeSpawnEvents() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					writer.Write(SpawnActorEvents.Count);
					for(int i = 0; i < SpawnActorEvents.Count; ++i) {
						SpawnActorEvents[i].WriteTo(writer);
					}

					return stream.ToArray();
				}
			}
		}

		public static SpawnMultipleActorsEvent FromData(IDictionary data) {
			int time = (int)data[TimeKey];
			List<SpawnActorEvent> spawnActorEvents = null;

			byte[] buffer = (byte[])data[SpawnEventsKey];
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					int count = reader.ReadInt32();
					spawnActorEvents = new List<SpawnActorEvent>(count);
					for(int i = 0; i < count; ++i) {
						SpawnActorEvent sae = SpawnActorEvent.ReadFrom(reader);
						spawnActorEvents.Add(sae);
					}
				}
			}

			return new SpawnMultipleActorsEvent() {
				Time = time,
				SpawnActorEvents = spawnActorEvents,
			};
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly int TimeKey = 0;
		public int Time { get; private set; }

		private static readonly int SpawnEventsKey = 1;
		private List<SpawnActorEvent> SpawnActorEvents { get; set; }

		private SpawnMultipleActorsEvent() { }

		#endregion

	}

}
