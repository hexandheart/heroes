﻿using System.Collections;

namespace BattleMechanics.Events {

	/// <summary>
	/// Triggered when the battle has ended.
	/// </summary>
	public class BattleEndedEvent : IBattleEvent {

		/*******************************************************************************/
		public BattleEndedEvent(int time, int winnerTeamId) {
			Time = time;
			WinnerTeamID = winnerTeamId;
		}

		public void ApplyTo(BattleState battleState) {
			battleState.Time = Time;
			battleState.BattlePhase = ReadyState.PostGame;
		}

		/*******************************************************************************/
		#region IBattleEvent implementation

		public BattleEventType EventType { get { return BattleEventType.BattleEnded; } }

		public IDictionary GetData() {
			Hashtable ret = new Hashtable() {
				{ TimeKey, Time },
				{ WinnerTeamIDKey, WinnerTeamID },
			};
			return ret;
		}

		public override string ToString() {
			return string.Format("[BattleEndedEvent:T{0}:{1} wins]", Time, WinnerTeamID);
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		public static BattleEndedEvent FromData(IDictionary data) {
			int time = (int)data[TimeKey];
			int winnerTeamId = (int)data[WinnerTeamIDKey];
			return new BattleEndedEvent(time, winnerTeamId);
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly int TimeKey = 0;
		public int Time { get; private set; }

		private static readonly int WinnerTeamIDKey = 1;
		public int WinnerTeamID { get; private set; }

		#endregion

	}

}
