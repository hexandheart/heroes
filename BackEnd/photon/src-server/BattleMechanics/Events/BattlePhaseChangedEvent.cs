﻿using System.Collections;
using System.Collections.Generic;

namespace BattleMechanics.Events {

	public class BattlePhaseChangedEvent : IBattleEvent {

		/*******************************************************************************/
		public BattlePhaseChangedEvent(int time, ReadyState battlePhase) {
			Time = time;
			BattlePhase = battlePhase;
		}

		/*******************************************************************************/
		#region IBattleEvent implementation

		public BattleEventType EventType { get { return BattleEventType.BattlePhaseChanged; } }

		public IDictionary GetData() {
			Dictionary<int, object> ret = new Dictionary<int, object>() {
				{ TimeKey, Time },
				{ BattlePhaseKey, (int)BattlePhase },
			};
			return ret;
		}

		public void ApplyTo(BattleState battleState) {
			battleState.Time = Time;
			battleState.BattlePhase = BattlePhase;
		}

		public override string ToString() {
			return string.Format("[BattlePhaseChangedEvent:T{0}:{1}]", Time, BattlePhase);
		}

		#endregion

		/*******************************************************************************/
		#region Serialization

		public static BattlePhaseChangedEvent FromData(IDictionary data) {
			int time = (int)data[TimeKey];
			ReadyState battlePhase = (ReadyState)(int)data[BattlePhaseKey];
			return new BattlePhaseChangedEvent(time, battlePhase);
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private static readonly int TimeKey = 0;
		public int Time { get; private set; }

		private static readonly int BattlePhaseKey = 1;
		public ReadyState BattlePhase { get; private set; }

		#endregion

	}

}
