﻿using System.Collections;

namespace BattleMechanics.Events.Data {

	public class TeamAPData {

		public int TeamID { get; private set; }
		public int AP { get; private set; }
		public int MaxAP { get; private set; }

		/*******************************************************************************/
		public TeamAPData(int teamId, int ap, int maxAp) {
			TeamID = teamId;
			AP = ap;
			MaxAP = maxAp;
		}

		public static TeamAPData FromTeam(Team team) {
			TeamAPData ret = new TeamAPData() {
				TeamID = team.TeamID,
				AP = team.AP,
				MaxAP = team.MaxAP,
			};
			return ret;
		}

		/*******************************************************************************/
		#region Serialization

		public object Serialize() {
			var ret = new byte[] {
				(byte)TeamID,
				(byte)AP,
				(byte)MaxAP,
			};
			return ret;
		}

		public static TeamAPData Deserialize(object data) {
			byte[] dataArray = (byte[])data;
			TeamAPData ret = new TeamAPData() {
				TeamID = dataArray[0],
				AP = dataArray[1],
				MaxAP = dataArray[2],
			};
			return ret;
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		private TeamAPData() { }

		#endregion

	}

}
