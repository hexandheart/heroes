﻿using System.Collections;
using System.IO;

namespace BattleMechanics.Events.Data {

	public class ActionResultData {

		public GridVector Target { get; private set; }
		public ActionResultType Type { get; private set; }

		public int HealthChange { get; private set; }

		public byte[] Data { get; private set; }

		/// <summary>
		/// Apply the action result to a targeted Actor.
		/// </summary>
		/// <param name="actor"></param>
		public void ApplyTo(BattleState battleState, Actor actor) {
			if(Type == ActionResultType.Hit) {
				actor.ChangeHealthBy(battleState, HealthChange);
			}
		}

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			Target.WriteTo(writer);
			writer.Write((int)Type);

			writer.Write(HealthChange);

			if(Data != null) {
				writer.Write(Data.Length);
				writer.Write(Data);
			}
			else {
				writer.Write(0);
			}
		}

		public static ActionResultData ReadFrom(BinaryReader reader) {
			GridVector target = GridVector.ReadFrom(reader);
			ActionResultType type = (ActionResultType)reader.ReadInt32();

			int healthChange = reader.ReadInt32();
			int dataSize = reader.ReadInt32();
			byte[] data = null;
			if(dataSize > 0) {
				data = reader.ReadBytes(dataSize);
			}

			return new ActionResultData(
				target, type,
				healthChange,
				data);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static byte[] SerializeArray(ActionResultData[] results) {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					writer.Write(results.Length);
					for(int i = 0; i < results.Length; ++i) {
						results[i].WriteTo(writer);
					}

					return stream.ToArray();
				}
			}
		}

		public static ActionResultData Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		public static ActionResultData[] DeserializeArray(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					int count = reader.ReadInt32();
					ActionResultData[] ret = new ActionResultData[count];
					for(int i = 0; i < count; ++i) {
						ret[i] = ReadFrom(reader);
					}

					return ret;
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private ActionResultData() { }

		public ActionResultData(
			GridVector target, ActionResultType type,
			int healthChange, byte[] data) {
			Target = target;
			Type = type;

			HealthChange = healthChange;

			Data = data;
		}

		#endregion

	}

}
