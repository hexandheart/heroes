﻿using System.Collections.Generic;

namespace BattleMechanics.Requests {

	/// <summary>
	/// I give up!
	/// </summary>
	public class ConcedeMatchRequest : IRequest {

		public ConcedeMatchRequest() { }

		public RequestType RequestType { get { return RequestType.ConcedeMatch; } }

		public Dictionary<byte, object> GetData() {
			return null;
		}

		public static ConcedeMatchRequest FromData(Dictionary<byte, object> data) {
			ConcedeMatchRequest ret = new ConcedeMatchRequest();
			return ret;
		}

	}

}
