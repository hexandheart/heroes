﻿using System.Collections;
using System.Collections.Generic;

namespace BattleMechanics.Requests {

	/// <summary>
	/// Move an actor!
	/// </summary>
	public class PerformActionRequest : IRequest {

		public PerformActionRequest(int actorId, BattleActionGroupType actionGroup, int actionSlot, GridVector target, GridFacing targetFacing) {
			ActorID = actorId;
			ActionGroup = actionGroup;
			ActionSlot = actionSlot;
			Target = target;
			TargetFacing = targetFacing;
		}

		public RequestType RequestType { get { return RequestType.PerformAction; } }

		public Dictionary<byte, object> GetData() {
			return new Dictionary<byte, object>() {
				{ ActorIDKey, ActorID },
				{ ActionGroupKey, (int)ActionGroup },
				{ ActionSlotKey, ActionSlot },
				{ TargetXKey, Target.GridX },
				{ TargetYKey, Target.GridY },
				{ TargetFacingKey, TargetFacing },
			};
		}

		public static PerformActionRequest FromData(Dictionary<byte, object> data) {
			int actorId = (int)data[ActorIDKey];
			BattleActionGroupType actionGroup = (BattleActionGroupType)(int)data[ActionGroupKey];
			int actionSlot = (int)data[ActionSlotKey];
			GridVector target = new GridVector((int)data[TargetXKey], (int)data[TargetYKey]);
			GridFacing targetFacing = (GridFacing)data[TargetFacingKey];

			PerformActionRequest ret = new PerformActionRequest(actorId, actionGroup, actionSlot, target, targetFacing);
			return ret;
		}

		private static readonly byte ActorIDKey = 0;
		public int ActorID { get; private set; }
		private static readonly byte ActionGroupKey = 1;
		public BattleActionGroupType ActionGroup { get; private set; }
		private static readonly byte ActionSlotKey = 2;
		public int ActionSlot { get; private set; }
		private static readonly byte TargetXKey = 3;
		private static readonly byte TargetYKey = 4;
		public GridVector Target { get; private set; }
		private static readonly byte TargetFacingKey = 5;
		public GridFacing TargetFacing { get; private set; }

	}

}
