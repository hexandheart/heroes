﻿using System.Collections;
using System.Collections.Generic;
using BattleMechanics.Breezes;

namespace BattleMechanics.Requests {

	/// <summary>
	/// Update a breeze!
	/// </summary>
	public class UpdateBreezeRequest : IRequest {

		public UpdateBreezeRequest(IBreeze breeze) {
			BreezeType = breeze.BreezeType;
			BreezeData = breeze.GetData();
		}

		public RequestType RequestType { get { return RequestType.UpdateBreeze; } }

		public Dictionary<byte, object> GetData() {
			return new Dictionary<byte, object>() {
				{ BreezeTypeKey, (int)BreezeType },
				{ BreezeDataKey, BreezeData },
			};
		}

		public static UpdateBreezeRequest FromData(Dictionary<byte, object> data) {
			BreezeType breezeType = (BreezeType)(int)data[BreezeTypeKey];
			IDictionary breezeData = (IDictionary)data[BreezeDataKey];

			return new UpdateBreezeRequest(breezeType, breezeData);
		}

		private UpdateBreezeRequest(BreezeType breezeType, IDictionary breezeData) {
			BreezeType = breezeType;
			BreezeData = breezeData;
		}
		
		private static readonly byte BreezeTypeKey = 0;
		public BreezeType BreezeType { get; private set; }
		private static readonly byte BreezeDataKey = 1;
		public IDictionary BreezeData { get; private set; }

	}

}
