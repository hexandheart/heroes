﻿using System.Collections.Generic;

namespace BattleMechanics.Requests {

	public enum RequestType {
		EndTurn,
		ConcedeMatch,

		UpdateBreeze,

		PerformAction,
	}

	/// <summary>
	/// Requests are sent from clients to the server. They are processed by the server,
	/// and clients are informed of results through BattleEvents.
	/// </summary>
	public interface IRequest {
		RequestType RequestType { get; }
		Dictionary<byte, object> GetData();
	}

}
