﻿using System.Collections.Generic;

namespace BattleMechanics.Requests {

	/// <summary>
	/// End my turn!
	/// </summary>
	public class EndTurnRequest : IRequest {
		
		public EndTurnRequest() { }

		public RequestType RequestType { get { return RequestType.EndTurn; } }

		public Dictionary<byte, object> GetData() {
			return null;
		}
		
		public static EndTurnRequest FromData(Dictionary<byte, object> data) {
			EndTurnRequest ret = new EndTurnRequest();
			return ret;
		}

	}

}
