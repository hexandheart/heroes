﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BattleMechanics {

	/// <summary>
	/// Utility class for determining valid targets for abilities and whatnot.
	/// </summary>
	public class TargetSolver {

		/*******************************************************************************/

		/// <summary>
		/// Construtor.
		/// </summary>
		/// <param name="battleState"></param>
		public TargetSolver(BattleState battleState) {
			this.battleState = battleState;
			results = new TargetSolverResult[battleState.Map.GridWidth * battleState.Map.GridHeight];
			Reset();
		}

		/// <summary>
		/// Resets the parameters and results of the solver, to prepare to solve something else.
		/// </summary>
		public void Reset() {
			ClearResults();

			sourcePosition = GridVector.Zero;
			sourceFacing = GridFacing.North;
			sourceTeamId = 0;

			useRange = false;
			minRange = 0;
			maxRange = 0;

			useLineOfSight = false;
			lineOfSightBlockFlags = TargetSolverBlockFlags.Nothing;

			usePathing = false;
			pathingBlockFlags = TargetSolverBlockFlags.Nothing;

			useArea = false;
			area = null;
		}

		/// <summary>
		/// Sets the source position for the solver.
		/// </summary>
		/// <param name="sourcePosition"></param>
		/// <param name="sourceFacing"></param>
		public void SetSource(GridVector sourcePosition, GridFacing sourceFacing, int sourceTeamId) {
			ClearResults();
			this.sourcePosition = sourcePosition;
			this.sourceFacing = sourceFacing;
			this.sourceTeamId = sourceTeamId;
		}

		/// <summary>
		/// Sets the maximum range of the solver.
		/// </summary>
		/// <param name="minRange"></param>
		/// <param name="maxRange"></param>
		public void SetRange(int minRange, int maxRange) {
			ClearResults();
			if(minRange > maxRange) {
				useRange = false;
				return;
			}
			useRange = true;
			this.minRange = minRange;
			this.maxRange = maxRange;
		}

		/// <summary>
		/// Sets the solver to consider line of sight from the source.
		/// </summary>
		/// <param name="lineOfSightBlockFlags"></param>
		public void UseLineOfSight(TargetSolverBlockFlags lineOfSightBlockFlags) {
			ClearResults();
			useLineOfSight = true;
			this.lineOfSightBlockFlags = lineOfSightBlockFlags;
		}

		/// <summary>
		/// Sets the solver to require a walkable path from the source.
		/// </summary>
		/// <param name="pathingBlockFlags"></param>
		public void UsePathing(TargetSolverBlockFlags pathingBlockFlags) {
			ClearResults();
			usePathing = true;
			this.pathingBlockFlags = pathingBlockFlags;
		}

		/// <summary>
		/// Sets the solver to use an area of affect around the target(s).
		/// </summary>
		/// <param name="alignToFacing"></param>
		/// <param name="shape"></param>
		public void UseArea(BattleActionArea area) {
			ClearResults();
			useArea = true;
			this.area = area;
		}

		/// <summary>
		/// Prepares the solver for the specified action as performed by the specified actor.
		/// </summary>
		/// <param name="actor"></param>
		/// <param name="action"></param>
		public void PrepareForAction(Actor actor, BattleAction action) {
			actionTargeting = action.Targeting;

			SetSource(actor.Position, actor.Facing, actor.TeamID);
			if(action.Targeting.RangeMax == -1) {
				SetRange(action.Targeting.RangeMin, actor.CurrentEntity.Stats.Step);
			}
			else {
				SetRange(action.Targeting.RangeMin, action.Targeting.RangeMax);
			}

			if(action.Targeting.RequirePath) {
				UsePathing(TargetSolverBlockFlags.TileGroundMovement | TargetSolverBlockFlags.Enemies);
			}
			if(action.Targeting.RequireLOS) {
				UseLineOfSight(TargetSolverBlockFlags.TileVision);
			}

			if(action.Area != null) {
				UseArea(action.Area);
			}
		}

		/// <summary>
		/// Gets the result of the solver given the current parameters at the specified position.
		/// </summary>
		/// <param name="targetPosition"></param>
		/// <returns></returns>
		public TargetSolverResult GetResultAtPosition(GridVector targetPosition) {
			if(!hasResults) {
				CalculateResults();
			}
			return results[targetPosition.GridY * battleState.Map.GridWidth + targetPosition.GridX];
		}

		/// <summary>
		/// Calculates the results for the solver across the entire map.
		/// </summary>
		public void CalculateResults() {
			for(int y = 0; y < battleState.Map.GridHeight; ++y) {
				for(int x = 0; x < battleState.Map.GridWidth; ++x) {
					GridVector targetPosition = new GridVector(x, y);
					// Facing is based on the delta between positions.
					TargetSolverResultType resultType = TargetSolverResultType.Valid;
					if(useLineOfSight) {
						// Blocked will be cleared in CalculateLineOfSight().
						resultType = TargetSolverResultType.VisionBlocked;
					}

					List<GridVector> affectedTiles = null;

					if(useArea) {
						GridFacing facing = sourceFacing;
						GridVector targetDelta = (targetPosition - sourcePosition);
						if(targetDelta.Magnitude() != 0) {
							facing = targetDelta.GetFacing();
						}

						affectedTiles = new List<GridVector>();
						GridVector[] shape = area.GetShape(facing);
						for(int i = 0; i < shape.Length; ++i) {
							GridVector affectedTile = targetPosition + shape[i];
							// Don't include tiles outside of the map!
							if(affectedTile.GridX < 0 || affectedTile.GridX >= battleState.Map.GridWidth ||
								affectedTile.GridY < 0 || affectedTile.GridY >= battleState.Map.GridHeight) {
								continue;
							}
							affectedTiles.Add(targetPosition + shape[i]);
						}
					}
					else {
						affectedTiles = new List<GridVector>() {
							targetPosition
						};
					}

					// In range?
					if(useRange) {
						bool isInRange = IsInRange(targetPosition);
						if(!isInRange) {
							resultType |= TargetSolverResultType.OutOfRange;
						}
					}

					// Immediately discount tiles which can't be targeted based on the action properties.
					if(actionTargeting != null) {
						Actor actorAtTarget = battleState.GetActorAtPosition(targetPosition);
						Tile tileAtTarget = battleState.Map.GetTileAt(targetPosition);

						if(!actionTargeting.CanTargetActor && actorAtTarget != null) {
							resultType |= TargetSolverResultType.Blocked;
						}

						// Is this a space? A block? An actor?
						bool isActor = actorAtTarget != null;
						bool isBlock =
							(tileAtTarget.Flags & TileFlags.BlocksGroundMovement) != 0 &&
							(tileAtTarget.Flags & TileFlags.BlocksVision) != 0;
						bool isNonWalkable =
							(tileAtTarget.Flags & TileFlags.BlocksGroundMovement) != 0 &&
							(tileAtTarget.Flags & TileFlags.BlocksVision) == 0;
						bool isSpace = !isBlock && !isNonWalkable && !isActor;

						// Blocks are always blocked.
						if(isBlock) {
							resultType |= TargetSolverResultType.Blocked;
						}
						// Blocked by not being able to target an actor?
						if(!actionTargeting.CanTargetActor && isActor) {
							resultType |= TargetSolverResultType.Blocked;
						}
						// Blocked by not being able to target a space?
						if(!actionTargeting.CanTargetSpace && isSpace) {
							resultType |= TargetSolverResultType.Blocked;
						}
						// Blocked by not being able to target a non-walkable tile?
						if(!actionTargeting.CanTargetNonWalkableTile && isNonWalkable) {
							resultType |= TargetSolverResultType.Blocked;
						}
					}

					// Line of sight and pathing are calculated across the map separately.

					TargetSolverResult result = new TargetSolverResult(targetPosition, resultType, affectedTiles);
					results[y * battleState.Map.GridWidth + x] = result;
				}
			}

			if(useLineOfSight) {
				CalculateLineOfSight();
			}

			if(usePathing) {
				CalculateValidPathings();
			}

			hasResults = true;
		}

		/*******************************************************************************/
		#region Privates

		/// <summary>
		/// Clears our solver results.
		/// </summary>
		private void ClearResults() {
			for(int i = 0; i < results.Length; ++i) {
				results[i] = null;
			}
			hasResults = false;
		}

		/// <summary>
		/// Is the target in range of the source?
		/// </summary>
		/// <param name="targetPosition"></param>
		/// <returns></returns>
		private bool IsInRange(GridVector targetPosition) {
			int distance = (targetPosition - sourcePosition).Magnitude();
			return distance >= minRange && distance <= maxRange;
		}

		private bool LosBlocksLight(int x, int y) {
			if(x < 0 || x >= battleState.Map.GridWidth ||
				y < 0 || y >= battleState.Map.GridHeight) {
				return true;
			}
			return IsBlocked(new GridVector(x, y), battleState.Map.GetTileAt(x, y).Flags, lineOfSightBlockFlags);
		}

		private void LosSetVisible(int x, int y) {
			if(x < 0 || x >= battleState.Map.GridWidth ||
				y < 0 || y >= battleState.Map.GridHeight) {
				return;
			}
			int tileIndex = y * battleState.Map.GridWidth + x;
			TargetSolverResult oldResult = results[tileIndex];
			results[tileIndex] = new TargetSolverResult(oldResult.TargetPosition, oldResult.ResultType & ~TargetSolverResultType.VisionBlocked, oldResult.AffectedTiles);
		}

		private int LosGetDistance(int x, int y) {
			return GridVector.Magnitude(sourcePosition.GridX, sourcePosition.GridY, x, y);
		}

		private void CalculateLineOfSight() {
			Visibility visibility = new ShadowCastVisibility(LosBlocksLight, LosSetVisible, LosGetDistance);
			visibility.Compute(sourcePosition, useRange ? maxRange : (battleState.Map.GridWidth + battleState.Map.GridHeight));
		}

		/// <summary>
		/// Calculates the valid pathings from the source, up to range if specified.
		/// </summary>
		private void CalculateValidPathings() {
			// Range is critical for pathing.
			if(!useRange) {
				return;
			}

			// Temporary array to hold cost to reach each tile.
			int[] pathLengths = new int[battleState.Map.GridWidth * battleState.Map.GridHeight];
			for(int i = 0; i < pathLengths.Length; ++i) {
				pathLengths[i] = int.MaxValue;
			}

			// Calculate the pathings!
			int startX = sourcePosition.GridX;
			int startY = sourcePosition.GridY;
			CalculateValidPathings(startX, startY, 0, pathLengths);

			// Examine the results!
			for(int y = 0; y < battleState.Map.GridHeight; ++y) {
				for(int x = 0; x < battleState.Map.GridWidth; ++x) {
					int tileIndex = y * battleState.Map.GridWidth + x;
					TargetSolverResult oldResult = results[tileIndex];

					// If the solver result for this tile has already invalidated the tile, skip it.
					/*if(results[tileIndex].ResultType != TargetSolverResultType.Valid) {
						continue;
					}*/

					// We now check the path length to see if it fits our criteria.
					if(pathLengths[tileIndex] < minRange ||
						pathLengths[tileIndex] > maxRange) {
						results[tileIndex] = new TargetSolverResult(oldResult.TargetPosition, oldResult.ResultType | TargetSolverResultType.OutOfRange, oldResult.AffectedTiles);
					}
				}
			}
		}

		/// <summary>
		/// Recursive method to calculate pathable targets.
		/// </summary>
		/// <param name="currentX"></param>
		/// <param name="currentY"></param>
		/// <param name="currentDepth"></param>
		/// <param name="maxDepth"></param>
		/// <param name="pathLengths"></param>
		/// <returns></returns>
		private void CalculateValidPathings(int currentX, int currentY, int currentDepth, int[] pathLengths) {
			// Have we dug too deep?
			if(currentDepth > maxRange) {
				return;
			}

			// We can check the tile as long as our route to get here costs less.
			Tile tile = battleState.Map.GetTileAt(currentX, currentY);
			if(tile.Flags == TileFlags.Invalid) {
				return;
			}

			int tileIndex = currentY * battleState.Map.GridWidth + currentX;
			bool isOpen = true;
			if(currentDepth < pathLengths[tileIndex]) {
				// Is the tile open, according to the flags we've been provided?
				isOpen = !IsBlocked(new GridVector(currentX, currentY), tile.Flags, pathingBlockFlags);
				if(isOpen) {
					// Despite what the flags say, to be a valid path the tile has to be free of actors.
					if(battleState.GetActorAtPosition(new GridVector(currentX, currentY)) == null) {
						pathLengths[tileIndex] = currentDepth;
					}
				}
			}

			// We can only move out of open spaces, unless we start in a closed space.
			if(isOpen || currentDepth == 0) {
				for(int i = 0; i < 4; ++i) {
					GridVector facingVector = GridVector.FromFacing((GridFacing)i);
					CalculateValidPathings(currentX + facingVector.GridX, currentY + facingVector.GridY, currentDepth + 1, pathLengths);
				}
			}
		}

		/// <summary>
		/// Is the target blocked, based on the mask?
		/// </summary>
		/// <param name="targetFlags"></param>
		/// <param name="mask"></param>
		/// <returns></returns>
		private bool IsBlocked(GridVector position, TileFlags targetFlags, TargetSolverBlockFlags mask) {
			if((mask & TargetSolverBlockFlags.TileGroundMovement) != 0 &&
				(targetFlags & TileFlags.BlocksGroundMovement) != 0) {
				return true;
			}
			if((mask & TargetSolverBlockFlags.TileVision) != 0 &&
				(targetFlags & TileFlags.BlocksVision) != 0) {
				return true;
			}

			// Check for actors.
			Actor actor = battleState.GetActorAtPosition(position);
			if(actor != null) {
				if((mask & TargetSolverBlockFlags.Enemies) != 0 &&
					actor.TeamID != sourceTeamId) {
					return true;
				}
				if((mask & TargetSolverBlockFlags.Allies) != 0 &&
					actor.TeamID == sourceTeamId) {
					return true;
				}
			}

			return false;
		}

		private BattleState battleState;

		private GridVector sourcePosition;
		private GridFacing sourceFacing;
		private int sourceTeamId;

		private bool useRange;
		private int minRange;
		private int maxRange;

		private bool useLineOfSight;
		private TargetSolverBlockFlags lineOfSightBlockFlags;

		private bool usePathing;
		private TargetSolverBlockFlags pathingBlockFlags;

		private bool useArea;
		private BattleActionArea area;

		private bool hasResults;
		private TargetSolverResult[] results;

		private BattleActionTargeting actionTargeting;

		#endregion

	}

}
