﻿using System.Collections.Generic;

namespace BattleMechanics {

	/// <summary>
	/// Represents anything that has a physical presence on the battle grid. (Primarily heroes.)
	/// </summary>
	public class Actor {

		/// <summary>
		/// ID used to reference the actor elsewhere.
		/// </summary>
		public int ID { get; private set; }

		/// <summary>
		/// Which team controls this actor?
		/// </summary>
		public int TeamID { get; private set; }

		/// <summary>
		/// Current position of the actor on the battle grid.
		/// </summary>
		public GridVector Position { get; set; }

		/// <summary>
		/// Current cardinal facing of the actor on the battle grid.
		/// </summary>
		public GridFacing Facing { get; set; }

		/// <summary>
		/// Base state of the hero's entity. (Stats, actions, etc.)
		/// </summary>
		public Entity BaseEntity { get; private set; }

		/// <summary>
		/// Current state of hero's entity.
		/// </summary>
		public Entity CurrentEntity { get; set; }

		/// <summary>
		/// The Actor's Team.
		/// </summary>
		public Team Team { get; set; }

		/// <summary>
		/// Can this actor take a primary action?
		/// </summary>
		public bool CanActPrimary {
			get {
				return CurrentEntity.Stats.PrimaryActionsTaken < CurrentEntity.Stats.MaxPrimaryActions;
			}
		}

		/// <summary>
		/// Can this entity take a minor action?
		/// </summary>
		public bool CanActMinor {
			get {
				return CurrentEntity.Stats.MinorActionsTaken < CurrentEntity.Stats.MaxMinorActions;
			}
		}

		/// <summary>
		/// Gets the actor's default movement action.
		/// </summary>
		/// <returns>null if the actor has no movement action</returns>
		public BattleAction GetDefaultMovementAction() {
			foreach(var group in CurrentEntity.ActionMap.ActionGroups) {
				foreach(var action in group.Actions.Values) {
					if(action.MovementEffect != null) {
						return action;
					}
				}
			}

			return null;
		}

		/// <summary>
		/// Gets a list of currently-affordable primary actions.
		/// </summary>
		/// <returns></returns>
		public List<BattleAction> FindAffordablePrimaryActions() {
			List<BattleAction> actions = new List<BattleAction>();

			foreach(var group in CurrentEntity.ActionMap.ActionGroups) {
				foreach(var action in group.Actions.Values) {
					if(action.Cost.Primary > 0 && CanAffordAction(action)) {
						actions.Add(action);
					}
				}
			}

			return actions;
		}

		/// <summary>
		/// Does the actor have enough resources to perform the specified action?
		/// </summary>
		/// <param name="action"></param>
		/// <returns></returns>
		public bool CanAffordAction(BattleAction action) {
			if(action == null) {
				return false;
			}
			if(CurrentEntity.Stats.PrimaryActionsTaken + action.Cost.Primary > CurrentEntity.Stats.MaxPrimaryActions) {
				return false;
			}
			if(CurrentEntity.Stats.MinorActionsTaken + action.Cost.Minor > CurrentEntity.Stats.MaxMinorActions) {
				return false;
			}
			if(Team.AP - action.Cost.AP < 0) {
				return false;
			}
			// Finally, we can't summon an active summon.
			if(action.Summon != null &&
				action.Summon.ActiveActorID != BattleActionSummon.InactiveID) {
				return false;
			}

			return true;
		}

		/// <summary>
		/// Gets an action from the actor's CurrentEntity.
		/// </summary>
		/// <param name="actionId"></param>
		/// <returns></returns>
		public BattleAction FindActionByID(string actionId) {
			return CurrentEntity.FindActionByID(actionId);
		}

		/// <summary>
		/// Searches the actor's CurrentEntity for the action which summoned the specified actor.
		/// </summary>
		/// <param name="actorId"></param>
		/// <returns></returns>
		public BattleAction FindSummonAction(int actorId) {
			return CurrentEntity.FindSummonAction(actorId);
		}

		/// <summary>
		/// Grab the CurrentEntity's counter action.
		/// </summary>
		/// <returns></returns>
		public BattleAction GetCounterAction() {
			return CurrentEntity.ActionMap.CounterAction;
		}
		
		public BattleAction GetAction(BattleActionGroupType actionGroup, int actionSlot) {
			return CurrentEntity.ActionMap.GetAction(actionGroup, actionSlot);
		}

		/// <summary>
		/// Add/subtract health.
		/// </summary>
		/// <param name="value"></param>
		public void ChangeHealthBy(BattleState battleState, int value) {
			if(!IsAlive) {
				// Shouldn't do anything to dead actors.
				return;
			}
			CurrentEntity.Stats.Health += value;
			if(CurrentEntity.Stats.Health > CurrentEntity.Stats.MaxHealth) {
				CurrentEntity.Stats.Health = CurrentEntity.Stats.MaxHealth;
			}

			if(!IsAlive) {
				// We just died!
				BattleAction summonAction = battleState.FindSummonAction(ID);
				if(summonAction != null) {
					summonAction.Summon.ActiveActorID = BattleActionSummon.InactiveID;
				}
			}
		}

		/// <summary>
		/// Is the actor alive?
		/// </summary>
		public bool IsAlive {
			get {
				return CurrentEntity.Stats.Health > 0;
			}
		}

		/*******************************************************************************/
		#region Constructors

		public Actor(int id, int teamId, GridVector position, GridFacing facing, Entity entity) {
			ID = id;
			TeamID = teamId;
			Position = position;
			Facing = facing;
			BaseEntity = entity;
			CurrentEntity = new Entity(entity);
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		#endregion

	}

}
