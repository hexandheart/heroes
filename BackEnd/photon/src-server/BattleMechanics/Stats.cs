﻿using System.IO;

namespace BattleMechanics {
	
	public class Stats {
		
		public int MaxHealth { get; set; }
		public int Health { get; set; }
		public int Power { get; set; }

		public int Armor { get; set; }
		public int Dodge { get; set; }

		public int Step { get; set; }

		public int MaxPrimaryActions { get; set; }
		public int PrimaryActionsTaken { get; set; }
		public int MaxMinorActions { get; set; }
		public int MinorActionsTaken { get; set; }

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write(MaxHealth);
			writer.Write(Health);
			writer.Write(Power);

			writer.Write(Armor);
			writer.Write(Dodge);

			writer.Write(Step);

			writer.Write(MaxPrimaryActions);
			writer.Write(PrimaryActionsTaken);
			writer.Write(MaxMinorActions);
			writer.Write(MinorActionsTaken);
		}

		public static Stats ReadFrom(BinaryReader reader) {
			int maxHealth = reader.ReadInt32();
			int health = reader.ReadInt32();
			int power = reader.ReadInt32();

			int armor = reader.ReadInt32();
			int dodge = reader.ReadInt32();

			int step = reader.ReadInt32();

			int maxPrimaryActions = reader.ReadInt32();
			int primaryActionsTaken = reader.ReadInt32();
			int maxMinorActions = reader.ReadInt32();
			int minorActionsTaken = reader.ReadInt32();

			return new Stats(maxHealth, health,
				power,
				armor, dodge,
				step,
				maxPrimaryActions, primaryActionsTaken,
				maxMinorActions, minorActionsTaken);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static Stats Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		public Stats() { }

		public Stats(int maxHealth, int health,
			int power,
			int armor, int dodge,
			int step,
			int maxPrimaryActions, int primaryActionsTaken,
			int maxMinorActions, int minorActionsTaken) {
			MaxHealth = maxHealth;
			Health = health;
			Power = power;

			Armor = armor;
			Dodge = dodge;

			Step = step;

			MaxPrimaryActions = maxPrimaryActions;
			PrimaryActionsTaken = primaryActionsTaken;
			MaxMinorActions = maxMinorActions;
			MinorActionsTaken = minorActionsTaken;
		}

		public Stats(Stats o) {
			MaxHealth = o.MaxHealth;
			Health = o.Health;
			Power = o.Power;

			Armor = o.Armor;
			Dodge = o.Dodge;

			Step = o.Step;

			MaxPrimaryActions = o.MaxPrimaryActions;
			PrimaryActionsTaken = o.PrimaryActionsTaken;
			MaxMinorActions = o.MaxMinorActions;
			MinorActionsTaken = o.MinorActionsTaken;
		}

		#endregion

	}

}
