﻿namespace BattleMechanics {

	[System.Flags]
	public enum TargetSolverResultType {

		Valid = 0x00,
		Blocked = 0x01,
		VisionBlocked = 0x02,
		OutOfRange = 0x04,

	}

}
