﻿using System.IO;

namespace BattleMechanics {

	public class BattleAction {

		#region Fields only valid in-game
		public BattleActionGroupType ActionGroup { get; private set; }
		public int ActionSlot { get; private set; }
		#endregion

		public string ID { get; private set; }
		public string LocID { get; private set; }
		public string FXID { get; private set; }
		public CounterType CounterableType { get; private set; }
		
		public BattleActionTargeting Targeting { get; private set; }
		public BattleActionCost Cost { get; private set; }

		/// <summary>
		/// How the actor moves when performing the action. May be null!
		/// </summary>
		public BattleActionMovementEffect MovementEffect { get; private set; }

		/// <summary>
		/// How do targets' health change when affected by the action? May be null!
		/// </summary>
		public BattleActionHealthEffect HealthEffect { get; private set; }

		/// <summary>
		/// Area of effect around target. May be null!
		/// </summary>
		public BattleActionArea Area { get; private set; }

		/// <summary>
		/// Hero to summon at target. May be null!
		/// </summary>
		public BattleActionSummon Summon { get; private set; }

		/// <summary>
		/// Counter properties. May be null!
		/// </summary>
		public BattleActionCounter Counter { get; private set; }

		/// <summary>
		/// Get the expected potency and evasion chance if source uses this action on target.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="target"></param>
		/// <param name="potency"></param>
		/// <param name="evasionChance"></param>
		public void GetExpectedPotency(Actor source, Actor target, out int potency, out int evasionChance) {
			if(HealthEffect == null) {
				potency = 0;
				evasionChance = 0;
				return;
			}

			evasionChance = 0;
			if(HealthEffect.EvasionType == EvasionType.Dodge && target != null) {
				evasionChance = target.CurrentEntity.Stats.Dodge;
			}

			potency = HealthEffect.PotencyValue;
			if(HealthEffect.PotencyIncludePower) {
				potency += source.CurrentEntity.Stats.Power;
			}
			if(HealthEffect.PotencyType == BattleActionEffectType.Harm) {
				potency = -potency;
				if(HealthEffect.MitigationType == MitigationType.Normal && target != null) {
					// Potency is negative, so we add target's armor to reduce potency.
					potency += target.CurrentEntity.Stats.Armor;
					if(potency > 0) {
						potency = 0;
					}
				}
			}
		}

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write((int)ActionGroup);
			writer.Write(ActionSlot);

			writer.Write(ID);
			writer.Write(LocID);
			writer.Write(FXID);
			writer.Write((int)CounterableType);

			Targeting.WriteTo(writer);
			Cost.WriteTo(writer);

			writer.Write(MovementEffect != null);
			if(MovementEffect != null) {
				MovementEffect.WriteTo(writer);
			}

			writer.Write(HealthEffect != null);
			if(HealthEffect != null) {
				HealthEffect.WriteTo(writer);
			}

			writer.Write(Area != null);
			if(Area != null) {
				Area.WriteTo(writer);
			}

			writer.Write(Summon != null);
			if(Summon != null) {
				Summon.WriteTo(writer);
			}

			writer.Write(Counter != null);
			if(Counter != null) {
				Counter.WriteTo(writer);
			}
		}

		public static BattleAction ReadFrom(BinaryReader reader) {
			BattleActionGroupType actionGroup = (BattleActionGroupType)reader.ReadInt32();
			int actionSlot = reader.ReadInt32();

			string id = reader.ReadString();
			string locId = reader.ReadString();
			string fxId = reader.ReadString();
			CounterType counterableType = (CounterType)reader.ReadInt32();

			BattleActionTargeting targeting = BattleActionTargeting.ReadFrom(reader);
			BattleActionCost cost = BattleActionCost.ReadFrom(reader);

			BattleActionMovementEffect movementEffect = null;
			bool hasMovementEffect = reader.ReadBoolean();
			if(hasMovementEffect) {
				movementEffect = BattleActionMovementEffect.ReadFrom(reader);
			}

			BattleActionHealthEffect healthEffect = null;
			bool hasHealthEffect = reader.ReadBoolean();
			if(hasHealthEffect) {
				healthEffect = BattleActionHealthEffect.ReadFrom(reader);
			}

			BattleActionArea area = null;
			bool hasArea = reader.ReadBoolean();
			if(hasArea) {
				area = BattleActionArea.ReadFrom(reader);
			}

			BattleActionSummon summon = null;
			bool hasSummon = reader.ReadBoolean();
			if(hasSummon) {
				summon = BattleActionSummon.ReadFrom(reader);
			}

			BattleActionCounter counter = null;
			bool hasCounter = reader.ReadBoolean();
			if(hasCounter) {
				counter = BattleActionCounter.ReadFrom(reader);
			}

			return new BattleAction(
				actionGroup, actionSlot,
				id, locId, fxId, counterableType,
				targeting, cost,
				movementEffect,
				healthEffect,
				area,
				summon,
				counter);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static BattleAction Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private BattleAction() { }

		public BattleAction(
			BattleActionGroupType actionGroup, int actionSlot,
			string id, string locId, string fxId, CounterType counterableType,
			BattleActionTargeting targeting, BattleActionCost cost,
			BattleActionMovementEffect movementEffect,
			BattleActionHealthEffect healthEffect,
			BattleActionArea area, 
			BattleActionSummon summon,
			BattleActionCounter counter) {
			ActionGroup = actionGroup;
			ActionSlot = actionSlot;

			ID = id;
			LocID = locId;
			FXID = fxId;
			CounterableType = counterableType;

			Targeting = targeting;
			Cost = cost;
			if(Targeting == null) {
				Targeting = BattleActionTargeting.Default;
			}

			MovementEffect = movementEffect;
			HealthEffect = healthEffect;
			Area = area;
			Summon = summon;
			Counter = counter;
		}

		public BattleAction(BattleAction o) {
			ActionGroup = o.ActionGroup;
			ActionSlot = o.ActionSlot;

			ID = o.ID;
			LocID = o.LocID;
			FXID = o.FXID;
			CounterableType = o.CounterableType;

			Targeting = new BattleActionTargeting(o.Targeting);
			Cost = new BattleActionCost(o.Cost);

			if(o.MovementEffect != null) {
				MovementEffect = new BattleActionMovementEffect(o.MovementEffect);
			}
			if(o.HealthEffect != null) {
				HealthEffect = new BattleActionHealthEffect(o.HealthEffect);
			}
			if(o.Area != null) {
				Area = new BattleActionArea(o.Area);
			}
			if(o.Summon != null) {
				Summon = new BattleActionSummon(o.Summon);
			}
			if(o.Counter != null) {
				Counter = new BattleActionCounter(o.Counter);
			}
		}

		#endregion

	}

}
