﻿using System;
using System.IO;

namespace BattleMechanics {

	public static class Utils {

		public static void Write(this BinaryWriter stream, Tile tile) {
			stream.Write((int)tile.Flags);
		}

		public static Tile ReadTile(this BinaryReader stream) {
			TileFlags flags = (TileFlags)stream.ReadInt32();
			return new Tile(flags);
		}

	}

}
