﻿using System.Collections.Generic;
using System.IO;

namespace BattleMechanics {

	/// <summary>
	/// Describes the loadout of a team. (ie champion + selected abilities, summons, etc)
	/// </summary>
	public class Loadout {

		public int GetNumHeroes() {
			return loadoutSlots.Count;
		}

		public LoadoutSlot GetDataForSpawnSlot(int spawnSlot) {
			LoadoutSlot slot = loadoutSlots.Find(s => s.SpawnSlot == spawnSlot);
			return slot;
		}

		/*******************************************************************************/
		public Loadout(List<LoadoutSlot> loadoutSlots) {
			this.loadoutSlots = loadoutSlots;
		}

		/*******************************************************************************/
		#region Binary serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write(loadoutSlots.Count);
			for(int i = 0; i < loadoutSlots.Count; ++i) {
				loadoutSlots[i].WriteTo(writer);
			}
		}

		public static Loadout ReadFrom(BinaryReader reader) {
			int numSlots = reader.ReadInt32();

			Loadout ret = new Loadout();

			for (int i = 0; i < numSlots; ++i) {
				ret.loadoutSlots.Add(LoadoutSlot.ReadFrom(reader));
			}

			return ret;
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		/// <summary>
		/// These are the heroes which get spawned at the beginning of a battle.
		/// </summary>
		private List<LoadoutSlot> loadoutSlots = new List<LoadoutSlot>();

		private Loadout() { }

		#endregion

	}

}
