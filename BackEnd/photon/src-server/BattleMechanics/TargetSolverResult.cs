﻿using System.Collections;
using System.Collections.Generic;

namespace BattleMechanics {

	public class TargetSolverResult {
		
		public GridVector TargetPosition { get; private set; }
		public TargetSolverResultType ResultType { get; private set; }

		public List<GridVector> AffectedTiles { get; private set; }

		/*******************************************************************************/
		#region Privates

		internal TargetSolverResult(GridVector targetPosition, TargetSolverResultType resultType, List<GridVector> affectedTiles) {
			TargetPosition = targetPosition;
			ResultType = resultType;
			AffectedTiles = affectedTiles;
		}

		private TargetSolverResult() { }

		#endregion

	}

}
