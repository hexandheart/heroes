﻿using System.IO;

namespace BattleMechanics {

	public class BattleActionHealthEffect {

		public EvasionType EvasionType { get; private set; }
		public MitigationType MitigationType { get; private set; }
		
		public BattleActionEffectType PotencyType { get; private set; }
		public int PotencyValue { get; private set; }
		public bool PotencyIncludePower { get; private set; }

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write((int)EvasionType);
			writer.Write((int)MitigationType);

			writer.Write((int)PotencyType);
			writer.Write(PotencyValue);
			writer.Write(PotencyIncludePower);
		}

		public static BattleActionHealthEffect ReadFrom(BinaryReader reader) {
			EvasionType evasionType = (EvasionType)reader.ReadInt32();
			MitigationType mitigationType = (MitigationType)reader.ReadInt32();

			BattleActionEffectType potencyType = (BattleActionEffectType)reader.ReadInt32();
			int potencyValue = reader.ReadInt32();
			bool potencyIncludePower = reader.ReadBoolean();

			return new BattleActionHealthEffect(
				evasionType, mitigationType,
				potencyType, potencyValue, potencyIncludePower);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static BattleActionHealthEffect Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private BattleActionHealthEffect() { }

		public BattleActionHealthEffect(
			EvasionType evasionType, MitigationType mitigationType,
			BattleActionEffectType potencyType,
			int potencyValue, bool potencyIncludePower) {
			EvasionType = evasionType;
			MitigationType = mitigationType;

			PotencyType = potencyType;
			PotencyValue = potencyValue;
			PotencyIncludePower = potencyIncludePower;
		}

		public BattleActionHealthEffect(BattleActionHealthEffect o) {
			EvasionType = o.EvasionType;
			MitigationType = o.MitigationType;

			PotencyType = o.PotencyType;
			PotencyValue = o.PotencyValue;
			PotencyIncludePower = o.PotencyIncludePower;
		}

		#endregion

	}

}
