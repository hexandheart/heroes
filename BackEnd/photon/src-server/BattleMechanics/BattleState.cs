﻿using System.Collections;
using System.Collections.Generic;

namespace BattleMechanics {

	/// <summary>
	/// Entry point for accessing everything there is to know about the state of a battle!
	/// </summary>
	public class BattleState {

		/*******************************************************************************/
		#region Accessors

		/// <summary>
		/// Map used for the battle.
		/// </summary>
		public Map Map { get; private set; }

		/// <summary>
		/// Current "battle time" (ie number of turns completed).
		/// </summary>
		public int Time { get; set; }

		/// <summary>
		/// ID of the team whose turn it currently is.
		/// </summary>
		public int CurrentTeamID { get; set; }

		/// <summary>
		/// Gets the team whose turn it currently is.
		/// </summary>
		public Team CurrentTeam {
			get {
				return GetTeam(CurrentTeamID);
			}
		}

		/// <summary>
		/// Gets a team by its ID.
		/// </summary>
		/// <param name="teamId"></param>
		/// <returns></returns>
		public Team GetTeam(int teamId) {
			for(int i = 0; i < teams.Length; ++i) {
				if(teams[i].TeamID == teamId) {
					return teams[i];
				}
			}
			return null;
		}

		/// <summary>
		/// The number of teams in the battle.
		/// </summary>
		public int NumTeams { get { return teams.Length; } }

		/// <summary>
		/// Gets a team by index. (Likely different from its team ID!)
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public Team GetTeamAtIndex(int index) {
			return teams[index];
		}

		/// <summary>
		/// Current phase of the battle (pre-game, ingame, post-game).
		/// </summary>
		public ReadyState BattlePhase { get; set; }

		#endregion

		/*******************************************************************************/
		#region Actors

		/// <summary>
		/// Adds an actor to the battle.
		/// </summary>
		/// <param name="actor"></param>
		public void RegisterActor(Actor actor) {
			// Check that we haven't already added the actor.
			if(actors.Contains(actor)) {
				return;
			}
			Team team = GetTeam(actor.TeamID);
			actor.Team = team;
			// Each team should only have one champion. Is this it?
			if(actor.CurrentEntity.Type == EntityType.Champion) {
				team.Champion = actor;
			}
			actors.Add(actor);
		}

		/// <summary>
		/// Gets a registered actor by its ID.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public Actor GetActorByID(int id) {
			return actors.Find(a => a.ID == id);
		}

		/// <summary>
		/// Gets a list of all active actors.
		/// </summary>
		/// <returns></returns>
		public List<Actor> GetAllActors() {
			return actors;
		}

		/// <summary>
		/// Gets the actor occupying the specified position.
		/// </summary>
		/// <param name="position"></param>
		/// <returns></returns>
		public Actor GetActorAtPosition(GridVector position) {
			return actors.Find(m => m.Position == position);
		}

		/// <summary>
		/// If this was a summoned actor, get the action that summoned them.
		/// </summary>
		/// <param name="actorId"></param>
		/// <returns></returns>
		public BattleAction FindSummonAction(int actorId) {
			foreach(var actor in actors) {
				BattleAction summon = actor.FindSummonAction(actorId);
				if(summon != null) {
					return summon;
				}
			}

			return null;
		}

		/// <summary>
		/// Purge the recently deceased.
		/// </summary>
		public void PurgeDeadActors() {
			actors.RemoveAll(a => !a.IsAlive);
		}

		#endregion

		/*******************************************************************************/
		#region Constructors

		public BattleState(Map map, Team[] teams) {
			Time = 0;
			Map = map;
			CurrentTeamID = 0;
			actors = new List<Actor>();

			BattlePhase = ReadyState.PreGame;

			this.teams = teams;
			for(int i = 0; i < teams.Length; ++i) {
				teams[i].InitializeBreezes(this);
			}
		}

		#endregion

		/*******************************************************************************/
		#region Privates

		/// <summary>
		/// Teams in the battle. Even if not all clients see all actors, team info is shared.
		/// </summary>
		private Team[] teams;

		/// <summary>
		/// Actors active in the battle. Not all actors are necessarily shared with all clients.
		/// </summary>
		private List<Actor> actors;

		#endregion

	}

}
