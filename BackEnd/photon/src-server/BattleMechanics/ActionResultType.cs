﻿namespace BattleMechanics {

	public enum ActionResultType {
		Empty,
		Hit,
		Dodge,
		Summon,
	}

}
