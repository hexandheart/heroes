﻿using System.IO;

namespace BattleMechanics {

	public class BattleActionSummon {

		public static readonly int InactiveID = -1;

		public string HeroID { get; private set; }
		public int ActiveActorID { get; set; }

		/*******************************************************************************/
		#region Serialization

		public void WriteTo(BinaryWriter writer) {
			writer.Write(HeroID);
			writer.Write(ActiveActorID);
		}

		public static BattleActionSummon ReadFrom(BinaryReader reader) {
			string heroId = reader.ReadString();
			int activeActorId = reader.ReadInt32();

			return new BattleActionSummon(
				heroId,
				activeActorId);
		}

		public byte[] Serialize() {
			using(var stream = new MemoryStream()) {
				using(var writer = new BinaryWriter(stream)) {
					WriteTo(writer);
					return stream.ToArray();
				}
			}
		}

		public static BattleActionSummon Deserialize(byte[] buffer) {
			using(var stream = new MemoryStream(buffer)) {
				using(var reader = new BinaryReader(stream)) {
					return ReadFrom(reader);
				}
			}
		}

		#endregion

		/*******************************************************************************/
		#region Constructor

		private BattleActionSummon() { }

		public BattleActionSummon(
			string heroId,
			int activeActorId) {
			HeroID = heroId;
			ActiveActorID = activeActorId;
		}

		public BattleActionSummon(BattleActionSummon o) {
			HeroID = o.HeroID;
			ActiveActorID = o.ActiveActorID;
		}

		#endregion

	}

}
